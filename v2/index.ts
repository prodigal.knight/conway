const d = document;
function getById<T extends HTMLElement = HTMLElement>(id: string) {
	return d.getElementById(id) as T;
}
const btnContainer = getById<HTMLDivElement>('quick-change-container');

const isNumeric = (c: string) => '0123456789'.includes(c);

function dedent(strings: string | TemplateStringsArray, ...values: string[]) {
	const raw = typeof strings === 'string' ? [strings] : strings.raw;

	let result = '';
	for (let i = 0; i < raw.length; i++) {
		result += raw[i].replace(/\\\n[ \t]*/g, '').replace(/\\`/g, '`');

		if (i < values.length) {
			result += values[i];
		}
	}

	const lines = result.split('\n');
	const mindent = lines.reduce(
		(acc, l) => Math.min(acc, l.match(/^(\s+)\S+/)?.[1].length ?? Infinity),
		Infinity,
	);

	if (mindent !== Infinity) {
		result = lines
			.map(l => l[0] === ' ' || l[0] === '\t' ? l.slice(mindent) : l)
			.join('\n');
	}

	return result.trim().replace(/\\n/g, '\n');
}

function clamp<T extends number | bigint>(v: T, min: T, max: T) {
	return v < min ? min : v > max ? max : v;
}

class Setting<T = unknown> {
	readonly #dv: T;
	readonly #storage: Storage;

	#value: T;

	constructor(
		readonly name: string,
		dv: T,
		storage: Storage = sessionStorage,
	) {
		this.#dv = dv;
		this.#storage = storage;
		this.#value = JSON.parse(this.#storage.getItem(this.name)!) as T ?? this.#dv;
	}

	get() {
		return this.#value;
	}

	set(value: T) {
		this.#value = value;

		this.#storage.setItem(this.name, JSON.stringify(value));
	}
}

// tslint:disable-next-line:no-magic-numbers
const Framerate = new Setting('framerate', 60);
const RaveMode = new Setting('rave', 0);
const RestartOnStagnation = new Setting('restart-on-stagnation', false);

class Modal {
	static readonly #modals = new Map<string, Modal>();

	static get(id: string) {
		return this.#modals.get(id);
	}

	readonly #id: string;
	readonly #element: HTMLDivElement;
	readonly #content: HTMLDivElement;

	#dismissed = false;

	constructor(id: string) {
		this.#id = id;
		this.#element = getById<HTMLDivElement>(this.#id);
		this.#content = this.#element.querySelector<HTMLDivElement>('.content')!;
		Modal.#modals.set(this.#id, this);
	}

	show(text?: string) {
		if (text) {
			this.#content.textContent = text;
		}

		if (!this.#dismissed && this.#element.hasAttribute('hidden')) {
			this.#element.removeAttribute('hidden');
		}
	}

	dismiss() {
		this.#dismissed = true;

		if (!this.#element.hasAttribute('hidden')) {
			this.#element.setAttribute('hidden', '');
		}
	}

	reset() {
		this.dismiss();
		this.#dismissed = false;
	}
}

const deadModal = new Modal('dead-warning');
const stagnantModal = new Modal('stagnant-warning');
const errorModal = new Modal('error-message');
const loadingModal = new Modal('loading');
const parsingModal = new Modal('parsing');

const enum Constants {
	BinaryRadix = 2,
	OctalRadix = 8,
	DecimalRadix = 10,
	HexRadix = 16,

	ShaderCompilationErrorLineCount = 5,

	BytesPerF32 = 4,
	BytesPerPixel = 4,
	CoordsPerRect = 4,
	CoordsPerChar = 24,

	MsPerSec = 1000,
	SecsPerMinute = 60,
	MsPerMinute = MsPerSec * SecsPerMinute,
	MinutesPerHour = 60,
	MsPerHour = MsPerMinute * MinutesPerHour,
	HoursPerDay = 24,
	MsPerDay = MsPerHour * HoursPerDay,

	MsDigitsSlice = -3,
	HMSDigitsSlice = -2,
}

class CacheEntry<V = unknown> {
	readonly #value: V;

	readonly created = Date.now();

	accessed = this.created;
	accesses = 0;

	constructor(value: V) {
		this.#value = value;
	}

	get val() {
		this.accessed = Date.now();
		this.accesses++;

		return this.#value;
	}
}

class CachePolicy {
	static readonly FIFO = new CachePolicy(
		([, a], [, b]) => a.created - b.created,
	);

	static readonly LIFO = new CachePolicy(
		([, a], [, b]) => b.created - a.created,
	);

	static readonly LRU = new CachePolicy(
		([, a], [, b]) => a.accessed - b.accessed,
	);

	static readonly MRU = new CachePolicy(
		([, a], [, b]) => b.accessed - a.accessed,
	);

	static readonly LFU = new CachePolicy(
		([, a], [, b]) => a.accesses - b.accesses,
	);

	static readonly RR = new CachePolicy(
		() => (Math.random() * 2) - 1,
	);

	// Aliases
	static readonly FILO = CachePolicy.LIFO;

	static readonly FirstInFirstOut = CachePolicy.FIFO;
	static readonly FirstInLastOut = CachePolicy.LIFO;
	static readonly LastInFirstOut = CachePolicy.LIFO;
	static readonly LeastFrequentlyUsed = CachePolicy.LFU;
	static readonly LeastRecentlyUsed = CachePolicy.LRU;
	static readonly MostRecentlyUsed = CachePolicy.MRU;
	static readonly Random = CachePolicy.RR;

	private constructor(
		readonly sort: <K>(a: [K, CacheEntry], b: [K, CacheEntry]) => number,
	) {}
}

class CustomCache<K, V> implements Map<K, V> {
	readonly [Symbol.toStringTag] = 'Cache';

	readonly #cache = new Map<K, CacheEntry<V>>();
	readonly #maxSize: number = Infinity;
	readonly #policy: CachePolicy;

	constructor(maxSize: number = Infinity, policy: CachePolicy) {
		this.#maxSize = maxSize;
		this.#policy = policy;
	}

	get size() {
		return this.#cache.size;
	}

	clear() {
		this.#cache.clear();
	}

	delete(key: K) {
		return this.#cache.delete(key);
	}

	get(key: K) {
		return this.#cache.get(key)?.val;
	}

	has(key: K) {
		return this.#cache.has(key);
	}

	set(key: K, value: V) {
		this.#cache.set(key, new CacheEntry(value));

		this.trim();

		return this;
	}

	trim() {
		if (this.#cache.size >= this.#maxSize) {
			const entries = [...this.#cache.entries()]
				.sort(this.#policy.sort)
				.slice(0, -this.#maxSize);

			for (const [key] of entries) {
				this.delete(key);
			}
		}
	}

	forEach(
		cb: ((value: V, key: K, cache: CustomCache<K, V>) => void),
		thisArg: unknown = this,
	) {
		for (const [key, entry] of this) {
			cb.call(thisArg, entry, key, this);
		}
	}

	entries() {
		return this[Symbol.iterator]();
	}

	keys() {
		return this.#cache.keys();
	}

	*values() {
		for (const entry of this.#cache.values()) {
			yield entry.val;
		}
	}

	*[Symbol.iterator](): IterableIterator<[K, V]> {
		for (const [key, entry] of this.#cache) {
			yield [key, entry.val];
		}
	}
}

/** Port/Extension of Igloo.js */
class ShaderCompilationError extends Error {
	static create(
		gl: WebGL2RenderingContext,
		name: string,
		type: GLenum,
		shader: WebGLShader,
		source: string,
	) {
		const shaderType = this.#getShaderTypeString(gl, type);
		const errors = this.#createShaderCompilationErrorLog(
			gl.getShaderInfoLog(shader),
			source,
		);

		gl.deleteShader(shader);

		return new ShaderCompilationError(
			`The following errors occurred while compiling the ${shaderType} shader for program "${name}:\n\n${errors}"`,
		);
	}

	static #getShaderTypeString(gl: WebGL2RenderingContext, type: GLenum) {
		switch (type) {
			case gl.VERTEX_SHADER: return 'vertex';
			case gl.FRAGMENT_SHADER: return 'fragment';
			default: return 'unknown';
		}
	}

	static #createShaderCompilationErrorLog(info: string | null, source: string) {
		if (!info) {
			return '';
		}

		const lines = source.split('\n');
		const l = lines.length;

		return info.trim().split('\n').map(err => {
			const idx = err.indexOf('0:') + 2;
			const rawLineNo = err.substring(idx, err.indexOf(':', idx));
			// Error message line numbers are 1-indexed
			const lineNumber = Number.parseInt(rawLineNo, Constants.DecimalRadix) - 1;
			let start = lineNumber - 2;
			let end = start + Constants.ShaderCompilationErrorLineCount; // Grab 5 lines

			const rawSnippet = lines.slice(Math.max(start, 0), Math.min(end, l));

			// Trim leading/trailing blank lines
			while (rawSnippet[rawSnippet.length - 1].trim() === '') {
				rawSnippet.pop();
				start++;
			}
			while (rawSnippet[0].trim() === '') {
				rawSnippet.shift();
				end--;
			}

			const s = `${end}`.length;
			const padding = ' '.repeat(s);

			const snippet = rawSnippet
				.map((line, idx) => {
					const no = `${start + idx + 1}`; // Lines are 0-indexed

					return `${(padding + (no === rawLineNo ? '>>' : no)).slice(-s)} ${line}`;
				})
				.join('\n');

			return `${err}\n\n${snippet}\n`;
		}).join('\n');
	}

	private constructor(msg: string) {
		super(msg);
	}
}

class ShaderProgramCompilationError extends Error {
	constructor(
		gl: WebGL2RenderingContext,
		name: string,
		readonly program: WebGLProgram,
	) {
		super(`Unable to initialize program "${name}": ${gl.getProgramInfoLog(program)}`);

		gl.deleteProgram(program);
	}
}

class GLError extends Error {
	constructor(msg: string, readonly code: number) {
		super(msg);
	}
}

class Buffer {
	readonly #gl: WebGL2RenderingContext;

	#target: GLenum;
	#buffer: WebGLBuffer | null;
	#size = -1;

	constructor(
		gl: WebGL2RenderingContext,
		target: GLenum = gl.ARRAY_BUFFER,
	) {
		this.#gl = gl;
		this.#target = target;
		this.#buffer = this.#gl.createBuffer();
	}

	bind() {
		this.#gl.bindBuffer(this.#target, this.#buffer);

		return this;
	}

	release() {
		this.#gl.deleteBuffer(this.#buffer);
	}

	update(
		data: number[] | ArrayBuffer | ArrayBufferView,
		usage: GLenum = this.#gl.DYNAMIC_DRAW,
	) {
		if (data instanceof Array) {
			this.#updateImpl(new Float32Array(data), usage);
		} else {
			this.#updateImpl(data, usage);
		}

		return this;
	}

	#updateImpl(data: ArrayBuffer | ArrayBufferView, usage: GLenum) {
		this.bind();

		if (this.#size !== data.byteLength) {
			this.#gl.bufferData(this.#target, data, usage);
			this.#size = data.byteLength;
		} else {
			this.#gl.bufferSubData(this.#target, 0, data);
		}
	}
}

class Texture {
	static delete(gl: WebGL2RenderingContext, tex: Texture) {
		gl.deleteTexture(tex.texture);
	}

	static #getConstructorForType(
		gl: WebGL2RenderingContext,
		type: GLenum,
	) {
		switch (type) {
			case gl.HALF_FLOAT: // return Float16Array;
				/* falls through */
			case gl.FLOAT: return Float32Array;
			case gl.INT: return Int32Array;
			case gl.UNSIGNED_INT_2_10_10_10_REV:
				/* falls through */
			case gl.UNSIGNED_INT_10F_11F_11F_REV:
				/* falls through */
			case gl.UNSIGNED_INT_5_9_9_9_REV:
				/* falls through */
			case gl.UNSIGNED_INT: return Uint32Array;
			case gl.SHORT: return Int16Array;
			case gl.UNSIGNED_SHORT_5_6_5:
				/* falls through */
			case gl.UNSIGNED_SHORT_4_4_4_4:
				/* falls through */
			case gl.UNSIGNED_SHORT_5_5_5_1: return Uint16Array;
			case gl.BYTE: return Int8Array;
			case gl.UNSIGNED_BYTE:
				/* falls through */
			default: return Uint8Array;
		}
	}

	static #ensureDataIsTypedArray(
		gl: WebGL2RenderingContext,
		type: number,
		data: number[] | ArrayBufferView | null,
	) {
		if (data === null || ArrayBuffer.isView(data)) {
			return data as ArrayBufferView;
		}

		return new (this.#getConstructorForType(gl, type))(data);
	}

	readonly #gl: WebGL2RenderingContext;
	readonly #format: GLenum;
	readonly #wrap: GLenum;
	readonly #filter: GLenum;
	readonly #type: GLenum;

	texture: WebGLTexture | null;

	#w = 0;
	#h = 0;

	constructor(
		gl: WebGL2RenderingContext,
		format: GLenum = gl.RGBA,
		wrap: GLenum = gl.CLAMP_TO_EDGE,
		filter: GLenum = gl.LINEAR,
		type: GLenum = gl.UNSIGNED_BYTE,
	) {
		this.#gl = gl;
		this.#format = format;
		this.#wrap = wrap;
		this.#filter = filter;
		this.#type = type;

		this.texture = this.#gl.createTexture();

		this.#init();
	}

	bind(unit = 0) {
		if (unit) {
			this.#gl.activeTexture(this.#gl.TEXTURE0 + unit);
		}

		this.#gl.bindTexture(this.#gl.TEXTURE_2D, this.texture);

		return this;
	}

	blank(width = this.#w, height = this.#h) {
		return this.setData(null, width, height);
	}

	copy(x = 0, y = 0, width = this.#w, height = this.#h) {
		this.#gl.copyTexImage2D(
			this.#gl.TEXTURE_2D,
			0,
			this.#format,
			x,
			y,
			width,
			height,
			0,
		);

		return this;
	}

	resize(w: number, h: number, fb?: FrameBuffer) {
		const old = this.texture;
		const ow = this.#w;
		const oh = this.#h;

		this.texture = this.#gl.createTexture();
		this.#init();
		this.blank(w, h);

		if (fb && ow && oh) {
			FrameBuffer.attach(fb, old!);
			this.bind();

			const dw = ow - w;
			const dh = oh - h;

			const ox = Math.max(dw / 2, 0);
			const oy = Math.max(dh / 2, 0);

			// TODO: determine how to center

			this.#gl.copyTexSubImage2D(this.#gl.TEXTURE_2D, 0, 0, 0, ox, oy, w, h);

			fb.unbind();
		}

		this.#gl.deleteTexture(old);

		GL.checkForErrors(this.#gl);

		return this;
	}

	setData(
		data: number[] | ArrayBufferView | null,
		width: number,
		height: number,
		format: GLenum = this.#format,
		type: GLenum = this.#type,
	) {
		this.#w = width;
		this.#h = height;

		this.bind();

		this.#gl.texImage2D(
			this.#gl.TEXTURE_2D,
			0,
			format,
			width,
			height,
			0,
			format,
			type,
			Texture.#ensureDataIsTypedArray(this.#gl, type, data),
		);

		return this;
	}

	setImage(
		image: TexImageSource,
		format: GLenum = this.#format,
		type: GLenum = this.#type,
	) {
		this.bind();

		this.#w = image.width;
		this.#h = image.height;

		this.#gl.texImage2D(
			this.#gl.TEXTURE_2D,
			0,
			format,
			format,
			type,
			image,
		);

		return this;
	}

	subsetData(
		data: number[] | ArrayBufferView,
		xOffset: number,
		yOffset: number,
		width: number,
		height: number,
		format: GLenum = this.#format,
		type: GLenum = this.#type,
	) {
		this.bind();

		this.#gl.texSubImage2D(
			this.#gl.TEXTURE_2D,
			0,
			xOffset,
			yOffset,
			width,
			height,
			format,
			type,
			Texture.#ensureDataIsTypedArray(this.#gl, type, data),
		);

		return this;
	}

	subsetImage(
		image: TexImageSource,
		xOffset: number,
		yOffset: number,
		format: GLenum = this.#format,
		type: GLenum = this.#type,
	) {
		this.bind();

		this.#gl.texSubImage2D(
			this.#gl.TEXTURE_2D,
			0,
			xOffset,
			yOffset,
			format,
			type,
			image,
		);

		return this;
	}

	#init() {
		this.#gl.bindTexture(this.#gl.TEXTURE_2D, this.texture);
		this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_WRAP_S, this.#wrap);
		this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_WRAP_T, this.#wrap);
		this.#gl.texParameteri(
			this.#gl.TEXTURE_2D,
			this.#gl.TEXTURE_MIN_FILTER,
			this.#filter,
		);
		this.#gl.texParameteri(
			this.#gl.TEXTURE_2D,
			this.#gl.TEXTURE_MAG_FILTER,
			this.#filter,
		);
	}
}

class FrameBuffer {
	static attach(fb: FrameBuffer, tex: WebGLTexture) {
		fb.attachTexture(tex);
	}

	static delete(fb: FrameBuffer) {
		if (fb.#frameBuffer) {
			fb.#gl.deleteFramebuffer(fb.#frameBuffer);
		}
		if (fb.#renderBuffer) {
			fb.#gl.deleteRenderbuffer(fb.#renderBuffer);
		}
	}

	readonly #gl: WebGL2RenderingContext;

	#frameBuffer: WebGLFramebuffer | null;
	#renderBuffer: WebGLRenderbuffer | null = null;

	constructor(
		gl: WebGL2RenderingContext,
		frameBuffer: WebGLFramebuffer | null = gl.createFramebuffer(),
	) {
		this.#gl = gl;
		this.#frameBuffer = frameBuffer;
	}

	bind() {
		this.#gl.bindFramebuffer(this.#gl.FRAMEBUFFER, this.#frameBuffer);

		return this;
	}

	unbind() {
		this.#gl.bindFramebuffer(this.#gl.FRAMEBUFFER, null);

		return this;
	}

	attach(texture: Texture) {
		this.bind();

		this.#gl.framebufferTexture2D(
			this.#gl.FRAMEBUFFER,
			this.#gl.COLOR_ATTACHMENT0,
			this.#gl.TEXTURE_2D,
			texture.texture,
			0,
		);

		return this;
	}

	attachDepth(width: number, height: number) {
		this.bind();

		if (this.#renderBuffer === null) {
			this.#renderBuffer = this.#gl.createRenderbuffer();

			this.#gl.renderbufferStorage(
				this.#gl.RENDERBUFFER,
				this.#gl.DEPTH_COMPONENT16,
				width,
				height,
			);
			this.#gl.framebufferRenderbuffer(
				this.#gl.FRAMEBUFFER,
				this.#gl.DEPTH_ATTACHMENT,
				this.#gl.RENDERBUFFER,
				this.#renderBuffer,
			);
		}

		return this;
	}

	attachTexture(texture: WebGLTexture) {
		this.bind();

		this.#gl.framebufferTexture2D(
			this.#gl.FRAMEBUFFER,
			this.#gl.COLOR_ATTACHMENT0,
			this.#gl.TEXTURE_2D,
			texture,
			0,
		);

		return this;
	}
}

type UniformOperationNameHelper<T, K = WebGLRenderingContextOverloads> = {
	[P in keyof K]: K[P] extends T ? P : never;
}[keyof K];
type UniformSpreadOperationName =
	| 'uniform1i'
	| 'uniform1f'
	| 'uniform2i'
	| 'uniform2f'
	| 'uniform3i'
	| 'uniform3f'
	| 'uniform4i'
	| 'uniform4f'
	;
type UniformVectorOperationName =
	| 'uniform1iv'
	| 'uniform1fv'
	| 'uniform2iv'
	| 'uniform2fv'
	| 'uniform3iv'
	| 'uniform3fv'
	| 'uniform4iv'
	| 'uniform4fv'
	;
type UniformMatrixOperationName = UniformOperationNameHelper<(
	location: WebGLUniformLocation | null,
	transpose: GLboolean,
	data: Float32List,
	srcOffset?: GLuint,
	srcLength?: GLuint,
) => void>;

type Matrix2DPoint = readonly [number, number, number];
type Matrix2DMatrix = readonly [
	number, number, number,
	number, number, number,
	number, number, number,
];

type Matrix3DPoint = readonly [number, number, number, number];
type Matrix3DMatrix = readonly [
	number, number, number, number,
	number, number, number, number,
	number, number, number, number,
	number, number, number, number,
];

abstract class Matrix<
	P extends Matrix2DPoint | Matrix3DPoint,
	M extends Matrix2DMatrix | Matrix3DMatrix,
> {
	protected constructor(public readonly matrix: M) {}

	protected abstract mult(matrix: M): Matrix<P, M>;
	protected abstract multPoint(point: P): P;
}

class Matrix2D extends Matrix<Matrix2DPoint, Matrix2DMatrix> {
	static readonly #Identity: Matrix2DMatrix = [
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,
	];

	constructor(matrix: Matrix2DMatrix = Matrix2D.#Identity) {
		super(matrix);
	}

	scale(x = 1, y = 1) {
		return this.mult([
			x, 0, 0,
			0, y, 0,
			0, 0, 1,
		]);
	}

	translate(x = 0, y = 0) {
		return this.mult([
			1, 0, 0,
			0, 1, 0,
			x, y, 1,
		]);
	}

	rotate(a: number) {
		const sin = Math.sin(a);
		const cos = Math.cos(a);

		return this.mult([
			cos,  -sin, 0,
			sin,  cos,  0,
			0,    0,    1,
		]);
	}

	protected mult(matrix: Matrix2DMatrix) {
		const [
			m00, m01, m02,
			m10, m11, m12,
			m20, m21, m22,
		] = matrix;

		return new Matrix2D([
			...this.multPoint([m00, m01, m02]),
			...this.multPoint([m10, m11, m12]),
			...this.multPoint([m20, m21, m22]),
		]);
	}

	protected multPoint(point: Matrix2DPoint) {
		const [
			m00, m01, m02,
			m10, m11, m12,
			m20, m21, m22,
		] = this.matrix;

		const [x, y, z] = point;

		return [
			(x * m00) + (y * m01) + (z * m02),
			(x * m10) + (y * m11) + (z * m12),
			(x * m20) + (y * m21) + (z * m22),
		] as Matrix2DPoint;
	}
}

/*class Matrix3D extends Matrix<Matrix3DPoint, Matrix3DMatrix> {
	static readonly #Identity: Matrix3DMatrix = [
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	];

	constructor(matrix: Matrix3DMatrix = Matrix3D.#Identity) {
		super(matrix);
	}

	scale(x = 1, y = 1, z = 1) {
		return this.mult([
			x, 0, 0, 0,
			0, y, 0, 0,
			0, 0, z, 0,
			0, 0, 0, 1,
		]);
	}

	translate(x = 0, y = 0, z = 0) {
		return this.mult([
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			x, y, z, 1,
		]);
	}

	rotate(x: number, y: number, z: number) {
		return this.rotateX(x).rotateY(y).rotateZ(z);
	}

	rotateX(x: number) {
		const sin = Math.sin(x);
		const cos = Math.cos(x);

		return this.mult([
			1,    0,    0,    0,
			0,    cos,  -sin, 0,
			0,    sin,  cos,  0,
			0,    0,    0,    1,
		]);
	}

	rotateY(y: number) {
		const sin = Math.sin(y);
		const cos = Math.cos(y);

		return this.mult([
			cos,  0,    sin,  0,
			0,    1,    0,    0,
			-sin, 0,    cos,  0,
			0,    0,    0,    1,
		]);
	}

	rotateZ(z: number) {
		const sin = Math.sin(z);
		const cos = Math.cos(z);

		return this.mult([
			cos,  -sin, 0,    0,
			sin,  cos,  0,    0,
			0,    0,    1,    0,
			0,    0,    0,    1,
		]);
	}

	protected mult(matrix: Matrix3DMatrix) {
		const [
			m00, m01, m02, m03,
			m10, m11, m12, m13,
			m20, m21, m22, m23,
			m30, m31, m32, m33,
		] = matrix;

		return new Matrix3D([
			...this.multPoint([m00, m01, m02, m03]),
			...this.multPoint([m10, m11, m12, m13]),
			...this.multPoint([m20, m21, m22, m23]),
			...this.multPoint([m30, m31, m32, m33]),
		]);
	}

	protected multPoint(point: Matrix3DPoint) {
		const [
			m00, m01, m02, m03,
			m10, m11, m12, m13,
			m20, m21, m22, m23,
			m30, m31, m32, m33,
		] = this.matrix;

		const [x, y, z, w] = point;

		return [
			(x * m00) + (y * m01) + (z * m02) + (w * m03),
			(x * m10) + (y * m11) + (z * m12) + (w * m13),
			(x * m20) + (y * m21) + (z * m22) + (w * m23),
			(x * m30) + (y * m31) + (z * m32) + (w * m33),
		] as Matrix3DPoint;
	}
}*/

/**
 * Compile & use a given vertex shader and fragment shader to create a program
 *
 * @class
 */
class ShaderProgram {
	static compile(
		gl: WebGL2RenderingContext,
		name: string,
		vSource: string,
		fSource: string,
	) {
		return new ShaderProgram(
			gl,
			name,
			this.compileProgramFromShaders(gl, name, dedent(vSource), dedent(fSource))
		);
	}

	protected static compileProgramFromShaders(
		gl: WebGL2RenderingContext,
		name: string,
		vSource: string,
		fSource: string,
	) {
		const vs = ShaderProgram.#compileShaderFromSource(
			gl,
			name,
			vSource,
			gl.VERTEX_SHADER,
		);
		const fs = ShaderProgram.#compileShaderFromSource(
			gl,
			name,
			fSource,
			gl.FRAGMENT_SHADER,
		);

		const program = gl.createProgram();
		if (!program) {
			throw new Error('Unable to create program!');
		}
		gl.attachShader(program, vs);
		gl.attachShader(program, fs);
		gl.linkProgram(program);

		if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
			throw new ShaderProgramCompilationError(gl, name, program);
		}

		return program;
	}

	static #compileShaderFromSource(
		gl: WebGL2RenderingContext,
		name: string,
		source: string,
		type: number,
	) {
		const shader = gl.createShader(type);
		if (!shader) {
			throw new Error('Unable to create shader!');
		}
		gl.shaderSource(shader, source);
		gl.compileShader(shader);

		if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
			throw ShaderCompilationError.create(gl, name, type, shader, source);
		}

		return shader;
	}

	readonly ratio: WebGLUniformLocation | null;

	protected readonly uniformLocations = new Map<string, WebGLUniformLocation | null>();
	protected readonly attributeLocations = new Map<string, number>();

	protected constructor(
		protected readonly gl: WebGL2RenderingContext,
		public readonly name: string,
		public readonly program: WebGLProgram,
	) {
		this.ratio = this.gl.getUniformLocation(this.program, 'ratio');
	}

	use() {
		this.gl.useProgram(this.program);

		return this;
	}

	uniform(
		name: string,
		value?: number | number[] | readonly number[] | Float32Array | Int32Array,
		int = false,
	) {
		const location = this.#getUniformLocation(name);

		if (value !== undefined) {
			if (Array.isArray(value) || ArrayBuffer.isView(value)) {
				// tslint:disable-next-line:max-line-length
				const method = `uniform${value.length}${int ? 'i' : 'f'}v` as UniformVectorOperationName;

				this.gl[method](location, value as Float32Array & number[]);
			} else if (typeof value === 'number') {
				this.gl[int ? 'uniform1i' : 'uniform1f'](location, value);
			} else {
				throw new Error(`Invalid uniform value in program "${this.name}" for "${name}": ${value}`);
			}
		}

		return this;
	}

	uniformSpread(
		name: string,
		value?: number[] | readonly number[] | Float32Array | Int32Array,
		int = false,
	) {
		const location = this.#getUniformLocation(name);

		if (value !== undefined) {
			// tslint:disable-next-line:max-line-length
			const method = `uniform${value.length}${int ? 'i' : 'f'}` as UniformSpreadOperationName;

			this.gl[method](location, ...(value as [number, number, number, number]));
		}

		return this;
	}

	uniformInt(name: string, value?: number) {
		return this.uniform(name, value, true);
	}

	uniformSpreadInt(name: string, value?: number[]) {
		return this.uniformSpread(name, value, true);
	}

	matrix(
		name: string,
		matrix:
			| number[]
			| readonly number[]
			| Float32Array
			| Matrix<Matrix2DPoint | Matrix3DPoint, Matrix2DMatrix | Matrix3DMatrix>
			,
		transpose = false,
	) {
		const location = this.#getUniformLocation(name);

		const actual = matrix instanceof Matrix
			? matrix.matrix
			: matrix;

		// tslint:disable-next-line:max-line-length
		const method = `uniformMatrix${Math.sqrt(actual.length)}fv` as UniformMatrixOperationName;

		this.gl[method](location, !!transpose, actual as number[]);

		return this;
	}

	attribute(
		name: string,
		value?: Buffer,
		size = 0,
		stride = 0,
		offset = 0,
	) {
		const location = this.#getAttributeLocation(name);

		if (value !== undefined) {
			value.bind();
			this.gl.enableVertexAttribArray(location);
			this.gl.vertexAttribPointer(
				location,
				size,
				this.gl.FLOAT,
				false,
				stride,
				offset,
			);
		}

		return this;
	}

	draw(mode: number, count: number, type?: GLenum) {
		if (!type) {
			this.gl.drawArrays(mode, 0, count);
		} else {
			this.gl.drawElements(mode, count, type, 0);
		}

		GL.checkForErrors(this.gl);

		return this;
	}

	disable() {
		for (const location of this.attributeLocations.values()) {
			this.gl.disableVertexAttribArray(location);
		}

		return this;
	}

	#getUniformLocation(name: string) {
		if (!this.uniformLocations.has(name)) {
			const location = this.gl.getUniformLocation(this.program, name);

			if (location === null) {
				// tslint:disable-next-line:no-console
				console.warn(`Unknown uniform location in program ${this.name}: "${name}". This may cause unexpected behavior.`);
			}

			this.uniformLocations.set(name, location);
		}

		return this.uniformLocations.get(name) ?? null;
	}

	#getAttributeLocation(name: string) {
		if (!this.attributeLocations.has(name)) {
			const location = this.gl.getAttribLocation(this.program, name);

			if (location === -1) {
				// tslint:disable-next-line:no-console
				console.warn(`Unknown attribute location in program ${this.name}: "${name}". This may cause unexpected behavior.`);
			}

			this.attributeLocations.set(name, location);
		}

		return this.attributeLocations.get(name) ?? -1;
	}
}

/**
 * Simplified version of ShaderProgram that has a pre-filled vertex shader for
 * rendering 2D content
 *
 * @class
 */
class ShaderProgram2D extends ShaderProgram {
	protected static readonly vSource = dedent`
		#version 300 es
		precision mediump float;

		// Inputs
		in      vec2 quad;

		// Uniform Inputs
		uniform mat3 u_matrix;

		void main() {
			gl_Position = vec4((u_matrix * vec3(quad, 1.0)).xy, 0.0, 1.0);
		}
	`;

	static readonly #QUAD_BASE = new Float32Array(
		[-1, -1, 1, -1, -1, 1, 1, 1]
	);

	static compile(
		gl: WebGL2RenderingContext,
		name: string,
		fSource: string,
	) {
		return new ShaderProgram2D(
			gl,
			name,
			this.compileProgramFromShaders(gl, name, this.vSource, dedent(fSource))
		);
	}

	readonly #QUAD = new Buffer(this.gl).update(ShaderProgram2D.#QUAD_BASE);

	use() {
		return super.use()
			.matrix('u_matrix', new Matrix2D())
			.attribute('quad', this.#QUAD, 2);
	}

	draw(type?: GLenum) {
		// tslint:disable-next-line:no-magic-numbers
		return super.draw(this.gl.TRIANGLE_STRIP, 4, type);
	}
}

class TextAtlas {
	static readonly Atlases = new Map<string, TextAtlas>();

	static readonly #canvas = d.createElement('canvas');
	static readonly #ctx = TextAtlas.#canvas.getContext('2d')!;
	static readonly #DEFAULT_CHARSET = '!"#$%&\'()*+,-./0123456789:;<=>?@[\\]^_`abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ{|}~';

	static register(
		gl: WebGL2RenderingContext,
		font: string,
		size: number,
		charset: string = TextAtlas.#DEFAULT_CHARSET,
	) {
		const key = [font, size].join(':');
		let atlas = this.Atlases.get(key);

		if (!atlas) {
			atlas = new TextAtlas(gl, font, size, charset);

			this.Atlases.set(key, atlas);
		}

		if (atlas.#charset !== charset) {
			atlas.#mergeCharsets(charset);
		}

		return atlas;
	}

	readonly #gl: WebGL2RenderingContext;
	readonly #font: string;
	readonly #size: number;
	readonly #tex: Texture;

	readonly #cache = new CustomCache<string, Float32Array>(
		500, // tslint:disable-line:no-magic-numbers
		CachePolicy.LRU,
	);

	#charset: string;

	#l = 0;
	#rects: Float32Array | undefined;
	#widths: Float32Array | undefined;

	#spaceWidth = 0;

	#initialized = false;

	private constructor(
		gl: WebGL2RenderingContext,
		font: string,
		size: number,
		charset: string = TextAtlas.#DEFAULT_CHARSET,
	) {
		this.#gl = gl;
		this.#font = font;
		this.#size = size;
		this.#charset = charset;

		this.#tex = new Texture(
			this.#gl,
			this.#gl.RGBA,
			this.#gl.REPEAT,
			this.#gl.NEAREST,
		);
	}

	measure(str: string) {
		if (!this.#initialized) {
			this.#init();
		}

		const l = str.length;
		let res = 0;
		let x = 0;

		for (let i = 0; i < l; i++) {
			const c = str.charAt(i);
			if (c === '\n') {
				x = 0;
			} else if (c === ' ') {
				x += this.#spaceWidth;
			} else {
				x += this.#widths![this.#getCharacterIndex(c, str)];
			}

			res = Math.max(res, x);
		}

		return res;
	}

	write(str: string, buf?: Float32Array, cache = !buf) {
		if (!this.#initialized) {
			this.#init();
		}

		this.#tex.bind(0);

		if (cache && this.#cache.has(str)) {
			return this.#cache.get(str)!;
		}

		const spaces = this.#countSpaces(str);
		const l = str.length;
		const cw = l - spaces;
		let x = 0;
		let y = 0;
		let idx = 0;

		// tslint:disable-next-line:binary-expression-operand-order
		const res = buf ?? new Float32Array(2 + (cw * Constants.CoordsPerChar));
		const v = res.subarray(2);

		for (let i = 0; i < l; i++) {
			const c = str.charAt(i);

			if (c === '\n') {
				x = 0;
				y += this.#size;
			} else if (c === ' ') {
				x += this.#spaceWidth;
			} else {
				x = this.#setCharacter(idx++, v, x, y, this.#getCharacterIndex(c, str));
			}

			res[0] = Math.max(x, res[0]);
			res[1] = y;
		}

		res[1] += this.#size;

		if (cache) {
			this.#cache.set(str, res);
		}

		return res;
	}

	#getCharacterIndex(c: string, str: string) {
		const i = this.#charset.indexOf(c);

		if (i === -1 || i >= this.#l) {
			throw new Error(`Character "${c}" is not in the character set for this atlas: "${this.#charset}"\n  Given: "${str}`);
		}

		return i;
	}

	#countSpaces(str: string) {
		let spaces = 0;
		const l = str.length;

		for (let i = 0; i < l; i++) {
			if (str.charAt(i) === ' ') {
				spaces++;
			}
		}

		return spaces;
	}

	#setCharacter(
		i: number,
		res: Float32Array,
		x: number,
		y: number,
		cIdx: number,
	) {
		const width = this.#widths![cIdx];
		const rIdx = cIdx * Constants.CoordsPerRect;

		const idx = i * Constants.CoordsPerChar;

		const rect = this.#rects!.slice(rIdx, rIdx + Constants.CoordsPerRect);
		const left = x;
		const right = left + width;
		const top = y;
		const bottom = this.#size;
		const pos = [left, top, right, bottom];

		const l = 0;
		const t = 1;
		const r = 2;
		const b = 3;

		let offs = 0;

		// Top left corner
		res[idx + offs++] = pos[l];
		res[idx + offs++] = pos[t];
		res[idx + offs++] = rect[l];
		res[idx + offs++] = rect[t];

		// Top right corner
		res[idx + offs++] = pos[r];
		res[idx + offs++] = pos[t];
		res[idx + offs++] = rect[r];
		res[idx + offs++] = rect[t];

		// Bottom left corner
		res[idx + offs++] = pos[l];
		res[idx + offs++] = pos[b];
		res[idx + offs++] = rect[l];
		res[idx + offs++] = rect[b];

		// Bottom left corner
		res[idx + offs++] = pos[l];
		res[idx + offs++] = pos[b];
		res[idx + offs++] = rect[l];
		res[idx + offs++] = rect[b];

		// Top right corner
		res[idx + offs++] = pos[r];
		res[idx + offs++] = pos[t];
		res[idx + offs++] = rect[r];
		res[idx + offs++] = rect[t];

		// Bottom right corner
		res[idx + offs++] = pos[r];
		res[idx + offs++] = pos[b];
		res[idx + offs++] = rect[r];
		res[idx + offs++] = rect[b];

		return right;
	}

	#mergeCharsets(charset: string) {
		this.#charset = [...new Set([...this.#charset, ...charset])].filter(c => c !== ' ').join('');
	}

	#init() {
		const size = this.#size;
		const chars = this.#charset;

		this.#l = this.#charset.length;
		this.#rects = new Float32Array(this.#l * Constants.CoordsPerRect);
		this.#widths = new Float32Array(this.#l);

		const w = size * this.#l; // Maximum possible width
		const h = size;
		const canvas = TextAtlas.#canvas;
		const ctx = TextAtlas.#ctx;
		canvas.width = w;
		canvas.height = h;

		ctx.clearRect(0, 0, w, h);
		ctx.font = `${size}px ${this.#font}`;
		ctx.textBaseline = 'top';
		ctx.fillStyle = 'rgb(255,255,255)';

		this.#spaceWidth = ctx.measureText(' ').width;

		const positions = new Float32Array(this.#l * Constants.CoordsPerRect);

		const l = 0;
		const t = 1;
		const r = 2;
		const b = 3;

		let right = 0; // Track final texture width
		for (let i = 0; i < this.#l; i++) {
			const posIdx = i * Constants.CoordsPerRect;
			const c = chars.charAt(i);
			const left = right;
			right = left + ctx.measureText(c).width;
			// NOTE: Flip text Y positions, canvas 2d rendering context has 0,0 in top
			// left but WebGL has 0,0 in bottom left
			positions[posIdx + l] = left;
			positions[posIdx + t] = size;
			positions[posIdx + r] = right;
			positions[posIdx + b] = 0;
			// NOTE: Record character width for "measure" method to use
			this.#widths![i] = right - left;
			ctx.fillText(c, left, 0, size);
		}

		this.#tex.setImage(ctx.getImageData(0, 0, right, size));

		for (let i = 0; i < this.#l; i++) {
			const idx = i * Constants.CoordsPerRect;

			this.#rects![idx + l] = positions[idx + l] / right;
			this.#rects![idx + t] = positions[idx + t] / size;
			this.#rects![idx + r] = positions[idx + r] / right;
			this.#rects![idx + b] = positions[idx + b] / size;
		}

		this.#initialized = true;

		return this;
	}
}

class TextShader {
	static readonly #vSource = dedent`
		#version 300 es
		precision mediump float;

		// Inputs
		in      vec2 a_position;
		in      vec2 a_texcoord;

		// Uniform Inputs
		uniform mat3 u_matrix;

		// Outputs
		out     vec2 v_texcoord;

		void main() {
			gl_Position = vec4((u_matrix * vec3(a_position, 1.0)).xy, 0.0, 1.0);

			v_texcoord = a_texcoord;
		}
	`;

	static readonly #fSource = dedent`
		#version 300 es
		precision mediump float;

		// Inputs
		in      vec2      v_texcoord;

		// Uniform Inputs
		uniform sampler2D u_texture;
		uniform float     u_opacity;
		uniform vec3      u_color;

		// Outputs
		out     vec4      FragColor;

		void main() {
			vec4 tex = texture(u_texture, v_texcoord);
			FragColor = vec4(tex.xyz * u_color, tex.a * u_opacity);
		}
	`;

	static #shaderprogram: ShaderProgram;

	static #getProgram(gl: WebGL2RenderingContext) {
		if (!this.#shaderprogram) {
			this.#shaderprogram = ShaderProgram.compile(
				gl,
				'TextShader',
				this.#vSource,
				this.#fSource,
			);
		}

		return this.#shaderprogram;
	}

	readonly #gl: WebGL2RenderingContext;
	readonly #color: readonly [number, number, number];
	readonly #opacity: number;
	readonly #tab: string;
	readonly #program: ShaderProgram;

	constructor(
		gl: WebGL2RenderingContext,
		color: readonly [number, number, number],
		opacity: number,
		tab: string = '  ',
	) {
		this.#gl = gl;
		this.#color = color;
		this.#opacity = opacity;
		this.#tab = tab;

		this.#program = TextShader.#getProgram(gl);
	}

	draw(text: string, atlas: TextAtlas, buf?: Float32Array) {
		const r = atlas.write(text.replace(/\t/g, this.#tab), buf);
		const [w, h] = r.subarray(0, 2);
		const vArr = r.subarray(2);

		const b = new Buffer(this.#gl).update(vArr);

		const half = -0.5;

		const dx = Math.round(half * w);
		const dy = Math.round(half * h);

		const sx = 2 / w;
		const sy = 2 / h;

		const mat = new Matrix2D([
			sx,      0,       0,
			0,       sy,      0,
			dx * sx, dy * sy, 1,
		]);

		const stride = 16;
		const texcoordOffset = 8;

		this.#program
			.use()
			.attribute('a_position', b, 2, stride, 0)
			.attribute('a_texcoord', b, 2, stride, texcoordOffset)
			.matrix('u_matrix', mat)
			.uniform('u_color', this.#color)
			.uniform('u_opacity', this.#opacity)
			.uniformInt('u_texture', 0)
			.draw(this.#gl.TRIANGLES, vArr.length / Constants.CoordsPerRect);

		return this;
	}
}

class BackgroundShader {
	static readonly #fSource = dedent`
		#version 300 es
		precision mediump float;

		// Uniform Inputs
		uniform vec3  u_color;
		uniform float u_opacity;

		// Outputs
		out     vec4  FragColor;

		void main() {
			FragColor = vec4(u_color, u_opacity);
		}
	`;

	static #shaderprogram: ShaderProgram2D;

	static #getProgram(gl: WebGL2RenderingContext) {
		if (!this.#shaderprogram) {
			this.#shaderprogram = ShaderProgram2D.compile(gl, 'BackgroundShader', this.#fSource);
		}

		return this.#shaderprogram;
	}

	readonly #color: readonly [number, number, number];
	readonly #program: ShaderProgram2D;

	constructor(
		gl: WebGL2RenderingContext,
		color: readonly [number, number, number] = [0, 0, 0]
	) {
		this.#color = color;

		this.#program = BackgroundShader.#getProgram(gl);
	}

	draw(opacity: number) {
		this.#program.use()
			.uniformSpread('u_color', this.#color)
			.uniform('u_opacity', opacity)
			.draw();
	}
}

class GL {
	static checkForErrors(gl: WebGL2RenderingContext) {
		const err = gl.getError();

		if (err !== gl.NO_ERROR) {
			throw new GLError(this.#getMessageForErrorCode(gl, err), err);
		}
	}

	static create(name: string) {
		const canvas = d.createElement('canvas');
		canvas.id = name;
		d.body.appendChild(canvas);

		return new GL(canvas);
	}

	static #getMessageForErrorCode(
		gl: WebGL2RenderingContext,
		code: GLenum,
	) {
		switch (code) {
			case gl.INVALID_ENUM:
				return 'An unacceptable value was specified for an enumerated argument.';
			case gl.INVALID_VALUE:
				return 'A numeric argument was out of range.';
			case gl.INVALID_OPERATION:
				return 'The specified command was not allowed for the current state.';
			case gl.INVALID_FRAMEBUFFER_OPERATION:
				return 'The currently bound framebuffer is not framebuffer complete when trying to render or to read from it.';
			case gl.OUT_OF_MEMORY:
				return 'Not enough memory is left to execute the command.';
			case gl.CONTEXT_LOST_WEBGL:
				return 'WebGL context lost';
			default:
				return `Unknown code ${code}.`;
		}
	}

	readonly gl: WebGL2RenderingContext;
	readonly DEFAULT_FRAMEBUFFER: FrameBuffer;

	constructor(readonly canvas: HTMLCanvasElement) {
		this.gl = this.canvas.getContext('webgl2', { powerPreference: 'high-performance' })!;
		this.DEFAULT_FRAMEBUFFER = new FrameBuffer(this.gl, null);
	}
}

/** RNG */
type TypedNumberArrayConstructor =
	| Int8ArrayConstructor
	| Uint8ArrayConstructor
	| Int16ArrayConstructor
	| Uint16ArrayConstructor
	| Int32ArrayConstructor
	| Uint32ArrayConstructor
	| Float32ArrayConstructor
	| Float64ArrayConstructor;
type TypedBigintArrayConstructor =
	| BigInt64ArrayConstructor
	| BigUint64ArrayConstructor;
type TypedArrayConstructor =
	| TypedNumberArrayConstructor
	| TypedBigintArrayConstructor;
const MAX_BYTES = 65536; // Maximum buffer size for crypto.getRandomValues
/* tslint:disable:max-line-length */
function RNG(Ctor: TypedNumberArrayConstructor): Generator<number, number, never>;
function RNG(Ctor: TypedBigintArrayConstructor): Generator<bigint, bigint, never>;
function *RNG(Ctor: TypedArrayConstructor): Generator<number | bigint, number | bigint, never> {
/* tslint:enable:max-line-length */
	const arr = new Ctor(MAX_BYTES / Ctor.BYTES_PER_ELEMENT);
	const l = arr.length;
	let idx = l;

	while (true) {
		if (idx >= l) {
			crypto.getRandomValues(arr);
			idx = 0;
		}

		yield arr[idx++];
	}
}

const RandomUint8 = RNG(Uint8Array);

class CircularArray<T = unknown> extends Array<T> {
	#idx = 0;

	push(...elements: T[]) {
		const max = this.length - 1;

		for (const e of elements) {
			this[this.#idx] = e;

			if (++this.#idx > max) {
				this.#idx = 0;
			}
		}

		return elements.length;
	}

	unshift(...elements: T[]) {
		const max = this.length - 1;

		for (const e of elements) {
			this[this.#idx] = e;

			if (--this.#idx < 0) {
				this.#idx = max;
			}
		}

		return elements.length;
	}

	resize(l: number, v: T) {
		const ol = this.length;

		this.length = l;

		if (l > ol) {
			for (let i = ol; i < l; i++) {
				this[i] = v;
			}
		}
	}

	reset(v: T) {
		this.fill(v);

		this.#idx = 0;
	}
}

/** FPS Counter */
class FPS {
	public static readonly maxFramerate = 60;

	public framerate = 0;
	public delay = Infinity;

	readonly #hist = new CircularArray<number>(0).fill(0);

	readonly #size = 12;
	readonly #opacity = 0.75;

	readonly #gl: WebGL2RenderingContext;
	readonly #bg: BackgroundShader;
	readonly #font: TextAtlas;
	readonly #text: TextShader;

	/* tslint:disable:no-magic-numbers */
	readonly #hPadding = this.#size / 3;
	readonly #vPadding = this.#size / 3;
	/* tslint:enable:no-magic-numbers */

	#lastFrame = 0;

	constructor(gl: WebGL2RenderingContext) {
		this.#gl = gl;
		this.#bg = new BackgroundShader(gl);
		this.#font = TextAtlas.register(gl, 'monospace', this.#size, '.1234567890FPS');
		this.#text = new TextShader(gl, [0, 1, 0], this.#opacity);
		this.setFramerate(Framerate.get() || FPS.maxFramerate);
	}

	get last() {
		return this.#lastFrame;
	}

	clear() {
		this.#hist.reset(Math.min(this.framerate, FPS.maxFramerate));
	}

	setFramerate(framerate: number) {
		// tslint:disable-next-line:max-line-length
		this.#hist.resize(clamp(framerate, 1, FPS.maxFramerate), Math.min(framerate, FPS.maxFramerate));
		this.framerate = framerate;
		this.delay = Math.floor(Constants.MsPerSec / framerate);
		Framerate.set(framerate);
	}

	measure(time: number) {
		return Constants.MsPerSec / (time - this.#lastFrame);
	}

	draw(time: number, measured: number = this.measure(time)) {
		const hist = this.#hist;
		const hPadding = this.#hPadding;
		const vPadding = this.#vPadding;
		const size = this.#size;
		this.#lastFrame = time;
		hist.push(measured);
		// Calculate framerate over last n frames
		const framerate = hist.reduce((acc, h) => acc + h, 0) / hist.length;
		const text = `${framerate.toFixed(2)} FPS`;

		const tw = this.#font.measure(text);
		const th = size;

		const bgl = hPadding * 2;
		// tslint:disable-next-line:no-magic-numbers
		const bgb = this.#gl.drawingBufferHeight - th - (vPadding * 3);
		const bgw = tw + (hPadding * 2);
		const bgh = th + (vPadding * 2);

		this.#gl.viewport(bgl, bgb, bgw, bgh);

		this.#bg.draw(this.#opacity);

		const tl = bgl + hPadding;
		const tb = bgb + hPadding;

		this.#gl.viewport(tl, tb, tw, th);

		this.#text.draw(text, this.#font);
	}
}

class Metadata {
	readonly #size = 12;
	readonly #opacity = 0.75;

	readonly #gl: WebGL2RenderingContext;
	readonly #bg: BackgroundShader;
	readonly #font: TextAtlas;
	readonly #text: TextShader;

	/* tslint:disable:no-magic-numbers */
	readonly #hPadding = this.#size / 3;
	readonly #vPadding = this.#size / 3;
	/* tslint:enable:no-magic-numbers */

	// NOTE: Static buffer for character positions, since this will never have
	// repeat strings that can be cached.
	// Assumed maximum size for one billion frames & several days' runtime
	// (unlikely to be hit)
	// Fixed characters: 15 (`Frame:`, `|`, `Elapsed:`)
	// Variable characters:
	//   10 frame count  (up to 9,999,999,999 frames w/ no thousands separators)
	//   17 elapsed time (9,999,999,999 * 2 / 86400 = 231481 days and change @ 0.5 FPS)
	// Spaces can be safely ignored as they just advance the X position of the
	// next character
	// tslint:disable-next-line:no-magic-numbers binary-expression-operand-order
	readonly #buf = new Float32Array(2 + ((15 + 10 + 17) * Constants.CoordsPerChar));

	#frame = 0;
	#start = Date.now();

	constructor(gl: WebGL2RenderingContext) {
		this.#gl = gl;
		this.#bg = new BackgroundShader(gl);
		this.#font = TextAtlas.register(gl, 'monospace', this.#size, '1234567890EFadelmprs:.|');
		this.#text = new TextShader(gl, [0, 1, 0], this.#opacity);
	}

	reset() {
		this.#frame = 0;
		this.#start = Date.now();

		const l = this.#buf.length;
		for (let i = 0; i < l; i++) {
			this.#buf[i] = 0;
		}
	}

	draw() {
		const frame = this.#frame;
		const hPadding = this.#hPadding;
		const vPadding = this.#vPadding;
		const font = this.#font;
		const size = this.#size;

		const text = `Frame: ${frame} | Elapsed: ${this.#formatElapsedTime()}`;

		const tw = font.measure(text);
		const th = size;

		const bgl = hPadding * 2;
		const bgb = vPadding;
		const bgw = tw + (hPadding * 2);
		const bgh = th + (vPadding * 2);

		this.#gl.viewport(bgl, bgb, bgw, bgh);

		this.#bg.draw(this.#opacity);

		const tl = bgl + hPadding;
		const tb = bgb + hPadding;

		this.#gl.viewport(tl, tb, tw, th);

		this.#text.draw(text, this.#font, this.#buf);

		this.#frame++;
	}

	#formatElapsedTime() {
		/* tslint:disable:max-line-length */
		const elapsed = Date.now() - this.#start;
		const ms = `00${elapsed % Constants.MsPerSec}`.slice(Constants.MsDigitsSlice);
		const s = `0${Math.floor(elapsed / Constants.MsPerSec) % Constants.SecsPerMinute}`.slice(Constants.HMSDigitsSlice);
		const m = `0${Math.floor(elapsed / Constants.MsPerMinute) % Constants.MinutesPerHour}`.slice(Constants.HMSDigitsSlice);
		const h = `0${Math.floor(elapsed / Constants.MsPerHour) % Constants.HoursPerDay}`.slice(Constants.HMSDigitsSlice);
		const d = Math.floor(elapsed / Constants.MsPerDay);
		/* tslint:enable:max-line-length */

		const res = [h, m, s].join(':').concat('.', ms);

		if (d) {
			return [d, res].join(' ');
		}

		return res;
	}
}

const enum Orientation {
	Portrait,
	Landscape
}

class Board {
	static readonly BLANK = new Board('BLANK', 0, 0, 2);

	static readonly prebuilt = new Map<string, () => Promise<Board>>();

	static readonly #prebuiltCache = new Map<string, Board>();

	static async load(file: string) {
		const contents = (await (await fetch(file)).text()).trim();

		const lines = contents.split('\n');

		for (let i = 0; i < lines.length;) {
			const name = lines[i++];
			const [pattern, scale] = lines[i++].split(' ');

			// tslint:disable-next-line:no-magic-numbers
			this.#create(name, pattern, scale ? Number.parseInt(scale, 10) : undefined);
		}
	}

	static #create(name: string, file: string, scale = 2) {
		const kebabCase = name.toLowerCase().replace(/\s+/g, '-');

		if (file.startsWith('./') || file.startsWith('../')) {
			this.prebuilt.set(
				kebabCase,
				Board.#defer(Board.#parse, name, file, kebabCase, scale),
			);
		} else {
			this.prebuilt.set(
				kebabCase,
				Board.#defer(Board.#from, name, file, kebabCase, scale),
			);
		}

		const btn = d.createElement('button');
		btn.setAttribute('data-pattern', kebabCase);

		btn.appendChild(d.createTextNode(name));

		if (location.hash.substring(1) === kebabCase) {
			btn.classList.add('active');
		}

		btnContainer.appendChild(btn);
	}

	static #defer(
		func: ((
			self: typeof Board,
			boardName: string,
			file: string,
			name: string,
			scale: number,
		) => Board | Promise<Board>),
		boardName: string,
		file: string,
		name: string,
		scale: number,
	) {
		return async () => await func(this, boardName, file, name, scale);
	}

	static async #parse(
		self: typeof Board,
		boardName: string,
		file: string,
		name: string,
		scale: number,
	) {
		if (self.#prebuiltCache.has(name)) {
			return self.#prebuiltCache.get(name)!;
		}

		loadingModal.show();

		try {
			const resp = await fetch(file);

			if (!resp.ok) {
				return Board.BLANK;
			}

			const txt = (await resp.text()).trim();

			if (file.endsWith('.lif')) {
				return Board.#from(self, boardName, txt, name, scale);
			} else if (file.endsWith('.rle')) {
				return Board.#from(self, boardName, Board.#expandRLE(txt), name, scale);
			}
		} finally {
			loadingModal.reset();
		}

		return Board.BLANK;
	}

	static #expandRLE(rle: string) {
		parsingModal.show();

		let res = '';

		const lines = rle.split('\n');

		let x = 0;
		let y = 0;

		for (const line of lines) {
			if (line.startsWith('#')) { // comment
				continue;
			} else if (line.startsWith('x')) {
				const [, _x, _y] = line.match(/x ?= ?(\d+), ?y ?= ?(\d+)/) ?? [];

				x = Number.parseInt(_x, 10); // tslint:disable-line:no-magic-numbers
				y = Number.parseInt(_y, 10); // tslint:disable-line:no-magic-numbers
			} else if (x && y) {
				let idx = 0;
				let count = 1;

				while (idx < line.length) {
					let c = line.charAt(idx++);

					/* tslint:disable:prefer-switch */
					if (isNumeric(c)) {
						let n = c;

						while (idx < line.length && isNumeric((c = line.charAt(idx)))) {
							n += c;
							idx++;
						}

						count = Number.parseInt(n, 10); // tslint:disable-line:no-magic-numbers
					} else if (c === 'b') {
						res += '.'.repeat(count);
						count = 1;
					} else if (c === 'o') {
						res += '*'.repeat(count);
						count = 1;
					} else if (c === '$') {
						res += '\n'.repeat(count);
						count = 1;
					} else if (c === '!') {
						break;
					}
					/* tslint:enable:prefer-switch */
				}

				if (line.endsWith('!')) {
					break;
				}
			}
		}

		parsingModal.reset();

		return res;
	}

	static #from(
		self: typeof Board,
		boardName: string,
		str: string,
		name: string,
		scale: number,
	) {
		if (self.#prebuiltCache.has(name)) {
			return self.#prebuiltCache.get(name)!;
		}

		parsingModal.show();

		const lines = str.replace(/[#!].*\n/g, '').replace(/\\n/g, '\n').split('\n');

		const h = lines.length;
		const w = Math.max(...lines.map(l => l.length));

		const board = new Board(boardName, w, h, scale);

		for (let y = 0; y < h; y++) {
			const line = lines[y];
			const l = line.length;
			const base = y * w;

			for (let x = 0; x < w; x++) {
				if (x < l && line.charAt(x) !== '.') {
					board.board[base + x] = 1;
				}
			}
		}

		self.#prebuiltCache.set(name, board);

		parsingModal.reset();

		return board;
	}

	readonly orientation: Orientation;
	readonly board: Uint8Array;

	constructor(
		readonly name: string,
		readonly w: number,
		readonly h: number,
		readonly scale: number = 2,
	) {
		this.orientation = this.w > this.h
			? Orientation.Landscape
			: Orientation.Portrait;
		this.board = new Uint8Array(this.w * this.h);
	}

	rotate() {
		const { w, h } = this;

		const res = new Board(this.name, h, w, this.scale);

		for (let y = 0; y < h; y++) {
			const base = y * w;

			for (let x = 0; x < w; x++) {
				res.board[(x * h) + y] = this.board[base + x];
			}
		}

		return res;
	}
}

class GOL {
	/** Private static members */
	static readonly #BASE_HASH = 0x4E67C6A7;
	// tslint:disable-next-line:no-magic-numbers
	static readonly #HISTORY = new CircularArray<number | null>(1024);
	static readonly #HASH_TRACKER = new Uint32Array([this.#BASE_HASH, 0]);

	static readonly #shadefSource = `
		#version 300 es
		precision mediump float;

		// Constants
		const   vec4      hue = vec4(0.0, 0.5, 1.0, 1.0);

		// Uniform Inputs
		uniform sampler2D state;
		uniform vec2      scale;

		// Outputs
		out     vec4      FragColor;

		void main() {
			FragColor = hue * texture(state, gl_FragCoord.xy / scale);
		}
	`;

	static readonly #golfSource = `
		#version 300 es
		precision mediump float;

		// Constants
		const   float     HUE_SHIFT_FRAMES      = 720.0;
		const   float     FAST_HUE_SHIFT_FRAMES = 36.0;
		const   float     HUE_SHIFT             = 1.0 / HUE_SHIFT_FRAMES;
		const   float     FAST_HUE_SHIFT        = 1.0 / FAST_HUE_SHIFT_FRAMES;
		const   float     ALIVE                 = 1.0;
		const   vec4      INIT_HUE              = vec4(0.0, 0.5, 1.0, 1.0);
		const   vec4      BLACK                 = vec4(0.0, 0.0, 0.0, 0.0);
		const   vec4      WHITE                 = vec4(1.0, 1.0, 1.0, 1.0);
		const   vec4      RED                   = vec4(1.0, 0.0, 0.0, 1.0);

		// Uniform Inputs
		uniform sampler2D state;
		uniform vec2      scale;
		uniform bool      rave;

		// Outputs
		out     vec4      FragColor;

		int get(vec2 offset) {
			return int(texture(state, (gl_FragCoord.xy + offset) / scale).a == ALIVE);
		}

		vec4 CalculateNewAge(vec4 age) {
			int neighbors =
				get(vec2(-1.0, -1.0)) +    get(vec2(-1.0,  0.0)) +    get(vec2(-1.0,  1.0)) +
				get(vec2( 0.0, -1.0)) + /* get(vec2( 0.0,  0.0)) + */ get(vec2( 0.0,  1.0)) +
				get(vec2( 1.0, -1.0)) +    get(vec2( 1.0,  0.0)) +    get(vec2( 1.0,  1.0));

			bool alive = age.a == ALIVE;

			if (neighbors == 3) {
				return alive ? age : WHITE;
			} else if (neighbors == 2 && alive) {
				return age;
			}

			return BLACK;
		}

		// source: http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
		vec3 rgb2hsv(vec3 c) {
			const float e = 1.0e-10;

			vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
			vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

			float d = q.x - min(q.w, q.y);

			return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		vec3 hsv2rgb(vec3 c) {
			vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);

			return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
		}

		vec4 CalculateNextColor(vec4 color) {
			if (color == BLACK || color == RED) {
				return color;
			}
			if (color == WHITE) {
				return INIT_HUE;
			}

			vec3 hsv = rgb2hsv(color.rgb);

			if (hsv.r < HUE_SHIFT) {
				return RED;
			}

			return vec4(hsv2rgb(vec3(hsv.r - HUE_SHIFT, hsv.gb)), color.a);
		}

		vec4 Rave(vec4 color) {
			if (color == BLACK) {
				return BLACK;
			}
			if (color == WHITE) {
				return INIT_HUE;
			}

			vec3 hsv = rgb2hsv(color.rgb);

			return vec4(hsv2rgb(vec3(hsv.r - FAST_HUE_SHIFT, hsv.gb)), color.a);
		}

		void main(void) {
			vec4 color = texture(state, gl_FragCoord.xy / scale);

			vec4 age = CalculateNewAge(color);

			FragColor = rave ? Rave(age) : CalculateNextColor(age);
		}
	`;

	static readonly #condensefSource = `
		#version 300 es
		precision mediump float;

		// Constants
		const   float     FIRST_PX  = 1.0 / 2.0;
		const   float     SECOND_PX = FIRST_PX / 2.0;
		const   float     THIRD_PX  = SECOND_PX / 2.0;
		const   float     FOURTH_PX = THIRD_PX / 2.0;

		// Uniform Inputs
		uniform sampler2D state;
		uniform vec2      scale;

		// Outputs
		out     vec4      FragColor;

		float get(vec2 offset) {
			return texture(state, (gl_FragCoord.xy + offset) / scale).a;
		}

		float amalgam(vec2 offset) {
			return (
				(get(offset + vec2(-0.25, -0.25)) * FIRST_PX) +
				(get(offset + vec2(-0.25,  0.0 )) * SECOND_PX) +
				(get(offset + vec2( 0.0 ,  0.0 )) * THIRD_PX) +
				(get(offset + vec2( 0.0 , -0.25)) * FOURTH_PX)
			);
		}

		void main() {
			FragColor = vec4(
				amalgam(vec2(-0.25, -0.25)),
				amalgam(vec2(-0.25,  0.25)),
				amalgam(vec2( 0.25,  0.25)),
				amalgam(vec2( 0.25, -0.25))
			);
		}
	`;

	static readonly #copyfSource = `
		#version 300 es
		precision mediump float;

		// Uniform Inputs
		uniform sampler2D state;
		uniform vec2      scale;

		// Outputs
		out     vec4      FragColor;

		void main() {
			FragColor = texture(state, gl_FragCoord.xy / scale);
		}
	`;

	/** Public static functions */
	static resetHistory() {
		this.#HISTORY.reset(null);
	}

	/** Private static functions */
	/* tslint:disable:no-magic-numbers */
	static #detectBoardStagnation(curr: number) {
		let matches = 0;

		for (const hash of this.#HISTORY) {
			if (hash === null) break;
			if (hash === curr) {
				if (++matches === 5) return true;
			}
		}

		this.#HISTORY.push(curr);

		return false;
	}

	/* tslint:disable:no-bitwise */
	static #hashBoard(actualBoard: Uint8Array) {
		const hash = this.#HASH_TRACKER;
		hash[0] = this.#BASE_HASH;
		hash[1] = 0;
		// NOTE: Massive speed improvement hashing 32 bits (entire pixel value) at a
		// time instead of 8 bits (subpixel value) at a time
		const board = new Uint32Array(actualBoard.buffer);
		const l = board.length;

		for (let i = 0; i < l; i++) {
			const v = board[i];
			hash[1] |= v;
			hash[0] ^= ((hash[0] << 5) + v + (hash[0] >> 2));
		}

		return hash[1] ? (hash[0] || 1) : 0;
	}
	/* tslint:enable:no-bitwise */
	/* tslint:enable:no-magic-numbers */

	/** Public data members */
	orientation: Orientation | undefined;

	w = 0; // Window/canvas width
	h = 0; // Window/canvas height

	/** Public bound function members */
	readonly advance = this.#advance.bind(this) as FrameRequestCallback;
	readonly resize = this.#resize.bind(this) as () => void;

	/** Private data members */
	readonly #GL: GL;
	readonly #canvas: HTMLCanvasElement;
	readonly #gl: WebGL2RenderingContext;
	readonly #fps: FPS;
	readonly #metadata: Metadata;

	readonly #shade: ShaderProgram2D;
	readonly #gol: ShaderProgram2D;
	readonly #condense: ShaderProgram2D;
	readonly #copy: ShaderProgram2D;

	readonly #frameBuffer: FrameBuffer;

	#t1: Texture;
	#t2: Texture;
	readonly #t3: Texture;

	#vw = 0; // Viewport width
	#vh = 0; // Viewport height
	#cw = 0; // Condensed width
	#ch = 0; // Condensed height

	#viewportSize: Float32Array | undefined;
	#stateSize: Float32Array | undefined;
	#condenseSize: Float32Array | undefined;

	#condenseBuffer: Uint8Array | undefined;

	#board = Board.BLANK;
	#last: number | undefined;

	#started = false;
	#dead = false;
	#playing = false;

	/** Private bound function members */
	readonly #iterate = this.#iterateImpl.bind(this) as FrameRequestCallback;
	// tslint:disable-next-line:max-line-length
	readonly #renderFirstFrame = this.#renderFirstFrameImpl.bind(this) as FrameRequestCallback;

	constructor(GL: GL) {
		this.#GL = GL;

		const gl = GL.gl;

		this.#canvas = GL.canvas;
		this.#gl = gl;

		this.#fps = new FPS(gl);
		this.#metadata = new Metadata(gl);

		this.#shade = ShaderProgram2D.compile(gl, 'shade', GOL.#shadefSource);
		this.#gol = ShaderProgram2D.compile(gl, 'gol', GOL.#golfSource);
		this.#condense = ShaderProgram2D.compile(gl, 'condense', GOL.#condensefSource);
		this.#copy = ShaderProgram2D.compile(gl, 'copy', GOL.#copyfSource);

		this.#frameBuffer = new FrameBuffer(gl);

		this.#t1 = new Texture(gl, gl.RGBA, gl.REPEAT, gl.NEAREST);
		this.#t2 = new Texture(gl, gl.RGBA, gl.REPEAT, gl.NEAREST);
		this.#t3 = new Texture(gl, gl.RGBA, gl.REPEAT, gl.NEAREST);

		this.#resize();
		this.#init();
	}

	get alive() {
		return !this.#dead;
	}

	get autorestart() {
		return RestartOnStagnation.get();
	}
	set autorestart(autorestart) {
		RestartOnStagnation.set(autorestart);
	}

	get raving() {
		return !!RaveMode.get();
	}
	set raving(rave) {
		RaveMode.set(rave ? 1 : 0);
	}

	get paused() {
		return !this.#playing;
	}
	set paused(_pause) {
		this.#playing ? this.stop() : this.start();
	}

	get targetFramerate(): number {
		return this.#fps.framerate;
	}
	set targetFramerate(target) {
		this.#fps.setFramerate(target);
	}

	start(force = false) {
		if (this.#dead && !force) {
			return;
		}

		this.#dead = false;

		this.#playing = true;
		this.#last = self.requestAnimationFrame(
			this.#started ? this.#iterate : this.#renderFirstFrame
		);
	}

	stop() {
		this.#playing = false;
		if (this.#last) {
			self.cancelAnimationFrame(this.#last);

			this.#last = undefined;
		}
	}

	setBoard(board: Board) {
		this.#fps.clear();
		this.#metadata.reset();
		this.#started = false;

		this.#board = board;

		this.#updateScale();

		if (board.w > this.#vw || board.h > this.#vh) {
			throw new Error('The selected pattern is too large to show on your current screen.');
		}

		if (board === Board.BLANK) {
			this.randomize();

			return;
		}

		this.#t1.blank(this.#vw, this.#vh);

		const { w, h } = board;

		const bhx = Math.floor(w / 2);
		const bhy = Math.floor(h / 2);

		const ghx = Math.floor(this.#vw / 2);
		const ghy = Math.floor(this.#vh / 2);

		const x = ghx - bhx;
		const y = ghy - bhy;

		const l = w * h;

		const rgba = new Uint32Array(l);

		for (let i = 0; i < l; i++) {
			// tslint:disable-next-line:no-magic-numbers
			rgba[i] = board.board[i] ? 0xFFFFFFFF : 0x0;
		}

		this.#t1.subsetData(new Uint8Array(rgba.buffer), x, y, w, h);

		GL.checkForErrors(this.#gl);
	}

	randomize() {
		const l = this.#vw * this.#vh;

		const rgba = new Uint32Array(l);

		let shift = 0;
		let rand = 0;
		for (let i = 0; i < rgba.length; i++) {
			if (shift === 0) {
				rand = RandomUint8.next().value;
			}
			/* tslint:disable:no-bitwise no-magic-numbers */
			rgba[i] = ((rand >> shift) & 0x01) ? 0xFFFFFFFF : 0x0;
			shift = ++shift % 8;
			/* tslint:enable:no-bitwise no-magic-numbers */
		}

		this.#t1.subsetData(new Uint8Array(rgba.buffer), 0, 0, this.#vw, this.#vh);

		GL.checkForErrors(this.#gl);
	}

	#advance(now: number) {
		const measured = this.#fps.measure(now);
		const { last, delay } = this.#fps;
		if (now - last >= delay) {
			this.#step();
			this.#detectStagnation();
			this.#draw();

			this.#gl.enable(this.#gl.BLEND);
			this.#drawFps(now, measured);
			this.#drawMetadata();
			this.#gl.disable(this.#gl.BLEND);
		}
	}

	#iterateImpl(now: number) {
		this.#advance(now);

		this.#last = requestAnimationFrame(this.#dead ? this.advance : this.#iterate);
	}

	#renderFirstFrameImpl(now: number) {
		this.#started = true;
		this.#initializeBoard();
		this.#draw();

		this.#gl.enable(this.#gl.BLEND);
		this.#drawFps(now, this.#fps.measure(now));
		this.#drawMetadata();
		this.#gl.disable(this.#gl.BLEND);

		this.#last = requestAnimationFrame(this.#iterate);
	}

	#resize() {
		const w = window.innerWidth;
		const h = window.innerHeight;

		this.orientation = w > h ? Orientation.Landscape : Orientation.Portrait;

		this.#updateScale();
	}

	#updateScale() {
		if (!this.#board?.scale) {
			return;
		}

		const scale = this.#board.scale;

		const w = window.innerWidth;
		const h = window.innerHeight;

		this.#canvas.width = w - (w % scale);
		this.#canvas.height = h - (h % scale);

		this.w = this.#gl.drawingBufferWidth;
		this.h = this.#gl.drawingBufferHeight;

		this.#vw = this.w / scale;
		this.#vh = this.h / scale;

		// Condense the alpha state of 4x4 block of pixels into one pixel
		const condense = 4;

		this.#cw = Math.ceil(this.#vw / condense);
		this.#ch = Math.ceil(this.#vh / condense);

		this.#viewportSize = new Float32Array([this.w, this.h]);
		this.#stateSize = new Float32Array([this.#vw, this.#vh]);
		this.#condenseSize = new Float32Array([this.#cw, this.#ch]);

		this.#condenseBuffer = new Uint8Array(
			this.#cw * this.#ch * Constants.BytesPerPixel
		);

		// Preserve contents of t1 only, let the others be blank
		this.#t1.resize(this.#vw, this.#vh, this.#frameBuffer);
		this.#t2.resize(this.#vw, this.#vh);
		this.#t3.resize(this.#cw, this.#ch);
	}

	#initializeBoard() {
		this.#frameBuffer.attach(this.#t2);

		this.#t1.bind();

		this.#gl.viewport(0, 0, this.#vw, this.#vh);

		this.#shade
			.use()
			.uniformInt('state', 0)
			.uniformSpread('scale', this.#stateSize)
			.draw();

		this.#swapBuffers();

		return this;
	}

	#step() {
		this.#frameBuffer.attach(this.#t2);

		this.#t1.bind();

		this.#gl.viewport(0, 0, this.#vw, this.#vh);

		this.#gol
			.use()
			.uniformInt('state', 0)
			.uniformInt('rave', RaveMode.get())
			.uniformSpread('scale', this.#stateSize)
			.draw();

		this.#swapBuffers();

		return this;
	}

	#detectStagnation() {
		this.#frameBuffer.attach(this.#t3);

		this.#t1.bind();

		// Condense alpha channels into smaller texture in order to reduce amount of
		// data copied off GPU & processed on CPU
		this.#condense
			.use()
			.uniformInt('state', 0)
			.uniformSpread('scale', this.#condenseSize)
			.draw();

		this.#gl.readPixels(
			0,
			0,
			this.#cw,
			this.#ch,
			this.#gl.RGBA,
			this.#gl.UNSIGNED_BYTE,
			this.#condenseBuffer!,
		);

		const hash = GOL.#hashBoard(this.#condenseBuffer!);

		if (hash === 0) {
			stagnantModal.dismiss();
			deadModal.show();

			this.stop();
			this.#dead = true;
		} else {
			if (GOL.#detectBoardStagnation(hash)) {
				if (RestartOnStagnation.get()) {
					this.setBoard(this.#board);
				} else {
					stagnantModal.show();
				}
			}
		}

		return this;
	}

	#draw() {
		this.#GL.DEFAULT_FRAMEBUFFER.bind();

		this.#t1.bind();

		this.#gl.viewport(0, 0, this.w, this.h);

		this.#copy
			.use()
			.uniformInt('state', 0)
			.uniformSpread('scale', this.#viewportSize)
			.draw();

		return this;
	}

	#drawFps(now: number, measured: number) {
		this.#GL.DEFAULT_FRAMEBUFFER.bind();

		this.#fps.draw(now, measured);
	}

	#drawMetadata() {
		this.#GL.DEFAULT_FRAMEBUFFER.bind();

		this.#metadata.draw();
	}

	#init() {
		this.#gl.disable(this.#gl.DEPTH_TEST);
		this.#gl.blendFunc(this.#gl.SRC_ALPHA, this.#gl.ONE_MINUS_SRC_ALPHA);
	}

	#swapBuffers() {
		const tmp = this.#t1;
		this.#t1 = this.#t2;
		this.#t2 = tmp;
	}
}

let game: GOL;

class Toggle {
	static readonly #container = getById<HTMLDivElement>('settings');
	static readonly #toggles = new Map<string, Toggle>();

	static get(id: string) {
		return this.#toggles.get(id);
	}

	static create(which: 'raving' | 'paused' | 'autorestart', text: string, shortcut: string) {
		if (!this.#toggles.has(which)) {
			this.#toggles.set(which, new Toggle(which, text, shortcut));
		}

		return this.#toggles.get(which)!;
	}

	static init() {
		for (const toggle of this.#toggles.values()) {
			toggle.init();
		}
	}

	readonly #which: 'raving' | 'paused' | 'autorestart';
	readonly #text: string;
	readonly #element = d.createElement('button');
	readonly #txt = d.createTextNode('');

	private constructor(
		which: 'raving' | 'paused' | 'autorestart',
		text: string,
		shortcut: string
	) {
		this.#which = which;
		this.#text = text;

		this.#element.appendChild(this.#txt);
		this.#element.setAttribute('data-toggle', this.#which);
		this.#element.setAttribute('title', shortcut);

		this.init();

		Toggle.#container.appendChild(this.#element);
	}

	init() {
		// tslint:disable-next-line:no-unsafe-any
		this.setText(game?.[this.#which] ?? false);
	}

	toggle() {
		const state = !game[this.#which];
		game[this.#which] = state;

		this.setText(state);
	}

	setText(state: boolean) {
		this.#txt.data = `${this.#text}: ${state ? 'On' : 'Off'}`;
	}
}

const raveButton = Toggle.create('raving', 'Rave Mode', 'x');
const pauseButton = Toggle.create('paused', 'Paused', 'Spacebar');
const autoRestartButton = Toggle.create('autorestart', 'Auto-Restart on Stagnation', 'a');

class Adjuster {
	static readonly #container = getById<HTMLDivElement>('settings');
	static readonly #adjusters = new Map<string, Adjuster>();

	static get(id: string) {
		return this.#adjusters.get(id);
	}

	static create(which: 'targetFramerate', text: string, allowedValues: (number | [number, string])[]) {
		if (!this.#adjusters.has(which)) {
			this.#adjusters.set(which, new Adjuster(which, text, allowedValues));
		}

		return this.#adjusters.get(which);
	}

	static init() {
		for (const adjuster of this.#adjusters.values()) {
			adjuster.init();
		}
	}

	readonly #which: 'targetFramerate';
	readonly #allowedValues: (number | [number, string])[];

	readonly #element = d.createElement('div');
	readonly #txt = d.createTextNode('');
	readonly #btnContainer = d.createElement('span');
	readonly #up = d.createElement('button');
	readonly #down = d.createElement('button');

	#idx = 0;

	private constructor(
		which: 'targetFramerate',
		text: string,
		allowedValues: (number | [number, string])[],
	) {
		this.#which = which;
		this.#allowedValues = allowedValues;

		this.#element.appendChild(d.createTextNode(`${text}: `));
		this.#element.appendChild(this.#txt);
		this.#element.appendChild(this.#btnContainer);

		this.#btnContainer.appendChild(this.#up);
		this.#btnContainer.appendChild(this.#down);
		this.#btnContainer.setAttribute('class', 'buttons');

		this.#element.setAttribute('data-adjuster', this.#which);

		this.#down.setAttribute('data-adjust', this.#which);
		this.#down.setAttribute('data-dir', 'down');

		this.#up.setAttribute('data-adjust', this.#which);
		this.#up.setAttribute('data-dir', 'up');

		this.init();

		Adjuster.#container.appendChild(this.#element);
	}

	init() {
		const v = game?.[this.#which];

		// tslint:disable-next-line:max-line-length
		this.#idx = this.#allowedValues.findIndex(val => Array.isArray(val) ? val[0] === v : val === v);
		const val = this.#allowedValues[this.#idx];

		this.setText(Array.isArray(val) ? val[1] : val);
	}

	increment() {
		this.setValue(Math.min(this.#idx + 1, this.#allowedValues.length - 1));
	}

	decrement() {
		this.setValue(Math.max(this.#idx - 1, 0));
	}

	setValue(idx: number) {
		this.#idx = idx;
		const v = this.#allowedValues[this.#idx];

		if (Array.isArray(v)) {
			game[this.#which] = v[0];
			this.setText(v[1]);
		} else {
			game[this.#which] = v;
			this.setText(v);
		}
	}

	setText(state: unknown) {
		this.#txt.data = `${state}`;
	}
}

/*const fpsAdjuster = */Adjuster.create(
	'targetFramerate',
	'Framerate',
	Array<number>(FPS.maxFramerate)
		.fill(0)
		/* tslint:disable:no-magic-numbers */
		.reduce(
			// tslint:disable-next-line:max-line-length
			(acc, _, idx) => FPS.maxFramerate % (idx + 1) === 0 ? acc.concat(idx + 1) : acc,
			[0.5] as (number | [number, string])[],
		)
		.concat([[600, 'Unlimited']])
		/* tslint:enable:no-magic-numbers */
);

async function loadBoardForCurrentHash() {
	const hash = location.hash.substring(1);

	if (Board.prebuilt.has(hash)) {
		const res = await Board.prebuilt.get(hash)!();

		let board = res;

		if (game.orientation !== board.orientation) {
			board = board.rotate();
		}

		game.setBoard(board);
	} else {
		game.setBoard(Board.BLANK);
	}
}

async function main() {
	game = new GOL(new GL(getById<HTMLCanvasElement>('screen')));

	loadingModal.show();
	try {
		await Board.load('../pattern.index');
	} catch {
		// Ensure no hash if there's an issue running the pattern index
		location.hash = '';
	} finally {
		await loadBoardForCurrentHash(); // Load pattern if available
		loadingModal.reset(); // In case pattern is pre-baked
	}

	if (!d.hidden) {
		game.start(true);
	}

	Toggle.init();
	Adjuster.init();

	self.addEventListener('resize', game.resize, { passive: true });
}

main();

async function reset() {
	try {
		game.stop();
		GOL.resetHistory();

		deadModal.reset();
		stagnantModal.reset();
		errorModal.reset();

		await loadBoardForCurrentHash();

		game.start(true);
	} catch (e) {
		errorModal.show((e as Error).message);
	}
}

self.addEventListener('visibilitychange', () => {
	if (d.hidden) {
		game.stop();
	} else if (game.alive) {
		game.start();
	}
});

self.addEventListener('keydown', (e: KeyboardEvent) => {
	switch (e.key) {
		case 'a':
			autoRestartButton.toggle();
			break;
		case 'n':
			if (game.paused) {
				requestAnimationFrame(game.advance);
			}
			break;
		case 'r':
			reset();
			break;
		case 'x':
			raveButton.toggle();
			break;
		case ' ':
			pauseButton.toggle();
			break;
		default:
			/* Do nothing */
			return;
	}

	e.preventDefault();
	e.stopPropagation();
});

self.addEventListener('click', (e: MouseEvent) => {
	for (const el of e.composedPath()) {
		if (el instanceof HTMLElement) {
			if (el.hasAttribute('data-pattern')) {
				const active = d.querySelector('button[data-pattern].active');
				if (active) {
					active.classList.remove('active');
				}

				e.preventDefault();
				e.stopPropagation();

				const pattern = el.getAttribute('data-pattern')!;
				if (location.hash.substring(1) !== pattern) {
					location.hash = pattern;
				}

				reset();

				el.classList.add('active');

				return;
			} else if (el.hasAttribute('data-dismiss')) {
				Modal.get(el.getAttribute('data-dismiss')!)?.dismiss();
			} else if (el.hasAttribute('data-toggle')) {
				Toggle.get(el.getAttribute('data-toggle')!)?.toggle();
			} else if (el.hasAttribute('data-adjust')) {
				const adjuster = Adjuster.get(el.getAttribute('data-adjust')!);

				if (el.getAttribute('data-dir') === 'up') {
					adjuster?.increment();
				} else {
					adjuster?.decrement();
				}
			}
		}
	}
});
