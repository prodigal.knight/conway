// deno-fmt-ignore-file
// deno-lint-ignore-file
// This code was bundled using `deno bundle` and it's not recommended to edit it manually

const d = document;
function getById(id) {
    return d.getElementById(id);
}
const btnContainer = getById('quick-change-container');
const isNumeric = (c)=>'0123456789'.includes(c);
function dedent(strings, ...values) {
    const raw = typeof strings === 'string' ? [
        strings
    ] : strings.raw;
    let result = '';
    for(let i = 0; i < raw.length; i++){
        result += raw[i].replace(/\\\n[ \t]*/g, '').replace(/\\`/g, '`');
        if (i < values.length) {
            result += values[i];
        }
    }
    const lines = result.split('\n');
    const mindent = lines.reduce((acc, l)=>Math.min(acc, l.match(/^(\s+)\S+/)?.[1].length ?? Infinity), Infinity);
    if (mindent !== Infinity) {
        result = lines.map((l)=>l[0] === ' ' || l[0] === '\t' ? l.slice(mindent) : l).join('\n');
    }
    return result.trim().replace(/\\n/g, '\n');
}
function clamp(v, min, max) {
    return v < min ? min : v > max ? max : v;
}
class Setting {
    #dv;
    #storage;
    #value;
    constructor(name, dv, storage = sessionStorage){
        this.name = name;
        this.#dv = dv;
        this.#storage = storage;
        this.#value = JSON.parse(this.#storage.getItem(this.name)) ?? this.#dv;
    }
    get() {
        return this.#value;
    }
    set(value) {
        this.#value = value;
        this.#storage.setItem(this.name, JSON.stringify(value));
    }
    name;
}
const Framerate = new Setting('framerate', 60);
const RaveMode = new Setting('rave', 0);
const RestartOnStagnation = new Setting('restart-on-stagnation', false);
class Modal {
    static #modals = new Map();
    static get(id) {
        return this.#modals.get(id);
    }
    #id;
    #element;
    #content;
    #dismissed = false;
    constructor(id){
        this.#id = id;
        this.#element = getById(this.#id);
        this.#content = this.#element.querySelector('.content');
        Modal.#modals.set(this.#id, this);
    }
    show(text) {
        if (text) {
            this.#content.textContent = text;
        }
        if (!this.#dismissed && this.#element.hasAttribute('hidden')) {
            this.#element.removeAttribute('hidden');
        }
    }
    dismiss() {
        this.#dismissed = true;
        if (!this.#element.hasAttribute('hidden')) {
            this.#element.setAttribute('hidden', '');
        }
    }
    reset() {
        this.dismiss();
        this.#dismissed = false;
    }
}
const deadModal = new Modal('dead-warning');
const stagnantModal = new Modal('stagnant-warning');
const errorModal = new Modal('error-message');
const loadingModal = new Modal('loading');
const parsingModal = new Modal('parsing');
var Constants;
(function(Constants) {
    Constants[Constants["BinaryRadix"] = 2] = "BinaryRadix";
    Constants[Constants["OctalRadix"] = 8] = "OctalRadix";
    Constants[Constants["DecimalRadix"] = 10] = "DecimalRadix";
    Constants[Constants["HexRadix"] = 16] = "HexRadix";
    Constants[Constants["ShaderCompilationErrorLineCount"] = 5] = "ShaderCompilationErrorLineCount";
    Constants[Constants["BytesPerF32"] = 4] = "BytesPerF32";
    Constants[Constants["BytesPerPixel"] = 4] = "BytesPerPixel";
    Constants[Constants["CoordsPerRect"] = 4] = "CoordsPerRect";
    Constants[Constants["CoordsPerChar"] = 24] = "CoordsPerChar";
    Constants[Constants["MsPerSec"] = 1000] = "MsPerSec";
    Constants[Constants["SecsPerMinute"] = 60] = "SecsPerMinute";
    Constants[Constants["MsPerMinute"] = 60000] = "MsPerMinute";
    Constants[Constants["MinutesPerHour"] = 60] = "MinutesPerHour";
    Constants[Constants["MsPerHour"] = 3600000] = "MsPerHour";
    Constants[Constants["HoursPerDay"] = 24] = "HoursPerDay";
    Constants[Constants["MsPerDay"] = 86400000] = "MsPerDay";
    Constants[Constants["MsDigitsSlice"] = -3] = "MsDigitsSlice";
    Constants[Constants["HMSDigitsSlice"] = -2] = "HMSDigitsSlice";
})(Constants || (Constants = {}));
class CacheEntry {
    #value;
    created = Date.now();
    accessed = this.created;
    accesses = 0;
    constructor(value){
        this.#value = value;
    }
    get val() {
        this.accessed = Date.now();
        this.accesses++;
        return this.#value;
    }
}
class CachePolicy {
    static FIFO = new CachePolicy(([, a], [, b])=>a.created - b.created);
    static LIFO = new CachePolicy(([, a], [, b])=>b.created - a.created);
    static LRU = new CachePolicy(([, a], [, b])=>a.accessed - b.accessed);
    static MRU = new CachePolicy(([, a], [, b])=>b.accessed - a.accessed);
    static LFU = new CachePolicy(([, a], [, b])=>a.accesses - b.accesses);
    static RR = new CachePolicy(()=>Math.random() * 2 - 1);
    static FILO = CachePolicy.LIFO;
    static FirstInFirstOut = CachePolicy.FIFO;
    static FirstInLastOut = CachePolicy.LIFO;
    static LastInFirstOut = CachePolicy.LIFO;
    static LeastFrequentlyUsed = CachePolicy.LFU;
    static LeastRecentlyUsed = CachePolicy.LRU;
    static MostRecentlyUsed = CachePolicy.MRU;
    static Random = CachePolicy.RR;
    constructor(sort){
        this.sort = sort;
    }
    sort;
}
class CustomCache {
    [Symbol.toStringTag] = 'Cache';
    #cache = new Map();
    #maxSize = Infinity;
    #policy;
    constructor(maxSize = Infinity, policy){
        this.#maxSize = maxSize;
        this.#policy = policy;
    }
    get size() {
        return this.#cache.size;
    }
    clear() {
        this.#cache.clear();
    }
    delete(key) {
        return this.#cache.delete(key);
    }
    get(key) {
        return this.#cache.get(key)?.val;
    }
    has(key) {
        return this.#cache.has(key);
    }
    set(key, value) {
        this.#cache.set(key, new CacheEntry(value));
        this.trim();
        return this;
    }
    trim() {
        if (this.#cache.size >= this.#maxSize) {
            const entries = [
                ...this.#cache.entries()
            ].sort(this.#policy.sort).slice(0, -this.#maxSize);
            for (const [key] of entries){
                this.delete(key);
            }
        }
    }
    forEach(cb, thisArg = this) {
        for (const [key, entry] of this){
            cb.call(thisArg, entry, key, this);
        }
    }
    entries() {
        return this[Symbol.iterator]();
    }
    keys() {
        return this.#cache.keys();
    }
    *values() {
        for (const entry of this.#cache.values()){
            yield entry.val;
        }
    }
    *[Symbol.iterator]() {
        for (const [key, entry] of this.#cache){
            yield [
                key,
                entry.val
            ];
        }
    }
}
class ShaderCompilationError extends Error {
    static create(gl, name, type, shader, source) {
        const shaderType = this.#getShaderTypeString(gl, type);
        const errors = this.#createShaderCompilationErrorLog(gl.getShaderInfoLog(shader), source);
        gl.deleteShader(shader);
        return new ShaderCompilationError(`The following errors occurred while compiling the ${shaderType} shader for program "${name}:\n\n${errors}"`);
    }
    static #getShaderTypeString(gl, type) {
        switch(type){
            case gl.VERTEX_SHADER:
                return 'vertex';
            case gl.FRAGMENT_SHADER:
                return 'fragment';
            default:
                return 'unknown';
        }
    }
    static #createShaderCompilationErrorLog(info, source) {
        if (!info) {
            return '';
        }
        const lines = source.split('\n');
        const l = lines.length;
        return info.trim().split('\n').map((err)=>{
            const idx = err.indexOf('0:') + 2;
            const rawLineNo = err.substring(idx, err.indexOf(':', idx));
            const lineNumber = Number.parseInt(rawLineNo, 10) - 1;
            let start = lineNumber - 2;
            let end = start + 5;
            const rawSnippet = lines.slice(Math.max(start, 0), Math.min(end, l));
            while(rawSnippet[rawSnippet.length - 1].trim() === ''){
                rawSnippet.pop();
                start++;
            }
            while(rawSnippet[0].trim() === ''){
                rawSnippet.shift();
                end--;
            }
            const s = `${end}`.length;
            const padding = ' '.repeat(s);
            const snippet = rawSnippet.map((line, idx)=>{
                const no = `${start + idx + 1}`;
                return `${(padding + (no === rawLineNo ? '>>' : no)).slice(-s)} ${line}`;
            }).join('\n');
            return `${err}\n\n${snippet}\n`;
        }).join('\n');
    }
    constructor(msg){
        super(msg);
    }
}
class ShaderProgramCompilationError extends Error {
    constructor(gl, name, program){
        super(`Unable to initialize program "${name}": ${gl.getProgramInfoLog(program)}`);
        this.program = program;
        gl.deleteProgram(program);
    }
    program;
}
class GLError extends Error {
    constructor(msg, code){
        super(msg);
        this.code = code;
    }
    code;
}
class Buffer {
    #gl;
    #target;
    #buffer;
    #size = -1;
    constructor(gl, target = gl.ARRAY_BUFFER){
        this.#gl = gl;
        this.#target = target;
        this.#buffer = this.#gl.createBuffer();
    }
    bind() {
        this.#gl.bindBuffer(this.#target, this.#buffer);
        return this;
    }
    release() {
        this.#gl.deleteBuffer(this.#buffer);
    }
    update(data, usage = this.#gl.DYNAMIC_DRAW) {
        if (data instanceof Array) {
            this.#updateImpl(new Float32Array(data), usage);
        } else {
            this.#updateImpl(data, usage);
        }
        return this;
    }
    #updateImpl(data, usage) {
        this.bind();
        if (this.#size !== data.byteLength) {
            this.#gl.bufferData(this.#target, data, usage);
            this.#size = data.byteLength;
        } else {
            this.#gl.bufferSubData(this.#target, 0, data);
        }
    }
}
class Texture {
    static delete(gl, tex) {
        gl.deleteTexture(tex.texture);
    }
    static #getConstructorForType(gl1, type1) {
        switch(type1){
            case gl1.HALF_FLOAT:
            case gl1.FLOAT:
                return Float32Array;
            case gl1.INT:
                return Int32Array;
            case gl1.UNSIGNED_INT_2_10_10_10_REV:
            case gl1.UNSIGNED_INT_10F_11F_11F_REV:
            case gl1.UNSIGNED_INT_5_9_9_9_REV:
            case gl1.UNSIGNED_INT:
                return Uint32Array;
            case gl1.SHORT:
                return Int16Array;
            case gl1.UNSIGNED_SHORT_5_6_5:
            case gl1.UNSIGNED_SHORT_4_4_4_4:
            case gl1.UNSIGNED_SHORT_5_5_5_1:
                return Uint16Array;
            case gl1.BYTE:
                return Int8Array;
            case gl1.UNSIGNED_BYTE:
            default:
                return Uint8Array;
        }
    }
    static #ensureDataIsTypedArray(gl2, type2, data1) {
        if (data1 === null || ArrayBuffer.isView(data1)) {
            return data1;
        }
        return new (this.#getConstructorForType(gl2, type2))(data1);
    }
    #gl;
    #format;
    #wrap;
    #filter;
    #type;
    texture;
    #w = 0;
    #h = 0;
    constructor(gl, format = gl.RGBA, wrap = gl.CLAMP_TO_EDGE, filter = gl.LINEAR, type = gl.UNSIGNED_BYTE){
        this.#gl = gl;
        this.#format = format;
        this.#wrap = wrap;
        this.#filter = filter;
        this.#type = type;
        this.texture = this.#gl.createTexture();
        this.#init();
    }
    bind(unit = 0) {
        if (unit) {
            this.#gl.activeTexture(this.#gl.TEXTURE0 + unit);
        }
        this.#gl.bindTexture(this.#gl.TEXTURE_2D, this.texture);
        return this;
    }
    blank(width = this.#w, height = this.#h) {
        return this.setData(null, width, height);
    }
    copy(x = 0, y = 0, width = this.#w, height = this.#h) {
        this.#gl.copyTexImage2D(this.#gl.TEXTURE_2D, 0, this.#format, x, y, width, height, 0);
        return this;
    }
    resize(w, h, fb) {
        const old = this.texture;
        const ow = this.#w;
        const oh = this.#h;
        this.texture = this.#gl.createTexture();
        this.#init();
        this.blank(w, h);
        if (fb && ow && oh) {
            FrameBuffer.attach(fb, old);
            this.bind();
            const dw = ow - w;
            const dh = oh - h;
            const ox = Math.max(dw / 2, 0);
            const oy = Math.max(dh / 2, 0);
            this.#gl.copyTexSubImage2D(this.#gl.TEXTURE_2D, 0, 0, 0, ox, oy, w, h);
            fb.unbind();
        }
        this.#gl.deleteTexture(old);
        GL.checkForErrors(this.#gl);
        return this;
    }
    setData(data, width, height, format = this.#format, type = this.#type) {
        this.#w = width;
        this.#h = height;
        this.bind();
        this.#gl.texImage2D(this.#gl.TEXTURE_2D, 0, format, width, height, 0, format, type, Texture.#ensureDataIsTypedArray(this.#gl, type, data));
        return this;
    }
    setImage(image, format = this.#format, type = this.#type) {
        this.bind();
        this.#w = image.width;
        this.#h = image.height;
        this.#gl.texImage2D(this.#gl.TEXTURE_2D, 0, format, format, type, image);
        return this;
    }
    subsetData(data, xOffset, yOffset, width, height, format = this.#format, type = this.#type) {
        this.bind();
        this.#gl.texSubImage2D(this.#gl.TEXTURE_2D, 0, xOffset, yOffset, width, height, format, type, Texture.#ensureDataIsTypedArray(this.#gl, type, data));
        return this;
    }
    subsetImage(image, xOffset, yOffset, format = this.#format, type = this.#type) {
        this.bind();
        this.#gl.texSubImage2D(this.#gl.TEXTURE_2D, 0, xOffset, yOffset, format, type, image);
        return this;
    }
    #init() {
        this.#gl.bindTexture(this.#gl.TEXTURE_2D, this.texture);
        this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_WRAP_S, this.#wrap);
        this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_WRAP_T, this.#wrap);
        this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_MIN_FILTER, this.#filter);
        this.#gl.texParameteri(this.#gl.TEXTURE_2D, this.#gl.TEXTURE_MAG_FILTER, this.#filter);
    }
}
class FrameBuffer {
    static attach(fb, tex) {
        fb.attachTexture(tex);
    }
    static delete(fb) {
        if (fb.#frameBuffer) {
            fb.#gl.deleteFramebuffer(fb.#frameBuffer);
        }
        if (fb.#renderBuffer) {
            fb.#gl.deleteRenderbuffer(fb.#renderBuffer);
        }
    }
    #gl;
    #frameBuffer;
    #renderBuffer = null;
    constructor(gl, frameBuffer = gl.createFramebuffer()){
        this.#gl = gl;
        this.#frameBuffer = frameBuffer;
    }
    bind() {
        this.#gl.bindFramebuffer(this.#gl.FRAMEBUFFER, this.#frameBuffer);
        return this;
    }
    unbind() {
        this.#gl.bindFramebuffer(this.#gl.FRAMEBUFFER, null);
        return this;
    }
    attach(texture) {
        this.bind();
        this.#gl.framebufferTexture2D(this.#gl.FRAMEBUFFER, this.#gl.COLOR_ATTACHMENT0, this.#gl.TEXTURE_2D, texture.texture, 0);
        return this;
    }
    attachDepth(width, height) {
        this.bind();
        if (this.#renderBuffer === null) {
            this.#renderBuffer = this.#gl.createRenderbuffer();
            this.#gl.renderbufferStorage(this.#gl.RENDERBUFFER, this.#gl.DEPTH_COMPONENT16, width, height);
            this.#gl.framebufferRenderbuffer(this.#gl.FRAMEBUFFER, this.#gl.DEPTH_ATTACHMENT, this.#gl.RENDERBUFFER, this.#renderBuffer);
        }
        return this;
    }
    attachTexture(texture) {
        this.bind();
        this.#gl.framebufferTexture2D(this.#gl.FRAMEBUFFER, this.#gl.COLOR_ATTACHMENT0, this.#gl.TEXTURE_2D, texture, 0);
        return this;
    }
}
class Matrix {
    constructor(matrix){
        this.matrix = matrix;
    }
    matrix;
}
class Matrix2D extends Matrix {
    static #Identity = [
        1,
        0,
        0,
        0,
        1,
        0,
        0,
        0,
        1, 
    ];
    constructor(matrix = Matrix2D.#Identity){
        super(matrix);
    }
    scale(x = 1, y = 1) {
        return this.mult([
            x,
            0,
            0,
            0,
            y,
            0,
            0,
            0,
            1, 
        ]);
    }
    translate(x = 0, y = 0) {
        return this.mult([
            1,
            0,
            0,
            0,
            1,
            0,
            x,
            y,
            1, 
        ]);
    }
    rotate(a) {
        const sin = Math.sin(a);
        const cos = Math.cos(a);
        return this.mult([
            cos,
            -sin,
            0,
            sin,
            cos,
            0,
            0,
            0,
            1, 
        ]);
    }
    mult(matrix) {
        const [m00, m01, m02, m10, m11, m12, m20, m21, m22, ] = matrix;
        return new Matrix2D([
            ...this.multPoint([
                m00,
                m01,
                m02
            ]),
            ...this.multPoint([
                m10,
                m11,
                m12
            ]),
            ...this.multPoint([
                m20,
                m21,
                m22
            ]), 
        ]);
    }
    multPoint(point) {
        const [m00, m01, m02, m10, m11, m12, m20, m21, m22, ] = this.matrix;
        const [x, y, z] = point;
        return [
            x * m00 + y * m01 + z * m02,
            x * m10 + y * m11 + z * m12,
            x * m20 + y * m21 + z * m22, 
        ];
    }
}
class ShaderProgram {
    static compile(gl, name, vSource, fSource) {
        return new ShaderProgram(gl, name, this.compileProgramFromShaders(gl, name, dedent(vSource), dedent(fSource)));
    }
    static compileProgramFromShaders(gl, name, vSource, fSource) {
        const vs = ShaderProgram.#compileShaderFromSource(gl, name, vSource, gl.VERTEX_SHADER);
        const fs = ShaderProgram.#compileShaderFromSource(gl, name, fSource, gl.FRAGMENT_SHADER);
        const program = gl.createProgram();
        if (!program) {
            throw new Error('Unable to create program!');
        }
        gl.attachShader(program, vs);
        gl.attachShader(program, fs);
        gl.linkProgram(program);
        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            throw new ShaderProgramCompilationError(gl, name, program);
        }
        return program;
    }
    static #compileShaderFromSource(gl3, name, source1, type3) {
        const shader = gl3.createShader(type3);
        if (!shader) {
            throw new Error('Unable to create shader!');
        }
        gl3.shaderSource(shader, source1);
        gl3.compileShader(shader);
        if (!gl3.getShaderParameter(shader, gl3.COMPILE_STATUS)) {
            throw ShaderCompilationError.create(gl3, name, type3, shader, source1);
        }
        return shader;
    }
    ratio;
    uniformLocations;
    attributeLocations;
    constructor(gl, name, program){
        this.gl = gl;
        this.name = name;
        this.program = program;
        this.uniformLocations = new Map();
        this.attributeLocations = new Map();
        this.ratio = this.gl.getUniformLocation(this.program, 'ratio');
    }
    use() {
        this.gl.useProgram(this.program);
        return this;
    }
    uniform(name, value, __int = false) {
        const location1 = this.#getUniformLocation(name);
        if (value !== undefined) {
            if (Array.isArray(value) || ArrayBuffer.isView(value)) {
                const method = `uniform${value.length}${__int ? 'i' : 'f'}v`;
                this.gl[method](location1, value);
            } else if (typeof value === 'number') {
                this.gl[__int ? 'uniform1i' : 'uniform1f'](location1, value);
            } else {
                throw new Error(`Invalid uniform value in program "${this.name}" for "${name}": ${value}`);
            }
        }
        return this;
    }
    uniformSpread(name, value, __int = false) {
        const location1 = this.#getUniformLocation(name);
        if (value !== undefined) {
            const method = `uniform${value.length}${__int ? 'i' : 'f'}`;
            this.gl[method](location1, ...value);
        }
        return this;
    }
    uniformInt(name, value) {
        return this.uniform(name, value, true);
    }
    uniformSpreadInt(name, value) {
        return this.uniformSpread(name, value, true);
    }
    matrix(name, matrix, transpose = false) {
        const location1 = this.#getUniformLocation(name);
        const actual = matrix instanceof Matrix ? matrix.matrix : matrix;
        const method = `uniformMatrix${Math.sqrt(actual.length)}fv`;
        this.gl[method](location1, !!transpose, actual);
        return this;
    }
    attribute(name, value, size = 0, stride = 0, offset = 0) {
        const location1 = this.#getAttributeLocation(name);
        if (value !== undefined) {
            value.bind();
            this.gl.enableVertexAttribArray(location1);
            this.gl.vertexAttribPointer(location1, size, this.gl.FLOAT, false, stride, offset);
        }
        return this;
    }
    draw(mode, count, type) {
        if (!type) {
            this.gl.drawArrays(mode, 0, count);
        } else {
            this.gl.drawElements(mode, count, type, 0);
        }
        GL.checkForErrors(this.gl);
        return this;
    }
    disable() {
        for (const location1 of this.attributeLocations.values()){
            this.gl.disableVertexAttribArray(location1);
        }
        return this;
    }
    #getUniformLocation(name1) {
        if (!this.uniformLocations.has(name1)) {
            const location1 = this.gl.getUniformLocation(this.program, name1);
            if (location1 === null) {
                console.warn(`Unknown uniform location in program ${this.name}: "${name1}". This may cause unexpected behavior.`);
            }
            this.uniformLocations.set(name1, location1);
        }
        return this.uniformLocations.get(name1) ?? null;
    }
    #getAttributeLocation(name2) {
        if (!this.attributeLocations.has(name2)) {
            const location2 = this.gl.getAttribLocation(this.program, name2);
            if (location2 === -1) {
                console.warn(`Unknown attribute location in program ${this.name}: "${name2}". This may cause unexpected behavior.`);
            }
            this.attributeLocations.set(name2, location2);
        }
        return this.attributeLocations.get(name2) ?? -1;
    }
    gl;
    name;
    program;
}
class ShaderProgram2D extends ShaderProgram {
    static vSource = dedent`
		#version 300 es
		precision mediump float;

		// Inputs
		in      vec2 quad;

		// Uniform Inputs
		uniform mat3 u_matrix;

		void main() {
			gl_Position = vec4((u_matrix * vec3(quad, 1.0)).xy, 0.0, 1.0);
		}
	`;
    static #QUAD_BASE = new Float32Array([
        -1,
        -1,
        1,
        -1,
        -1,
        1,
        1,
        1
    ]);
    static compile(gl, name, fSource) {
        return new ShaderProgram2D(gl, name, this.compileProgramFromShaders(gl, name, this.vSource, dedent(fSource)));
    }
    #QUAD = new Buffer(this.gl).update(ShaderProgram2D.#QUAD_BASE);
    use() {
        return super.use().matrix('u_matrix', new Matrix2D()).attribute('quad', this.#QUAD, 2);
    }
    draw(type) {
        return super.draw(this.gl.TRIANGLE_STRIP, 4, type);
    }
}
class TextAtlas {
    static Atlases = new Map();
    static #canvas = d.createElement('canvas');
    static #ctx = TextAtlas.#canvas.getContext('2d');
    static #DEFAULT_CHARSET = '!"#$%&\'()*+,-./0123456789:;<=>?@[\\]^_`abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ{|}~';
    static register(gl, font, size, charset = TextAtlas.#DEFAULT_CHARSET) {
        const key = [
            font,
            size
        ].join(':');
        let atlas = this.Atlases.get(key);
        if (!atlas) {
            atlas = new TextAtlas(gl, font, size, charset);
            this.Atlases.set(key, atlas);
        }
        if (atlas.#charset !== charset) {
            atlas.#mergeCharsets(charset);
        }
        return atlas;
    }
    #gl;
    #font;
    #size;
    #tex;
    #cache = new CustomCache(500, CachePolicy.LRU);
    #charset;
    #l = 0;
    #rects;
    #widths;
    #spaceWidth = 0;
    #initialized = false;
    constructor(gl, font, size, charset = TextAtlas.#DEFAULT_CHARSET){
        this.#gl = gl;
        this.#font = font;
        this.#size = size;
        this.#charset = charset;
        this.#tex = new Texture(this.#gl, this.#gl.RGBA, this.#gl.REPEAT, this.#gl.NEAREST);
    }
    measure(str) {
        if (!this.#initialized) {
            this.#init();
        }
        const l = str.length;
        let res = 0;
        let x = 0;
        for(let i = 0; i < l; i++){
            const c = str.charAt(i);
            if (c === '\n') {
                x = 0;
            } else if (c === ' ') {
                x += this.#spaceWidth;
            } else {
                x += this.#widths[this.#getCharacterIndex(c, str)];
            }
            res = Math.max(res, x);
        }
        return res;
    }
    write(str, buf, cache = !buf) {
        if (!this.#initialized) {
            this.#init();
        }
        this.#tex.bind(0);
        if (cache && this.#cache.has(str)) {
            return this.#cache.get(str);
        }
        const spaces = this.#countSpaces(str);
        const l = str.length;
        const cw = l - spaces;
        let x = 0;
        let y = 0;
        let idx = 0;
        const res = buf ?? new Float32Array(2 + cw * 24);
        const v = res.subarray(2);
        for(let i = 0; i < l; i++){
            const c = str.charAt(i);
            if (c === '\n') {
                x = 0;
                y += this.#size;
            } else if (c === ' ') {
                x += this.#spaceWidth;
            } else {
                x = this.#setCharacter(idx++, v, x, y, this.#getCharacterIndex(c, str));
            }
            res[0] = Math.max(x, res[0]);
            res[1] = y;
        }
        res[1] += this.#size;
        if (cache) {
            this.#cache.set(str, res);
        }
        return res;
    }
    #getCharacterIndex(c, str) {
        const i = this.#charset.indexOf(c);
        if (i === -1 || i >= this.#l) {
            throw new Error(`Character "${c}" is not in the character set for this atlas: "${this.#charset}"\n  Given: "${str}`);
        }
        return i;
    }
    #countSpaces(str1) {
        let spaces = 0;
        const l1 = str1.length;
        for(let i1 = 0; i1 < l1; i1++){
            if (str1.charAt(i1) === ' ') {
                spaces++;
            }
        }
        return spaces;
    }
    #setCharacter(i2, res, x, y, cIdx) {
        const width = this.#widths[cIdx];
        const rIdx = cIdx * 4;
        const idx = i2 * 24;
        const rect = this.#rects.slice(rIdx, rIdx + 4);
        const left = x;
        const right = left + width;
        const top = y;
        const bottom = this.#size;
        const pos = [
            left,
            top,
            right,
            bottom
        ];
        const l2 = 0;
        const t = 1;
        const r = 2;
        const b = 3;
        let offs = 0;
        res[idx + offs++] = pos[l2];
        res[idx + offs++] = pos[t];
        res[idx + offs++] = rect[l2];
        res[idx + offs++] = rect[t];
        res[idx + offs++] = pos[r];
        res[idx + offs++] = pos[t];
        res[idx + offs++] = rect[r];
        res[idx + offs++] = rect[t];
        res[idx + offs++] = pos[l2];
        res[idx + offs++] = pos[b];
        res[idx + offs++] = rect[l2];
        res[idx + offs++] = rect[b];
        res[idx + offs++] = pos[l2];
        res[idx + offs++] = pos[b];
        res[idx + offs++] = rect[l2];
        res[idx + offs++] = rect[b];
        res[idx + offs++] = pos[r];
        res[idx + offs++] = pos[t];
        res[idx + offs++] = rect[r];
        res[idx + offs++] = rect[t];
        res[idx + offs++] = pos[r];
        res[idx + offs++] = pos[b];
        res[idx + offs++] = rect[r];
        res[idx + offs++] = rect[b];
        return right;
    }
    #mergeCharsets(charset) {
        this.#charset = [
            ...new Set([
                ...this.#charset,
                ...charset
            ])
        ].filter((c)=>c !== ' ').join('');
    }
    #init() {
        const size = this.#size;
        const chars = this.#charset;
        this.#l = this.#charset.length;
        this.#rects = new Float32Array(this.#l * 4);
        this.#widths = new Float32Array(this.#l);
        const w = size * this.#l;
        const h = size;
        const canvas = TextAtlas.#canvas;
        const ctx = TextAtlas.#ctx;
        canvas.width = w;
        canvas.height = h;
        ctx.clearRect(0, 0, w, h);
        ctx.font = `${size}px ${this.#font}`;
        ctx.textBaseline = 'top';
        ctx.fillStyle = 'rgb(255,255,255)';
        this.#spaceWidth = ctx.measureText(' ').width;
        const positions = new Float32Array(this.#l * 4);
        const l3 = 0;
        const t1 = 1;
        const r1 = 2;
        const b1 = 3;
        let right1 = 0;
        for(let i3 = 0; i3 < this.#l; i3++){
            const posIdx = i3 * 4;
            const c1 = chars.charAt(i3);
            const left1 = right1;
            right1 = left1 + ctx.measureText(c1).width;
            positions[posIdx + l3] = left1;
            positions[posIdx + t1] = size;
            positions[posIdx + r1] = right1;
            positions[posIdx + b1] = 0;
            this.#widths[i3] = right1 - left1;
            ctx.fillText(c1, left1, 0, size);
        }
        this.#tex.setImage(ctx.getImageData(0, 0, right1, size));
        for(let i4 = 0; i4 < this.#l; i4++){
            const idx1 = i4 * 4;
            this.#rects[idx1 + l3] = positions[idx1 + l3] / right1;
            this.#rects[idx1 + t1] = positions[idx1 + t1] / size;
            this.#rects[idx1 + r1] = positions[idx1 + r1] / right1;
            this.#rects[idx1 + b1] = positions[idx1 + b1] / size;
        }
        this.#initialized = true;
        return this;
    }
}
class TextShader {
    static #vSource = dedent`
		#version 300 es
		precision mediump float;

		// Inputs
		in      vec2 a_position;
		in      vec2 a_texcoord;

		// Uniform Inputs
		uniform mat3 u_matrix;

		// Outputs
		out     vec2 v_texcoord;

		void main() {
			gl_Position = vec4((u_matrix * vec3(a_position, 1.0)).xy, 0.0, 1.0);

			v_texcoord = a_texcoord;
		}
	`;
    static #fSource = dedent`
		#version 300 es
		precision mediump float;

		// Inputs
		in      vec2      v_texcoord;

		// Uniform Inputs
		uniform sampler2D u_texture;
		uniform float     u_opacity;
		uniform vec3      u_color;

		// Outputs
		out     vec4      FragColor;

		void main() {
			vec4 tex = texture(u_texture, v_texcoord);
			FragColor = vec4(tex.xyz * u_color, tex.a * u_opacity);
		}
	`;
    static #shaderprogram;
    static #getProgram(gl4) {
        if (!this.#shaderprogram) {
            this.#shaderprogram = ShaderProgram.compile(gl4, 'TextShader', this.#vSource, this.#fSource);
        }
        return this.#shaderprogram;
    }
    #gl;
    #color;
    #opacity;
    #tab;
    #program;
    constructor(gl, color, opacity, tab = '  '){
        this.#gl = gl;
        this.#color = color;
        this.#opacity = opacity;
        this.#tab = tab;
        this.#program = TextShader.#getProgram(gl);
    }
    draw(text, atlas, buf) {
        const r = atlas.write(text.replace(/\t/g, this.#tab), buf);
        const [w, h] = r.subarray(0, 2);
        const vArr = r.subarray(2);
        const b = new Buffer(this.#gl).update(vArr);
        const half = -0.5;
        const dx = Math.round(half * w);
        const dy = Math.round(half * h);
        const sx = 2 / w;
        const sy = 2 / h;
        const mat = new Matrix2D([
            sx,
            0,
            0,
            0,
            sy,
            0,
            dx * sx,
            dy * sy,
            1, 
        ]);
        this.#program.use().attribute('a_position', b, 2, 16, 0).attribute('a_texcoord', b, 2, 16, 8).matrix('u_matrix', mat).uniform('u_color', this.#color).uniform('u_opacity', this.#opacity).uniformInt('u_texture', 0).draw(this.#gl.TRIANGLES, vArr.length / 4);
        return this;
    }
}
class BackgroundShader {
    static #fSource = dedent`
		#version 300 es
		precision mediump float;

		// Uniform Inputs
		uniform vec3  u_color;
		uniform float u_opacity;

		// Outputs
		out     vec4  FragColor;

		void main() {
			FragColor = vec4(u_color, u_opacity);
		}
	`;
    static #shaderprogram;
    static #getProgram(gl5) {
        if (!this.#shaderprogram) {
            this.#shaderprogram = ShaderProgram2D.compile(gl5, 'BackgroundShader', this.#fSource);
        }
        return this.#shaderprogram;
    }
    #color;
    #program;
    constructor(gl, color = [
        0,
        0,
        0
    ]){
        this.#color = color;
        this.#program = BackgroundShader.#getProgram(gl);
    }
    draw(opacity) {
        this.#program.use().uniformSpread('u_color', this.#color).uniform('u_opacity', opacity).draw();
    }
}
class GL {
    static checkForErrors(gl) {
        const err = gl.getError();
        if (err !== gl.NO_ERROR) {
            throw new GLError(this.#getMessageForErrorCode(gl, err), err);
        }
    }
    static create(name) {
        const canvas = d.createElement('canvas');
        canvas.id = name;
        d.body.appendChild(canvas);
        return new GL(canvas);
    }
    static #getMessageForErrorCode(gl6, code) {
        switch(code){
            case gl6.INVALID_ENUM:
                return 'An unacceptable value was specified for an enumerated argument.';
            case gl6.INVALID_VALUE:
                return 'A numeric argument was out of range.';
            case gl6.INVALID_OPERATION:
                return 'The specified command was not allowed for the current state.';
            case gl6.INVALID_FRAMEBUFFER_OPERATION:
                return 'The currently bound framebuffer is not framebuffer complete when trying to render or to read from it.';
            case gl6.OUT_OF_MEMORY:
                return 'Not enough memory is left to execute the command.';
            case gl6.CONTEXT_LOST_WEBGL:
                return 'WebGL context lost';
            default:
                return `Unknown code ${code}.`;
        }
    }
    gl;
    DEFAULT_FRAMEBUFFER;
    constructor(canvas){
        this.canvas = canvas;
        this.gl = this.canvas.getContext('webgl2', {
            powerPreference: 'high-performance'
        });
        this.DEFAULT_FRAMEBUFFER = new FrameBuffer(this.gl, null);
    }
    canvas;
}
function* RNG(Ctor) {
    const arr = new Ctor(65536 / Ctor.BYTES_PER_ELEMENT);
    const l = arr.length;
    let idx = l;
    while(true){
        if (idx >= l) {
            crypto.getRandomValues(arr);
            idx = 0;
        }
        yield arr[idx++];
    }
}
const RandomUint8 = RNG(Uint8Array);
class CircularArray extends Array {
    #idx = 0;
    push(...elements) {
        const max = this.length - 1;
        for (const e of elements){
            this[this.#idx] = e;
            if (++this.#idx > max) {
                this.#idx = 0;
            }
        }
        return elements.length;
    }
    unshift(...elements) {
        const max = this.length - 1;
        for (const e of elements){
            this[this.#idx] = e;
            if (--this.#idx < 0) {
                this.#idx = max;
            }
        }
        return elements.length;
    }
    resize(l, v) {
        const ol = this.length;
        this.length = l;
        if (l > ol) {
            for(let i = ol; i < l; i++){
                this[i] = v;
            }
        }
    }
    reset(v) {
        this.fill(v);
        this.#idx = 0;
    }
}
class FPS {
    static maxFramerate = 60;
    framerate = 0;
    delay = Infinity;
    #hist = new CircularArray(0).fill(0);
    #size = 12;
    #opacity = 0.75;
    #gl;
    #bg;
    #font;
    #text;
    #hPadding = this.#size / 3;
    #vPadding = this.#size / 3;
    #lastFrame = 0;
    constructor(gl){
        this.#gl = gl;
        this.#bg = new BackgroundShader(gl);
        this.#font = TextAtlas.register(gl, 'monospace', this.#size, '.1234567890FPS');
        this.#text = new TextShader(gl, [
            0,
            1,
            0
        ], this.#opacity);
        this.setFramerate(Framerate.get() || FPS.maxFramerate);
    }
    get last() {
        return this.#lastFrame;
    }
    clear() {
        this.#hist.reset(Math.min(this.framerate, FPS.maxFramerate));
    }
    setFramerate(framerate) {
        this.#hist.resize(clamp(framerate, 1, FPS.maxFramerate), Math.min(framerate, FPS.maxFramerate));
        this.framerate = framerate;
        this.delay = Math.floor(1000 / framerate);
        Framerate.set(framerate);
    }
    measure(time) {
        return 1000 / (time - this.#lastFrame);
    }
    draw(time, measured = this.measure(time)) {
        const hist = this.#hist;
        const hPadding = this.#hPadding;
        const vPadding = this.#vPadding;
        const size = this.#size;
        this.#lastFrame = time;
        hist.push(measured);
        const framerate = hist.reduce((acc, h)=>acc + h, 0) / hist.length;
        const text = `${framerate.toFixed(2)} FPS`;
        const tw = this.#font.measure(text);
        const th = size;
        const bgl = hPadding * 2;
        const bgb = this.#gl.drawingBufferHeight - th - vPadding * 3;
        const bgw = tw + hPadding * 2;
        const bgh = th + vPadding * 2;
        this.#gl.viewport(bgl, bgb, bgw, bgh);
        this.#bg.draw(this.#opacity);
        const tl = bgl + hPadding;
        const tb = bgb + hPadding;
        this.#gl.viewport(tl, tb, tw, th);
        this.#text.draw(text, this.#font);
    }
}
class Metadata {
    #size = 12;
    #opacity = 0.75;
    #gl;
    #bg;
    #font;
    #text;
    #hPadding = this.#size / 3;
    #vPadding = this.#size / 3;
    #buf = new Float32Array(2 + (15 + 10 + 17) * 24);
    #frame = 0;
    #start = Date.now();
    constructor(gl){
        this.#gl = gl;
        this.#bg = new BackgroundShader(gl);
        this.#font = TextAtlas.register(gl, 'monospace', this.#size, '1234567890EFadelmprs:.|');
        this.#text = new TextShader(gl, [
            0,
            1,
            0
        ], this.#opacity);
    }
    reset() {
        this.#frame = 0;
        this.#start = Date.now();
        const l = this.#buf.length;
        for(let i = 0; i < l; i++){
            this.#buf[i] = 0;
        }
    }
    draw() {
        const frame = this.#frame;
        const hPadding = this.#hPadding;
        const vPadding = this.#vPadding;
        const font = this.#font;
        const size = this.#size;
        const text = `Frame: ${frame} | Elapsed: ${this.#formatElapsedTime()}`;
        const tw = font.measure(text);
        const th = size;
        const bgl = hPadding * 2;
        const bgb = vPadding;
        const bgw = tw + hPadding * 2;
        const bgh = th + vPadding * 2;
        this.#gl.viewport(bgl, bgb, bgw, bgh);
        this.#bg.draw(this.#opacity);
        const tl = bgl + hPadding;
        const tb = bgb + hPadding;
        this.#gl.viewport(tl, tb, tw, th);
        this.#text.draw(text, this.#font, this.#buf);
        this.#frame++;
    }
    #formatElapsedTime() {
        const elapsed = Date.now() - this.#start;
        const ms = `00${elapsed % 1000}`.slice(-3);
        const s = `0${Math.floor(elapsed / 1000) % 60}`.slice(-2);
        const m = `0${Math.floor(elapsed / 60000) % 60}`.slice(-2);
        const h1 = `0${Math.floor(elapsed / 3600000) % 24}`.slice(-2);
        const d1 = Math.floor(elapsed / 86400000);
        const res1 = [
            h1,
            m,
            s
        ].join(':').concat('.', ms);
        if (d1) {
            return [
                d1,
                res1
            ].join(' ');
        }
        return res1;
    }
}
var Orientation;
(function(Orientation) {
    Orientation[Orientation["Portrait"] = 0] = "Portrait";
    Orientation[Orientation["Landscape"] = 1] = "Landscape";
})(Orientation || (Orientation = {}));
class Board {
    static BLANK = new Board('BLANK', 0, 0, 2);
    static prebuilt = new Map();
    static #prebuiltCache = new Map();
    static async load(file) {
        const contents = (await (await fetch(file)).text()).trim();
        const lines = contents.split('\n');
        for(let i = 0; i < lines.length;){
            const name = lines[i++];
            const [pattern, scale] = lines[i++].split(' ');
            this.#create(name, pattern, scale ? Number.parseInt(scale, 10) : undefined);
        }
    }
    static #create(name3, file, scale = 2) {
        const kebabCase = name3.toLowerCase().replace(/\s+/g, '-');
        if (file.startsWith('./') || file.startsWith('../')) {
            this.prebuilt.set(kebabCase, Board.#defer(Board.#parse, name3, file, kebabCase, scale));
        } else {
            this.prebuilt.set(kebabCase, Board.#defer(Board.#from, name3, file, kebabCase, scale));
        }
        const btn = d.createElement('button');
        btn.setAttribute('data-pattern', kebabCase);
        btn.appendChild(d.createTextNode(name3));
        if (location.hash.substring(1) === kebabCase) {
            btn.classList.add('active');
        }
        btnContainer.appendChild(btn);
    }
    static #defer(func, boardName, file1, name4, scale1) {
        return async ()=>await func(this, boardName, file1, name4, scale1);
    }
    static async #parse(self1, boardName1, file2, name5, scale2) {
        if (self1.#prebuiltCache.has(name5)) {
            return self1.#prebuiltCache.get(name5);
        }
        loadingModal.show();
        try {
            const resp = await fetch(file2);
            if (!resp.ok) {
                return Board.BLANK;
            }
            const txt = (await resp.text()).trim();
            if (file2.endsWith('.lif')) {
                return Board.#from(self1, boardName1, txt, name5, scale2);
            } else if (file2.endsWith('.rle')) {
                return Board.#from(self1, boardName1, Board.#expandRLE(txt), name5, scale2);
            }
        } finally{
            loadingModal.reset();
        }
        return Board.BLANK;
    }
    static #expandRLE(rle) {
        parsingModal.show();
        let res2 = '';
        const lines1 = rle.split('\n');
        let x1 = 0;
        let y1 = 0;
        for (const line of lines1){
            if (line.startsWith('#')) {
                continue;
            } else if (line.startsWith('x')) {
                const [, _x, _y] = line.match(/x ?= ?(\d+), ?y ?= ?(\d+)/) ?? [];
                x1 = Number.parseInt(_x, 10);
                y1 = Number.parseInt(_y, 10);
            } else if (x1 && y1) {
                let idx2 = 0;
                let count = 1;
                while(idx2 < line.length){
                    let c2 = line.charAt(idx2++);
                    if (isNumeric(c2)) {
                        let n = c2;
                        while(idx2 < line.length && isNumeric(c2 = line.charAt(idx2))){
                            n += c2;
                            idx2++;
                        }
                        count = Number.parseInt(n, 10);
                    } else if (c2 === 'b') {
                        res2 += '.'.repeat(count);
                        count = 1;
                    } else if (c2 === 'o') {
                        res2 += '*'.repeat(count);
                        count = 1;
                    } else if (c2 === '$') {
                        res2 += '\n'.repeat(count);
                        count = 1;
                    } else if (c2 === '!') {
                        break;
                    }
                }
                if (line.endsWith('!')) {
                    break;
                }
            }
        }
        parsingModal.reset();
        return res2;
    }
    static #from(self2, boardName2, str2, name6, scale3) {
        if (self2.#prebuiltCache.has(name6)) {
            return self2.#prebuiltCache.get(name6);
        }
        parsingModal.show();
        const lines2 = str2.replace(/[#!].*\n/g, '').replace(/\\n/g, '\n').split('\n');
        const h2 = lines2.length;
        const w1 = Math.max(...lines2.map((l)=>l.length));
        const board = new Board(boardName2, w1, h2, scale3);
        for(let y2 = 0; y2 < h2; y2++){
            const line1 = lines2[y2];
            const l4 = line1.length;
            const base = y2 * w1;
            for(let x2 = 0; x2 < w1; x2++){
                if (x2 < l4 && line1.charAt(x2) !== '.') {
                    board.board[base + x2] = 1;
                }
            }
        }
        self2.#prebuiltCache.set(name6, board);
        parsingModal.reset();
        return board;
    }
    orientation;
    board;
    constructor(name, w, h, scale = 2){
        this.name = name;
        this.w = w;
        this.h = h;
        this.scale = scale;
        this.orientation = this.w > this.h ? 1 : 0;
        this.board = new Uint8Array(this.w * this.h);
    }
    rotate() {
        const { w , h  } = this;
        const res = new Board(this.name, h, w, this.scale);
        for(let y = 0; y < h; y++){
            const base = y * w;
            for(let x = 0; x < w; x++){
                res.board[x * h + y] = this.board[base + x];
            }
        }
        return res;
    }
    name;
    w;
    h;
    scale;
}
class GOL {
    static #BASE_HASH = 0x4E67C6A7;
    static #HISTORY = new CircularArray(1024);
    static #HASH_TRACKER = new Uint32Array([
        this.#BASE_HASH,
        0
    ]);
    static #shadefSource = `
		#version 300 es
		precision mediump float;

		// Constants
		const   vec4      hue = vec4(0.0, 0.5, 1.0, 1.0);

		// Uniform Inputs
		uniform sampler2D state;
		uniform vec2      scale;

		// Outputs
		out     vec4      FragColor;

		void main() {
			FragColor = hue * texture(state, gl_FragCoord.xy / scale);
		}
	`;
    static #golfSource = `
		#version 300 es
		precision mediump float;

		// Constants
		const   float     HUE_SHIFT_FRAMES      = 720.0;
		const   float     FAST_HUE_SHIFT_FRAMES = 36.0;
		const   float     HUE_SHIFT             = 1.0 / HUE_SHIFT_FRAMES;
		const   float     FAST_HUE_SHIFT        = 1.0 / FAST_HUE_SHIFT_FRAMES;
		const   float     ALIVE                 = 1.0;
		const   vec4      INIT_HUE              = vec4(0.0, 0.5, 1.0, 1.0);
		const   vec4      BLACK                 = vec4(0.0, 0.0, 0.0, 0.0);
		const   vec4      WHITE                 = vec4(1.0, 1.0, 1.0, 1.0);
		const   vec4      RED                   = vec4(1.0, 0.0, 0.0, 1.0);

		// Uniform Inputs
		uniform sampler2D state;
		uniform vec2      scale;
		uniform bool      rave;

		// Outputs
		out     vec4      FragColor;

		int get(vec2 offset) {
			return int(texture(state, (gl_FragCoord.xy + offset) / scale).a == ALIVE);
		}

		vec4 CalculateNewAge(vec4 age) {
			int neighbors =
				get(vec2(-1.0, -1.0)) +    get(vec2(-1.0,  0.0)) +    get(vec2(-1.0,  1.0)) +
				get(vec2( 0.0, -1.0)) + /* get(vec2( 0.0,  0.0)) + */ get(vec2( 0.0,  1.0)) +
				get(vec2( 1.0, -1.0)) +    get(vec2( 1.0,  0.0)) +    get(vec2( 1.0,  1.0));

			bool alive = age.a == ALIVE;

			if (neighbors == 3) {
				return alive ? age : WHITE;
			} else if (neighbors == 2 && alive) {
				return age;
			}

			return BLACK;
		}

		// source: http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
		vec3 rgb2hsv(vec3 c) {
			const float e = 1.0e-10;

			vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
			vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

			float d = q.x - min(q.w, q.y);

			return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		vec3 hsv2rgb(vec3 c) {
			vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);

			return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
		}

		vec4 CalculateNextColor(vec4 color) {
			if (color == BLACK || color == RED) {
				return color;
			}
			if (color == WHITE) {
				return INIT_HUE;
			}

			vec3 hsv = rgb2hsv(color.rgb);

			if (hsv.r < HUE_SHIFT) {
				return RED;
			}

			return vec4(hsv2rgb(vec3(hsv.r - HUE_SHIFT, hsv.gb)), color.a);
		}

		vec4 Rave(vec4 color) {
			if (color == BLACK) {
				return BLACK;
			}
			if (color == WHITE) {
				return INIT_HUE;
			}

			vec3 hsv = rgb2hsv(color.rgb);

			return vec4(hsv2rgb(vec3(hsv.r - FAST_HUE_SHIFT, hsv.gb)), color.a);
		}

		void main(void) {
			vec4 color = texture(state, gl_FragCoord.xy / scale);

			vec4 age = CalculateNewAge(color);

			FragColor = rave ? Rave(age) : CalculateNextColor(age);
		}
	`;
    static #condensefSource = `
		#version 300 es
		precision mediump float;

		// Constants
		const   float     FIRST_PX  = 1.0 / 2.0;
		const   float     SECOND_PX = FIRST_PX / 2.0;
		const   float     THIRD_PX  = SECOND_PX / 2.0;
		const   float     FOURTH_PX = THIRD_PX / 2.0;

		// Uniform Inputs
		uniform sampler2D state;
		uniform vec2      scale;

		// Outputs
		out     vec4      FragColor;

		float get(vec2 offset) {
			return texture(state, (gl_FragCoord.xy + offset) / scale).a;
		}

		float amalgam(vec2 offset) {
			return (
				(get(offset + vec2(-0.25, -0.25)) * FIRST_PX) +
				(get(offset + vec2(-0.25,  0.0 )) * SECOND_PX) +
				(get(offset + vec2( 0.0 ,  0.0 )) * THIRD_PX) +
				(get(offset + vec2( 0.0 , -0.25)) * FOURTH_PX)
			);
		}

		void main() {
			FragColor = vec4(
				amalgam(vec2(-0.25, -0.25)),
				amalgam(vec2(-0.25,  0.25)),
				amalgam(vec2( 0.25,  0.25)),
				amalgam(vec2( 0.25, -0.25))
			);
		}
	`;
    static #copyfSource = `
		#version 300 es
		precision mediump float;

		// Uniform Inputs
		uniform sampler2D state;
		uniform vec2      scale;

		// Outputs
		out     vec4      FragColor;

		void main() {
			FragColor = texture(state, gl_FragCoord.xy / scale);
		}
	`;
    static resetHistory() {
        this.#HISTORY.reset(null);
    }
    static #detectBoardStagnation(curr) {
        let matches = 0;
        for (const hash of this.#HISTORY){
            if (hash === null) break;
            if (hash === curr) {
                if (++matches === 5) return true;
            }
        }
        this.#HISTORY.push(curr);
        return false;
    }
    static #hashBoard(actualBoard) {
        const hash1 = this.#HASH_TRACKER;
        hash1[0] = this.#BASE_HASH;
        hash1[1] = 0;
        const board1 = new Uint32Array(actualBoard.buffer);
        const l5 = board1.length;
        for(let i5 = 0; i5 < l5; i5++){
            const v = board1[i5];
            hash1[1] |= v;
            hash1[0] ^= (hash1[0] << 5) + v + (hash1[0] >> 2);
        }
        return hash1[1] ? hash1[0] || 1 : 0;
    }
    orientation;
    w = 0;
    h = 0;
    advance = this.#advance.bind(this);
    resize = this.#resize.bind(this);
    #GL;
    #canvas;
    #gl;
    #fps;
    #metadata;
    #shade;
    #gol;
    #condense;
    #copy;
    #frameBuffer;
    #t1;
    #t2;
    #t3;
    #vw = 0;
    #vh = 0;
    #cw = 0;
    #ch = 0;
    #viewportSize;
    #stateSize;
    #condenseSize;
    #condenseBuffer;
    #board = Board.BLANK;
    #last;
    #started = false;
    #dead = false;
    #playing = false;
    #iterate = this.#iterateImpl.bind(this);
    #renderFirstFrame = this.#renderFirstFrameImpl.bind(this);
    constructor(GL){
        this.#GL = GL;
        const gl = GL.gl;
        this.#canvas = GL.canvas;
        this.#gl = gl;
        this.#fps = new FPS(gl);
        this.#metadata = new Metadata(gl);
        this.#shade = ShaderProgram2D.compile(gl, 'shade', GOL.#shadefSource);
        this.#gol = ShaderProgram2D.compile(gl, 'gol', GOL.#golfSource);
        this.#condense = ShaderProgram2D.compile(gl, 'condense', GOL.#condensefSource);
        this.#copy = ShaderProgram2D.compile(gl, 'copy', GOL.#copyfSource);
        this.#frameBuffer = new FrameBuffer(gl);
        this.#t1 = new Texture(gl, gl.RGBA, gl.REPEAT, gl.NEAREST);
        this.#t2 = new Texture(gl, gl.RGBA, gl.REPEAT, gl.NEAREST);
        this.#t3 = new Texture(gl, gl.RGBA, gl.REPEAT, gl.NEAREST);
        this.#resize();
        this.#init();
    }
    get alive() {
        return !this.#dead;
    }
    get autorestart() {
        return RestartOnStagnation.get();
    }
    set autorestart(autorestart) {
        RestartOnStagnation.set(autorestart);
    }
    get raving() {
        return !!RaveMode.get();
    }
    set raving(rave) {
        RaveMode.set(rave ? 1 : 0);
    }
    get paused() {
        return !this.#playing;
    }
    set paused(_pause) {
        this.#playing ? this.stop() : this.start();
    }
    get targetFramerate() {
        return this.#fps.framerate;
    }
    set targetFramerate(target) {
        this.#fps.setFramerate(target);
    }
    start(force = false) {
        if (this.#dead && !force) {
            return;
        }
        this.#dead = false;
        this.#playing = true;
        this.#last = self.requestAnimationFrame(this.#started ? this.#iterate : this.#renderFirstFrame);
    }
    stop() {
        this.#playing = false;
        if (this.#last) {
            self.cancelAnimationFrame(this.#last);
            this.#last = undefined;
        }
    }
    setBoard(board) {
        this.#fps.clear();
        this.#metadata.reset();
        this.#started = false;
        this.#board = board;
        this.#updateScale();
        if (board.w > this.#vw || board.h > this.#vh) {
            throw new Error('The selected pattern is too large to show on your current screen.');
        }
        if (board === Board.BLANK) {
            this.randomize();
            return;
        }
        this.#t1.blank(this.#vw, this.#vh);
        const { w , h  } = board;
        const bhx = Math.floor(w / 2);
        const bhy = Math.floor(h / 2);
        const ghx = Math.floor(this.#vw / 2);
        const ghy = Math.floor(this.#vh / 2);
        const x = ghx - bhx;
        const y = ghy - bhy;
        const l = w * h;
        const rgba = new Uint32Array(l);
        for(let i = 0; i < l; i++){
            rgba[i] = board.board[i] ? 0xFFFFFFFF : 0x0;
        }
        this.#t1.subsetData(new Uint8Array(rgba.buffer), x, y, w, h);
        GL.checkForErrors(this.#gl);
    }
    randomize() {
        const l = this.#vw * this.#vh;
        const rgba = new Uint32Array(l);
        let shift = 0;
        let rand = 0;
        for(let i = 0; i < rgba.length; i++){
            if (shift === 0) {
                rand = RandomUint8.next().value;
            }
            rgba[i] = rand >> shift & 0x01 ? 0xFFFFFFFF : 0x0;
            shift = ++shift % 8;
        }
        this.#t1.subsetData(new Uint8Array(rgba.buffer), 0, 0, this.#vw, this.#vh);
        GL.checkForErrors(this.#gl);
    }
    #advance(now) {
        const measured = this.#fps.measure(now);
        const { last , delay  } = this.#fps;
        if (now - last >= delay) {
            this.#step();
            this.#detectStagnation();
            this.#draw();
            this.#gl.enable(this.#gl.BLEND);
            this.#drawFps(now, measured);
            this.#drawMetadata();
            this.#gl.disable(this.#gl.BLEND);
        }
    }
    #iterateImpl(now1) {
        this.#advance(now1);
        this.#last = requestAnimationFrame(this.#dead ? this.advance : this.#iterate);
    }
    #renderFirstFrameImpl(now2) {
        this.#started = true;
        this.#initializeBoard();
        this.#draw();
        this.#gl.enable(this.#gl.BLEND);
        this.#drawFps(now2, this.#fps.measure(now2));
        this.#drawMetadata();
        this.#gl.disable(this.#gl.BLEND);
        this.#last = requestAnimationFrame(this.#iterate);
    }
    #resize() {
        const w2 = window.innerWidth;
        const h3 = window.innerHeight;
        this.orientation = w2 > h3 ? 1 : 0;
        this.#updateScale();
    }
    #updateScale() {
        if (!this.#board?.scale) {
            return;
        }
        const scale4 = this.#board.scale;
        const w3 = window.innerWidth;
        const h4 = window.innerHeight;
        this.#canvas.width = w3 - w3 % scale4;
        this.#canvas.height = h4 - h4 % scale4;
        this.w = this.#gl.drawingBufferWidth;
        this.h = this.#gl.drawingBufferHeight;
        this.#vw = this.w / scale4;
        this.#vh = this.h / scale4;
        const condense = 4;
        this.#cw = Math.ceil(this.#vw / condense);
        this.#ch = Math.ceil(this.#vh / condense);
        this.#viewportSize = new Float32Array([
            this.w,
            this.h
        ]);
        this.#stateSize = new Float32Array([
            this.#vw,
            this.#vh
        ]);
        this.#condenseSize = new Float32Array([
            this.#cw,
            this.#ch
        ]);
        this.#condenseBuffer = new Uint8Array(this.#cw * this.#ch * 4);
        this.#t1.resize(this.#vw, this.#vh, this.#frameBuffer);
        this.#t2.resize(this.#vw, this.#vh);
        this.#t3.resize(this.#cw, this.#ch);
    }
    #initializeBoard() {
        this.#frameBuffer.attach(this.#t2);
        this.#t1.bind();
        this.#gl.viewport(0, 0, this.#vw, this.#vh);
        this.#shade.use().uniformInt('state', 0).uniformSpread('scale', this.#stateSize).draw();
        this.#swapBuffers();
        return this;
    }
    #step() {
        this.#frameBuffer.attach(this.#t2);
        this.#t1.bind();
        this.#gl.viewport(0, 0, this.#vw, this.#vh);
        this.#gol.use().uniformInt('state', 0).uniformInt('rave', RaveMode.get()).uniformSpread('scale', this.#stateSize).draw();
        this.#swapBuffers();
        return this;
    }
    #detectStagnation() {
        this.#frameBuffer.attach(this.#t3);
        this.#t1.bind();
        this.#condense.use().uniformInt('state', 0).uniformSpread('scale', this.#condenseSize).draw();
        this.#gl.readPixels(0, 0, this.#cw, this.#ch, this.#gl.RGBA, this.#gl.UNSIGNED_BYTE, this.#condenseBuffer);
        const hash2 = GOL.#hashBoard(this.#condenseBuffer);
        if (hash2 === 0) {
            stagnantModal.dismiss();
            deadModal.show();
            this.stop();
            this.#dead = true;
        } else {
            if (GOL.#detectBoardStagnation(hash2)) {
                if (RestartOnStagnation.get()) {
                    this.setBoard(this.#board);
                } else {
                    stagnantModal.show();
                }
            }
        }
        return this;
    }
    #draw() {
        this.#GL.DEFAULT_FRAMEBUFFER.bind();
        this.#t1.bind();
        this.#gl.viewport(0, 0, this.w, this.h);
        this.#copy.use().uniformInt('state', 0).uniformSpread('scale', this.#viewportSize).draw();
        return this;
    }
    #drawFps(now3, measured1) {
        this.#GL.DEFAULT_FRAMEBUFFER.bind();
        this.#fps.draw(now3, measured1);
    }
    #drawMetadata() {
        this.#GL.DEFAULT_FRAMEBUFFER.bind();
        this.#metadata.draw();
    }
    #init() {
        this.#gl.disable(this.#gl.DEPTH_TEST);
        this.#gl.blendFunc(this.#gl.SRC_ALPHA, this.#gl.ONE_MINUS_SRC_ALPHA);
    }
    #swapBuffers() {
        const tmp = this.#t1;
        this.#t1 = this.#t2;
        this.#t2 = tmp;
    }
}
let game;
class Toggle {
    static #container = getById('settings');
    static #toggles = new Map();
    static get(id) {
        return this.#toggles.get(id);
    }
    static create(which, text, shortcut) {
        if (!this.#toggles.has(which)) {
            this.#toggles.set(which, new Toggle(which, text, shortcut));
        }
        return this.#toggles.get(which);
    }
    static init() {
        for (const toggle of this.#toggles.values()){
            toggle.init();
        }
    }
    #which;
    #text;
    #element = d.createElement('button');
    #txt = d.createTextNode('');
    constructor(which, text, shortcut){
        this.#which = which;
        this.#text = text;
        this.#element.appendChild(this.#txt);
        this.#element.setAttribute('data-toggle', this.#which);
        this.#element.setAttribute('title', shortcut);
        this.init();
        Toggle.#container.appendChild(this.#element);
    }
    init() {
        this.setText(game?.[this.#which] ?? false);
    }
    toggle() {
        const state = !game[this.#which];
        game[this.#which] = state;
        this.setText(state);
    }
    setText(state) {
        this.#txt.data = `${this.#text}: ${state ? 'On' : 'Off'}`;
    }
}
const raveButton = Toggle.create('raving', 'Rave Mode', 'x');
const pauseButton = Toggle.create('paused', 'Paused', 'Spacebar');
const autoRestartButton = Toggle.create('autorestart', 'Auto-Restart on Stagnation', 'a');
class Adjuster {
    static #container = getById('settings');
    static #adjusters = new Map();
    static get(id) {
        return this.#adjusters.get(id);
    }
    static create(which, text, allowedValues) {
        if (!this.#adjusters.has(which)) {
            this.#adjusters.set(which, new Adjuster(which, text, allowedValues));
        }
        return this.#adjusters.get(which);
    }
    static init() {
        for (const adjuster of this.#adjusters.values()){
            adjuster.init();
        }
    }
    #which;
    #allowedValues;
    #element = d.createElement('div');
    #txt = d.createTextNode('');
    #btnContainer = d.createElement('span');
    #up = d.createElement('button');
    #down = d.createElement('button');
    #idx = 0;
    constructor(which, text, allowedValues){
        this.#which = which;
        this.#allowedValues = allowedValues;
        this.#element.appendChild(d.createTextNode(`${text}: `));
        this.#element.appendChild(this.#txt);
        this.#element.appendChild(this.#btnContainer);
        this.#btnContainer.appendChild(this.#up);
        this.#btnContainer.appendChild(this.#down);
        this.#btnContainer.setAttribute('class', 'buttons');
        this.#element.setAttribute('data-adjuster', this.#which);
        this.#down.setAttribute('data-adjust', this.#which);
        this.#down.setAttribute('data-dir', 'down');
        this.#up.setAttribute('data-adjust', this.#which);
        this.#up.setAttribute('data-dir', 'up');
        this.init();
        Adjuster.#container.appendChild(this.#element);
    }
    init() {
        const v = game?.[this.#which];
        this.#idx = this.#allowedValues.findIndex((val)=>Array.isArray(val) ? val[0] === v : val === v);
        const val = this.#allowedValues[this.#idx];
        this.setText(Array.isArray(val) ? val[1] : val);
    }
    increment() {
        this.setValue(Math.min(this.#idx + 1, this.#allowedValues.length - 1));
    }
    decrement() {
        this.setValue(Math.max(this.#idx - 1, 0));
    }
    setValue(idx) {
        this.#idx = idx;
        const v = this.#allowedValues[this.#idx];
        if (Array.isArray(v)) {
            game[this.#which] = v[0];
            this.setText(v[1]);
        } else {
            game[this.#which] = v;
            this.setText(v);
        }
    }
    setText(state) {
        this.#txt.data = `${state}`;
    }
}
Adjuster.create('targetFramerate', 'Framerate', Array(FPS.maxFramerate).fill(0).reduce((acc, _, idx)=>FPS.maxFramerate % (idx + 1) === 0 ? acc.concat(idx + 1) : acc, [
    0.5
]).concat([
    [
        600,
        'Unlimited'
    ]
]));
async function loadBoardForCurrentHash() {
    const hash = location.hash.substring(1);
    if (Board.prebuilt.has(hash)) {
        const res = await Board.prebuilt.get(hash)();
        let board = res;
        if (game.orientation !== board.orientation) {
            board = board.rotate();
        }
        game.setBoard(board);
    } else {
        game.setBoard(Board.BLANK);
    }
}
async function main() {
    game = new GOL(new GL(getById('screen')));
    loadingModal.show();
    try {
        await Board.load('../pattern.index');
    } catch  {
        location.hash = '';
    } finally{
        await loadBoardForCurrentHash();
        loadingModal.reset();
    }
    if (!d.hidden) {
        game.start(true);
    }
    Toggle.init();
    Adjuster.init();
    self.addEventListener('resize', game.resize, {
        passive: true
    });
}
main();
async function reset() {
    try {
        game.stop();
        GOL.resetHistory();
        deadModal.reset();
        stagnantModal.reset();
        errorModal.reset();
        await loadBoardForCurrentHash();
        game.start(true);
    } catch (e) {
        errorModal.show(e.message);
    }
}
self.addEventListener('visibilitychange', ()=>{
    if (d.hidden) {
        game.stop();
    } else if (game.alive) {
        game.start();
    }
});
self.addEventListener('keydown', (e)=>{
    switch(e.key){
        case 'a':
            autoRestartButton.toggle();
            break;
        case 'n':
            if (game.paused) {
                requestAnimationFrame(game.advance);
            }
            break;
        case 'r':
            reset();
            break;
        case 'x':
            raveButton.toggle();
            break;
        case ' ':
            pauseButton.toggle();
            break;
        default:
            return;
    }
    e.preventDefault();
    e.stopPropagation();
});
self.addEventListener('click', (e)=>{
    for (const el of e.composedPath()){
        if (el instanceof HTMLElement) {
            if (el.hasAttribute('data-pattern')) {
                const active = d.querySelector('button[data-pattern].active');
                if (active) {
                    active.classList.remove('active');
                }
                e.preventDefault();
                e.stopPropagation();
                const pattern = el.getAttribute('data-pattern');
                if (location.hash.substring(1) !== pattern) {
                    location.hash = pattern;
                }
                reset();
                el.classList.add('active');
                return;
            } else if (el.hasAttribute('data-dismiss')) {
                Modal.get(el.getAttribute('data-dismiss'))?.dismiss();
            } else if (el.hasAttribute('data-toggle')) {
                Toggle.get(el.getAttribute('data-toggle'))?.toggle();
            } else if (el.hasAttribute('data-adjust')) {
                const adjuster = Adjuster.get(el.getAttribute('data-adjust'));
                if (el.getAttribute('data-dir') === 'up') {
                    adjuster?.increment();
                } else {
                    adjuster?.decrement();
                }
            }
        }
    }
});
