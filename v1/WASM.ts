/* tslint:disable:no-bitwise no-magic-numbers whitespace */
// tslint:disable-next-line:no-reference
///<reference path="./index.d.ts" />

// tslint:disable-next-line:interface-name
interface Stream {
	readonly position: number;

	getdents: unknown;
}

declare class ErrnoError extends Error {
	public readonly errno: number;
}

type IErrnoErrorConstructor = new () => ErrnoError;

declare const FS: {
	readonly ErrnoError: IErrnoErrorConstructor;

	close(stream: Stream): void;
	llseek(stream: Stream, offset: number, whence: unknown): unknown;
};

const enum WASM {
	MaxTotalMemory = 0x80000000,
	MinTotalMemory = 0x01000000,
	PageSize       = 0x00010000,
	StackAlign     = 0x00000010,
	StaticBase     = 0x00000400,
	StaticBump     = 0x00800610,

	MaxMemoryLimit = MaxTotalMemory - PageSize,
	QuarterMaxTotalMemory = MaxTotalMemory / 4,
}

if (typeof WebAssembly !== 'object') {
	throw new TypeError('No native WebAssembly support');
}

interface IExceptionInfo {
	readonly ptr: pointer;
	readonly type: number;
	readonly destructor: pointer;

	adjusted: pointer;
	refcount: number;
	caught: boolean;
	rethrown: boolean;
}

const UTF8Decoder = new TextDecoder('utf8');
const UTF16Decoder = new TextDecoder('utf-16le');

function bind<T, K extends FunctionProps<T>>(self: T, member: K): T[K] {
	return (self[member] as Fn).bind(self) as T[K];
}

/* tslint:disable:no-console */
const err = bind(console, 'warn');
const out = bind(console, 'log');
/* tslint:enable:no-console */

class WasmModuleInstance<T extends BaseInstanceType = {}> {
	public static BASE_DESCRIPTOR: Partial<IWasmInstanceDescriptor> = {
		initialMemory: WASM.MinTotalMemory,
		maxTableSize: 0,
	};

	public static ALIGN_UP(x: number) {
		if (x % WASM.PageSize > 0) {
			x += WASM.PageSize - (x % WASM.PageSize);
		}

		return x;
	}

	public static ALIGN_MEM(size: number, factor: number = WASM.StackAlign) {
		return Math.ceil(size / factor) * factor;
	}

	/** Callable exported WASM functions */
	/* tslint:disable:max-line-length variable-name */
	public readonly _malloc: (size: number) => number;
	public readonly _free: (ptr: pointer) => void;

	public readonly stackAlloc: (size: number) => pointer;
	public readonly stackSave: () => pointer;
	public readonly stackRestore: (ptr: pointer) => void;
	public readonly establishStackSpace: ($0: number, $1: number) => void;

	public readonly setTempRet0: (ptr: pointer) => void;
	public readonly getTempRet0: () => pointer;
	public readonly setThrew: ($0: number, $1: number) => void;

	public readonly dynCall: () => number;
	public readonly dynCall_i: ($0: number) => number;
	public readonly dynCall_ii: ($0: number, $1: number) => number;
	public readonly dynCall_iii: ($0: number, $1: number, $2: number) => number;
	public readonly dynCall_iiii: ($0: number, $1: number, $2: number, $3: number) => number;
	public readonly dynCall_iiiii: ($0: number, $1: number, $2: number, $3: number, $4: number) => number;
	public readonly dynCall_iiiiii: ($0: number, $1: number, $2: number, $3: number, $4: number, $5: number) => number;
	public readonly dynCall_v: ($0: number) => void;
	public readonly dynCall_vi: ($0: number, $1: number) => void;
	public readonly dynCall_vii: ($0: number, $1: number, $2: number) => void;
	public readonly dynCall_viii: ($0: number, $1: number, $2: number, $3: number) => void;
	public readonly dynCall_viiii: ($0: number, $1: number, $2: number, $3: number, $4: number) => void;
	public readonly dynCall_viiiii: ($0: number, $1: number, $2: number, $3: number, $4: number, $5: number) => void;
	public readonly dynCall_viiiiii: ($0: number, $1: number, $2: number, $3: number, $4: number, $5: number, $6: number) => void;
	/* tslint:enable:max-line-length variable-name */
	/** End callable exported WASM functions */

	public HEAP8: Int8Array;
	public HEAP16: Int16Array;
	public HEAP32: Int32Array;

	public HEAPU8: Uint8Array;
	public HEAPU16: Uint16Array;
	public HEAPU32: Uint32Array;

	public HEAPF32: Float32Array;
	public HEAPF64: Float64Array;

	public instance: T;

	public readonly initialMemory: number;
	public totalMemory: number;
	public readonly initialTableSize: number;
	public readonly maxTableSize: number;

	/* tslint:disable:max-line-length variable-name */
	public readonly ___cxa_can_catch: (ptr: pointer, type: number, thrown: number) => number;
	public readonly ___cxa_is_pointer_type: (type: number) => pointer;
	public readonly ___errno_location: () => number;
	/* tslint:enable:max-line-length variable-name */
	public readonly onAbort: (what: string) => void;

	public readonly STATIC_BASE: number = WASM.StaticBase;
	public STATIC_TOP: number = 0;
	public STATIC_SEALED = false;

	public STACK_BASE: number = 0;
	public STACK_TOP: number = 0;
	public STACK_MAX: number = 0;

	public DYNAMIC_BASE: number = 0;
	public DYNAMIC_TOP_PTR: number = 0;

	public memory: WebAssembly.Memory;

	/** Miscellaneous members necessary for syscalls and exceptions */
	public pos: pointer = 0;
	public outputBuffers = [null, [], []];
	public readonly exceptionInfo = new Map<pointer, IExceptionInfo>();
	// @ts-ignore
	public readonly caughtExceptions: IExceptionInfo[] = [];
	public lastException: pointer;
	/* tslint:disable:variable-name */
	public __ZSt18uncaught_exceptionv_uncaught_exception: number = 0;
	public ___cxa_find_matching_catch_buffer: pointer;
	/* tslint:enable:variable-name */

	public constructor(
		public readonly module: WebAssembly.Module,
		descriptor: IWasmInstanceDescriptor,
	) {
		const {
			___errno_location,
			initialMemory,
			maxTableSize,
			initialTableSize = maxTableSize,
			onAbort,
			staticBump,
			totalMemory = initialMemory,
			totalStack = (totalMemory * 0.3125)
		} = {
			...WasmModuleInstance.BASE_DESCRIPTOR,
			...descriptor,
		} as IWasmInstanceDescriptor;

		if (totalMemory < totalStack) {
			// tslint:disable-next-line:max-line-length
			err(`Total memory should be larger than total stack, was ${totalMemory} (< ${totalStack}`);
		}

		this.initialMemory = initialMemory;
		this.totalMemory = totalMemory;
		this.initialTableSize = initialTableSize;
		this.maxTableSize = maxTableSize;

		this.___errno_location = ___errno_location;
		this.onAbort = onAbort;

		this.STATIC_TOP = this.STATIC_BASE + staticBump;
		this.DYNAMIC_TOP_PTR = this.staticAlloc(4);
		this.STACK_TOP = WasmModuleInstance.ALIGN_MEM(this.STATIC_TOP);
		this.STACK_BASE = this.STACKTOP;
		this.STACK_MAX = this.STACK_BASE + totalStack;
		this.DYNAMIC_BASE = WasmModuleInstance.ALIGN_MEM(this.STACK_MAX);
	}

	public get buffer(): ArrayBuffer {
		return this.memory.buffer;
	}

	public get STACKTOP() {
		return this.STACK_TOP;
	}
	public set STACKTOP(value) {
		this.STACK_TOP = value;
	}

	public get DYNAMICTOP_PTR() {
		return this.DYNAMIC_TOP_PTR;
	}
	public set DYNAMICTOP_PTR(value) {
		this.DYNAMIC_TOP_PTR = value;
	}

	public async init() {
		if (!this.instance) {
			const instance = WebAssembly.instantiate(
				this.module,
				this.createImports()
			) as Promise<IWasmInstance<T>>;

			this.instance = this.addHooks((await instance).exports);
		}

		// NOTE: Middle type conversion is for compatibility with
		// WasmInstantiatedModule
		return this as (this & T) as WasmInstantiatedModule<T>;
	}

	public addHooks(instanceExports: T) {
		Object.entries(instanceExports).forEach(([key, value]) => {
			this[key] = value;
		});

		return instanceExports;
	}

	public createImports() {
		const self: WasmModuleInstance = this;

		this.memory = new WebAssembly.Memory({
			initial: this.initialMemory / WASM.PageSize,
			maximum: this.totalMemory / WASM.PageSize,
		});

		this.setHeapReferences();

		this.HEAP32[this.DYNAMIC_TOP_PTR>>2] = this.DYNAMIC_BASE;
		this.STATIC_SEALED = true;

		const table = new WebAssembly.Table({
			element: 'anyfunc',
			initial: this.initialTableSize,
			maximum: this.maxTableSize,
		});

		const imports: IWasmImportsDescriptor = {
			env: {
				DYNAMICTOP_PTR: self.DYNAMICTOP_PTR,
				STACKTOP: self.STACKTOP,
				STACK_MAX: self.STACK_MAX,

				memory: self.memory,
				memoryBase: this.STATIC_BASE,
				table,
				tableBase: 0,

				_abort: bind(self, 'abort'),
				_emscripten_memcpy_big: bind(self, '_memcpy_big'),
				_time: bind(self, '_time'),
				abort: bind(self, 'abort'),
				abortOnCannotGrowMemory: bind(self, 'abortOnCannotGrowMemory'),
				enlargeMemory: bind(self, 'enlargeMemory'),
				getTotalMemory() { return self.totalMemory; },
				invoke_vii: bind(self, 'invoke_vii'),

				___cxa_allocate_exception: bind(self, '___cxa_allocate_exception'),
				___cxa_find_matching_catch_2: bind(self, '___cxa_find_matching_catch_2'),
				___cxa_free_exception: bind(self, '___cxa_free_exception'),
				___cxa_throw: bind(self, '___cxa_throw'),
				___resumeException: bind(self, '___resumeException'),

				__ZSt18uncaught_exceptionv: bind(self, '__ZSt18uncaught_exceptionv'),

				/* tslint:disable:object-literal-sort-keys */
				___setErrNo: bind(self, '___setErrNo'),
				___syscall6: bind(self, '___syscall6'),
				___syscall54: bind(self, '___syscall54'),
				___syscall140: bind(self, '___syscall140'),
				___syscall146: bind(self, '___syscall146'),
				/* tslint:enable:object-literal-sort-keys */
			},
		};

		return imports;
	}

	public _memcpy_big(dest: number, src: number, size: number) {
		const heap = this.HEAPU8;

		heap.set(heap.subarray(src, src + size), dest);

		return dest;
	}

	public _time(ptr: number) {
		const time = (Date.now() / 1000) | 0;

		if (ptr) {
			this.HEAP32[ptr >> 2] = time;
		}

		return time;
	}

	public abort(what?: unknown) {
		if (this.onAbort) {
			this.onAbort(what as string);
		}

		if (what !== undefined) {
			out(what);
			err(what);
			what = JSON.stringify(what);
		} else {
			what = '';
		}

		throw new Error(`Aborted: ${what}`);
	}

	public abortOnCannotGrowMemory() {
		this.abort('Cannot enlarge memory arrays.');
	}

	public enlargeMemory() {
		const heap = this.HEAP32;

		if (heap[this.DYNAMICTOP_PTR>>2] > WASM.MaxMemoryLimit) {
			return false;
		}

		let totalMemory = Math.max(this.totalMemory, WASM.MinTotalMemory);

		while (totalMemory < heap[this.DYNAMICTOP_PTR>>2]) {
			if (totalMemory <= WASM.QuarterMaxTotalMemory) {
				totalMemory = WasmModuleInstance.ALIGN_UP(totalMemory * 2);
			} else {
				totalMemory = Math.min(
					WasmModuleInstance.ALIGN_UP(
						(totalMemory * 3 + (WASM.MaxTotalMemory as number)) / 4
					),
					WASM.MaxMemoryLimit
				);
			}
		}

		const replacement = this.reallocateBuffer(totalMemory);
		if (!replacement || replacement.byteLength !== totalMemory) {
			return false;
		}

		this.setHeapReferences();

		return true;
	}

	public reallocateBuffer(size: number) {
		const newSize = WasmModuleInstance.ALIGN_UP(size);
		const old = this.buffer;
		const oldSize = old.byteLength;

		try {
			const result = this.memory.grow((newSize - oldSize) / WASM.PageSize);

			if (result !== (-1 | 0)) {
				return this.buffer;
			}
		} catch {
			/* Do nothing */
		}

		return null;
	}

	public setHeapReferences() {
		const { buffer } = this;

		this.HEAP8 = new Int8Array(buffer);
		this.HEAP16 = new Int16Array(buffer);
		this.HEAP32 = new Int32Array(buffer);
		this.HEAPU8 = new Uint8Array(buffer);
		this.HEAPU16 = new Uint16Array(buffer);
		this.HEAPU32 = new Uint32Array(buffer);
		this.HEAPF32 = new Float32Array(buffer);
		this.HEAPF64 = new Float64Array(buffer);
	}

	public staticAlloc(size: number) {
		this.STATIC_TOP = (this.STATIC_TOP + size + 15) & -16;

		return this.STATIC_TOP;
	}

	public dynamicAlloc(size: number) {
		const ret = this.HEAP32[this.DYNAMIC_TOP_PTR>>2];
		const end = (ret + size + 15) & -16;
		this.HEAP32[this.DYNAMIC_TOP_PTR>>2] = end;

		if (end >= this.totalMemory && !this.enlargeMemory()) {
			this.HEAP32[this.DYNAMIC_TOP_PTR>>2] = ret;

			return 0;
		}

		return ret;
	}

	// @ts-ignore
	public getMemory(size: number) {
		if (!this.STATIC_SEALED) {
			return this.staticAlloc(size);
		} else if (!this.instance) {
			return this.dynamicAlloc(size);
		}

		return (this.instance as Debuggable<T>)._malloc(size);
	}

	public invoke_vii(index: number, a1: pointer, a2: pointer) {
		const sp = this.stackSave();

		try {
			this.dynCall_vii(index, a1, a2);
		} catch (e) {
			this.stackRestore(sp);

			// @ts-ignore: Extended `typeof` matches
			if (typeof e !== 'number' && typeof e !== 'longjmp') throw e;
			this.setThrew(1, 0);
		}
	}

	/** Exceptions */
	// @ts-ignore
	public deAdjustException(adjusted: pointer) {
		if (!adjusted || this.exceptionInfo.get(adjusted)) {
			return adjusted;
		}

		for (const [ptr, exception] of this.exceptionInfo) {
			if (exception.adjusted === adjusted) {
				return ptr;
			}
		}

		return adjusted;
	}

	// @ts-ignore
	public addExceptionRef(ptr: pointer) {
		if (!ptr) {
			return;
		}

		this.exceptionInfo.get(ptr).refcount++;
	}

	// @ts-ignore
	public decExceptionRef(ptr: pointer) {
		if (!ptr) {
			return;
		}

		const info = this.exceptionInfo.get(ptr);

		if (info.refcount > 0) {
			info.refcount--;

			if (info.refcount === 0 && !info.rethrown) {
				if (info.destructor) {
					this.dynCall_vi(info.destructor, ptr);
				}
				this.exceptionInfo.delete(ptr);
				this.___cxa_free_exception(ptr);
			}
		}
	}

	// @ts-ignore
	public clearExceptionRef(ptr: pointer) {
		if (!ptr) {
			return;
		}

		this.exceptionInfo.get(ptr).refcount = 0;
	}

	public ___cxa_allocate_exception(size: number) {
		return this._malloc(size);
	}

	public ___cxa_find_matching_catch_2(...a) {
		return this.___cxa_find_matching_catch(...a);
	}

	public ___cxa_free_exception(ptr: pointer) {
		try {
			/*return */this._free(ptr);
		} catch {
			/* Do nothing */
		}
	}

	public ___resumeException(ptr: pointer) {
		if (!this.lastException) {
			this.lastException = ptr;
		}

		throw ptr;
	}

	public ___cxa_find_matching_catch(...args: pointer[]) {
		let thrown = this.lastException;

		if (!thrown) {
			this.setTempRet0(0);

			return 0 | 0;
		}

		const info = this.exceptionInfo.get(thrown);
		const { type } = info;

		if (!type) {
			this.setTempRet0(0);

			return thrown | 0;
		}

		// @ts-ignore: This may be used in another context that I haven't added yet
		const ptr = this.___cxa_is_pointer_type(type);

		if (!this.___cxa_find_matching_catch_buffer) {
			this.___cxa_find_matching_catch_buffer = this._malloc(4);
		}

		this.HEAP32[this.___cxa_find_matching_catch_buffer >> 2] = thrown;
		thrown = this.___cxa_find_matching_catch_buffer;

		for (const arg of args) {
			if (arg && this.___cxa_can_catch(arg, type, thrown)) {
				thrown = this.HEAP32[thrown >> 2];
				info.adjusted = thrown;
				this.setTempRet0(arg);

				return thrown | 0;
			}
		}

		thrown = this.HEAP32[thrown >> 2];
		this.setTempRet0(type);

		return thrown | 0;
	}

	public ___cxa_throw(ptr: pointer, type: number, destructor: pointer) {
		this.exceptionInfo.set(ptr, {
			adjusted: ptr,
			caught: false,
			destructor,
			ptr,
			refcount: 0,
			rethrown: false,
			type,
		});

		this.lastException = ptr;

		this.__ZSt18uncaught_exceptionv_uncaught_exception++;

		throw ptr;
	}

	public __ZSt18uncaught_exceptionv() {
		return !!this.__ZSt18uncaught_exceptionv_uncaught_exception;
	}

	/** Miscellaneous helpers */
	public UTF8ArrayToString(ptr: Uint8Array | number[], idx?: pointer) {
		let end = idx;

		while (ptr[end]) ++end;

		if (end - idx > 16 && ptr instanceof Uint8Array) {
			return UTF8Decoder.decode(ptr.subarray(idx, end));
		}

		let str = '';

		while (1) {
			let u0 = ptr[idx++];
			if (!u0) {
				return str;
			}
			if (!(u0 & 128)) {
				str += String.fromCharCode(u0);
				continue;
			}
			const u1 = ptr[idx++] & 63;
			if ((u0 & 224) === 192) {
				str += String.fromCharCode((u0 & 31) << 6 | u1);
				continue;
			}
			const u2 = ptr[idx++] & 63;
			if ((u0 & 240) === 224) {
				u0 = (u0 & 7) << 12 | u1 << 6 | u2;
			} else {
				const u3 = ptr[idx++] & 63;
				if ((u0 & 248) === 240) {
					u0 = (u0 & 7) << 18 | u1 << 12 | u2 << 6 | u3;
				} else {
					const u4 = ptr[idx++] & 63;
					if ((u0 & 252) === 248) {
						u0 = (u0 & 3) << 24 | u1 << 18 | u2 << 12 | u3 << 6 | u4;
					} else {
						const u5 = ptr[idx++] & 63;
						u0 = (u1 & 1) << 30 | u1 << 24 | u2 << 18 | u3 << 12 | u4 << 6 | u5;
					}
				}
			}
			if (u0 < 65536) {
				str += String.fromCharCode(u0);
			} else {
				const ch = u0 - 65536;
				// tslint:disable-next-line:binary-expression-operand-order
				str += String.fromCharCode(55296 | ch >> 10, 56320 | ch & 1023);
			}
		}
	}

	// public UTF8ToString(ptr: pointer) {
	// 	return this.UTF8ArrayToString(this.HEAPU8, ptr);
	// }

	public printChar(stream: number, curr: number) {
		const buffer = this.outputBuffers[stream];
		if (curr === 0 || curr === 10) {
			(stream === 1 ? out : err)(this.UTF8ArrayToString(buffer, 0));
			buffer.length = 0;
		} else {
			buffer.push(curr);
		}
	}

	public get(_varargs?: number) {
		this.pos += 4;

		return this.HEAP32[(this.pos - 4) >> 2];
	}

	// public get64() {
	// 	const low = this.get();
	// 	const high = this.get();

	// 	if (
	// 		(low >= 0 && high === 0) ||
	// 		high === -1
	// 	) {
	// 		return low;
	// 	}

	// 	this.abort();
	// }

	// public getStr() {
	// 	return this.stringifyPointer(this.get());
	// }

	// public getZero() {
	// 	return this.get() === 0;
	// }

	// public stringifyPointer(ptr: pointer, length?: number) {
	// 	if (length === 0 || !ptr) {
	// 		return '';
	// 	}

	// 	let hasUtf = 0;
	// 	let t: number;
	// 	let i = 0;

	// 	while (1) {
	// 		t = this.HEAPU8[ptr + i >> 0];

	// 		hasUtf |= t;

	// 		if (
	// 			(t === 0 || !length) ||
	// 			(length && ++i === length)
	// 		) {
	// 			break;
	// 		}
	// 	}

	// 	if (!length) length = i;

	// 	let ret = '';

	// 	if (hasUtf < 128) {
	// 		const MAX_CHUNK = 1024;

	// 		while (length > 0) {
	// 			const curr = String.fromCharCode.apply(
	// 				String,
	// 				this.HEAPU8.subarray(ptr, ptr + Math.min(length, MAX_CHUNK))
	// 			);
	// 			ret += curr;
	// 			ptr += MAX_CHUNK;
	// 			length -= MAX_CHUNK;
	// 		}

	// 		return ret;
	// 	}

	// 	return this.UTF8ToString(ptr);
	// }

	public getStreamFromFD(): Stream {
		throw new Error('Not implemented');
	}

	public ___setErrNo(value: number) {
		if (this.___errno_location) {
			this.HEAP32[this.___errno_location()>>2] = value;
		}

		return value;
	}

	/** Syscalls */
	public ___syscall6(_which, varargs: pointer) {
		this.pos = varargs;

		try {
			const stream = this.getStreamFromFD();

			FS.close(stream);

			return 0;
		} catch (e) {
			if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) {
				this.abort(e);
			}

			return -(e as ErrnoError).errno;
		}
	}

	public ___syscall54(_which, varargs: pointer) {
		this.pos = varargs;

		try {
			return 0;
		} catch (e) {
			if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) {
				this.abort(e);
			}

			return -(e as ErrnoError).errno;
		}
	}

	public ___syscall140(_which, varargs: pointer) {
		this.pos = varargs;

		try {
			const stream = this.getStreamFromFD();
			// @ts-ignore
			const offsetHi = this.get();
			const offsetLo = this.get();
			const result = this.get();
			const whence = this.get();
			const offset = offsetLo;
			FS.llseek(stream, offset, whence);
			this.HEAP32[result >> 2] = stream.position;

			if (stream.getdents && offset === 0 && whence === 0) {
				stream.getdents = null;
			}

			return 0;
		} catch (e) {
			if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) {
				this.abort(e);
			}

			return -(e as ErrnoError).errno;
		}
	}

	public ___syscall146(_which, varargs: pointer) {
		this.pos = varargs;

		try {
			const stream = this.get();
			const iov = this.get();
			const iovcnt = this.get();
			let ret = 0;

			for (let i = 0; i < iovcnt; i++) {
				const ptr = this.HEAP32[(iov + (i * 8)) >> 2];
				const len = this.HEAP32[(iov + (i * 8) + 4) >> 2];
				for (let j = 0; j < len; j++) {
					this.printChar(stream, this.HEAPU8[ptr + j]);
				}
				ret += len;
			}

			return ret;
		} catch (e) {
			if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) {
				this.abort(e);
			}

			return -(e as ErrnoError).errno;
		}
	}
}

// @ts-ignore: Bad duplicate identifier detection
class WasmModule<T extends BaseInstanceType = {}> {
	public static module<T extends BaseInstanceType>(file: string) {
		return new WasmModule<T>(file);
	}

	public static instance<T extends BaseInstanceType>(
		file: string,
		descriptor?: IWasmInstanceDescriptor,
	) {
		return new WasmModule<T>(file).instance(descriptor);
	}

	public static compile(file: string) {
		const resp = fetch(file, { credentials: 'same-origin' });

		return WebAssembly.compileStreaming(resp);
	}

	public compiledModule: WebAssembly.Module;

	public constructor(public readonly file: string) {}

	public async instance(descriptor?: IWasmInstanceDescriptor) {
		return new WasmModuleInstance<T>(await this.compile(), descriptor).init();
	}

	public async compile() {
		if (!this.compiledModule) {
			this.compiledModule = await WasmModule.compile(this.file);
		}

		return this.compiledModule;
	}
}
