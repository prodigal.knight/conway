declare type Fn = (...args: any[]) => any;

// Stolen from @types/jest, with some minor modifications
declare type FunctionProps<T> = { [K in keyof T]: T[K] extends Fn ? K : never }[keyof T];

declare type TypedArray =
	Uint8Array |
	Uint16Array |
	Uint32Array |
	Int8Array |
	Int16Array |
	Int32Array |
	Float32Array |
	Float64Array;

declare interface TypedArrayConstructor {
	new (length?: number): TypedArray;
	new (buffer: ArrayBuffer, start?: number, length?: number): TypedArray;
}

declare type ByteWidth = 1 | 2 | 4 | 8;
declare type size_t = number;
declare type pointer = number;

declare interface IWasmInstance<T extends BaseInstanceType> extends WebAssembly.Instance {
	readonly exports: T;
}

declare interface IWasmImportsEnvDescriptor extends Record<string, WebAssembly.ImportValue> {
	readonly memory?: WebAssembly.Memory;
	readonly table?: WebAssembly.Table;
}

declare interface IWasmImportsDescriptor extends Record<string, Record<string, WebAssembly.ImportValue>> {
	env: IWasmImportsEnvDescriptor;
}

declare interface IWasmInstanceDescriptor {
	readonly staticBump: number;

	readonly initialMemory?: number;
	readonly totalMemory?: number;
	readonly totalStack?: number;

	readonly initialTableSize?: number;
	readonly maxTableSize?: number;

	___errno_location?(): number;
	onAbort?(what: string): void;
}

declare interface IWasmModuleInstance<T> {
	readonly instance: T;

	readonly buffer: ArrayBuffer;
}

declare type WasmInstantiatedModule<T extends BaseInstanceType> = IWasmModuleInstance<T> & T;

declare type BaseInstanceType = Record<string, WebAssembly.ExportValue>;

declare interface IWasmModule<T extends BaseInstanceType = {}> {
	compiledModule: WebAssembly.Module;

	compile(): Promise<WebAssembly.Module>;
	instance(descriptor?: IWasmInstanceDescriptor): Promise<WasmInstantiatedModule<T>>;
}

declare interface IWasmModuleConstructor {
	compile(file: string): Promise<WebAssembly.Module>;
	instance<T extends BaseInstanceType>(file: string, descriptor?: IWasmInstanceDescriptor): Promise<WasmInstantiatedModule<T>>;
	module<T extends BaseInstanceType>(file: string): Promise<IWasmModule<T>>;

	new <T extends BaseInstanceType>(file: string): IWasmModule<T>;
}

// @ts-ignore: Bad duplicate identifier detection
declare var WasmModule: IWasmModuleConstructor;

declare interface IEmscriptenDebugExports {
	__growWasmMemory(by: number): number;
	_emscripten_replace_memory(buf: ArrayBuffer): boolean;
	_free($0: number): void;
	_malloc(size: size_t): pointer;
	_memcpy(dst: pointer, src: pointer, size: size_t): pointer;
	_memset(ptr: pointer, value: number, num: size_t): pointer;
	_sbrk(incr: pointer): pointer;
	establishedStackSpace(ptr: number): number;
	getTempRet0(): number;
	runPostSets(): void;
	setTempRet0(ptr: number): void;
	setThrew(loc: pointer, value: pointer): void;
	stackAlloc(size: number): pointer;
	stackRestore(pos: pointer): void;
	stackSave(): pointer;
}

declare type Debuggable<T> = T & IEmscriptenDebugExports;

