#!/bin/sh

docker run --rm -v "$(pwd)":/src -t apiaryio/emcc /usr/bin/make "$@"
