#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#define DEBUGGING_ENABLED

// Debugging helper macros so that I can have debug statements littered all around
// but not actually have any running code if debugging isn't enabled
#ifdef DEBUGGING_ENABLED
#include <stdio.h>
#define DEBUG_REAL(M, ...) printf("[DEBUG] " M "\n", ##__VA_ARGS__)
#else
#define DEBUG_REAL(...) /* __VA_ARGS__ */
#endif
#define DEBUG_SINGLE(M) DEBUG_REAL(M, NULL)
#define DEBUG(M, ...) DEBUG_REAL(M, ##__VA_ARGS__)

// C includes because I'm old-fashioned/weird
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdint.h>

// C++ includes because I'm too lazy to write pure C

#define MAX_HUE_VALUE 210

#if MAX_HUE_VALUE < 255
#define BOARD_TYPE uint8_t
#elif MAX_HUE_VALUE < 65535
#define BOARD_TYPE uint16_t
#else
#define BOARD_TYPE uint32_t
#endif

#define POS (uint32_t) ((y * w) + x)

template <class T> inline T min(T a, T b) { return (a < b) ? a : b; }
template <class T> inline T max(T a, T b) { return (a < b) ? b : a; }
template <class T> inline T clamp(T v, T min, T max) { return (v < min) ? min : (v > max) ? max : v; }
template <class T> inline bool between(T v, T min, T max) { return v >= min && v <= max; }

const uint8_t BOARD_ELEMENT_SIZE = sizeof((BOARD_TYPE) 0);
const uint32_t MAX_HUE = MAX_HUE_VALUE;
const uint32_t NEW_BOUNDED_CELL = (BOARD_TYPE) -1; // convert to unsigned integer value
const uint32_t CHECKED_EMPTY_CELL = (BOARD_TYPE) -2; // convert to unsigned integer value
const uint32_t BASE_HASH = 1315423911;
const uint32_t MAX_SIZE = 1048576;
const uint16_t MAX_BOXEN = 16384;

uint16_t width = 0;
uint16_t height = 0;

BOARD_TYPE old[MAX_SIZE];
BOARD_TYPE board[MAX_SIZE];
uint8_t checked[MAX_SIZE];

const BOARD_TYPE MIN_ACTIVE_THRESHOLD = 1;
const BOARD_TYPE MAX_ACTIVE_THRESHOLD = 44;

const int16_t DEFAULT_MARGIN = 3;

void addBoundingBox(int16_t l, int16_t t, int16_t r, int16_t b);

const uint8_t BOX_BUF_SIZE = 40;
struct BoundingBox {
	int16_t left = 0;
	int16_t top = 0;
	int16_t right = 0;
	int16_t bottom = 0;

	BoundingBox() { /* Do nothing */ }

	BoundingBox(int16_t x, int16_t y) {
		left = x;
		top = y;

		right = x;
		bottom = y;
	}

	BoundingBox(int16_t l, int16_t t, int16_t r, int16_t b) {
		left = l;
		top = t;
		right = r;
		bottom = b;
	}

	bool contains(int16_t x, int16_t y) const {
		return between(x, left, right) && between(y, top, bottom);
	}

	bool near(int16_t x, int16_t y, int16_t margin = DEFAULT_MARGIN) const {
		return (
			between(x, (int16_t) (left - margin), (int16_t) (right + margin)) &&
			between(y, (int16_t) (top - margin), (int16_t) (bottom + margin))
		);
	}

	bool intersects(const BoundingBox box) const {
		const bool l = between(box.left, left, right) || between(left, box.left, box.right);
		const bool t = between(box.top, top, bottom) || between(top, box.top, box.bottom);
		const bool r = between(box.right, left, right) || between(right, box.left, box.right);
		const bool b = between(box.bottom, top, bottom) || between(bottom, box.top, box.bottom);

		return (
			((l && t) || (l && b) || (r && t) || (r && b)) ||
			(l && r && (t || b)) || (t && b && (l || r))
		);
	}

	bool containsLife() const {
		if (right - left <= 0 || bottom - top <= 0) {
			return false;
		}

		const uint16_t w = width;
		const uint16_t h = height;

		for (int16_t x = left; x <= right; x++) {
			for (int16_t y = top; y <= bottom; y++) {
				const uint32_t i = (((y + h) % h) * w) + ((x + w) % w);

				if (old[i] || board[i]) {
					return true;
				}
			}
		}

		return false;
	}

	BoundingBox intersectionWith(const BoundingBox box) {
		// const bool l = between(box.left, left, right) || between(left, box.left, box.right);
		// const bool t = between(box.top, top, bottom) || between(top, box.top, box.bottom);
		// const bool r = between(box.right, left, right) || between(right, box.left, box.right);
		// const bool b = between(box.bottom, top, bottom) || between(bottom, box.top, box.bottom);

		return BoundingBox(
			max(left, box.left),
			max(top, box.top),
			min(right, box.right),
			min(bottom, box.bottom)
		);
	}

	void reset(int16_t w, int16_t h) {
		top = h;
		left = w;
		right = 0;
		bottom = 0;
	}

	void resizeToInclude(int16_t x, int16_t y) {
		if (x < left) { left = x; }
		if (x > right) { right = x; }
		if (y < top) { top = y; }
		if (y > bottom) { bottom = y; }
	}

	void expand_old(int16_t w, int16_t h, int16_t margin = DEFAULT_MARGIN) {
		expand_nowrap(w, h, margin);

		if (top == 0) {
			bottom = h;
		} else if (bottom == h) {
			top = 0;
		}
		if (left == 0) {
			right = w;
		} else if (right == w) {
			left = 0;
		}
	}

	void expand_nowrap(int16_t w, int16_t h, int16_t margin = DEFAULT_MARGIN) {
		if (top > margin) { top -= margin; }
		else { top = 0; }
		if (left > margin) { left -= margin; }
		else { left = 0; }
		if (right < w - margin) { right += margin; }
		else { right = w; }
		if (bottom < h - margin) { bottom += margin; }
		else { bottom = h; }
	}

	void expand(int16_t w, int16_t h, int16_t margin = DEFAULT_MARGIN) {
		// expand_old(w, h, margin);
		expand_nowrap(w, h, margin);

		check_wrap(w, h);
	}

	void expand_ignoreWrap(int16_t w, int16_t h, int16_t margin = DEFAULT_MARGIN) {
		top -= margin;
		left -= margin;
		right += margin;
		bottom += margin;
	}

	void check_wrap(int16_t w, int16_t h) {
		if (top == 0) {
			addBoundingBox(left, h - 1, right, h);
		} else if (bottom == h) {
			addBoundingBox(left, 0, right, 1);
		}
		if (left == 0) {
			addBoundingBox(w - 1, top, w, bottom);
		} else if (right == w) {
			addBoundingBox(0, top, 1, bottom);
		}
	}

	void expandToFit(const BoundingBox box) {
		top = min(top, box.top);
		left = min(left, box.left);
		right = max(right, box.right);
		bottom = max(bottom, box.bottom);
	}

	char *string(char *buf) const {
		sprintf(buf, "BoundingBox { (%3d,%3d) (%3d,%3d) }", left, top, right, bottom);

		return buf;
	}
};

BoundingBox allLife;
BoundingBox box;

uint16_t boxCount = 0;
BoundingBox boxen[MAX_BOXEN];

void addBoundingBox(int16_t l, int16_t t, int16_t r, int16_t b) {
	boxen[boxCount++] = BoundingBox(l, t, r, b);
}

uint16_t collapseBoundingBoxen(uint16_t from = 0) {
	if (from >= boxCount) {
		return boxCount;
	}

	bool collapsed = false;

	do {
		collapsed = false;

		for (uint16_t i = from; i < boxCount; i++) {
			for (uint16_t j = boxCount - 1; j > i; j--) {
				if (
					boxen[i].intersects(boxen[j]) &&
					boxen[i].intersectionWith(boxen[j]).containsLife()
				) {
					collapsed = true;

					boxen[i].expandToFit(boxen[j]);

					for (uint16_t k = j + 1; k < boxCount; k++) {
						boxen[k - 1] = boxen[k];
					}

					boxCount--;
				}
			}
		}
	} while (collapsed);

	return boxCount;
}

bool copyBoard(uint16_t w, uint16_t h) {
	bool dead = true;

	allLife.reset(w, h);
	box.reset(w, h);

	boxCount = 0;

	for (uint32_t y = 0; y < h; y++) {
		for (uint32_t x = 0; x < w; x++) {
			const uint32_t i = POS;
			checked[i] = 0;
			const BOARD_TYPE old_v = old[i];
			const BOARD_TYPE v = board[i];

			if (v) {
				dead = false;

				allLife.resizeToInclude(x, y);
			}

			// Tested threshold range for minimum amount to be correct while also minimizing size of box
			if (between(v, MIN_ACTIVE_THRESHOLD, MAX_ACTIVE_THRESHOLD)) {
				box.resizeToInclude(x, y);
			}

			// Catch blinking elements that disappear for one frame and then reappear
			// by looking at the previous frame before copying over its value
			if (
				between(v, MIN_ACTIVE_THRESHOLD, MAX_ACTIVE_THRESHOLD) ||
				between(old_v, MIN_ACTIVE_THRESHOLD, MAX_ACTIVE_THRESHOLD)
			) {
				bool foundBox = false;
				for (uint16_t i = 0; i < boxCount; i++) {
					if (boxen[i].near(x, y)) {
						foundBox = true;
						boxen[i].resizeToInclude(x, y);

						break;
					}
				}

				if (!foundBox) {
					boxen[boxCount++] = BoundingBox(x, y);
				}
			}

			old[i] = v;
		}
	}

	// Expand known boxen
	for (uint16_t i = 0; i < boxCount; i++) {
		boxen[i].expand_ignoreWrap(w, h);
		// boxen[i].expand_old(w, h);
		// boxen[i].expand_nowrap(w, h);
	}
	// Collapse boxen if they overlap
	uint16_t boxenCount = collapseBoundingBoxen(); // Save current boxen count
	// Check boxen for screen edge wrap and create bounding boxen on the opposite
	// edge of the screen
	// for (uint16_t i = 0; i < boxenCount; i++) {
	// 	boxen[i].check_wrap(w, h);
	// 	allLife.expandToFit(boxen[i]);
	// }
	// If new boxen were spawned on the edges of the screen
	if (boxenCount != boxCount) {
		// Collapse new boxen on the edges of the screen if they overlap
		// collapseBoundingBoxen(boxenCount);
		// Expand all life box to encapsulate additional boxes that were created
		for (uint16_t i = boxenCount; i < boxCount; i++) {
			allLife.expandToFit(boxen[i]);
		}
	}

	box.expand_old(w, h);

	return dead;
}

#define CALC_Y_VARIANTS(y)\
	const uint16_t y0 = ((y + h - 1) % h) * w;\
	const uint16_t y1 = y * w;\
	const uint16_t y2 = ((y + 1) % h) * w;
#define CALC_X_VARIANTS(x)\
	const uint16_t x0 = (x + w - 1) % w;\
	const uint16_t x1 = x;\
	const uint16_t x2 = (x + 1) % w;

#define ALIVE(cell) (uint8_t) (cell != 0)

#define LIVE_NEIGHBORS\
	ALIVE(old[y0 + x0]) +    ALIVE(old[y0 + x1]) +    ALIVE(old[y0 + x2]) +\
	ALIVE(old[y1 + x0]) + /* ALIVE(old[y1 + x1]) + */ ALIVE(old[y1 + x2]) +\
	ALIVE(old[y2 + x0]) +    ALIVE(old[y2 + x1]) +    ALIVE(old[y2 + x2])

#define HASH_CELL(v) hash ^= ((hash << 5) + v + (hash >> 2))
#define HASH_LIVE_CELL HASH_CELL(1)
#define HASH_DEAD_CELL HASH_CELL(0)

// #define OFFSET_POS(x, y) (((y + h) % h) * w) + ((x + w) % w)

// void print_grid(const char which[], uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
// 	DEBUG(
// 		"%s:\n%d %d %d %d %d %d %d   %d %d %d %d %d %d %d\n%d %d %d %d %d %d %d   %d %d %d %d %d %d %d\n%d %d %d %d %d %d %d   %d %d %d %d %d %d %d\n%d %d %d %d %d %d %d > %d %d %d %d %d %d %d\n%d %d %d %d %d %d %d   %d %d %d %d %d %d %d\n%d %d %d %d %d %d %d   %d %d %d %d %d %d %d\n%d %d %d %d %d %d %d   %d %d %d %d %d %d %d",
// 		which,
// 		old[OFFSET_POS(x - 3, y - 3)],
// 		old[OFFSET_POS(x - 2, y - 3)],
// 		old[OFFSET_POS(x - 1, y - 3)],
// 		old[OFFSET_POS(x, y - 3)],
// 		old[OFFSET_POS(x + 1, y - 3)],
// 		old[OFFSET_POS(x + 2, y - 3)],
// 		old[OFFSET_POS(x + 3, y - 3)],
// 		board[OFFSET_POS(x - 3, y - 3)],
// 		board[OFFSET_POS(x - 2, y - 3)],
// 		board[OFFSET_POS(x - 1, y - 3)],
// 		board[OFFSET_POS(x, y - 3)],
// 		board[OFFSET_POS(x + 1, y - 3)],
// 		board[OFFSET_POS(x + 2, y - 3)],
// 		board[OFFSET_POS(x + 3, y - 3)],
// 		old[OFFSET_POS(x - 3, y - 2)],
// 		old[OFFSET_POS(x - 2, y - 2)],
// 		old[OFFSET_POS(x - 1, y - 2)],
// 		old[OFFSET_POS(x, y - 2)],
// 		old[OFFSET_POS(x + 1, y - 2)],
// 		old[OFFSET_POS(x + 2, y - 2)],
// 		old[OFFSET_POS(x + 3, y - 2)],
// 		board[OFFSET_POS(x - 3, y - 2)],
// 		board[OFFSET_POS(x - 2, y - 2)],
// 		board[OFFSET_POS(x - 1, y - 2)],
// 		board[OFFSET_POS(x, y - 2)],
// 		board[OFFSET_POS(x + 1, y - 2)],
// 		board[OFFSET_POS(x + 2, y - 2)],
// 		board[OFFSET_POS(x + 3, y - 2)],
// 		old[OFFSET_POS(x - 3, y - 1)],
// 		old[OFFSET_POS(x - 2, y - 1)],
// 		old[OFFSET_POS(x - 1, y - 1)],
// 		old[OFFSET_POS(x, y - 1)],
// 		old[OFFSET_POS(x + 1, y - 1)],
// 		old[OFFSET_POS(x + 2, y - 1)],
// 		old[OFFSET_POS(x + 3, y - 1)],
// 		board[OFFSET_POS(x - 3, y - 1)],
// 		board[OFFSET_POS(x - 2, y - 1)],
// 		board[OFFSET_POS(x - 1, y - 1)],
// 		board[OFFSET_POS(x, y - 1)],
// 		board[OFFSET_POS(x + 1, y - 1)],
// 		board[OFFSET_POS(x + 2, y - 1)],
// 		board[OFFSET_POS(x + 3, y - 1)],
// 		old[OFFSET_POS(x - 3, y)],
// 		old[OFFSET_POS(x - 2, y)],
// 		old[OFFSET_POS(x - 1, y)],
// 		old[POS],
// 		old[OFFSET_POS(x + 1, y)],
// 		old[OFFSET_POS(x + 2, y)],
// 		old[OFFSET_POS(x + 3, y)],
// 		board[OFFSET_POS(x - 3, y)],
// 		board[OFFSET_POS(x - 2, y)],
// 		board[OFFSET_POS(x - 1, y)],
// 		board[POS],
// 		board[OFFSET_POS(x + 1, y)],
// 		board[OFFSET_POS(x + 2, y)],
// 		board[OFFSET_POS(x + 3, y)],
// 		old[OFFSET_POS(x - 3, y + 1)],
// 		old[OFFSET_POS(x - 2, y + 1)],
// 		old[OFFSET_POS(x - 1, y + 1)],
// 		old[OFFSET_POS(x, y - 1)],
// 		old[OFFSET_POS(x + 1, y + 1)],
// 		old[OFFSET_POS(x + 2, y + 1)],
// 		old[OFFSET_POS(x + 3, y + 1)],
// 		board[OFFSET_POS(x - 3, y + 1)],
// 		board[OFFSET_POS(x - 2, y + 1)],
// 		board[OFFSET_POS(x - 1, y + 1)],
// 		board[OFFSET_POS(x, y - 1)],
// 		board[OFFSET_POS(x + 1, y + 1)],
// 		board[OFFSET_POS(x + 2, y + 1)],
// 		board[OFFSET_POS(x + 3, y + 1)],
// 		old[OFFSET_POS(x - 3, y + 2)],
// 		old[OFFSET_POS(x - 2, y + 2)],
// 		old[OFFSET_POS(x - 1, y + 2)],
// 		old[OFFSET_POS(x, y + 2)],
// 		old[OFFSET_POS(x + 1, y + 2)],
// 		old[OFFSET_POS(x + 2, y + 2)],
// 		old[OFFSET_POS(x + 3, y + 2)],
// 		board[OFFSET_POS(x - 3, y + 2)],
// 		board[OFFSET_POS(x - 2, y + 2)],
// 		board[OFFSET_POS(x - 1, y + 2)],
// 		board[OFFSET_POS(x, y + 2)],
// 		board[OFFSET_POS(x + 1, y + 2)],
// 		board[OFFSET_POS(x + 2, y + 2)],
// 		board[OFFSET_POS(x + 3, y + 2)],
// 		old[OFFSET_POS(x - 3, y + 3)],
// 		old[OFFSET_POS(x - 2, y + 3)],
// 		old[OFFSET_POS(x - 1, y + 3)],
// 		old[OFFSET_POS(x, y + 3)],
// 		old[OFFSET_POS(x + 1, y + 3)],
// 		old[OFFSET_POS(x + 2, y + 3)],
// 		old[OFFSET_POS(x + 3, y + 3)],
// 		board[OFFSET_POS(x - 3, y + 3)],
// 		board[OFFSET_POS(x - 2, y + 3)],
// 		board[OFFSET_POS(x - 1, y + 3)],
// 		board[OFFSET_POS(x, y + 3)],
// 		board[OFFSET_POS(x + 1, y + 3)],
// 		board[OFFSET_POS(x + 2, y + 3)],
// 		board[OFFSET_POS(x + 3, y + 3)]
// 	);
// }

extern "C" {
	int16_t *getBox() {
		return &(box.left);
	}

	uint16_t getNumBoxen() {
		return boxCount;
	}

	int16_t *getBoxen() {
		return &(boxen[0].left);
	}

	uint16_t getMaxBoxen() {
		return MAX_BOXEN;
	}

	int16_t *getAllLife() {
		return &(allLife.left);
	}

	uint32_t getElementSize() {
		return BOARD_ELEMENT_SIZE;
	}

	uint32_t getMaxHue() {
		return MAX_HUE + 1; // Minimum age is 1, so max hue needs to be one greater in order to be seen
	}

	uint32_t getMaxSize() {
		return MAX_SIZE;
	}

	BOARD_TYPE *getBoard() {
		return &board[0];
	}

	void clear(uint32_t _size) {
		const uint32_t size = min(_size, MAX_SIZE);
		for (uint32_t i = 0; i < size; i++) {
			board[i] = 0;
		}
	}

	uint32_t createLife(uint16_t w, uint16_t h) {
		srand(time(NULL));

		uint32_t hash = BASE_HASH;

		const uint32_t size = min((uint32_t) (w * h), MAX_SIZE);
		for (uint32_t i = 0; i < size; i++) {
			BOARD_TYPE v = rand() % 2;

			board[i] = v;

			HASH_CELL(v);
		}

		allLife.left = 0;
		allLife.top = 0;
		allLife.right = w;
		allLife.bottom = h;

		return hash;
	}

	uint32_t resize(uint16_t w, uint16_t h) {
		if (w == 0 || h == 0) {
			return 0;
		}

		if (w * h > MAX_SIZE) {
			clear(MAX_SIZE);

			return 0;
		}

		int16_t dx = (int16_t) ((int16_t) (width - w) / 2);
		int16_t dy = (int16_t) ((int16_t) (height - h) / 2);

		if (width == 0 && height == 0) {
			width = w;
			height = h;

			return 0;
		}

		if (copyBoard(width, height)) {
			return 0;
		}

		uint32_t hash = BASE_HASH;

		for (uint16_t x = 0; x < w; x++) {
			for (uint16_t y = 0; y < h; y++) {
				const uint32_t idx = POS;

				if (
					!between((uint16_t) (x + dx), (uint16_t) 0, width) ||
					!between((uint16_t) (y + dy), (uint16_t) 0, width)
				) {
					board[idx] = 0;
					hash ^= ((hash << 5) + (hash >> 2));
				} else {
					const BOARD_TYPE v = old[((y + dy) * width) + (x + dx)];

					board[idx] = v;

					if (v) {
						HASH_LIVE_CELL;
					} else {
						HASH_DEAD_CELL;
					}
				}
			}
		}

		width = w;
		height = h;

		allLife.left = 0;
		allLife.top = 0;
		allLife.right = w;
		allLife.bottom = h;

		return hash;
	}

	uint32_t life(uint16_t w, uint16_t h) {
		if (copyBoard(w, h)) {
			return 0;
		}

		bool dead = true;
		uint32_t hash = BASE_HASH;

		for (uint16_t y = 0; y < h; y++) {
			CALC_Y_VARIANTS(y);

			for (uint16_t x = 0; x < w; x++) {
				if (!allLife.contains(x, y)) {
					HASH_DEAD_CELL;

					continue;
				}

				CALC_X_VARIANTS(x);

				const uint32_t idx = y1 + x1;

				const BOARD_TYPE age = old[idx];

				if (box.contains(x, y)) {
					uint8_t neighbors = LIVE_NEIGHBORS;

					if ((age && neighbors == 2) || neighbors == 3) {
						dead = false;
						if (age < MAX_HUE) {
							board[idx] = age + 1;
						}
						HASH_LIVE_CELL;
					} else {
						board[idx] = 0;
						HASH_DEAD_CELL;
					}
				} else if (age) {
					dead = false;
					if (age < MAX_HUE) {
						board[idx] = age + 1;
					}
					HASH_LIVE_CELL;
				} else {
					HASH_DEAD_CELL;
				}
			}
		}

		// print_grid("STABLE", 0, 331, w, h);

		if (dead) {
			return 0;
		}

		return (hash & 0x7FFFFFFF);
	}

	uint32_t life_experimental(uint16_t w, uint16_t h) {
		if (copyBoard(w, h)) {
			return 0;
		}

		bool dead = true;
		uint32_t hash = BASE_HASH;
		const uint32_t max = w * h;

		for (uint16_t i = 0; i < boxCount; i++) {
			const BoundingBox box = boxen[i];

			for (uint16_t y = box.top; y <= box.bottom; y++) {
				CALC_Y_VARIANTS(y);

				for (uint16_t x = box.left; x <= box.right; x++) {
					CALC_X_VARIANTS(x);

					const uint32_t idx = y1 + x1;

					if (checked[idx]) continue; // Already checked by a previous bounding box
					checked[idx] = 1;

					const BOARD_TYPE age = old[idx];

					uint8_t neighbors = LIVE_NEIGHBORS;

					if ((age && neighbors == 2) || neighbors == 3) {
						dead = false;
						if (age < MAX_HUE) {
							board[idx] = age + 1;
						}
					} else {
						board[idx] = 0;
					}
				}
			}
		}

		for (uint16_t x = 0; x < w; x++) {
			for (uint16_t y = 0; y < h; y++) {
				const uint32_t idx = POS;
				const BOARD_TYPE age = board[idx];

				if (age == 0 || !allLife.contains(x, y)) {
					HASH_DEAD_CELL;
				} else {
					HASH_LIVE_CELL;
				}
			}
		}

		// print_grid("EXPERIMENTAL", 0, 331, w, h);

		if (dead) {
			return 0;
		}

		return (hash & 0x7FFFFFFF);
	}
}
