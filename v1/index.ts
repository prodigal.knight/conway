/* tslint:disable:no-reference */
///<reference path="./index.d.ts" />
///<reference path="./WASM.ts" />
/* tslint:enable:no-reference */

interface ILifeWASM extends Record<string, WebAssembly.ExportValue> {
	_getBox(): pointer;
	_getNumBoxen(): number;
	_getBoxen(): pointer;
	_getMaxBoxen(): number;

	_getAllLife(): pointer;
	_getElementSize(): ByteWidth;
	_getMaxHue(): number;
	_getMaxSize(): number;
	_getBoard(): pointer;
	_clear(size: number): void;
	_createLife(w: number, h: number): number;
	_resize(w: number, h: number): number;
	_life(w: number, h: number): number;
	_life_experimental(w: number, h: number): number;
}

let Life: WasmInstantiatedModule<ILifeWASM>;
WasmModule.instance<ILifeWASM>('./life.wasm', {
	maxTableSize: 6,
	staticBump: 2098736,
}).then(res => {
	Life = res;

	reset();
});

const d = document;
const c = d.getElementById('screen') as HTMLCanvasElement;
const ctx = c.getContext('2d');
const gl = c.getContext('webgl2');
const sizeWarning = d.getElementById('size-warning');
const stagnantWarning = d.getElementById('stagnant-warning');
const deadWarning = d.getElementById('dead-warning');
const btnContainer = d.getElementById('quick-change-container');

let CELL_SIZE = 2;
const BOX_SIZE = 4;

const isNumeric = (c: string) => '0123456789'.includes(c);

interface IGlobal {
	MAX_SIZE: number;
	MaxHue: number;
	all: Int16Array;
	boardCopy?: TypedArray;
	box?: Int16Array;
	boxen: Int16Array;
	h: number;
	w: number;
	x: number;
	y: number;

	debug: boolean;
	experimental: boolean;
}

const Global: IGlobal = {
	MAX_SIZE: 0,
	MaxHue: 0,
	all: undefined,
	boardCopy: undefined,
	box: undefined,
	boxen: undefined,
	h: 0,
	w: 0,
	x: 0,
	y: 0,

	debug: true,
	experimental: true,
};

class Rect {
	private readonly left: number;
	private readonly top: number;
	private readonly right: number;
	private readonly bottom: number;

	public constructor(arr: Int16Array) {
		[this.left, this.top, this.right, this.bottom] = arr;
	}

	public contains(x: number, y: number) {
		return x >= this.left && x <= this.right && y >= this.top && y <= this.bottom;
	}
}

abstract class AbstractBoard {
	public abstract board: TypedArray;
	public hash: number = 0;

	public constructor(public w: number, public h: number) {}

	public get l() {
		return this.w * this.h;
	}

	public abstract isStagnant(): boolean;

	public setSize(w: number, h: number) {
		this.w = w;
		this.h = h;
	}

	protected idxFromPos(x: number, y: number) {
		return (y * this.w) + x;
	}
}

class Board extends AbstractBoard {
	public static readonly prebuilt = new Map<string, () => Promise<Board>>();

	private static readonly prebuiltCache = new Map<string, Board>();

	private static readonly MAX_HIST_STATES = 1000;
	private static history: number[] = [];

	public static create(name: string, file: string, cellSize: number = 2) {
		const kebabCase = name.toLowerCase().replace(/\s+/g, '-');

		if (file.startsWith('./') || file.startsWith('../')) {
			this.prebuilt.set(
				kebabCase,
				Board.defer(Board.parse, file, kebabCase, cellSize),
			);
		} else {
			this.prebuilt.set(
				kebabCase,
				Board.defer(Board.from, file, kebabCase, cellSize),
			);
		}

		const btn = d.createElement('button');
		btn.setAttribute('data-pattern', kebabCase);

		btn.appendChild(d.createTextNode(name));

		if (location.hash.substring(1) === kebabCase) {
			btn.classList.add('active');
		}

		btnContainer.appendChild(btn);
	}

	public static clearHistory() {
		this.history.length = 0;
	}

	private static defer(func: (...a) => Board | Promise<Board>, ...a) {
		return async () => await func(this, ...a);
	}

	private static async parse(
		self: typeof Board,
		file: string,
		name: string,
		cellSize: number,
	) {
		CELL_SIZE = Math.max(cellSize, 1.5); // tslint:disable-line:no-magic-numbers

		if (self.prebuiltCache.has(name)) {
			return self.prebuiltCache.get(name);
		}

		const loadingModal = document.getElementById('loading');
		loadingModal.removeAttribute('hidden');

		const resp = await fetch(file);

		loadingModal.setAttribute('hidden', 'true');

		if (!resp.ok) {
			return Board.from(self, '', name, cellSize);
		}

		const txt = (await resp.text()).trim();

		if (file.endsWith('.lif')) {
			return Board.from(self, txt, name, cellSize);
		} else if (file.endsWith('.rle')) {
			return Board.from(self, Board.expandRLE(txt), name, cellSize);
		}

		return Board.from(self, '', name, cellSize);
	}

	private static expandRLE(rle: string) {
		let res = '';

		const lines = rle.split('\n');

		let x = 0;
		let y = 0;

		for (const line of lines) {
			if (line.startsWith('#')) { // comment
				continue;
			} else if (line.startsWith('x')) {
				const [, _x, _y] = line.match(/x ?= ?(\d+), ?y ?= ?(\d+)/);

				x = Number.parseInt(_x, 10); // tslint:disable-line:no-magic-numbers
				y = Number.parseInt(_y, 10); // tslint:disable-line:no-magic-numbers
			} else if (x && y) {
				let idx = 0;
				let count = 1;

				while (idx < line.length) {
					let c = line.charAt(idx++);

					/* tslint:disable:prefer-switch */
					if (isNumeric(c)) {
						let n = c;

						while (idx < line.length && isNumeric((c = line.charAt(idx)))) {
							n += c;
							idx++;
						}

						count = Number.parseInt(n, 10); // tslint:disable-line:no-magic-numbers
					} else if (c === 'b') {
						res += '.'.repeat(count);
						count = 1;
					} else if (c === 'o') {
						res += '*'.repeat(count);
						count = 1;
					} else if (c === '$') {
						res += '\n'.repeat(count);
						count = 1;
					} else if (c === '!') {
						break;
					}
					/* tslint:enable:prefer-switch */
				}

				if (line.endsWith('!')) {
					break;
				}
			}
		}

		return res;
	}

	private static from(
		self: typeof Board,
		str: string,
		name: string,
		cellSize: number,
	) {
		CELL_SIZE = Math.max(cellSize, 1.5); // tslint:disable-line:no-magic-numbers

		if (self.prebuiltCache.has(name)) {
			return self.prebuiltCache.get(name);
		}

		const lines = str.replace(/[#!].*\n/g, '').split('\n');

		const h = lines.length;
		const w = Math.max(...lines.map(l => l.length));

		const board = new Board(w, h);

		for (let y = 0; y < h; y++) {
			const line = lines[y];
			const l = line.length;

			for (let x = 0; x < w; x++) {
				if (x < l && line.charAt(x) !== '.') {
					board.set(x, y, 1);
				}
			}
		}

		self.prebuiltCache.set(name, board);

		return board;
	}

	public board: TypedArray = new Uint8Array(this.l);

	public get(x: number, y: number) {
		return this.board[this.idxFromPos(x, y)];
	}

	public set(x: number, y: number, val: number) {
		this.board[this.idxFromPos(x, y)] = val;
	}

	public clear() {
		Life._clear(this.l);
	}

	public createLife(x: number, y: number) {
		this.hash = Life._createLife(x, y);
	}

	public copyBoard() {
		let Arr: TypedArrayConstructor;

		switch (Life._getElementSize()) {
			/* tslint:disable:no-magic-numbers */
			case 4: Arr = Uint32Array; break;
			case 2: Arr = Uint16Array; break;
			case 1: /* falls through */
			default: Arr = Uint8Array;
			/* tslint:enable:no-magic-numbers */
		}

		const copy = new Arr(this.l);

		for (let i = 0; i < this.l; i++) {
			copy[i] = this.board[i];
		}

		return copy;
	}

	public restoreBoard(board: TypedArray) {
		for (let i = 0; i < this.l; i++) {
			this.board[i] = board[i];
		}
	}

	public visualizeDiff(otherBoard: TypedArray) {
		for (let x = 0; x < this.w; x++) {
			for (let y = 0; y < this.h; y++) {
				const idx = (y * this.w) + x;

				const v1 = this.board[idx];
				const v2 = otherBoard[idx];

				let color: string;

				if (v1 && v2 && v1 === v2) {
					color = '#0000ff80';
				} else if (v1 && !v2) {
					color = '#ff0000';
				} else if (v2 && !v1) {
					color = '#00ff00';
				}

				if (color) {
					ctx.fillStyle = color;
					ctx.fillRect(
						x * CELL_SIZE,
						y * CELL_SIZE,
						CELL_SIZE,
						CELL_SIZE,
					);
				}
			}
		}
	}

	public resize(w: number = this.w, h: number = this.h) {
		if (!this.board.length) {
			let Arr: TypedArrayConstructor;

			switch (Life._getElementSize()) {
				/* tslint:disable:no-magic-numbers */
				case 4: Arr = Uint32Array; break;
				case 2: Arr = Uint16Array; break;
				case 1: /* falls through */
				default: Arr = Uint8Array;
				/* tslint:enable:no-magic-numbers */
			}

			this.board = new Arr(Life.buffer, Life._getBoard(), Life._getMaxSize());
		}

		this.w = w;
		this.h = h;
		Life._resize(w, h);
	}

	public isStagnant() {
		const hist = Board.history;

		let matches = 0;

		// Since comparing numbers is faster than comparing ArrayBuffers, check for
		// multiple matches instead of one
		for (const hash of hist) {
			if (this.hash === hash) {
				// tslint:disable-next-line:no-magic-numbers
				if (++matches === 3) return true;
			}
		}

		hist.push(this.hash);

		if (hist.length > Board.MAX_HIST_STATES) {
			hist.shift();
		}

		return false;
	}

	public center(other: Board) {
		const { w, h } = other;

		const dx = Math.floor((this.w - w) / 2);
		const dy = Math.floor((this.h - h) / 2);

		for (let x = 0; x < w; x++) {
			for (let y = 0; y < h; y++) {
				this.set(x + dx, y + dy, other.get(x, y) ? 1 : 0);
			}
		}
	}

	public rotate() {
		const { w, h } = this;

		const res = new Board(h, w);

		const l = w * h;
		for (let i = 0; i < l; i++) {
			const y = i % this.w;
			const x = Math.floor(i / this.w);

			res.set(x, y, this.board[i]);
		}

		return res;
	}

	public render(ctx: CanvasRenderingContext2D) {
		const { MaxHue } = Global;

		const [left, top, right, bottom] = Global.all;

		for (let x = left; x <= right; x++) {
			for (let y = top; y <= bottom; y++) {
				const v = this.board[this.idxFromPos(x, y)];
				if (v) {
					ctx.fillStyle = `hsl(${Math.max(MaxHue - v, 0)}deg, 100%, 50%)`;
					ctx.fillRect(
						(x * CELL_SIZE),
						(y * CELL_SIZE),
						CELL_SIZE,
						CELL_SIZE,
					);
				}
			}
		}
	}
}
/* tslint:disable:max-line-length no-magic-numbers */
Board.create('Acorn', '..*\n....*\n.**..***', 4);
Board.create('Backrake 1', '../patterns/backrake1.lif');
Board.create('Backrake 2', '../patterns/backrake2.lif');
Board.create('Backrake 3', '../patterns/backrake3.lif');
Board.create('Barbershop', '../patterns/barbershop.rle', 1.5);
Board.create('Barge', '../patterns/barge.rle', 4);
Board.create('Barge 2', '../patterns/barge2.rle', 4);
Board.create('Birthday Puffer', '../patterns/birthdaypuffer.rle');
Board.create('Blinker 1', '../patterns/blinker1.lif');
Board.create('Blinker 2', '../patterns/blinker2.lif');
Board.create('Blockstacker', '../patterns/blockstacker.rle', 1.5);
Board.create('Boojum Reflector', '../patterns/boojumreflector.rle');
Board.create('Die Hard', '../patterns/methuselah.rle', 8);
Board.create('Die Hard 1192', '../patterns/diehard1192.rle', 4);
Board.create('Die Hard 1638', '../patterns/diehard1638.rle', 4);
Board.create('Fermat Prime Calculator', '../patterns/fermatprimecalc.rle', 1.5);
Board.create('Frothing', '../patterns/frothing.lif');
Board.create('Glider Switch', '../patterns/gliderswitch.lif');
Board.create('Glider Train', '../patterns/glidertrain.rle');
Board.create('Gosper', '../patterns/gosper.rle', 8);
Board.create('Halfmax', '../patterns/halfmax.rle', 4);
Board.create('Halfmax V2', '../patterns/halfmaxv2.rle', 4);
Board.create('Halfmax V3', '../patterns/halfmaxv3.rle', 4);
Board.create('Hivenudger', '../patterns/hivenudger.rle', 4);
Board.create('Hivenudger 2', '../patterns/hivenudger2.lif');
Board.create('Hivenudgers', '../patterns/hivenudgers.rle');
Board.create('Indefinite Growth 1', '......*\n....*.**\n....*.*\n....*\n..*\n*.*', 1.5);
Board.create('Indefinite Growth 2', '***.*\n*\n...**\n.**.*\n*.*.*', 1.5);
Board.create('Indefinite Growth 3', '********.*****...***......*******.*****', 1.5);
Board.create('Max', '../patterns/max.rle', 4);
Board.create('Maximum Volatility Gun', '../patterns/maxvolatilitygun.rle', 1.5);
Board.create('Mosquito', '../patterns/mosquito.rle');
Board.create("Noah's Ark", '../patterns/noahsark.lif');
Board.create('Line Puffer', '../patterns/linepuffer.rle', 1.5);
Board.create('Lobster', '../patterns/lobster.rle', 4);
Board.create('P-52 Glider Gun', '../patterns/p52glidergun.rle');
Board.create('P-15 Pre-Pulsar Spaceship', '../patterns/p15prepulsarspaceship.rle');
Board.create('P-15 Pre-Pulsar Spaceship (Original)', '../patterns/p15prepulsarspaceshiporig.rle');
Board.create('Puffer 1', '../patterns/puffer1.lif');
Board.create('Puffer 2', '../patterns/puffer2.lif');
Board.create('Puffer Fish', '../patterns/pufferfish.lif');
Board.create('R-Pentomino', '..**\n.**\n..*');
Board.create('Sir Robin', '../patterns/sirrobin.rle', 3);
Board.create('Slow Puffer 1', '../patterns/slowpuffer1.lif', 1.5);
Board.create('Slow Puffer 2', '../patterns/slowpuffer2.lif', 1.5);
Board.create('Space Rake', '../patterns/spacerake.lif');
Board.create('Spacefiller', '../patterns/spacefiller.rle', 4);
Board.create('Spaghetti Monster', '../patterns/spaghettimonster.rle', 4);
Board.create('Spaghetti Monsters', '../patterns/spaghettimonsters.rle', 4);
Board.create('Switch Engine', '../patterns/switchengine.rle');
Board.create('Switchwave', '../patterns/switchwave.rle', 4);
Board.create('Teeth', '../patterns/teeth.rle');
Board.create('Time Bomb', '../patterns/timebomb.rle', 4);
Board.create('Tri-Engine Cordership Rake', '../patterns/trienginecordershiprake.rle');
Board.create('6-in-a-row Cordership', '../patterns/6inarowcordership.rle', 4);
Board.create('40 Engine Cordership', '../patterns/40enginecordership.rle');
Board.create('Weekender', '../patterns/weekender.lif', 8);
Board.create('Wickstretcher', '../patterns/wickstretcher.rle');
Board.create('Wing', '../patterns/wing.rle', 4);
/* tslint:enable:max-line-length no-magic-numbers */

const board: Board = new Board(0, 0);
let last: number;
let active = false;
let dead = false;
let stagnant = false;

function calcSettings() {
	if (!Global.MAX_SIZE) {
		Global.MAX_SIZE = Life._getMaxSize();
	}
	if (!Global.MaxHue) {
		Global.MaxHue = Life._getMaxHue();
	}
	if (!Global.all) {
		Global.all = new Int16Array(Life.buffer, Life._getAllLife(), BOX_SIZE);
	}
	if (!Global.box) {
		Global.box = new Int16Array(Life.buffer, Life._getBox(), BOX_SIZE);
	}
	if (!Global.boxen) {
		Global.boxen = new Int16Array(
			Life.buffer,
			Life._getBoxen(),
			Life._getMaxBoxen() * BOX_SIZE,
		);
	}

	Global.w = window.innerWidth;
	Global.h = window.innerHeight;

	Global.x = Math.floor(Global.w / CELL_SIZE);
	Global.y = Math.floor(Global.h / CELL_SIZE);
}

function resizeEverything() {
	calcSettings();

	c.width = Global.w;
	c.height = Global.h;

	if (Global.x * Global.y > Global.MAX_SIZE) {
		sizeWarning.removeAttribute('hidden');
	} else {
		sizeWarning.setAttribute('hidden', 'true');

		board.resize(Global.x, Global.y);
	}
}

function reset() {
	if (typeof last !== 'undefined') window.cancelAnimationFrame(last);
	resizeEverything();

	dead = false;
	stagnant = false;

	stagnantWarning.setAttribute('hidden', 'true');
	deadWarning.setAttribute('hidden', 'true');

	Board.clearHistory();

	const hash = location.hash.substring(1);
	const { w, h } = board;

	if (Board.prebuilt.has(hash)) {
		active = false;

		Board.prebuilt.get(hash)().then(pre => {
			resizeEverything();
			board.clear();
			// Life._clear(Global.x * Global.y);

			if (w < h) {
				pre = pre.rotate();
			}

			board.center(pre);

			last = window.requestAnimationFrame(firstDraw);
		});
	} else {
		CELL_SIZE = 2;
		resizeEverything();

		board.createLife(Global.x, Global.y);
		// board.hash = Life._createLife(Global.x, Global.y);

		last = window.requestAnimationFrame(firstDraw);
	}
}

abstract class Singleton {
	protected constructor() {}
}

/** FPS Counter */
class FPS extends Singleton {
	private static _instance: FPS;

	private static frameCount = 10;

	public static get instance() {
		if (!this._instance) {
			this._instance = new FPS();
		}

		return this._instance;
	}

	private readonly hist: number[] = Array(FPS.frameCount).fill(0);

	private readonly size = 12;

	private lastFrame: number = 0;

	/* tslint:disable:no-magic-numbers */
	private get hPadding() { return this.size / 5; }
	private get vPadding() { return this.size / 5; }

	private get w() { return this.size * 6 + this.hPadding; }
	private get h() { return this.size + this.vPadding; }
	private get cw() { return this.w / 2; }
	private get ch() { return this.h - this.vPadding; }
	/* tslint:enable:no-magic-numbers */

	/**
	 * Render the FPS Counter
	 *
	 * @param   {number}        time Duration between frames
	 * @param   {CanvasContext} ctx  Canvas context to render on
	 * @returns {void}
	 */
	public render(time: number, ctx: CanvasRenderingContext2D) {
		const { hist, size, w, h, cw, ch } = this;
		const diff = time - this.lastFrame;
		this.lastFrame = time;
		const renderTime = 1000 / diff; // tslint:disable-line:no-magic-numbers
		if (renderTime) {
			hist.push(renderTime);
			if (hist.length > FPS.frameCount) {
				hist.shift();
			}
		}
		// Calculate framerate over last n frames
		const framerate = hist.reduce((acc, h) => acc + h, 0) / hist.length;
		ctx.save();
		ctx.resetTransform();
		ctx.shadowBlur = 0;
		ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
		ctx.fillRect(0, 0, w, h);
		ctx.fillStyle = 'rgb(255,255,255)';
		ctx.font = `${size}px monospace`;
		ctx.fillText(`${framerate.toFixed(2)} FPS`, cw, ch);
		ctx.restore();
	}
}

function drawBB(
	ctx: CanvasRenderingContext2D,
	[left, top, right, bottom]: TypedArray,
	color: string = '#fff'
) {
	ctx.strokeStyle = color;
	ctx.strokeRect(
		left * CELL_SIZE,
		top * CELL_SIZE,
		((right - left) + 1) * CELL_SIZE,
		((bottom - top) + 1) * CELL_SIZE,
	);
}

function draw(now: number, skip: boolean = false) {
	/* tslint:disable:max-line-length */
	if (!skip) {
		if (Global.experimental) { // tslint:disable-line:prefer-conditional-expression max-line-length
			board.hash = Life._life_experimental(board.w, board.h);
			if (!Global.debug) {
				board.hash = Life._life_experimental(board.w, board.h);
			} else {
				// TODO: Attempt to migrate to GPU
				const copy = board.copyBoard();
				Life._life_experimental(board.w, board.h);
				const copy2 = board.copyBoard();
				board.restoreBoard(copy);
				board.hash = Life._life(board.w, board.h);

				for (let i = 0; i < board.l; i++) {
					if (!!copy2[i] !== !!board.board[i]) {
						ctx.clearRect(0, 0, Global.w, Global.h);
						drawBB(ctx, Global.all, '#ff000040');
						drawBB(ctx, Global.box, '#00ffff40');

						const numBoxen = Life._getNumBoxen();
						for (let i = 0; i < numBoxen; i++) {
							const start = i * BOX_SIZE;
							drawBB(ctx, Global.boxen.subarray(start, start + BOX_SIZE), '#00ff0040');
						}
						board.visualizeDiff(copy2);

						return;
					}
				}
			}
		} else {
			board.hash = Life._life(board.w, board.h);
		}

		if (board.hash !== 0) { // board has life
			if (dead) {
				dead = false;
				deadWarning.removeAttribute('hidden');
			}

			const boardIsStagnant = board.isStagnant();

			if (!stagnant && boardIsStagnant) {
				stagnant = true;
				stagnantWarning.removeAttribute('hidden');
			} else if (!stagnant) {
				stagnantWarning.setAttribute('hidden', 'true');
			} else if (stagnant && !boardIsStagnant) {
				stagnant = false;
				stagnantWarning.setAttribute('hidden', 'true');
			}
		} else if (!dead) {
			dead = true;
			deadWarning.removeAttribute('hidden');
		}
	}
	/* tslint:enable:max-line-length */

	/** Draw ops follow */
	ctx.save();
	const { w, h, x, y/*, all: [left, top, right, bottom]*/ } = Global;

	const l = Math.floor((w - (x * CELL_SIZE)) / 2);
	const t = Math.floor((h - (y * CELL_SIZE)) / 2);

	// if (!Global.debug) {
	// 	ctx.clearRect(
	// 		left * CELL_SIZE,
	// 		top * CELL_SIZE,
	// 		((right - left) + 1) * CELL_SIZE,
	// 		((bottom - top) + 1) * CELL_SIZE
	// 	);
	// } else {
	// 	ctx.clearRect(0, 0, w, h);
	// }
	ctx.clearRect(0, 0, w, h);

	ctx.translate(l, t);

	board.render(ctx);

	if (Global.debug) {
		drawBB(ctx, Global.all, '#f00');
		drawBB(ctx, Global.box, '#0ff');

		const numBoxen = Life._getNumBoxen();
		for (let i = 0; i < numBoxen; i++) {
			const start = i * BOX_SIZE;
			drawBB(ctx, Global.boxen.subarray(start, start + BOX_SIZE), '#0f0');
		}
	}

	// if (Global.debug || Global.experimental) {
	// 	FPS.instance.render(now, ctx);
	// }
	FPS.instance.render(now, ctx);

	ctx.restore();
	/** Draw ops end */
	// if (Global.all[0] >= 1 && Global.all[3] <= 330) {
	last = window.requestAnimationFrame(draw);
	// } else {
	// setTimeout(() => {
	// last = window.requestAnimationFrame(draw);
	// }, 1000);
	// }
}

function firstDraw(now: number) {
	if (active) {
		return;
	}

	ctx.clearRect(0, 0, Global.w, Global.h);
	ctx.textAlign = 'center';

	active = true;
	draw(now, true);
}

window.addEventListener('resize', resizeEverything, { passive: true });

window.addEventListener('visibilitychange', () => {
	if (d.hidden) {
		window.cancelAnimationFrame(last);
		active = false;
	} else {
		last = window.requestAnimationFrame(firstDraw);
	}
});

window.addEventListener('keydown', (e: KeyboardEvent) => {
	const { key } = e;
	if (e.ctrlKey && (key === '=' || key === '-')) {
		e.preventDefault();
		e.stopPropagation();
		e.stopImmediatePropagation();
	}
});

window.addEventListener(
	'wheel',
	(e: WheelEvent) => {
		if (e.ctrlKey) {
			e.preventDefault();
			e.stopPropagation();
			e.stopImmediatePropagation();
		}
	},
	{ passive: false },
);

window.addEventListener('click', (e: MouseEvent) => {
	for (const el of e.composedPath()) {
		if (el instanceof HTMLElement) {
			if (el.hasAttribute('data-pattern')) {
				const active = d.querySelector('button[data-pattern].active');
				if (active) {
					active.classList.remove('active');
				}

				e.preventDefault();
				e.stopPropagation();

				const pattern = el.getAttribute('data-pattern');
				if (location.hash.substring(1) !== pattern) {
					location.hash = pattern;
				}

				reset();

				el.classList.add('active');

				return;
			} else if (el.hasAttribute('data-dismiss')) {
				const dismiss = d.getElementById(el.getAttribute('data-dismiss'));

				if (dismiss) {
					dismiss.setAttribute('hidden', 'true');
				}

				return;
			} else if (el.hasAttribute('data-toggle-value')) {
				const prop = el.getAttribute('data-toggle-value');

				if (prop in Global) {
					const on = !Global[prop];
					Global[prop] = on;

					if (el.hasAttribute('data-text-on')) {
						const text = el.getAttribute('data-text-on');

						if (on) {
							el.textContent += text;
						} else {
							el.textContent = el.textContent.slice(0, -text.length);
						}
					}
				}

				return;
			}
		}
	}
});

document.querySelectorAll('[data-toggle-value][data-text-on]').forEach(el => {
	const prop = el.getAttribute('data-toggle-value');

	if (Global[prop]) {
		el.textContent += el.getAttribute('data-text-on');
	}
});
