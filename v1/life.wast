(module
 (type $FUNCSIG$i (func (result i32)))
 (type $FUNCSIG$vi (func (param i32)))
 (type $FUNCSIG$v (func))
 (type $FUNCSIG$ii (func (param i32) (result i32)))
 (import "env" "memory" (memory $0 256 256))
 (import "env" "DYNAMICTOP_PTR" (global $DYNAMICTOP_PTR$asm2wasm$import i32))
 (import "env" "STACKTOP" (global $STACKTOP$asm2wasm$import i32))
 (import "env" "STACK_MAX" (global $STACK_MAX$asm2wasm$import i32))
 (import "env" "enlargeMemory" (func $enlargeMemory (result i32)))
 (import "env" "getTotalMemory" (func $getTotalMemory (result i32)))
 (import "env" "abortOnCannotGrowMemory" (func $abortOnCannotGrowMemory (result i32)))
 (import "env" "___setErrNo" (func $___setErrNo (param i32)))
 (import "env" "_abort" (func $_abort))
 (import "env" "_time" (func $_time (param i32) (result i32)))
 (global $DYNAMICTOP_PTR (mut i32) (get_global $DYNAMICTOP_PTR$asm2wasm$import))
 (global $STACKTOP (mut i32) (get_global $STACKTOP$asm2wasm$import))
 (global $STACK_MAX (mut i32) (get_global $STACK_MAX$asm2wasm$import))
 (global $__THREW__ (mut i32) (i32.const 0))
 (global $threwValue (mut i32) (i32.const 0))
 (global $tempRet0 (mut i32) (i32.const 0))
 (export "__GLOBAL__sub_I_life_cpp" (func $__GLOBAL__sub_I_life_cpp))
 (export "_clear" (func $_clear))
 (export "_createLife" (func $_createLife))
 (export "_free" (func $_free))
 (export "_getAllLife" (func $_getAllLife))
 (export "_getBoard" (func $_getBoard))
 (export "_getBox" (func $_getBox))
 (export "_getBoxen" (func $_getBoxen))
 (export "_getElementSize" (func $_getElementSize))
 (export "_getMaxBoxen" (func $_getMaxBoxen))
 (export "_getMaxHue" (func $_getMaxHue))
 (export "_getMaxSize" (func $_getMaxSize))
 (export "_getNumBoxen" (func $_getNumBoxen))
 (export "_life" (func $_life))
 (export "_life_experimental" (func $_life_experimental))
 (export "_malloc" (func $_malloc))
 (export "_memset" (func $_memset))
 (export "_resize" (func $_resize))
 (export "_sbrk" (func $_sbrk))
 (export "establishStackSpace" (func $establishStackSpace))
 (export "getTempRet0" (func $getTempRet0))
 (export "runPostSets" (func $runPostSets))
 (export "setTempRet0" (func $setTempRet0))
 (export "setThrew" (func $setThrew))
 (export "stackAlloc" (func $stackAlloc))
 (export "stackRestore" (func $stackRestore))
 (export "stackSave" (func $stackSave))
 (func $stackAlloc (; 6 ;) (; has Stack IR ;) (param $0 i32) (result i32)
  (local $1 i32)
  (set_local $1
   (get_global $STACKTOP)
  )
  (set_global $STACKTOP
   (i32.add
    (get_global $STACKTOP)
    (get_local $0)
   )
  )
  (set_global $STACKTOP
   (i32.and
    (i32.add
     (get_global $STACKTOP)
     (i32.const 15)
    )
    (i32.const -16)
   )
  )
  (get_local $1)
 )
 (func $stackSave (; 7 ;) (; has Stack IR ;) (result i32)
  (get_global $STACKTOP)
 )
 (func $stackRestore (; 8 ;) (; has Stack IR ;) (param $0 i32)
  (set_global $STACKTOP
   (get_local $0)
  )
 )
 (func $establishStackSpace (; 9 ;) (; has Stack IR ;) (param $0 i32) (param $1 i32)
  (set_global $STACKTOP
   (get_local $0)
  )
  (set_global $STACK_MAX
   (get_local $1)
  )
 )
 (func $setThrew (; 10 ;) (; has Stack IR ;) (param $0 i32) (param $1 i32)
  (if
   (i32.eqz
    (get_global $__THREW__)
   )
   (block
    (set_global $__THREW__
     (get_local $0)
    )
    (set_global $threwValue
     (get_local $1)
    )
   )
  )
 )
 (func $setTempRet0 (; 11 ;) (; has Stack IR ;) (param $0 i32)
  (set_global $tempRet0
   (get_local $0)
  )
 )
 (func $getTempRet0 (; 12 ;) (; has Stack IR ;) (result i32)
  (get_global $tempRet0)
 )
 (func $__Z21collapseBoundingBoxent (; 13 ;) (; has Stack IR ;) (param $0 i32) (result i32)
  (local $1 i32)
  (local $2 i32)
  (local $3 i32)
  (local $4 i32)
  (local $5 i32)
  (local $6 i32)
  (local $7 i32)
  (local $8 i32)
  (local $9 i32)
  (local $10 i32)
  (local $11 i32)
  (local $12 i64)
  (local $13 i32)
  (local $14 i32)
  (local $15 i32)
  (local $16 i32)
  (local $17 i32)
  (local $18 i32)
  (local $19 i32)
  (local $20 i32)
  (local $21 i32)
  (local $22 i32)
  (local $23 i32)
  (local $24 i32)
  (local $25 i32)
  (local $26 i32)
  (local $27 i32)
  (local $28 i32)
  (local $29 i32)
  (local $30 i32)
  (local $31 i32)
  (local $32 i32)
  ;;@ life.cpp:191:0
  (set_local $7
   (i32.load16_s
    (i32.const 2229776)
   )
  )
  (if
   (i32.le_s
    (i32.and
     (get_local $7)
     (i32.const 65535)
    )
    (i32.and
     (get_local $0)
     (i32.const 65535)
    )
   )
   ;;@ life.cpp:218:0
   (return
    (get_local $7)
   )
  )
  (set_local $31
   (i32.and
    (get_local $0)
    (i32.const 65535)
   )
  )
  (block $__rjto$1 (result i32)
   (block $__rjti$1
    (loop $while-in
     (block $while-out
      ;;@ life.cpp:200:0
      (br_if $__rjti$1
       (i32.le_s
        (i32.and
         (get_local $7)
         (i32.const 65535)
        )
        (i32.and
         (get_local $0)
         (i32.const 65535)
        )
       )
      )
      (set_local $3
       (i32.const 0)
      )
      (set_local $11
       (get_local $31)
      )
      (loop $while-in1
       (set_local $14
        (i32.and
         (get_local $7)
         (i32.const 65535)
        )
       )
       ;;@ life.cpp:201:0
       (set_local $6
        (i32.shr_s
         (i32.shl
          (i32.add
           (get_local $7)
           (i32.const -1)
          )
          (i32.const 16)
         )
         (i32.const 16)
        )
       )
       (set_local $6
        (i32.and
         (get_local $6)
         (i32.const 65535)
        )
       )
       (if
        (i32.lt_u
         (get_local $11)
         (get_local $6)
        )
        (block
         (set_local $14
          (i32.shr_s
           (i32.shl
            (i32.add
             (get_local $7)
             (i32.const -1)
            )
            (i32.const 16)
           )
           (i32.const 16)
          )
         )
         (set_local $26
          (i32.add
           (i32.shl
            (get_local $11)
            (i32.const 3)
           )
           (i32.const 2098176)
          )
         )
         (set_local $28
          (i32.add
           (i32.shl
            (get_local $11)
            (i32.const 3)
           )
           (i32.const 2098180)
          )
         )
         (set_local $29
          (i32.add
           (i32.shl
            (get_local $11)
            (i32.const 3)
           )
           (i32.const 2098178)
          )
         )
         (set_local $30
          (i32.add
           (i32.shl
            (get_local $11)
            (i32.const 3)
           )
           (i32.const 2098182)
          )
         )
         (set_local $6
          (tee_local $14
           (i32.and
            (get_local $14)
            (i32.const 65535)
           )
          )
         )
         (loop $while-in3
          ;;@ life.cpp:202:0
          (set_local $8
           (i32.add
            (i32.shl
             (get_local $6)
             (i32.const 3)
            )
            (i32.const 2098176)
           )
          )
          (set_local $16
           (i32.and
            (i32.wrap/i64
             (tee_local $12
              (i64.load
               (get_local $8)
              )
             )
            )
            (i32.const 65535)
           )
          )
          (set_local $22
           (i32.and
            (i32.wrap/i64
             (i64.shr_u
              (get_local $12)
              (i64.const 16)
             )
            )
            (i32.const 65535)
           )
          )
          (set_local $8
           (i32.and
            (i32.wrap/i64
             (i64.shr_u
              (get_local $12)
              (i64.const 32)
             )
            )
            (i32.const 65535)
           )
          )
          (set_local $13
           (i32.and
            (i32.wrap/i64
             (i64.shr_u
              (get_local $12)
              (i64.const 48)
             )
            )
            (i32.const 65535)
           )
          )
          ;;@ life.cpp:83:0
          (set_local $17
           (i32.load16_s
            (get_local $26)
           )
          )
          (set_local $23
           (i32.load16_s
            (get_local $28)
           )
          )
          ;;@ life.cpp:41:0
          (set_local $1
           (i32.le_s
            (i32.and
             (get_local $17)
             (i32.const 65535)
            )
            (get_local $16)
           )
          )
          (set_local $2
           (i32.ge_s
            (i32.and
             (get_local $23)
             (i32.const 65535)
            )
            (get_local $16)
           )
          )
          (set_local $24
           (i32.and
            (get_local $1)
            (get_local $2)
           )
          )
          ;;@ life.cpp:84:0
          (set_local $18
           (i32.load16_s
            (get_local $29)
           )
          )
          (set_local $25
           (i32.load16_s
            (get_local $30)
           )
          )
          ;;@ life.cpp:41:0
          (set_local $4
           (i32.le_s
            (i32.and
             (get_local $18)
             (i32.const 65535)
            )
            (get_local $22)
           )
          )
          (set_local $5
           (i32.ge_s
            (i32.and
             (get_local $25)
             (i32.const 65535)
            )
            (get_local $22)
           )
          )
          (set_local $9
           (i32.and
            (get_local $4)
            (get_local $5)
           )
          )
          (set_local $19
           (i32.le_s
            (i32.and
             (get_local $17)
             (i32.const 65535)
            )
            (get_local $8)
           )
          )
          (set_local $10
           (i32.ge_s
            (i32.and
             (get_local $23)
             (i32.const 65535)
            )
            (get_local $8)
           )
          )
          (set_local $27
           (i32.and
            (get_local $19)
            (get_local $10)
           )
          )
          (set_local $15
           (i32.le_s
            (i32.and
             (get_local $18)
             (i32.const 65535)
            )
            (get_local $13)
           )
          )
          (set_local $20
           (i32.ge_s
            (i32.and
             (get_local $25)
             (i32.const 65535)
            )
            (get_local $13)
           )
          )
          (set_local $21
           (i32.and
            (get_local $15)
            (get_local $20)
           )
          )
          ;;@ life.cpp:89:0
          (set_local $1
           (i32.and
            (get_local $1)
            (get_local $2)
           )
          )
          (set_local $1
           (i32.xor
            (get_local $1)
            (i32.const 1)
           )
          )
          (set_local $2
           (i32.and
            (get_local $4)
            (get_local $5)
           )
          )
          (set_local $2
           (i32.xor
            (get_local $2)
            (i32.const 1)
           )
          )
          (block $__rjto$0
           (block $__rjti$0
            (br_if $__rjti$0
             (i32.eqz
              (i32.or
               (get_local $1)
               (get_local $2)
              )
             )
            )
            (set_local $4
             (i32.and
              (get_local $15)
              (get_local $20)
             )
            )
            (set_local $4
             (i32.xor
              (get_local $4)
              (i32.const 1)
             )
            )
            (br_if $__rjti$0
             (i32.eqz
              (i32.or
               (get_local $1)
               (get_local $4)
              )
             )
            )
            (set_local $5
             (i32.and
              (get_local $19)
              (get_local $10)
             )
            )
            (set_local $5
             (i32.xor
              (get_local $5)
              (i32.const 1)
             )
            )
            (set_local $2
             (i32.and
              (get_local $2)
              (get_local $4)
             )
            )
            (br_if $__rjti$0
             (i32.eqz
              (i32.or
               (get_local $2)
               (get_local $5)
              )
             )
            )
            (if
             ;;@ life.cpp:90:0
             (i32.or
              (get_local $1)
              (get_local $5)
             )
             (block
              (set_local $1
               (i32.or
                (get_local $24)
                (get_local $27)
               )
              )
              (set_local $1
               (i32.and
                (get_local $1)
                (get_local $21)
               )
              )
              (br_if $__rjti$0
               (i32.and
                (get_local $9)
                (get_local $1)
               )
              )
             )
             (br_if $__rjti$0
              (i32.or
               (get_local $9)
               (get_local $21)
              )
             )
            )
            ;;@ life.cpp:202:0
            (set_local $1
             (i32.and
              (i32.wrap/i64
               (tee_local $12
                (i64.load
                 (get_local $26)
                )
               )
              )
              (i32.const 65535)
             )
            )
            (set_local $2
             (i32.and
              (i32.wrap/i64
               (i64.shr_u
                (get_local $12)
                (i64.const 16)
               )
              )
              (i32.const 65535)
             )
            )
            (set_local $4
             (i32.and
              (i32.wrap/i64
               (i64.shr_u
                (get_local $12)
                (i64.const 32)
               )
              )
              (i32.const 65535)
             )
            )
            (set_local $5
             (i32.and
              (i32.wrap/i64
               (i64.shr_u
                (get_local $12)
                (i64.const 48)
               )
              )
              (i32.const 65535)
             )
            )
            ;;@ life.cpp:83:0
            (set_local $9
             (i32.add
              (i32.shl
               (get_local $6)
               (i32.const 3)
              )
              (i32.const 2098180)
             )
            )
            (set_local $9
             (i32.load16_u
              (get_local $9)
             )
            )
            ;;@ life.cpp:41:0
            (set_local $19
             (i32.le_s
              (get_local $16)
              (get_local $1)
             )
            )
            (set_local $1
             (i32.ge_s
              (i32.and
               (get_local $9)
               (i32.const 65535)
              )
              (get_local $1)
             )
            )
            (set_local $27
             (i32.and
              (get_local $19)
              (get_local $1)
             )
            )
            ;;@ life.cpp:84:0
            (set_local $10
             (i32.add
              (i32.shl
               (get_local $6)
               (i32.const 3)
              )
              (i32.const 2098178)
             )
            )
            (set_local $10
             (i32.load16_u
              (get_local $10)
             )
            )
            (set_local $15
             (i32.add
              (i32.shl
               (get_local $6)
               (i32.const 3)
              )
              (i32.const 2098182)
             )
            )
            (set_local $15
             (i32.load16_u
              (get_local $15)
             )
            )
            ;;@ life.cpp:41:0
            (set_local $20
             (i32.le_s
              (i32.and
               (get_local $10)
               (i32.const 65535)
              )
              (get_local $2)
             )
            )
            (set_local $2
             (i32.ge_s
              (i32.and
               (get_local $15)
               (i32.const 65535)
              )
              (get_local $2)
             )
            )
            (set_local $21
             (i32.and
              (get_local $20)
              (get_local $2)
             )
            )
            (set_local $24
             (i32.le_s
              (get_local $16)
              (get_local $4)
             )
            )
            (set_local $4
             (i32.ge_s
              (i32.and
               (get_local $9)
               (i32.const 65535)
              )
              (get_local $4)
             )
            )
            (set_local $32
             (i32.and
              (get_local $24)
              (get_local $4)
             )
            )
            (set_local $9
             (i32.le_s
              (i32.and
               (get_local $10)
               (i32.const 65535)
              )
              (get_local $5)
             )
            )
            (set_local $5
             (i32.ge_s
              (i32.and
               (get_local $15)
               (i32.const 65535)
              )
              (get_local $5)
             )
            )
            (set_local $10
             (i32.and
              (get_local $9)
              (get_local $5)
             )
            )
            ;;@ life.cpp:89:0
            (set_local $1
             (i32.and
              (get_local $19)
              (get_local $1)
             )
            )
            (set_local $1
             (i32.xor
              (get_local $1)
              (i32.const 1)
             )
            )
            (set_local $2
             (i32.and
              (get_local $20)
              (get_local $2)
             )
            )
            (set_local $2
             (i32.xor
              (get_local $2)
              (i32.const 1)
             )
            )
            (br_if $__rjti$0
             (i32.eqz
              (i32.or
               (get_local $1)
               (get_local $2)
              )
             )
            )
            (set_local $5
             (i32.and
              (get_local $9)
              (get_local $5)
             )
            )
            (set_local $5
             (i32.xor
              (get_local $5)
              (i32.const 1)
             )
            )
            (br_if $__rjti$0
             (i32.eqz
              (i32.or
               (get_local $1)
               (get_local $5)
              )
             )
            )
            (set_local $4
             (i32.and
              (get_local $24)
              (get_local $4)
             )
            )
            (set_local $4
             (i32.xor
              (get_local $4)
              (i32.const 1)
             )
            )
            (set_local $2
             (i32.and
              (get_local $2)
              (get_local $5)
             )
            )
            (br_if $__rjti$0
             (i32.eqz
              (i32.or
               (get_local $2)
               (get_local $4)
              )
             )
            )
            (if
             ;;@ life.cpp:90:0
             (i32.or
              (get_local $1)
              (get_local $4)
             )
             (block
              (set_local $1
               (i32.or
                (get_local $27)
                (get_local $32)
               )
              )
              (set_local $1
               (i32.and
                (get_local $1)
                (get_local $10)
               )
              )
              (br_if $__rjti$0
               (i32.and
                (get_local $21)
                (get_local $1)
               )
              )
             )
             (br_if $__rjti$0
              (i32.or
               (get_local $21)
               (get_local $10)
              )
             )
            )
            (br $__rjto$0)
           )
           ;;@ life.cpp:38:0
           (set_local $3
            (i32.lt_s
             (i32.and
              (get_local $18)
              (i32.const 65535)
             )
             (get_local $22)
            )
           )
           (if
            (i32.eqz
             (get_local $3)
            )
            (set_local $18
             (get_local $22)
            )
           )
           ;;@ life.cpp:154:0
           (i32.store16
            (get_local $29)
            (get_local $18)
           )
           ;;@ life.cpp:38:0
           (set_local $3
            (i32.lt_s
             (i32.and
              (get_local $17)
              (i32.const 65535)
             )
             (get_local $16)
            )
           )
           (if
            (i32.eqz
             (get_local $3)
            )
            (set_local $17
             (get_local $16)
            )
           )
           ;;@ life.cpp:155:0
           (i32.store16
            (get_local $26)
            (get_local $17)
           )
           ;;@ life.cpp:39:0
           (set_local $3
            (i32.lt_s
             (i32.and
              (get_local $23)
              (i32.const 65535)
             )
             (get_local $8)
            )
           )
           (if
            (i32.eqz
             (get_local $3)
            )
            (set_local $8
             (get_local $23)
            )
           )
           ;;@ life.cpp:156:0
           (i32.store16
            (get_local $28)
            (get_local $8)
           )
           ;;@ life.cpp:39:0
           (set_local $3
            (i32.lt_s
             (i32.and
              (get_local $25)
              (i32.const 65535)
             )
             (get_local $13)
            )
           )
           (if
            (i32.eqz
             (get_local $3)
            )
            (set_local $13
             (get_local $25)
            )
           )
           ;;@ life.cpp:157:0
           (i32.store16
            (get_local $30)
            (get_local $13)
           )
           ;;@ life.cpp:207:0
           (set_local $3
            (i32.add
             (get_local $6)
             (i32.const 1)
            )
           )
           (set_local $6
            (i32.and
             (get_local $3)
             (i32.const 65535)
            )
           )
           (if
            (i32.gt_s
             (i32.and
              (get_local $7)
              (i32.const 65535)
             )
             (get_local $6)
            )
            (block
             (set_local $3
              (i32.and
               (get_local $3)
               (i32.const 65535)
              )
             )
             (set_local $6
              (i32.and
               (get_local $7)
               (i32.const 65535)
              )
             )
             (loop $while-in5
              ;;@ life.cpp:208:0
              (set_local $8
               (i32.add
                (i32.shl
                 (get_local $3)
                 (i32.const 3)
                )
                (i32.const 2098176)
               )
              )
              (set_local $13
               (i32.add
                (get_local $3)
                (i32.const -1)
               )
              )
              (set_local $13
               (i32.add
                (i32.shl
                 (get_local $13)
                 (i32.const 3)
                )
                (i32.const 2098176)
               )
              )
              (set_local $12
               (i64.load
                (get_local $8)
               )
              )
              (i64.store
               (get_local $13)
               (get_local $12)
              )
              ;;@ life.cpp:207:0
              (set_local $3
               (i32.add
                (get_local $3)
                (i32.const 1)
               )
              )
              (br_if $while-in5
               (i32.lt_u
                (get_local $3)
                (get_local $6)
               )
              )
             )
            )
           )
           ;;@ life.cpp:211:0
           (set_local $7
            (i32.shr_s
             (i32.shl
              (i32.add
               (get_local $7)
               (i32.const -1)
              )
              (i32.const 16)
             )
             (i32.const 16)
            )
           )
           (i32.store16
            (i32.const 2229776)
            (get_local $7)
           )
           (set_local $3
            (i32.const 1)
           )
          )
          ;;@ life.cpp:201:0
          (set_local $6
           (i32.add
            (get_local $14)
            (i32.const 65535)
           )
          )
          (set_local $6
           (i32.and
            (get_local $6)
            (i32.const 65535)
           )
          )
          (set_local $8
           (i32.gt_u
            (get_local $6)
            (get_local $11)
           )
          )
          (set_local $14
           (i32.add
            (get_local $14)
            (i32.const -1)
           )
          )
          (br_if $while-in3
           (get_local $8)
          )
         )
         ;;@ life.cpp:200:0
         (set_local $14
          (i32.and
           (get_local $7)
           (i32.const 65535)
          )
         )
        )
       )
       (set_local $11
        (i32.add
         (get_local $11)
         (i32.const 1)
        )
       )
       (br_if $while-in1
        (i32.lt_u
         (get_local $11)
         (get_local $14)
        )
       )
      )
      (br_if $while-in
       (get_local $3)
      )
     )
    )
   )
   ;;@ life.cpp:218:0
   (get_local $7)
  )
 )
 (func $__Z9copyBoardtt (; 14 ;) (; has Stack IR ;) (param $0 i32) (param $1 i32) (result i32)
  (local $2 i32)
  (local $3 i32)
  (local $4 i32)
  (local $5 i32)
  (local $6 i32)
  (local $7 i32)
  (local $8 i32)
  (local $9 i32)
  (local $10 i32)
  (local $11 i64)
  (local $12 i32)
  (local $13 i32)
  (local $14 i32)
  (local $15 i32)
  (local $16 i32)
  (local $17 i32)
  (local $18 i32)
  (local $19 i32)
  (local $20 i32)
  (local $21 i32)
  (local $22 i32)
  (local $23 i32)
  (local $24 i32)
  (local $25 i32)
  (local $26 i32)
  (local $27 i32)
  (local $28 i32)
  (local $29 i32)
  (local $30 i32)
  (local $31 i32)
  (local $32 i32)
  (local $33 i32)
  (local $34 i64)
  (local $35 i32)
  (local $36 i64)
  ;;@ life.cpp:95:0
  (i32.store16
   (i32.const 2229762)
   (get_local $1)
  )
  ;;@ life.cpp:96:0
  (i32.store16
   (i32.const 2229760)
   (get_local $0)
  )
  ;;@ life.cpp:97:0
  (i32.store16
   (i32.const 2229764)
   (i32.const 0)
  )
  ;;@ life.cpp:98:0
  (i32.store16
   (i32.const 2229766)
   (i32.const 0)
  )
  ;;@ life.cpp:95:0
  (i32.store16
   (i32.const 2229770)
   (get_local $1)
  )
  ;;@ life.cpp:96:0
  (i32.store16
   (i32.const 2229768)
   (get_local $0)
  )
  ;;@ life.cpp:97:0
  (i32.store16
   (i32.const 2229772)
   (i32.const 0)
  )
  ;;@ life.cpp:98:0
  (i32.store16
   (i32.const 2229774)
   (i32.const 0)
  )
  ;;@ life.cpp:226:0
  (i32.store16
   (i32.const 2229776)
   (i32.const 0)
  )
  ;;@ life.cpp:228:0
  (set_local $23
   (i32.and
    (get_local $1)
    (i32.const 65535)
   )
  )
  (if
   (i32.and
    (get_local $1)
    (i32.const 65535)
   )
   (block
    (set_local $26
     (i32.and
      (get_local $0)
      (i32.const 65535)
     )
    )
    (set_local $32
     (i32.eqz
      (i32.and
       (get_local $0)
       (i32.const 65535)
      )
     )
    )
    (set_local $3
     (i32.const 1)
    )
    (set_local $9
     (tee_local $2
      (get_local $1)
     )
    )
    (set_local $16
     (tee_local $12
      (get_local $0)
     )
    )
    (loop $while-in
     (if
      (i32.eqz
       (get_local $32)
      )
      (block
       (set_local $33
        (i32.mul
         (get_local $14)
         (get_local $26)
        )
       )
       (set_local $7
        (i32.and
         (get_local $14)
         (i32.const 65535)
        )
       )
       (set_local $34
        (i64.or
         (i64.shl
          (i64.extend_u/i32
           (get_local $14)
          )
          (i64.const 48)
         )
         (i64.extend_u/i32
          (i32.shl
           (get_local $14)
           (i32.const 16)
          )
         )
        )
       )
       (set_local $18
        (i32.const 0)
       )
       (loop $while-in1
        ;;@ life.cpp:230:0
        (set_local $10
         (i32.add
          (get_local $18)
          (get_local $33)
         )
        )
        ;;@ life.cpp:231:0
        (set_local $27
         (i32.add
          (get_local $10)
          (i32.const 1024)
         )
        )
        (set_local $17
         (i32.load8_s
          (get_local $27)
         )
        )
        ;;@ life.cpp:232:0
        (set_local $10
         (i32.add
          (get_local $10)
          (i32.const 1049600)
         )
        )
        (set_local $24
         (i32.load8_s
          (get_local $10)
         )
        )
        (block $label$break$L36
         (block $__rjti$1
          (if
           ;;@ life.cpp:234:0
           (get_local $24)
           (block
            ;;@ life.cpp:237:0
            (set_local $3
             (i32.and
              (get_local $18)
              (i32.const 65535)
             )
            )
            (if
             ;;@ life.cpp:102:0
             (i32.gt_s
              (i32.and
               (get_local $16)
               (i32.const 65535)
              )
              (get_local $3)
             )
             (block
              (i32.store16
               (i32.const 2229760)
               (get_local $3)
              )
              (set_local $16
               (get_local $3)
              )
             )
            )
            (if
             ;;@ life.cpp:103:0
             (i32.lt_s
              (i32.and
               (get_local $15)
               (i32.const 65535)
              )
              (get_local $3)
             )
             (block
              (i32.store16
               (i32.const 2229764)
               (get_local $3)
              )
              (set_local $15
               (get_local $3)
              )
             )
            )
            (if
             ;;@ life.cpp:104:0
             (i32.gt_s
              (i32.and
               (get_local $9)
               (i32.const 65535)
              )
              (get_local $7)
             )
             (block
              (i32.store16
               (i32.const 2229762)
               (get_local $7)
              )
              (set_local $9
               (get_local $7)
              )
             )
            )
            (if
             ;;@ life.cpp:105:0
             (i32.lt_s
              (i32.and
               (get_local $8)
               (i32.const 65535)
              )
              (get_local $7)
             )
             (block
              (i32.store16
               (i32.const 2229766)
               (get_local $7)
              )
              (set_local $8
               (get_local $7)
              )
             )
            )
            (set_local $3
             (if (result i32)
              ;;@ life.cpp:41:0
              (i32.lt_s
               (i32.and
                (get_local $24)
                (i32.const 255)
               )
               (i32.const 33)
              )
              (block
               (if
                ;;@ life.cpp:102:0
                (i32.gt_s
                 (i32.and
                  (get_local $12)
                  (i32.const 65535)
                 )
                 (get_local $3)
                )
                (block
                 (i32.store16
                  (i32.const 2229768)
                  (get_local $3)
                 )
                 (set_local $12
                  (get_local $3)
                 )
                )
               )
               (if
                ;;@ life.cpp:103:0
                (i32.lt_s
                 (i32.and
                  (get_local $6)
                  (i32.const 65535)
                 )
                 (get_local $3)
                )
                (block
                 (i32.store16
                  (i32.const 2229772)
                  (get_local $3)
                 )
                 (set_local $6
                  (get_local $3)
                 )
                )
               )
               (if
                ;;@ life.cpp:104:0
                (i32.gt_s
                 (i32.and
                  (get_local $2)
                  (i32.const 65535)
                 )
                 (get_local $7)
                )
                (block
                 (i32.store16
                  (i32.const 2229770)
                  (get_local $7)
                 )
                 (set_local $2
                  (get_local $7)
                 )
                )
               )
               (if
                ;;@ life.cpp:105:0
                (i32.lt_s
                 (i32.and
                  (get_local $5)
                  (i32.const 65535)
                 )
                 (get_local $7)
                )
                (block
                 (i32.store16
                  (i32.const 2229774)
                  (get_local $7)
                 )
                 (set_local $3
                  (i32.const 0)
                 )
                 (set_local $5
                  (get_local $7)
                 )
                )
                (set_local $3
                 (i32.const 0)
                )
               )
               (br $__rjti$1)
              )
              (i32.const 0)
             )
            )
           )
          )
          ;;@ life.cpp:41:0
          (set_local $10
           (i32.shr_s
            (i32.shl
             (i32.add
              (get_local $17)
              (i32.const -1)
             )
             (i32.const 24)
            )
            (i32.const 24)
           )
          )
          (br_if $__rjti$1
           (i32.lt_s
            (i32.and
             (get_local $10)
             (i32.const 255)
            )
            (i32.const 32)
           )
          )
          (br $label$break$L36)
         )
         (block $label$break$L38
          (set_local $4
           (if (result i32)
            ;;@ life.cpp:252:0
            (i32.and
             (get_local $4)
             (i32.const 65535)
            )
            (block
             (set_local $17
              (i32.and
               (get_local $18)
               (i32.const 65535)
              )
             )
             (set_local $35
              (i32.and
               (get_local $4)
               (i32.const 65535)
              )
             )
             (set_local $10
              (i32.const 0)
             )
             (loop $while-in4
              (block $while-out3
               ;;@ life.cpp:77:0
               (set_local $28
                (i32.add
                 (i32.shl
                  (get_local $10)
                  (i32.const 3)
                 )
                 (i32.const 2098176)
                )
               )
               (set_local $29
                (i32.load16_u
                 (get_local $28)
                )
               )
               (set_local $20
                (get_local $29)
               )
               (set_local $20
                (i32.add
                 (get_local $20)
                 (i32.const 65533)
                )
               )
               (set_local $21
                (i32.and
                 (get_local $20)
                 (i32.const 65535)
                )
               )
               (set_local $20
                (i32.add
                 (i32.shl
                  (get_local $10)
                  (i32.const 3)
                 )
                 (i32.const 2098180)
                )
               )
               (set_local $30
                (i32.load16_u
                 (get_local $20)
                )
               )
               (set_local $13
                (get_local $30)
               )
               (set_local $13
                (i32.add
                 (get_local $13)
                 (i32.const 3)
                )
               )
               (set_local $13
                (i32.and
                 (get_local $13)
                 (i32.const 65535)
                )
               )
               ;;@ life.cpp:41:0
               (set_local $21
                (i32.le_s
                 (get_local $21)
                 (get_local $17)
                )
               )
               (set_local $13
                (i32.ge_s
                 (get_local $13)
                 (get_local $17)
                )
               )
               (if
                (i32.and
                 (get_local $21)
                 (get_local $13)
                )
                (block
                 ;;@ life.cpp:78:0
                 (set_local $21
                  (i32.add
                   (i32.shl
                    (get_local $10)
                    (i32.const 3)
                   )
                   (i32.const 2098178)
                  )
                 )
                 (set_local $13
                  (i32.load16_u
                   (get_local $21)
                  )
                 )
                 (set_local $22
                  (i32.and
                   (get_local $13)
                   (i32.const 65535)
                  )
                 )
                 (set_local $22
                  (i32.add
                   (get_local $22)
                   (i32.const 65533)
                  )
                 )
                 (set_local $25
                  (i32.and
                   (get_local $22)
                   (i32.const 65535)
                  )
                 )
                 (set_local $22
                  (i32.add
                   (i32.shl
                    (get_local $10)
                    (i32.const 3)
                   )
                   (i32.const 2098182)
                  )
                 )
                 (set_local $31
                  (i32.load16_u
                   (get_local $22)
                  )
                 )
                 (set_local $19
                  (get_local $31)
                 )
                 (set_local $19
                  (i32.add
                   (get_local $19)
                   (i32.const 3)
                  )
                 )
                 (set_local $19
                  (i32.and
                   (get_local $19)
                   (i32.const 65535)
                  )
                 )
                 ;;@ life.cpp:41:0
                 (set_local $25
                  (i32.le_s
                   (get_local $25)
                   (get_local $7)
                  )
                 )
                 (set_local $19
                  (i32.ge_s
                   (get_local $19)
                   (get_local $7)
                  )
                 )
                 (br_if $while-out3
                  (i32.and
                   (get_local $25)
                   (get_local $19)
                  )
                 )
                )
               )
               ;;@ life.cpp:252:0
               (set_local $10
                (i32.add
                 (get_local $10)
                 (i32.const 1)
                )
               )
               (br_if $while-in4
                (i32.lt_u
                 (get_local $10)
                 (get_local $35)
                )
               )
               (br $label$break$L38)
              )
             )
             (if
              ;;@ life.cpp:102:0
              (i32.gt_s
               (get_local $29)
               (get_local $17)
              )
              (i32.store16
               (get_local $28)
               (get_local $17)
              )
             )
             (if
              ;;@ life.cpp:103:0
              (i32.lt_s
               (get_local $30)
               (get_local $17)
              )
              (i32.store16
               (get_local $20)
               (get_local $17)
              )
             )
             (if
              ;;@ life.cpp:104:0
              (i32.gt_s
               (i32.and
                (get_local $13)
                (i32.const 65535)
               )
               (get_local $7)
              )
              (i32.store16
               (get_local $21)
               (get_local $7)
              )
             )
             ;;@ life.cpp:105:0
             (br_if $label$break$L36
              (i32.ge_s
               (get_local $31)
               (get_local $7)
              )
             )
             (i32.store16
              (get_local $22)
              (get_local $7)
             )
             (br $label$break$L36)
            )
            (i32.const 0)
           )
          )
         )
         ;;@ life.cpp:262:0
         (set_local $10
          (i32.shr_s
           (i32.shl
            (i32.add
             (get_local $4)
             (i32.const 1)
            )
            (i32.const 16)
           )
           (i32.const 16)
          )
         )
         (i32.store16
          (i32.const 2229776)
          (get_local $10)
         )
         (set_local $4
          (i32.and
           (get_local $4)
           (i32.const 65535)
          )
         )
         (set_local $4
          (i32.add
           (i32.shl
            (get_local $4)
            (i32.const 3)
           )
           (i32.const 2098176)
          )
         )
         (set_local $11
          (i64.extend_u/i32
           (get_local $18)
          )
         )
         (set_local $36
          (i64.shl
           (get_local $11)
           (i64.const 32)
          )
         )
         (set_local $11
          (i64.or
           (get_local $34)
           (get_local $11)
          )
         )
         (set_local $11
          (i64.or
           (get_local $11)
           (get_local $36)
          )
         )
         (i64.store
          (get_local $4)
          (get_local $11)
         )
         (set_local $4
          (get_local $10)
         )
        )
        ;;@ life.cpp:266:0
        (i32.store8
         (get_local $27)
         (get_local $24)
        )
        ;;@ life.cpp:229:0
        (set_local $18
         (i32.add
          (get_local $18)
          (i32.const 1)
         )
        )
        (br_if $while-in1
         (i32.ne
          (get_local $18)
          (get_local $26)
         )
        )
       )
      )
     )
     ;;@ life.cpp:228:0
     (set_local $14
      (i32.add
       (get_local $14)
       (i32.const 1)
      )
     )
     (br_if $while-in
      (i32.ne
       (get_local $14)
       (get_local $23)
      )
     )
    )
    (if
     ;;@ life.cpp:271:0
     (i32.and
      (get_local $4)
      (i32.const 65535)
     )
     (block
      (set_local $6
       (i32.add
        (i32.and
         (get_local $0)
         (i32.const 65535)
        )
        (i32.const -3)
       )
      )
      (set_local $12
       (i32.add
        (get_local $23)
        (i32.const -3)
       )
      )
      (set_local $8
       (i32.and
        (get_local $4)
        (i32.const 65535)
       )
      )
      (set_local $4
       (i32.const 0)
      )
      (loop $while-in6
       ;;@ life.cpp:124:0
       (set_local $5
        (i32.add
         (i32.shl
          (get_local $4)
          (i32.const 3)
         )
         (i32.const 2098178)
        )
       )
       (set_local $2
        (i32.load16_u
         (get_local $5)
        )
       )
       (set_local $9
        (i32.gt_s
         (i32.and
          (get_local $2)
          (i32.const 65535)
         )
         (i32.const 3)
        )
       )
       (set_local $2
        (i32.and
         (get_local $2)
         (i32.const 65535)
        )
       )
       (set_local $2
        (i32.add
         (get_local $2)
         (i32.const 65533)
        )
       )
       (set_local $2
        (i32.and
         (get_local $2)
         (i32.const 65535)
        )
       )
       (i32.store16
        (get_local $5)
        (if (result i32)
         (get_local $9)
         (get_local $2)
         (i32.const 0)
        )
       )
       ;;@ life.cpp:126:0
       (set_local $5
        (i32.add
         (i32.shl
          (get_local $4)
          (i32.const 3)
         )
         (i32.const 2098176)
        )
       )
       (set_local $2
        (i32.load16_u
         (get_local $5)
        )
       )
       (set_local $9
        (i32.gt_s
         (i32.and
          (get_local $2)
          (i32.const 65535)
         )
         (i32.const 3)
        )
       )
       (set_local $2
        (i32.and
         (get_local $2)
         (i32.const 65535)
        )
       )
       (set_local $2
        (i32.add
         (get_local $2)
         (i32.const 65533)
        )
       )
       (set_local $2
        (i32.and
         (get_local $2)
         (i32.const 65535)
        )
       )
       (i32.store16
        (get_local $5)
        (if (result i32)
         (get_local $9)
         (get_local $2)
         (i32.const 0)
        )
       )
       ;;@ life.cpp:128:0
       (set_local $5
        (i32.add
         (i32.shl
          (get_local $4)
          (i32.const 3)
         )
         (i32.const 2098180)
        )
       )
       (set_local $2
        (i32.load16_u
         (get_local $5)
        )
       )
       (set_local $2
        (i32.and
         (get_local $2)
         (i32.const 65535)
        )
       )
       (set_local $9
        (i32.gt_s
         (get_local $6)
         (get_local $2)
        )
       )
       (set_local $2
        (i32.add
         (get_local $2)
         (i32.const 3)
        )
       )
       (set_local $2
        (i32.and
         (get_local $2)
         (i32.const 65535)
        )
       )
       (i32.store16
        (get_local $5)
        (if (result i32)
         (get_local $9)
         (get_local $2)
         (get_local $0)
        )
       )
       ;;@ life.cpp:130:0
       (set_local $5
        (i32.add
         (i32.shl
          (get_local $4)
          (i32.const 3)
         )
         (i32.const 2098182)
        )
       )
       (set_local $2
        (i32.load16_u
         (get_local $5)
        )
       )
       (set_local $2
        (i32.and
         (get_local $2)
         (i32.const 65535)
        )
       )
       (set_local $9
        (i32.gt_s
         (get_local $12)
         (get_local $2)
        )
       )
       (set_local $2
        (i32.add
         (get_local $2)
         (i32.const 3)
        )
       )
       (set_local $2
        (i32.and
         (get_local $2)
         (i32.const 65535)
        )
       )
       (i32.store16
        (get_local $5)
        (if (result i32)
         (get_local $9)
         (get_local $2)
         (get_local $1)
        )
       )
       ;;@ life.cpp:271:0
       (set_local $4
        (i32.add
         (get_local $4)
         (i32.const 1)
        )
       )
       (br_if $while-in6
        (i32.lt_u
         (get_local $4)
         (get_local $8)
        )
       )
      )
      (set_local $12
       (get_local $3)
      )
     )
     (set_local $12
      (get_local $3)
     )
    )
   )
   (set_local $12
    (i32.const 1)
   )
  )
  ;;@ life.cpp:275:0
  (set_local $6
   (call $__Z21collapseBoundingBoxent
    (i32.const 0)
   )
  )
  (if
   ;;@ life.cpp:278:0
   (i32.and
    (get_local $6)
    (i32.const 65535)
   )
   (block
    (set_local $15
     (i32.and
      (get_local $6)
      (i32.const 65535)
     )
    )
    (set_local $4
     (i32.const 0)
    )
    (loop $while-in8
     ;;@ life.cpp:279:0
     (set_local $3
      (i32.add
       (i32.shl
        (get_local $4)
        (i32.const 3)
       )
       (i32.const 2098176)
      )
     )
     (call $__ZN11BoundingBox10check_wrapEtt
      (get_local $3)
      (get_local $0)
      (get_local $1)
     )
     ;;@ life.cpp:280:0
     (set_local $8
      (i32.and
       (i32.wrap/i64
        (tee_local $11
         (i64.load
          (get_local $3)
         )
        )
       )
       (i32.const 65535)
      )
     )
     (set_local $9
      (i32.and
       (i32.wrap/i64
        (i64.shr_u
         (get_local $11)
         (i64.const 16)
        )
       )
       (i32.const 65535)
      )
     )
     (set_local $3
      (i32.and
       (i32.wrap/i64
        (i64.shr_u
         (get_local $11)
         (i64.const 32)
        )
       )
       (i32.const 65535)
      )
     )
     (set_local $5
      (i32.and
       (i32.wrap/i64
        (i64.shr_u
         (get_local $11)
         (i64.const 48)
        )
       )
       (i32.const 65535)
      )
     )
     ;;@ life.cpp:154:0
     (set_local $2
      (i32.load16_s
       (i32.const 2229762)
      )
     )
     ;;@ life.cpp:38:0
     (set_local $16
      (i32.lt_s
       (i32.and
        (get_local $2)
        (i32.const 65535)
       )
       (get_local $9)
      )
     )
     (if
      (i32.eqz
       (get_local $16)
      )
      (set_local $2
       (get_local $9)
      )
     )
     ;;@ life.cpp:154:0
     (i32.store16
      (i32.const 2229762)
      (get_local $2)
     )
     ;;@ life.cpp:155:0
     (set_local $2
      (i32.load16_s
       (i32.const 2229760)
      )
     )
     ;;@ life.cpp:38:0
     (set_local $9
      (i32.lt_s
       (i32.and
        (get_local $2)
        (i32.const 65535)
       )
       (get_local $8)
      )
     )
     (if
      (i32.eqz
       (get_local $9)
      )
      (set_local $2
       (get_local $8)
      )
     )
     ;;@ life.cpp:155:0
     (i32.store16
      (i32.const 2229760)
      (get_local $2)
     )
     ;;@ life.cpp:156:0
     (set_local $2
      (i32.load16_s
       (i32.const 2229764)
      )
     )
     ;;@ life.cpp:39:0
     (set_local $8
      (i32.lt_s
       (i32.and
        (get_local $2)
        (i32.const 65535)
       )
       (get_local $3)
      )
     )
     (if
      (i32.eqz
       (get_local $8)
      )
      (set_local $3
       (get_local $2)
      )
     )
     ;;@ life.cpp:156:0
     (i32.store16
      (i32.const 2229764)
      (get_local $3)
     )
     ;;@ life.cpp:157:0
     (set_local $3
      (i32.load16_s
       (i32.const 2229766)
      )
     )
     ;;@ life.cpp:39:0
     (set_local $2
      (i32.lt_s
       (i32.and
        (get_local $3)
        (i32.const 65535)
       )
       (get_local $5)
      )
     )
     (if
      (i32.eqz
       (get_local $2)
      )
      (set_local $5
       (get_local $3)
      )
     )
     ;;@ life.cpp:157:0
     (i32.store16
      (i32.const 2229766)
      (get_local $5)
     )
     ;;@ life.cpp:278:0
     (set_local $4
      (i32.add
       (get_local $4)
       (i32.const 1)
      )
     )
     (br_if $while-in8
      (i32.ne
       (get_local $4)
       (get_local $15)
      )
     )
    )
   )
  )
  ;;@ life.cpp:283:0
  (set_local $4
   (i32.load16_s
    (i32.const 2229776)
   )
  )
  (if
   (i32.ne
    (i32.shr_s
     (i32.shl
      (get_local $6)
      (i32.const 16)
     )
     (i32.const 16)
    )
    (get_local $4)
   )
   (block
    ;;@ life.cpp:285:0
    (drop
     (call $__Z21collapseBoundingBoxent
      (get_local $6)
     )
    )
    ;;@ life.cpp:287:0
    (set_local $8
     (i32.load16_u
      (i32.const 2229776)
     )
    )
    (if
     (i32.lt_s
      (i32.and
       (get_local $6)
       (i32.const 65535)
      )
      (i32.and
       (get_local $8)
       (i32.const 65535)
      )
     )
     (block
      ;;@ life.cpp:154:0
      (set_local $4
       (i32.load16_s
        (i32.const 2229762)
       )
      )
      ;;@ life.cpp:155:0
      (set_local $3
       (i32.load16_s
        (i32.const 2229760)
       )
      )
      ;;@ life.cpp:156:0
      (set_local $5
       (i32.load16_s
        (i32.const 2229764)
       )
      )
      ;;@ life.cpp:157:0
      (set_local $2
       (i32.load16_s
        (i32.const 2229766)
       )
      )
      ;;@ life.cpp:287:0
      (set_local $6
       (i32.and
        (get_local $6)
        (i32.const 65535)
       )
      )
      (set_local $7
       (i32.and
        (get_local $8)
        (i32.const 65535)
       )
      )
      (loop $while-in10
       ;;@ life.cpp:288:0
       (set_local $8
        (i32.add
         (i32.shl
          (get_local $6)
          (i32.const 3)
         )
         (i32.const 2098176)
        )
       )
       (set_local $15
        (i32.and
         (i32.wrap/i64
          (tee_local $11
           (i64.load
            (get_local $8)
           )
          )
         )
         (i32.const 65535)
        )
       )
       (set_local $16
        (i32.and
         (i32.wrap/i64
          (i64.shr_u
           (get_local $11)
           (i64.const 16)
          )
         )
         (i32.const 65535)
        )
       )
       (set_local $8
        (i32.and
         (i32.wrap/i64
          (i64.shr_u
           (get_local $11)
           (i64.const 32)
          )
         )
         (i32.const 65535)
        )
       )
       (set_local $9
        (i32.and
         (i32.wrap/i64
          (i64.shr_u
           (get_local $11)
           (i64.const 48)
          )
         )
         (i32.const 65535)
        )
       )
       ;;@ life.cpp:38:0
       (set_local $14
        (i32.lt_s
         (i32.and
          (get_local $4)
          (i32.const 65535)
         )
         (get_local $16)
        )
       )
       (if
        (i32.eqz
         (get_local $14)
        )
        (set_local $4
         (get_local $16)
        )
       )
       (set_local $16
        (i32.lt_s
         (i32.and
          (get_local $3)
          (i32.const 65535)
         )
         (get_local $15)
        )
       )
       (if
        (i32.eqz
         (get_local $16)
        )
        (set_local $3
         (get_local $15)
        )
       )
       ;;@ life.cpp:39:0
       (set_local $15
        (i32.lt_s
         (i32.and
          (get_local $5)
          (i32.const 65535)
         )
         (get_local $8)
        )
       )
       (if
        (get_local $15)
        (set_local $5
         (get_local $8)
        )
       )
       (set_local $8
        (i32.lt_s
         (i32.and
          (get_local $2)
          (i32.const 65535)
         )
         (get_local $9)
        )
       )
       (if
        (get_local $8)
        (set_local $2
         (get_local $9)
        )
       )
       ;;@ life.cpp:287:0
       (set_local $6
        (i32.add
         (get_local $6)
         (i32.const 1)
        )
       )
       (br_if $while-in10
        (i32.lt_u
         (get_local $6)
         (get_local $7)
        )
       )
      )
      ;;@ life.cpp:154:0
      (i32.store16
       (i32.const 2229762)
       (get_local $4)
      )
      ;;@ life.cpp:155:0
      (i32.store16
       (i32.const 2229760)
       (get_local $3)
      )
      ;;@ life.cpp:156:0
      (i32.store16
       (i32.const 2229764)
       (get_local $5)
      )
      ;;@ life.cpp:157:0
      (i32.store16
       (i32.const 2229766)
       (get_local $2)
      )
     )
    )
   )
  )
  ;;@ life.cpp:124:0
  (set_local $4
   (i32.load16_u
    (i32.const 2229770)
   )
  )
  (set_local $3
   (i32.gt_s
    (i32.and
     (get_local $4)
     (i32.const 65535)
    )
    (i32.const 3)
   )
  )
  (set_local $4
   (i32.and
    (get_local $4)
    (i32.const 65535)
   )
  )
  (set_local $4
   (i32.add
    (get_local $4)
    (i32.const 65533)
   )
  )
  (set_local $4
   (i32.and
    (get_local $4)
    (i32.const 65535)
   )
  )
  (i32.store16
   (i32.const 2229770)
   (if (result i32)
    (get_local $3)
    (get_local $4)
    (tee_local $4
     (i32.const 0)
    )
   )
  )
  ;;@ life.cpp:126:0
  (set_local $3
   (i32.load16_u
    (i32.const 2229768)
   )
  )
  (set_local $5
   (i32.gt_s
    (i32.and
     (get_local $3)
     (i32.const 65535)
    )
    (i32.const 3)
   )
  )
  (set_local $3
   (i32.and
    (get_local $3)
    (i32.const 65535)
   )
  )
  (set_local $3
   (i32.add
    (get_local $3)
    (i32.const 65533)
   )
  )
  (set_local $3
   (i32.and
    (get_local $3)
    (i32.const 65535)
   )
  )
  (i32.store16
   (i32.const 2229768)
   (if (result i32)
    (get_local $5)
    (get_local $3)
    (tee_local $3
     (i32.const 0)
    )
   )
  )
  ;;@ life.cpp:128:0
  (set_local $5
   (i32.load16_u
    (i32.const 2229772)
   )
  )
  (set_local $5
   (i32.and
    (get_local $5)
    (i32.const 65535)
   )
  )
  (set_local $2
   (i32.and
    (get_local $0)
    (i32.const 65535)
   )
  )
  (set_local $2
   (i32.add
    (get_local $2)
    (i32.const -3)
   )
  )
  (set_local $2
   (i32.gt_s
    (get_local $2)
    (get_local $5)
   )
  )
  (set_local $5
   (i32.add
    (get_local $5)
    (i32.const 3)
   )
  )
  (set_local $5
   (i32.and
    (get_local $5)
    (i32.const 65535)
   )
  )
  (i32.store16
   (i32.const 2229772)
   (if (result i32)
    (get_local $2)
    (get_local $5)
    (tee_local $5
     (get_local $0)
    )
   )
  )
  ;;@ life.cpp:130:0
  (set_local $2
   (i32.load16_u
    (i32.const 2229774)
   )
  )
  (set_local $2
   (i32.and
    (get_local $2)
    (i32.const 65535)
   )
  )
  (set_local $6
   (i32.add
    (get_local $23)
    (i32.const -3)
   )
  )
  (set_local $6
   (i32.gt_s
    (get_local $6)
    (get_local $2)
   )
  )
  (set_local $2
   (i32.add
    (get_local $2)
    (i32.const 3)
   )
  )
  (set_local $2
   (i32.and
    (get_local $2)
    (i32.const 65535)
   )
  )
  (i32.store16
   (i32.const 2229774)
   (if (result i32)
    (get_local $6)
    (get_local $2)
    (tee_local $2
     (get_local $1)
    )
   )
  )
  (if
   ;;@ life.cpp:111:0
   (i32.and
    (get_local $4)
    (i32.const 65535)
   )
   (if
    ;;@ life.cpp:113:0
    (i32.eq
     (i32.and
      (get_local $2)
      (i32.const 65535)
     )
     (i32.and
      (get_local $1)
      (i32.const 65535)
     )
    )
    ;;@ life.cpp:114:0
    (i32.store16
     (i32.const 2229770)
     (i32.const 0)
    )
   )
   ;;@ life.cpp:112:0
   (i32.store16
    (i32.const 2229774)
    (get_local $1)
   )
  )
  (if
   ;;@ life.cpp:116:0
   (i32.eqz
    (i32.and
     (get_local $3)
     (i32.const 65535)
    )
   )
   (block
    ;;@ life.cpp:117:0
    (i32.store16
     (i32.const 2229772)
     (get_local $0)
    )
    ;;@ life.cpp:294:0
    (return
     (get_local $12)
    )
   )
  )
  (if
   ;;@ life.cpp:118:0
   (i32.ne
    (i32.and
     (get_local $5)
     (i32.const 65535)
    )
    (i32.and
     (get_local $0)
     (i32.const 65535)
    )
   )
   ;;@ life.cpp:294:0
   (return
    (get_local $12)
   )
  )
  ;;@ life.cpp:119:0
  (i32.store16
   (i32.const 2229768)
   (i32.const 0)
  )
  ;;@ life.cpp:294:0
  (get_local $12)
 )
 (func $__ZN11BoundingBox10check_wrapEtt (; 15 ;) (; has Stack IR ;) (param $0 i32) (param $1 i32) (param $2 i32)
  (local $3 i32)
  (local $4 i64)
  (local $5 i64)
  (local $6 i64)
  (local $7 i32)
  (local $8 i32)
  (local $9 i32)
  (local $10 i64)
  (local $11 i32)
  ;;@ life.cpp:141:0
  (set_local $7
   (i32.add
    (get_local $0)
    (i32.const 2)
   )
  )
  (set_local $3
   (i32.load16_s
    (get_local $7)
   )
  )
  (block $__rjto$0
   (block $__rjti$0
    (set_local $2
     (if (result i32)
      (get_local $3)
      (block (result i32)
       ;;@ life.cpp:143:0
       (set_local $3
        (i32.add
         (get_local $0)
         (i32.const 6)
        )
       )
       (set_local $3
        (i32.load16_s
         (get_local $3)
        )
       )
       (if (result i32)
        (i32.eq
         (get_local $3)
         (i32.shr_s
          (i32.shl
           (get_local $2)
           (i32.const 16)
          )
          (i32.const 16)
         )
        )
        (block
         ;;@ life.cpp:144:0
         (set_local $8
          (i32.load16_u
           (get_local $0)
          )
         )
         (set_local $2
          (i32.add
           (get_local $0)
           (i32.const 4)
          )
         )
         (set_local $9
          (i32.load16_u
           (get_local $2)
          )
         )
         ;;@ life.cpp:187:0
         (set_local $2
          (i32.load16_s
           (i32.const 2229776)
          )
         )
         (set_local $3
          (i32.shr_s
           (i32.shl
            (i32.add
             (get_local $2)
             (i32.const 1)
            )
            (i32.const 16)
           )
           (i32.const 16)
          )
         )
         (i32.store16
          (i32.const 2229776)
          (get_local $3)
         )
         (set_local $3
          (i32.and
           (get_local $2)
           (i32.const 65535)
          )
         )
         (set_local $4
          (i64.extend_u/i32
           (get_local $9)
          )
         )
         (set_local $5
          (i64.shl
           (get_local $4)
           (i64.const 32)
          )
         )
         (set_local $6
          (i64.extend_u/i32
           (get_local $8)
          )
         )
         (set_local $2
          (get_local $0)
         )
         (set_local $4
          (i64.const 281474976710656)
         )
         (set_local $5
          (i64.or
           (get_local $5)
           (get_local $6)
          )
         )
         (br $__rjti$0)
        )
        (get_local $0)
       )
      )
      (block
       ;;@ life.cpp:142:0
       (set_local $8
        (i32.load16_u
         (get_local $0)
        )
       )
       (set_local $3
        (i32.add
         (get_local $0)
         (i32.const 4)
        )
       )
       (set_local $9
        (i32.load16_u
         (get_local $3)
        )
       )
       ;;@ life.cpp:187:0
       (set_local $3
        (i32.load16_s
         (i32.const 2229776)
        )
       )
       (set_local $11
        (i32.shr_s
         (i32.shl
          (i32.add
           (get_local $3)
           (i32.const 1)
          )
          (i32.const 16)
         )
         (i32.const 16)
        )
       )
       (i32.store16
        (i32.const 2229776)
        (get_local $11)
       )
       (set_local $3
        (i32.and
         (get_local $3)
         (i32.const 65535)
        )
       )
       (set_local $4
        (i64.extend_u/i32
         (i32.and
          (get_local $2)
          (i32.const 65535)
         )
        )
       )
       (set_local $5
        (i64.shl
         (get_local $4)
         (i64.const 48)
        )
       )
       (set_local $4
        (i64.extend_u/i32
         (get_local $9)
        )
       )
       (set_local $4
        (i64.shl
         (get_local $4)
         (i64.const 32)
        )
       )
       (set_local $2
        (i32.shr_s
         (i32.shl
          (i32.add
           (get_local $2)
           (i32.const -1)
          )
          (i32.const 16)
         )
         (i32.const 16)
        )
       )
       (set_local $6
        (i64.extend_u/i32
         (i32.and
          (get_local $2)
          (i32.const 65535)
         )
        )
       )
       (set_local $6
        (i64.shl
         (get_local $6)
         (i64.const 16)
        )
       )
       (set_local $10
        (i64.extend_u/i32
         (get_local $8)
        )
       )
       (set_local $5
        (i64.or
         (get_local $6)
         (get_local $5)
        )
       )
       (set_local $2
        (get_local $0)
       )
       (set_local $5
        (i64.or
         (get_local $5)
         (get_local $10)
        )
       )
       (br $__rjti$0)
      )
     )
    )
    (br $__rjto$0)
   )
   (set_local $3
    (i32.add
     (i32.shl
      (get_local $3)
      (i32.const 3)
     )
     (i32.const 2098176)
    )
   )
   (set_local $4
    (i64.or
     (get_local $5)
     (get_local $4)
    )
   )
   (i64.store
    (get_local $3)
    (get_local $4)
   )
  )
  ;;@ life.cpp:146:0
  (set_local $2
   (i32.load16_s
    (get_local $2)
   )
  )
  (if
   (get_local $2)
   (block
    ;;@ life.cpp:148:0
    (set_local $2
     (i32.add
      (get_local $0)
      (i32.const 4)
     )
    )
    (set_local $2
     (i32.load16_s
      (get_local $2)
     )
    )
    (set_local $4
     (if (result i64)
      (i32.eq
       (get_local $2)
       (i32.shr_s
        (i32.shl
         (get_local $1)
         (i32.const 16)
        )
        (i32.const 16)
       )
      )
      (block (result i64)
       ;;@ life.cpp:149:0
       (set_local $1
        (i32.load16_u
         (get_local $7)
        )
       )
       (set_local $0
        (i32.add
         (get_local $0)
         (i32.const 6)
        )
       )
       (set_local $2
        (i32.load16_u
         (get_local $0)
        )
       )
       ;;@ life.cpp:187:0
       (set_local $0
        (i32.load16_s
         (i32.const 2229776)
        )
       )
       (set_local $3
        (i32.shr_s
         (i32.shl
          (i32.add
           (get_local $0)
           (i32.const 1)
          )
          (i32.const 16)
         )
         (i32.const 16)
        )
       )
       (i32.store16
        (i32.const 2229776)
        (get_local $3)
       )
       (set_local $0
        (i32.and
         (get_local $0)
         (i32.const 65535)
        )
       )
       (set_local $4
        (i64.extend_u/i32
         (i32.and
          (get_local $2)
          (i32.const 65535)
         )
        )
       )
       (set_local $5
        (i64.shl
         (get_local $4)
         (i64.const 48)
        )
       )
       (set_local $4
        (i64.extend_u/i32
         (i32.and
          (get_local $1)
          (i32.const 65535)
         )
        )
       )
       (set_local $6
        (i64.shl
         (get_local $4)
         (i64.const 16)
        )
       )
       (set_local $5
        (i64.or
         (get_local $6)
         (get_local $5)
        )
       )
       (i64.const 4294967296)
      )
      ;;@ life.cpp:151:0
      (return)
     )
    )
   )
   (block
    ;;@ life.cpp:147:0
    (set_local $2
     (i32.load16_u
      (get_local $7)
     )
    )
    (set_local $0
     (i32.add
      (get_local $0)
      (i32.const 6)
     )
    )
    (set_local $3
     (i32.load16_u
      (get_local $0)
     )
    )
    ;;@ life.cpp:187:0
    (set_local $0
     (i32.load16_s
      (i32.const 2229776)
     )
    )
    (set_local $7
     (i32.shr_s
      (i32.shl
       (i32.add
        (get_local $0)
        (i32.const 1)
       )
       (i32.const 16)
      )
      (i32.const 16)
     )
    )
    (i32.store16
     (i32.const 2229776)
     (get_local $7)
    )
    (set_local $0
     (i32.and
      (get_local $0)
      (i32.const 65535)
     )
    )
    (set_local $4
     (i64.extend_u/i32
      (i32.and
       (get_local $3)
       (i32.const 65535)
      )
     )
    )
    (set_local $4
     (i64.shl
      (get_local $4)
      (i64.const 48)
     )
    )
    (set_local $5
     (i64.extend_u/i32
      (i32.and
       (get_local $1)
       (i32.const 65535)
      )
     )
    )
    (set_local $5
     (i64.shl
      (get_local $5)
      (i64.const 32)
     )
    )
    (set_local $6
     (i64.extend_u/i32
      (i32.and
       (get_local $2)
       (i32.const 65535)
      )
     )
    )
    (set_local $6
     (i64.shl
      (get_local $6)
      (i64.const 16)
     )
    )
    (set_local $1
     (i32.shr_s
      (i32.shl
       (i32.add
        (get_local $1)
        (i32.const -1)
       )
       (i32.const 16)
      )
      (i32.const 16)
     )
    )
    (set_local $10
     (i64.extend_u/i32
      (i32.and
       (get_local $1)
       (i32.const 65535)
      )
     )
    )
    (set_local $5
     (i64.or
      (get_local $5)
      (get_local $10)
     )
    )
    (set_local $5
     (i64.or
      (get_local $5)
      (get_local $6)
     )
    )
   )
  )
  (set_local $0
   (i32.add
    (i32.shl
     (get_local $0)
     (i32.const 3)
    )
    (i32.const 2098176)
   )
  )
  (set_local $4
   (i64.or
    (get_local $5)
    (get_local $4)
   )
  )
  (i64.store
   (get_local $0)
   (get_local $4)
  )
 )
 (func $_getBox (; 16 ;) (; has Stack IR ;) (result i32)
  ;;@ life.cpp:327:0
  (i32.const 2229768)
 )
 (func $_getNumBoxen (; 17 ;) (; has Stack IR ;) (result i32)
  (local $0 i32)
  ;;@ life.cpp:331:0
  (set_local $0
   (i32.load16_s
    (i32.const 2229776)
   )
  )
  (get_local $0)
 )
 (func $_getBoxen (; 18 ;) (; has Stack IR ;) (result i32)
  ;;@ life.cpp:335:0
  (i32.const 2098176)
 )
 (func $_getMaxBoxen (; 19 ;) (; has Stack IR ;) (result i32)
  ;;@ life.cpp:339:0
  (i32.const 16384)
 )
 (func $_getAllLife (; 20 ;) (; has Stack IR ;) (result i32)
  ;;@ life.cpp:343:0
  (i32.const 2229760)
 )
 (func $_getElementSize (; 21 ;) (; has Stack IR ;) (result i32)
  ;;@ life.cpp:347:0
  (i32.const 1)
 )
 (func $_getMaxHue (; 22 ;) (; has Stack IR ;) (result i32)
  ;;@ life.cpp:351:0
  (i32.const 211)
 )
 (func $_getMaxSize (; 23 ;) (; has Stack IR ;) (result i32)
  ;;@ life.cpp:355:0
  (i32.const 1048576)
 )
 (func $_getBoard (; 24 ;) (; has Stack IR ;) (result i32)
  ;;@ life.cpp:359:0
  (i32.const 1049600)
 )
 (func $_clear (; 25 ;) (; has Stack IR ;) (param $0 i32)
  (local $1 i32)
  ;;@ life.cpp:38:0
  (set_local $1
   (i32.lt_u
    (get_local $0)
    (i32.const 1048576)
   )
  )
  (if
   (i32.eqz
    (get_local $1)
   )
   (set_local $0
    (i32.const 1048576)
   )
  )
  (if
   ;;@ life.cpp:364:0
   (i32.eqz
    (get_local $0)
   )
   ;;@ life.cpp:367:0
   (return)
  )
  ;;@ life.cpp:365:0
  (drop
   (call $_memset
    (i32.const 1049600)
    (i32.const 0)
    (get_local $0)
   )
  )
 )
 (func $_createLife (; 26 ;) (; has Stack IR ;) (param $0 i32) (param $1 i32) (result i32)
  (local $2 i32)
  (local $3 i32)
  (local $4 i32)
  (local $5 i32)
  (local $6 i32)
  (local $7 i32)
  (local $8 i32)
  ;;@ life.cpp:370:0
  (set_local $2
   (call $_time
    (i32.const 0)
   )
  )
  (call $_srand
   (get_local $2)
  )
  ;;@ life.cpp:374:0
  (set_local $4
   (i32.and
    (get_local $0)
    (i32.const 65535)
   )
  )
  (set_local $2
   (i32.and
    (get_local $1)
    (i32.const 65535)
   )
  )
  (set_local $5
   (i32.mul
    (get_local $2)
    (get_local $4)
   )
  )
  ;;@ life.cpp:38:0
  (set_local $2
   (i32.lt_u
    (get_local $5)
    (i32.const 1048576)
   )
  )
  (if
   (i32.eqz
    (get_local $2)
   )
   (set_local $5
    (i32.const 1048576)
   )
  )
  (if
   ;;@ life.cpp:375:0
   (i32.eqz
    (get_local $5)
   )
   (block
    ;;@ life.cpp:383:0
    (i32.store16
     (i32.const 2229760)
     (i32.const 0)
    )
    ;;@ life.cpp:384:0
    (i32.store16
     (i32.const 2229762)
     (i32.const 0)
    )
    ;;@ life.cpp:385:0
    (i32.store16
     (i32.const 2229764)
     (get_local $0)
    )
    ;;@ life.cpp:386:0
    (i32.store16
     (i32.const 2229766)
     (get_local $1)
    )
    ;;@ life.cpp:388:0
    (return
     (i32.const 1315423911)
    )
   )
  )
  (set_local $4
   (i32.const 0)
  )
  (set_local $2
   (i32.const 1315423911)
  )
  (loop $while-in
   ;;@ life.cpp:376:0
   (set_local $3
    (call $_rand)
   )
   (set_local $7
    (i32.rem_s
     (get_local $3)
     (i32.const 2)
    )
   )
   (set_local $6
    (i32.and
     (get_local $7)
     (i32.const 255)
    )
   )
   ;;@ life.cpp:378:0
   (set_local $3
    (i32.add
     (get_local $4)
     (i32.const 1049600)
    )
   )
   (i32.store8
    (get_local $3)
    (get_local $6)
   )
   ;;@ life.cpp:380:0
   (set_local $8
    (i32.shl
     (get_local $2)
     (i32.const 5)
    )
   )
   (set_local $6
    (i32.and
     (get_local $7)
     (i32.const 255)
    )
   )
   (set_local $3
    (i32.shr_u
     (get_local $2)
     (i32.const 2)
    )
   )
   (set_local $3
    (i32.add
     (get_local $3)
     (get_local $8)
    )
   )
   (set_local $3
    (i32.add
     (get_local $3)
     (get_local $6)
    )
   )
   (set_local $2
    (i32.xor
     (get_local $3)
     (get_local $2)
    )
   )
   ;;@ life.cpp:375:0
   (set_local $4
    (i32.add
     (get_local $4)
     (i32.const 1)
    )
   )
   (br_if $while-in
    (i32.lt_u
     (get_local $4)
     (get_local $5)
    )
   )
  )
  ;;@ life.cpp:383:0
  (i32.store16
   (i32.const 2229760)
   (i32.const 0)
  )
  ;;@ life.cpp:384:0
  (i32.store16
   (i32.const 2229762)
   (i32.const 0)
  )
  ;;@ life.cpp:385:0
  (i32.store16
   (i32.const 2229764)
   (get_local $0)
  )
  ;;@ life.cpp:386:0
  (i32.store16
   (i32.const 2229766)
   (get_local $1)
  )
  ;;@ life.cpp:388:0
  (get_local $2)
 )
 (func $_resize (; 27 ;) (; has Stack IR ;) (param $0 i32) (param $1 i32) (result i32)
  (local $2 i32)
  (local $3 i32)
  (local $4 i32)
  (local $5 i32)
  (local $6 i32)
  (local $7 i32)
  (local $8 i32)
  (local $9 i32)
  (local $10 i32)
  (local $11 i32)
  (local $12 i32)
  (local $13 i32)
  (local $14 i32)
  (block $folding-inner0
   ;;@ life.cpp:392:0
   (set_local $7
    (i32.and
     (get_local $0)
     (i32.const 65535)
    )
   )
   (br_if $folding-inner0
    (i32.eqz
     (i32.and
      (get_local $0)
      (i32.const 65535)
     )
    )
   )
   (set_local $8
    (i32.and
     (get_local $1)
     (i32.const 65535)
    )
   )
   (br_if $folding-inner0
    (i32.eqz
     (i32.and
      (get_local $1)
      (i32.const 65535)
     )
    )
   )
   ;;@ life.cpp:396:0
   (set_local $5
    (i32.mul
     (get_local $8)
     (get_local $7)
    )
   )
   (if
    (i32.gt_u
     (get_local $5)
     (i32.const 1048576)
    )
    (block
     ;;@ life.cpp:365:0
     (drop
      (call $_memset
       (i32.const 1049600)
       (i32.const 0)
       (i32.const 1048576)
      )
     )
     (br $folding-inner0)
    )
   )
   ;;@ life.cpp:402:0
   (set_local $5
    (i32.load16_s
     (i32.const 2229756)
    )
   )
   (set_local $4
    (i32.and
     (get_local $5)
     (i32.const 65535)
    )
   )
   (set_local $4
    (i32.sub
     (get_local $4)
     (get_local $7)
    )
   )
   (set_local $4
    (i32.shl
     (get_local $4)
     (i32.const 16)
    )
   )
   (set_local $4
    (i32.shr_s
     (get_local $4)
     (i32.const 16)
    )
   )
   (set_local $10
    (i32.div_s
     (get_local $4)
     (i32.const 2)
    )
   )
   ;;@ life.cpp:403:0
   (set_local $4
    (i32.load16_s
     (i32.const 2229758)
    )
   )
   (set_local $2
    (i32.and
     (get_local $4)
     (i32.const 65535)
    )
   )
   (set_local $2
    (i32.sub
     (get_local $2)
     (get_local $8)
    )
   )
   (set_local $2
    (i32.shl
     (get_local $2)
     (i32.const 16)
    )
   )
   (set_local $2
    (i32.shr_s
     (get_local $2)
     (i32.const 16)
    )
   )
   (set_local $11
    (i32.div_s
     (get_local $2)
     (i32.const 2)
    )
   )
   ;;@ life.cpp:405:0
   (set_local $2
    (i32.or
     (get_local $4)
     (get_local $5)
    )
   )
   (if
    (i32.eqz
     (i32.and
      (get_local $2)
      (i32.const 65535)
     )
    )
    (block
     ;;@ life.cpp:406:0
     (i32.store16
      (i32.const 2229756)
      (get_local $0)
     )
     ;;@ life.cpp:407:0
     (i32.store16
      (i32.const 2229758)
      (get_local $1)
     )
     (br $folding-inner0)
    )
   )
   ;;@ life.cpp:412:0
   (br_if $folding-inner0
    (call $__Z9copyBoardtt
     (get_local $5)
     (get_local $4)
    )
   )
   (set_local $12
    (tee_local $9
     (i32.load16_u
      (i32.const 2229756)
     )
    )
   )
   (set_local $5
    (i32.const 1315423911)
   )
   (set_local $4
    (i32.const 0)
   )
   (loop $while-in
    (if
     (i32.lt_s
      (get_local $9)
      (i32.and
       (tee_local $13
        (i32.add
         (get_local $4)
         (get_local $10)
        )
       )
       (i32.const 65535)
      )
     )
     (block
      (set_local $2
       (i32.const 0)
      )
      (loop $while-in1
       ;;@ life.cpp:420:0
       (set_local $3
        (i32.mul
         (get_local $2)
         (get_local $7)
        )
       )
       (set_local $3
        (i32.add
         (get_local $3)
         (get_local $4)
        )
       )
       ;;@ life.cpp:426:0
       (set_local $3
        (i32.add
         (get_local $3)
         (i32.const 1049600)
        )
       )
       (i32.store8
        (get_local $3)
        (i32.const 0)
       )
       ;;@ life.cpp:427:0
       (set_local $3
        (i32.shl
         (get_local $5)
         (i32.const 5)
        )
       )
       (set_local $6
        (i32.shr_u
         (get_local $5)
         (i32.const 2)
        )
       )
       (set_local $5
        (i32.xor
         (i32.add
          (get_local $3)
          (get_local $6)
         )
         (get_local $5)
        )
       )
       ;;@ life.cpp:419:0
       (set_local $2
        (i32.add
         (get_local $2)
         (i32.const 1)
        )
       )
       (br_if $while-in1
        (i32.ne
         (get_local $2)
         (get_local $8)
        )
       )
      )
     )
     (block
      (set_local $2
       (i32.const 0)
      )
      (loop $while-in3
       ;;@ life.cpp:420:0
       (set_local $3
        (i32.mul
         (get_local $2)
         (get_local $7)
        )
       )
       (set_local $3
        (i32.add
         (get_local $3)
         (get_local $4)
        )
       )
       ;;@ life.cpp:424:0
       (set_local $6
        (i32.add
         (get_local $2)
         (get_local $11)
        )
       )
       (set_local $14
        (i32.and
         (get_local $6)
         (i32.const 65535)
        )
       )
       (set_local $5
        (i32.xor
         (i32.add
          (tee_local $3
           (if (result i32)
            ;;@ life.cpp:41:0
            (i32.lt_s
             (get_local $9)
             (get_local $14)
            )
            (block (result i32)
             ;;@ life.cpp:426:0
             (set_local $3
              (i32.add
               (get_local $3)
               (i32.const 1049600)
              )
             )
             (i32.store8
              (get_local $3)
              (i32.const 0)
             )
             ;;@ life.cpp:427:0
             (i32.shl
              (get_local $5)
              (i32.const 5)
             )
            )
            (block (result i32)
             ;;@ life.cpp:429:0
             (set_local $6
              (i32.mul
               (get_local $6)
               (get_local $12)
              )
             )
             (set_local $6
              (i32.add
               (get_local $6)
               (get_local $13)
              )
             )
             (set_local $6
              (i32.add
               (get_local $6)
               (i32.const 1024)
              )
             )
             (set_local $6
              (i32.load8_s
               (get_local $6)
              )
             )
             ;;@ life.cpp:431:0
             (set_local $3
              (i32.add
               (get_local $3)
               (i32.const 1049600)
              )
             )
             (i32.store8
              (get_local $3)
              (get_local $6)
             )
             ;;@ life.cpp:433:0
             (set_local $3
              (i32.ne
               (get_local $6)
               (i32.const 0)
              )
             )
             (set_local $6
              (i32.shl
               (get_local $5)
               (i32.const 5)
              )
             )
             (i32.or
              (get_local $6)
              (get_local $3)
             )
            )
           )
          )
          (i32.shr_u
           (get_local $5)
           (i32.const 2)
          )
         )
         (get_local $5)
        )
       )
       ;;@ life.cpp:419:0
       (set_local $2
        (i32.add
         (get_local $2)
         (i32.const 1)
        )
       )
       (br_if $while-in3
        (i32.ne
         (get_local $2)
         (get_local $8)
        )
       )
      )
     )
    )
    ;;@ life.cpp:418:0
    (set_local $4
     (i32.add
      (get_local $4)
      (i32.const 1)
     )
    )
    (br_if $while-in
     (i32.ne
      (get_local $4)
      (get_local $7)
     )
    )
   )
   ;;@ life.cpp:442:0
   (i32.store16
    (i32.const 2229756)
    (get_local $0)
   )
   ;;@ life.cpp:443:0
   (i32.store16
    (i32.const 2229758)
    (get_local $1)
   )
   ;;@ life.cpp:445:0
   (i32.store16
    (i32.const 2229760)
    (i32.const 0)
   )
   ;;@ life.cpp:446:0
   (i32.store16
    (i32.const 2229762)
    (i32.const 0)
   )
   ;;@ life.cpp:447:0
   (i32.store16
    (i32.const 2229764)
    (get_local $0)
   )
   ;;@ life.cpp:448:0
   (i32.store16
    (i32.const 2229766)
    (get_local $1)
   )
   ;;@ life.cpp:451:0
   (return
    (get_local $5)
   )
  )
  (i32.const 0)
 )
 (func $_life (; 28 ;) (; has Stack IR ;) (param $0 i32) (param $1 i32) (result i32)
  (local $2 i32)
  (local $3 i32)
  (local $4 i32)
  (local $5 i32)
  (local $6 i32)
  (local $7 i32)
  (local $8 i32)
  (local $9 i32)
  (local $10 i32)
  (local $11 i32)
  (local $12 i32)
  (local $13 i32)
  (local $14 i32)
  (local $15 i32)
  (local $16 i32)
  (local $17 i32)
  (local $18 i32)
  (local $19 i32)
  (local $20 i32)
  (local $21 i32)
  (local $22 i32)
  (local $23 i32)
  (local $24 i32)
  (local $25 i32)
  (local $26 i32)
  (local $27 i32)
  (local $28 i32)
  (if
   ;;@ life.cpp:454:0
   (call $__Z9copyBoardtt
    (get_local $0)
    (get_local $1)
   )
   ;;@ life.cpp:506:0
   (return
    (i32.const 0)
   )
  )
  ;;@ life.cpp:461:0
  (set_local $7
   (i32.and
    (get_local $0)
    (i32.const 65535)
   )
  )
  (if
   (i32.and
    (get_local $0)
    (i32.const 65535)
   )
   (block
    (set_local $11
     (i32.and
      (get_local $1)
      (i32.const 65535)
     )
    )
    (set_local $16
     (i32.eqz
      (i32.and
       (get_local $1)
       (i32.const 65535)
      )
     )
    )
    (set_local $0
     (i32.load16_u
      (i32.const 2229760)
     )
    )
    (set_local $1
     (i32.load16_u
      (i32.const 2229764)
     )
    )
    (set_local $3
     (i32.load16_u
      (i32.const 2229762)
     )
    )
    (set_local $4
     (i32.load16_u
      (i32.const 2229766)
     )
    )
    (set_local $5
     (i32.load16_u
      (i32.const 2229768)
     )
    )
    (set_local $2
     (i32.load16_u
      (i32.const 2229772)
     )
    )
    (set_local $9
     (i32.load16_u
      (i32.const 2229770)
     )
    )
    (set_local $10
     (i32.load16_u
      (i32.const 2229774)
     )
    )
    (set_local $17
     (i32.and
      (get_local $10)
      (i32.const 65535)
     )
    )
    (set_local $18
     (i32.and
      (get_local $9)
      (i32.const 65535)
     )
    )
    (set_local $19
     (i32.and
      (get_local $4)
      (i32.const 65535)
     )
    )
    (set_local $20
     (i32.and
      (get_local $3)
      (i32.const 65535)
     )
    )
    (set_local $21
     (i32.and
      (get_local $2)
      (i32.const 65535)
     )
    )
    (set_local $22
     (i32.and
      (get_local $5)
      (i32.const 65535)
     )
    )
    (set_local $23
     (i32.and
      (get_local $1)
      (i32.const 65535)
     )
    )
    (set_local $24
     (i32.and
      (get_local $0)
      (i32.const 65535)
     )
    )
    (set_local $0
     (i32.const 1)
    )
    (set_local $1
     (i32.const 1315423911)
    )
    (set_local $4
     (i32.const 0)
    )
    (loop $while-in
     (if
      (i32.eqz
       (get_local $16)
      )
      (block
       (set_local $25
        (i32.and
         (i32.ge_u
          (get_local $4)
          (get_local $24)
         )
         (i32.le_u
          (get_local $4)
          (get_local $23)
         )
        )
       )
       (set_local $26
        (i32.and
         (i32.ge_u
          (get_local $4)
          (get_local $22)
         )
         (i32.le_u
          (get_local $4)
          (get_local $21)
         )
        )
       )
       (set_local $27
        (i32.add
         (tee_local $14
          (i32.add
           (get_local $4)
           (get_local $7)
          )
         )
         (i32.const -1)
        )
       )
       (set_local $28
        (i32.add
         (get_local $14)
         (i32.const 1)
        )
       )
       (set_local $3
        (get_local $1)
       )
       (set_local $5
        (i32.const 0)
       )
       (loop $while-in1
        (set_local $3
         (i32.xor
          (i32.add
           (tee_local $1
            (block $__rjto$1 (result i32)
             (block $__rjti$1
              (br_if $__rjti$1
               (i32.eqz
                (get_local $25)
               )
              )
              ;;@ life.cpp:41:0
              (set_local $1
               (i32.ge_u
                (get_local $5)
                (get_local $20)
               )
              )
              (set_local $2
               (i32.le_u
                (get_local $5)
                (get_local $19)
               )
              )
              (br_if $__rjti$1
               (i32.eqz
                (i32.and
                 (get_local $1)
                 (get_local $2)
                )
               )
              )
              ;;@ life.cpp:469:0
              (set_local $1
               (i32.mul
                (get_local $5)
                (get_local $7)
               )
              )
              (set_local $10
               (i32.add
                (get_local $1)
                (get_local $4)
               )
              )
              ;;@ life.cpp:471:0
              (set_local $1
               (i32.add
                (get_local $10)
                (i32.const 1024)
               )
              )
              (set_local $9
               (i32.load8_s
                (get_local $1)
               )
              )
              (if
               (get_local $26)
               (block
                ;;@ life.cpp:41:0
                (set_local $1
                 (i32.ge_u
                  (get_local $5)
                  (get_local $18)
                 )
                )
                (set_local $2
                 (i32.le_u
                  (get_local $5)
                  (get_local $17)
                 )
                )
                (if
                 (i32.and
                  (get_local $1)
                  (get_local $2)
                 )
                 (block
                  ;;@ life.cpp:476:0
                  (set_local $8
                   (i32.add
                    (get_local $5)
                    (get_local $11)
                   )
                  )
                  (set_local $1
                   (i32.add
                    (get_local $8)
                    (i32.const -1)
                   )
                  )
                  (set_local $1
                   (i32.rem_s
                    (get_local $1)
                    (get_local $11)
                   )
                  )
                  (set_local $2
                   (i32.mul
                    (get_local $1)
                    (get_local $7)
                   )
                  )
                  (set_local $13
                   (i32.rem_s
                    (get_local $27)
                    (get_local $7)
                   )
                  )
                  (set_local $1
                   (i32.add
                    (get_local $2)
                    (get_local $13)
                   )
                  )
                  (set_local $1
                   (i32.add
                    (get_local $1)
                    (i32.const 1024)
                   )
                  )
                  (set_local $1
                   (i32.load8_s
                    (get_local $1)
                   )
                  )
                  (set_local $1
                   (i32.ne
                    (get_local $1)
                    (i32.const 0)
                   )
                  )
                  (set_local $15
                   (i32.rem_u
                    (get_local $14)
                    (get_local $7)
                   )
                  )
                  (set_local $6
                   (i32.add
                    (get_local $15)
                    (get_local $2)
                   )
                  )
                  (set_local $6
                   (i32.add
                    (get_local $6)
                    (i32.const 1024)
                   )
                  )
                  (set_local $6
                   (i32.load8_s
                    (get_local $6)
                   )
                  )
                  (set_local $6
                   (i32.eqz
                    (get_local $6)
                   )
                  )
                  (set_local $12
                   (if (result i32)
                    (get_local $1)
                    (i32.const 2)
                    (i32.const 1)
                   )
                  )
                  (if
                   (i32.eqz
                    (get_local $6)
                   )
                   (set_local $1
                    (get_local $12)
                   )
                  )
                  (set_local $6
                   (i32.rem_u
                    (get_local $28)
                    (get_local $7)
                   )
                  )
                  (set_local $2
                   (i32.add
                    (get_local $6)
                    (get_local $2)
                   )
                  )
                  (set_local $2
                   (i32.add
                    (get_local $2)
                    (i32.const 1024)
                   )
                  )
                  (set_local $2
                   (i32.load8_s
                    (get_local $2)
                   )
                  )
                  (set_local $2
                   (i32.ne
                    (get_local $2)
                    (i32.const 0)
                   )
                  )
                  (set_local $1
                   (i32.shr_s
                    (i32.shl
                     (i32.add
                      (get_local $1)
                      (get_local $2)
                     )
                     (i32.const 24)
                    )
                    (i32.const 24)
                   )
                  )
                  (set_local $2
                   (i32.rem_u
                    (get_local $8)
                    (get_local $11)
                   )
                  )
                  (set_local $12
                   (i32.mul
                    (get_local $2)
                    (get_local $7)
                   )
                  )
                  (set_local $2
                   (i32.add
                    (get_local $12)
                    (get_local $13)
                   )
                  )
                  (set_local $2
                   (i32.add
                    (get_local $2)
                    (i32.const 1024)
                   )
                  )
                  (set_local $2
                   (i32.load8_s
                    (get_local $2)
                   )
                  )
                  (block $do-once
                   (block $__rjti$0
                    (br_if $__rjti$0
                     (i32.eqz
                      (get_local $2)
                     )
                    )
                    (set_local $2
                     (i32.shr_s
                      (i32.shl
                       (i32.add
                        (get_local $1)
                        (i32.const 1)
                       )
                       (i32.const 24)
                      )
                      (i32.const 24)
                     )
                    )
                    (if
                     (i32.le_s
                      (i32.and
                       (get_local $1)
                       (i32.const 255)
                      )
                      (i32.const 2)
                     )
                     (block
                      (set_local $1
                       (get_local $2)
                      )
                      (br $__rjti$0)
                     )
                    )
                    (br $do-once)
                   )
                   (set_local $2
                    (i32.add
                     (get_local $12)
                     (get_local $6)
                    )
                   )
                   (set_local $2
                    (i32.add
                     (get_local $2)
                     (i32.const 1024)
                    )
                   )
                   (set_local $2
                    (i32.load8_s
                     (get_local $2)
                    )
                   )
                   (if
                    (get_local $2)
                    (block
                     (set_local $1
                      (i32.shr_s
                       (i32.shl
                        (i32.add
                         (get_local $1)
                         (i32.const 1)
                        )
                        (i32.const 24)
                       )
                       (i32.const 24)
                      )
                     )
                     (br_if $do-once
                      (i32.gt_s
                       (i32.and
                        (get_local $1)
                        (i32.const 255)
                       )
                       (i32.const 3)
                      )
                     )
                    )
                   )
                   (set_local $2
                    (i32.add
                     (get_local $8)
                     (i32.const 1)
                    )
                   )
                   (set_local $2
                    (i32.rem_u
                     (get_local $2)
                     (get_local $11)
                    )
                   )
                   (set_local $8
                    (i32.mul
                     (get_local $2)
                     (get_local $7)
                    )
                   )
                   (set_local $2
                    (i32.add
                     (get_local $8)
                     (get_local $13)
                    )
                   )
                   (set_local $2
                    (i32.add
                     (get_local $2)
                     (i32.const 1024)
                    )
                   )
                   (set_local $2
                    (i32.load8_s
                     (get_local $2)
                    )
                   )
                   (if
                    (get_local $2)
                    (block
                     (set_local $1
                      (i32.shr_s
                       (i32.shl
                        (i32.add
                         (get_local $1)
                         (i32.const 1)
                        )
                        (i32.const 24)
                       )
                       (i32.const 24)
                      )
                     )
                     (br_if $do-once
                      (i32.gt_s
                       (i32.and
                        (get_local $1)
                        (i32.const 255)
                       )
                       (i32.const 3)
                      )
                     )
                    )
                   )
                   (set_local $2
                    (i32.add
                     (get_local $8)
                     (get_local $15)
                    )
                   )
                   (set_local $2
                    (i32.add
                     (get_local $2)
                     (i32.const 1024)
                    )
                   )
                   (set_local $2
                    (i32.load8_s
                     (get_local $2)
                    )
                   )
                   (if
                    (get_local $2)
                    (block
                     (set_local $2
                      (i32.shr_s
                       (i32.shl
                        (i32.add
                         (get_local $1)
                         (i32.const 1)
                        )
                        (i32.const 24)
                       )
                       (i32.const 24)
                      )
                     )
                     (br_if $do-once
                      (i32.gt_s
                       (i32.and
                        (get_local $1)
                        (i32.const 255)
                       )
                       (i32.const 2)
                      )
                     )
                     (set_local $1
                      (get_local $2)
                     )
                    )
                   )
                   (set_local $2
                    (i32.add
                     (get_local $8)
                     (get_local $6)
                    )
                   )
                   (set_local $2
                    (i32.add
                     (get_local $2)
                     (i32.const 1024)
                    )
                   )
                   (set_local $2
                    (i32.load8_s
                     (get_local $2)
                    )
                   )
                   (if
                    (get_local $2)
                    (block
                     (set_local $2
                      (i32.shr_s
                       (i32.shl
                        (i32.add
                         (get_local $1)
                         (i32.const 1)
                        )
                        (i32.const 24)
                       )
                       (i32.const 24)
                      )
                     )
                     (set_local $1
                      (i32.shr_s
                       (i32.shl
                        (i32.add
                         (get_local $1)
                         (i32.const -1)
                        )
                        (i32.const 24)
                       )
                       (i32.const 24)
                      )
                     )
                     (br_if $do-once
                      (i32.gt_s
                       (i32.and
                        (get_local $1)
                        (i32.const 255)
                       )
                       (i32.const 1)
                      )
                     )
                     (set_local $1
                      (get_local $2)
                     )
                    )
                   )
                   ;;@ life.cpp:478:0
                   (set_local $2
                    (i32.ne
                     (get_local $9)
                     (i32.const 0)
                    )
                   )
                   (set_local $8
                    (i32.eq
                     (i32.and
                      (get_local $1)
                      (i32.const 255)
                     )
                     (i32.const 2)
                    )
                   )
                   (set_local $2
                    (i32.and
                     (get_local $2)
                     (get_local $8)
                    )
                   )
                   (set_local $1
                    (i32.eq
                     (i32.and
                      (get_local $1)
                      (i32.const 255)
                     )
                     (i32.const 3)
                    )
                   )
                   (if
                    (i32.or
                     (get_local $1)
                     (get_local $2)
                    )
                    (block
                     (if
                      ;;@ life.cpp:480:0
                      (i32.lt_s
                       (i32.and
                        (get_local $9)
                        (i32.const 255)
                       )
                       (i32.const 210)
                      )
                      (block
                       ;;@ life.cpp:481:0
                       (set_local $0
                        (i32.add
                         (get_local $10)
                         (i32.const 1049600)
                        )
                       )
                       ;;@ life.cpp:480:0
                       (set_local $1
                        (i32.and
                         (get_local $9)
                         (i32.const 255)
                        )
                       )
                       ;;@ life.cpp:481:0
                       (set_local $1
                        (i32.add
                         (get_local $1)
                         (i32.const 1)
                        )
                       )
                       (set_local $1
                        (i32.and
                         (get_local $1)
                         (i32.const 255)
                        )
                       )
                       (i32.store8
                        (get_local $0)
                        (get_local $1)
                       )
                      )
                     )
                     ;;@ life.cpp:483:0
                     (set_local $1
                      (i32.shl
                       (get_local $3)
                       (i32.const 5)
                      )
                     )
                     (set_local $0
                      (i32.const 0)
                     )
                     (br $__rjto$1
                      (i32.or
                       (get_local $1)
                       (i32.const 1)
                      )
                     )
                    )
                   )
                  )
                  ;;@ life.cpp:486:0
                  (set_local $1
                   (i32.add
                    (get_local $10)
                    (i32.const 1049600)
                   )
                  )
                  (i32.store8
                   (get_local $1)
                   (i32.const 0)
                  )
                  ;;@ life.cpp:487:0
                  (br $__rjto$1
                   (i32.shl
                    (get_local $3)
                    (i32.const 5)
                   )
                  )
                 )
                )
               )
              )
              (if
               ;;@ life.cpp:489:0
               (i32.eqz
                (get_local $9)
               )
               ;;@ life.cpp:496:0
               (br $__rjto$1
                (i32.shl
                 (get_local $3)
                 (i32.const 5)
                )
               )
              )
              (if
               ;;@ life.cpp:491:0
               (i32.lt_s
                (i32.and
                 (get_local $9)
                 (i32.const 255)
                )
                (i32.const 210)
               )
               (block
                ;;@ life.cpp:492:0
                (set_local $0
                 (i32.add
                  (get_local $10)
                  (i32.const 1049600)
                 )
                )
                ;;@ life.cpp:491:0
                (set_local $1
                 (i32.and
                  (get_local $9)
                  (i32.const 255)
                 )
                )
                ;;@ life.cpp:492:0
                (set_local $1
                 (i32.add
                  (get_local $1)
                  (i32.const 1)
                 )
                )
                (set_local $1
                 (i32.and
                  (get_local $1)
                  (i32.const 255)
                 )
                )
                (i32.store8
                 (get_local $0)
                 (get_local $1)
                )
               )
              )
              ;;@ life.cpp:494:0
              (set_local $1
               (i32.shl
                (get_local $3)
                (i32.const 5)
               )
              )
              (set_local $0
               (i32.const 0)
              )
              (br $__rjto$1
               (i32.or
                (get_local $1)
                (i32.const 1)
               )
              )
             )
             ;;@ life.cpp:464:0
             (i32.shl
              (get_local $3)
              (i32.const 5)
             )
            )
           )
           (i32.shr_u
            (get_local $3)
            (i32.const 2)
           )
          )
          (get_local $3)
         )
        )
        ;;@ life.cpp:462:0
        (set_local $5
         (i32.add
          (get_local $5)
          (i32.const 1)
         )
        )
        (br_if $while-in1
         (i32.ne
          (get_local $5)
          (get_local $11)
         )
        )
       )
       (set_local $1
        (get_local $3)
       )
      )
     )
     ;;@ life.cpp:461:0
     (set_local $4
      (i32.add
       (get_local $4)
       (i32.const 1)
      )
     )
     (br_if $while-in
      (i32.ne
       (get_local $4)
       (get_local $7)
      )
     )
    )
    ;;@ life.cpp:505:0
    (set_local $1
     (i32.and
      (get_local $1)
      (i32.const 2147483647)
     )
    )
    (if
     (i32.eqz
      (get_local $0)
     )
     ;;@ life.cpp:506:0
     (return
      (get_local $1)
     )
    )
   )
  )
  (i32.const 0)
 )
 (func $_life_experimental (; 29 ;) (; has Stack IR ;) (param $0 i32) (param $1 i32) (result i32)
  (local $2 i32)
  (local $3 i32)
  (local $4 i32)
  (local $5 i32)
  (local $6 i32)
  (local $7 i32)
  (local $8 i32)
  (local $9 i32)
  (local $10 i32)
  (local $11 i32)
  (local $12 i32)
  (local $13 i32)
  (local $14 i32)
  (local $15 i32)
  (local $16 i32)
  (local $17 i32)
  (local $18 i32)
  (local $19 i32)
  (local $20 i32)
  (local $21 i32)
  (local $22 i32)
  (local $23 i32)
  (local $24 i32)
  (local $25 i32)
  (if
   ;;@ life.cpp:509:0
   (call $__Z9copyBoardtt
    (get_local $0)
    (get_local $1)
   )
   ;;@ life.cpp:574:0
   (return
    (i32.const 0)
   )
  )
  ;;@ life.cpp:516:0
  (set_local $2
   (i32.load16_s
    (i32.const 2229776)
   )
  )
  (set_local $9
   (i32.and
    (get_local $0)
    (i32.const 65535)
   )
  )
  (if
   (get_local $2)
   (block
    (set_local $11
     (i32.and
      (get_local $1)
      (i32.const 65535)
     )
    )
    (set_local $21
     (i32.and
      (get_local $2)
      (i32.const 65535)
     )
    )
    (loop $while-in
     ;;@ life.cpp:517:0
     (set_local $2
      (i32.add
       (i32.shl
        (get_local $4)
        (i32.const 3)
       )
       (i32.const 2098176)
      )
     )
     (set_local $6
      (i32.load16_s
       (get_local $2)
      )
     )
     (set_local $2
      (i32.add
       (i32.shl
        (get_local $4)
        (i32.const 3)
       )
       (i32.const 2098178)
      )
     )
     (set_local $12
      (i32.load16_s
       (get_local $2)
      )
     )
     (set_local $2
      (i32.add
       (i32.shl
        (get_local $4)
        (i32.const 3)
       )
       (i32.const 2098180)
      )
     )
     (set_local $17
      (i32.load16_u
       (get_local $2)
      )
     )
     (set_local $2
      (i32.add
       (i32.shl
        (get_local $4)
        (i32.const 3)
       )
       (i32.const 2098182)
      )
     )
     (set_local $18
      (i32.load16_u
       (get_local $2)
      )
     )
     (if
      ;;@ life.cpp:519:0
      (i32.le_s
       (i32.and
        (get_local $6)
        (i32.const 65535)
       )
       (get_local $17)
      )
      (block
       (set_local $15
        (i32.and
         (get_local $12)
         (i32.const 65535)
        )
       )
       (set_local $22
        (i32.gt_s
         (i32.and
          (get_local $12)
          (i32.const 65535)
         )
         (get_local $18)
        )
       )
       (set_local $10
        (i32.and
         (get_local $6)
         (i32.const 65535)
        )
       )
       (loop $while-in1
        (if
         (i32.eqz
          (get_local $22)
         )
         (block
          (set_local $23
           (i32.add
            (tee_local $19
             (i32.add
              (get_local $10)
              (get_local $9)
             )
            )
            (i32.const -1)
           )
          )
          (set_local $24
           (i32.add
            (get_local $19)
            (i32.const 1)
           )
          )
          (set_local $5
           (get_local $12)
          )
          (set_local $2
           (get_local $15)
          )
          (loop $while-in3
           ;;@ life.cpp:521:0
           (set_local $3
            (i32.mul
             (get_local $2)
             (get_local $9)
            )
           )
           (set_local $3
            (i32.add
             (get_local $3)
             (get_local $10)
            )
           )
           ;;@ life.cpp:523:0
           (set_local $13
            (i32.add
             (get_local $3)
             (i32.const 1049600)
            )
           )
           (set_local $7
            (i32.load8_s
             (get_local $13)
            )
           )
           (block $label$break$L16
            (if
             (i32.ne
              (get_local $7)
              (i32.const -1)
             )
             (block
              ;;@ life.cpp:525:0
              (set_local $3
               (i32.add
                (get_local $3)
                (i32.const 1024)
               )
              )
              (set_local $25
               (i32.load8_s
                (get_local $3)
               )
              )
              ;;@ life.cpp:529:0
              (set_local $7
               (i32.add
                (get_local $2)
                (get_local $11)
               )
              )
              (set_local $2
               (i32.add
                (get_local $7)
                (i32.const -1)
               )
              )
              (set_local $2
               (i32.rem_s
                (get_local $2)
                (get_local $11)
               )
              )
              (set_local $3
               (i32.mul
                (get_local $2)
                (get_local $9)
               )
              )
              (set_local $16
               (i32.rem_s
                (get_local $23)
                (get_local $9)
               )
              )
              (set_local $2
               (i32.add
                (get_local $3)
                (get_local $16)
               )
              )
              (set_local $2
               (i32.add
                (get_local $2)
                (i32.const 1024)
               )
              )
              (set_local $2
               (i32.load8_s
                (get_local $2)
               )
              )
              (set_local $2
               (i32.ne
                (get_local $2)
                (i32.const 0)
               )
              )
              (set_local $20
               (i32.rem_u
                (get_local $19)
                (get_local $9)
               )
              )
              (set_local $8
               (i32.add
                (get_local $20)
                (get_local $3)
               )
              )
              (set_local $8
               (i32.add
                (get_local $8)
                (i32.const 1024)
               )
              )
              (set_local $8
               (i32.load8_s
                (get_local $8)
               )
              )
              (set_local $8
               (i32.eqz
                (get_local $8)
               )
              )
              (set_local $14
               (if (result i32)
                (get_local $2)
                (i32.const 2)
                (i32.const 1)
               )
              )
              (if
               (i32.eqz
                (get_local $8)
               )
               (set_local $2
                (get_local $14)
               )
              )
              (set_local $8
               (i32.rem_u
                (get_local $24)
                (get_local $9)
               )
              )
              (set_local $3
               (i32.add
                (get_local $8)
                (get_local $3)
               )
              )
              (set_local $3
               (i32.add
                (get_local $3)
                (i32.const 1024)
               )
              )
              (set_local $3
               (i32.load8_s
                (get_local $3)
               )
              )
              (set_local $3
               (i32.ne
                (get_local $3)
                (i32.const 0)
               )
              )
              (set_local $2
               (i32.shr_s
                (i32.shl
                 (i32.add
                  (get_local $2)
                  (get_local $3)
                 )
                 (i32.const 24)
                )
                (i32.const 24)
               )
              )
              (set_local $3
               (i32.rem_u
                (get_local $7)
                (get_local $11)
               )
              )
              (set_local $14
               (i32.mul
                (get_local $3)
                (get_local $9)
               )
              )
              (set_local $3
               (i32.add
                (get_local $14)
                (get_local $16)
               )
              )
              (set_local $3
               (i32.add
                (get_local $3)
                (i32.const 1024)
               )
              )
              (set_local $3
               (i32.load8_s
                (get_local $3)
               )
              )
              (block $do-once
               (block $__rjti$0
                (br_if $__rjti$0
                 (i32.eqz
                  (get_local $3)
                 )
                )
                (set_local $3
                 (i32.shr_s
                  (i32.shl
                   (i32.add
                    (get_local $2)
                    (i32.const 1)
                   )
                   (i32.const 24)
                  )
                  (i32.const 24)
                 )
                )
                (if
                 (i32.le_s
                  (i32.and
                   (get_local $2)
                   (i32.const 255)
                  )
                  (i32.const 2)
                 )
                 (block
                  (set_local $2
                   (get_local $3)
                  )
                  (br $__rjti$0)
                 )
                )
                (br $do-once)
               )
               (set_local $3
                (i32.add
                 (get_local $14)
                 (get_local $8)
                )
               )
               (set_local $3
                (i32.add
                 (get_local $3)
                 (i32.const 1024)
                )
               )
               (set_local $3
                (i32.load8_s
                 (get_local $3)
                )
               )
               (if
                (get_local $3)
                (block
                 (set_local $2
                  (i32.shr_s
                   (i32.shl
                    (i32.add
                     (get_local $2)
                     (i32.const 1)
                    )
                    (i32.const 24)
                   )
                   (i32.const 24)
                  )
                 )
                 (br_if $do-once
                  (i32.gt_s
                   (i32.and
                    (get_local $2)
                    (i32.const 255)
                   )
                   (i32.const 3)
                  )
                 )
                )
               )
               (set_local $3
                (i32.add
                 (get_local $7)
                 (i32.const 1)
                )
               )
               (set_local $3
                (i32.rem_u
                 (get_local $3)
                 (get_local $11)
                )
               )
               (set_local $7
                (i32.mul
                 (get_local $3)
                 (get_local $9)
                )
               )
               (set_local $3
                (i32.add
                 (get_local $7)
                 (get_local $16)
                )
               )
               (set_local $3
                (i32.add
                 (get_local $3)
                 (i32.const 1024)
                )
               )
               (set_local $3
                (i32.load8_s
                 (get_local $3)
                )
               )
               (if
                (get_local $3)
                (block
                 (set_local $2
                  (i32.shr_s
                   (i32.shl
                    (i32.add
                     (get_local $2)
                     (i32.const 1)
                    )
                    (i32.const 24)
                   )
                   (i32.const 24)
                  )
                 )
                 (br_if $do-once
                  (i32.gt_s
                   (i32.and
                    (get_local $2)
                    (i32.const 255)
                   )
                   (i32.const 3)
                  )
                 )
                )
               )
               (set_local $3
                (i32.add
                 (get_local $7)
                 (get_local $20)
                )
               )
               (set_local $3
                (i32.add
                 (get_local $3)
                 (i32.const 1024)
                )
               )
               (set_local $3
                (i32.load8_s
                 (get_local $3)
                )
               )
               (if
                (get_local $3)
                (block
                 (set_local $3
                  (i32.shr_s
                   (i32.shl
                    (i32.add
                     (get_local $2)
                     (i32.const 1)
                    )
                    (i32.const 24)
                   )
                   (i32.const 24)
                  )
                 )
                 (br_if $do-once
                  (i32.gt_s
                   (i32.and
                    (get_local $2)
                    (i32.const 255)
                   )
                   (i32.const 2)
                  )
                 )
                 (set_local $2
                  (get_local $3)
                 )
                )
               )
               (set_local $3
                (i32.add
                 (get_local $7)
                 (get_local $8)
                )
               )
               (set_local $3
                (i32.add
                 (get_local $3)
                 (i32.const 1024)
                )
               )
               (set_local $3
                (i32.load8_s
                 (get_local $3)
                )
               )
               (if
                (get_local $3)
                (block
                 (set_local $3
                  (i32.shr_s
                   (i32.shl
                    (i32.add
                     (get_local $2)
                     (i32.const 1)
                    )
                    (i32.const 24)
                   )
                   (i32.const 24)
                  )
                 )
                 (set_local $2
                  (i32.shr_s
                   (i32.shl
                    (i32.add
                     (get_local $2)
                     (i32.const -1)
                    )
                    (i32.const 24)
                   )
                   (i32.const 24)
                  )
                 )
                 (br_if $do-once
                  (i32.gt_s
                   (i32.and
                    (get_local $2)
                    (i32.const 255)
                   )
                   (i32.const 1)
                  )
                 )
                 (set_local $2
                  (get_local $3)
                 )
                )
               )
               ;;@ life.cpp:531:0
               (set_local $3
                (i32.ne
                 (get_local $25)
                 (i32.const 0)
                )
               )
               (set_local $7
                (i32.eq
                 (i32.and
                  (get_local $2)
                  (i32.const 255)
                 )
                 (i32.const 2)
                )
               )
               (set_local $7
                (i32.and
                 (get_local $3)
                 (get_local $7)
                )
               )
               (set_local $2
                (i32.eq
                 (i32.and
                  (get_local $2)
                  (i32.const 255)
                 )
                 (i32.const 3)
                )
               )
               (if
                (i32.or
                 (get_local $2)
                 (get_local $7)
                )
                (block
                 (br_if $label$break$L16
                  (get_local $3)
                 )
                 ;;@ life.cpp:533:0
                 (i32.store8
                  (get_local $13)
                  (i32.const -1)
                 )
                 (br $label$break$L16)
                )
               )
              )
              ;;@ life.cpp:537:0
              (i32.store8
               (get_local $13)
               (i32.const 0)
              )
             )
            )
           )
           ;;@ life.cpp:520:0
           (set_local $5
            (i32.shr_s
             (i32.shl
              (i32.add
               (get_local $5)
               (i32.const 1)
              )
              (i32.const 16)
             )
             (i32.const 16)
            )
           )
           (set_local $2
            (i32.and
             (get_local $5)
             (i32.const 65535)
            )
           )
           (br_if $while-in3
            (i32.le_s
             (i32.and
              (get_local $5)
              (i32.const 65535)
             )
             (get_local $18)
            )
           )
          )
         )
        )
        ;;@ life.cpp:519:0
        (set_local $6
         (i32.shr_s
          (i32.shl
           (i32.add
            (get_local $6)
            (i32.const 1)
           )
           (i32.const 16)
          )
          (i32.const 16)
         )
        )
        (set_local $10
         (i32.and
          (get_local $6)
          (i32.const 65535)
         )
        )
        (br_if $while-in1
         (i32.le_s
          (i32.and
           (get_local $6)
           (i32.const 65535)
          )
          (get_local $17)
         )
        )
       )
      )
     )
     ;;@ life.cpp:516:0
     (set_local $4
      (i32.add
       (get_local $4)
       (i32.const 1)
      )
     )
     (br_if $while-in
      (i32.lt_u
       (get_local $4)
       (get_local $21)
      )
     )
    )
   )
  )
  ;;@ life.cpp:543:0
  (set_local $0
   (i32.eqz
    (i32.and
     (get_local $0)
     (i32.const 65535)
    )
   )
  )
  (set_local $2
   (i32.eqz
    (i32.and
     (get_local $1)
     (i32.const 65535)
    )
   )
  )
  (if
   (i32.eqz
    (i32.or
     (get_local $0)
     (get_local $2)
    )
   )
   (block
    (set_local $0
     (i32.load16_u
      (i32.const 2229766)
     )
    )
    (set_local $2
     (i32.load16_u
      (i32.const 2229762)
     )
    )
    (set_local $6
     (i32.load16_u
      (i32.const 2229764)
     )
    )
    (set_local $5
     (i32.load16_u
      (i32.const 2229760)
     )
    )
    (set_local $12
     (i32.and
      (get_local $2)
      (i32.const 65535)
     )
    )
    (set_local $15
     (i32.and
      (get_local $0)
      (i32.const 65535)
     )
    )
    (set_local $3
     (i32.and
      (get_local $5)
      (i32.const 65535)
     )
    )
    (set_local $11
     (i32.and
      (get_local $6)
      (i32.const 65535)
     )
    )
    (set_local $13
     (i32.and
      (get_local $1)
      (i32.const 65535)
     )
    )
    (set_local $0
     (i32.const 1)
    )
    (set_local $2
     (i32.const 1315423911)
    )
    (set_local $6
     (i32.const 0)
    )
    (loop $while-in6
     (if
      (i32.and
       (i32.ge_u
        (get_local $6)
        (get_local $3)
       )
       (i32.le_u
        (get_local $6)
        (get_local $11)
       )
      )
      (block
       (set_local $5
        (i32.const 0)
       )
       (loop $while-in8
        ;;@ life.cpp:41:0
        (set_local $4
         (i32.ge_u
          (get_local $5)
          (get_local $12)
         )
        )
        (set_local $10
         (i32.le_u
          (get_local $5)
          (get_local $15)
         )
        )
        (set_local $2
         (i32.xor
          (i32.add
           (tee_local $4
            (block $label$break$L49 (result i32)
             (if (result i32)
              (i32.and
               (get_local $4)
               (get_local $10)
              )
              (block (result i32)
               ;;@ life.cpp:551:0
               (set_local $4
                (i32.mul
                 (get_local $5)
                 (get_local $9)
                )
               )
               (set_local $4
                (i32.add
                 (get_local $4)
                 (get_local $6)
                )
               )
               ;;@ life.cpp:553:0
               (set_local $4
                (i32.add
                 (get_local $4)
                 (i32.const 1049600)
                )
               )
               (block $__rjto$2
                (block $__rjti$2
                 (block $switch-default
                  (block $switch-case10
                   (block $switch-case
                    (br_table $switch-case10 $switch-case $switch-default
                     (i32.sub
                      (tee_local $10
                       (i32.load8_s
                        (get_local $4)
                       )
                      )
                      (i32.const -1)
                     )
                    )
                   )
                   ;;@ life.cpp:564:0
                   (br $label$break$L49
                    (i32.shl
                     (get_local $2)
                     (i32.const 5)
                    )
                   )
                  )
                  (set_local $0
                   (i32.const 1)
                  )
                  (br $__rjti$2)
                 )
                 (if
                  ;;@ life.cpp:559:0
                  (i32.lt_s
                   (i32.and
                    (get_local $10)
                    (i32.const 255)
                   )
                   (i32.const 210)
                  )
                  (block
                   ;;@ life.cpp:557:0
                   (set_local $0
                    (i32.and
                     (get_local $10)
                     (i32.const 255)
                    )
                   )
                   ;;@ life.cpp:560:0
                   (set_local $0
                    (i32.add
                     (get_local $0)
                     (i32.const 1)
                    )
                   )
                   (set_local $0
                    (i32.and
                     (get_local $0)
                     (i32.const 255)
                    )
                   )
                   (br $__rjti$2)
                  )
                 )
                 (br $__rjto$2)
                )
                (i32.store8
                 (get_local $4)
                 (get_local $0)
                )
               )
               ;;@ life.cpp:562:0
               (set_local $4
                (i32.shl
                 (get_local $2)
                 (i32.const 5)
                )
               )
               (set_local $0
                (i32.const 0)
               )
               (i32.or
                (get_local $4)
                (i32.const 1)
               )
              )
              ;;@ life.cpp:546:0
              (i32.shl
               (get_local $2)
               (i32.const 5)
              )
             )
            )
           )
           (i32.shr_u
            (get_local $2)
            (i32.const 2)
           )
          )
          (get_local $2)
         )
        )
        ;;@ life.cpp:544:0
        (set_local $5
         (i32.add
          (get_local $5)
          (i32.const 1)
         )
        )
        (br_if $while-in8
         (i32.ne
          (get_local $5)
          (get_local $13)
         )
        )
       )
      )
      (block
       (set_local $5
        (i32.const 0)
       )
       (loop $while-in12
        ;;@ life.cpp:546:0
        (set_local $4
         (i32.shl
          (get_local $2)
          (i32.const 5)
         )
        )
        (set_local $10
         (i32.shr_u
          (get_local $2)
          (i32.const 2)
         )
        )
        (set_local $2
         (i32.xor
          (i32.add
           (get_local $4)
           (get_local $10)
          )
          (get_local $2)
         )
        )
        ;;@ life.cpp:544:0
        (set_local $5
         (i32.shr_s
          (i32.shl
           (i32.add
            (get_local $5)
            (i32.const 1)
           )
           (i32.const 16)
          )
          (i32.const 16)
         )
        )
        (br_if $while-in12
         (i32.lt_s
          (i32.and
           (get_local $5)
           (i32.const 65535)
          )
          (i32.and
           (get_local $1)
           (i32.const 65535)
          )
         )
        )
       )
      )
     )
     ;;@ life.cpp:543:0
     (set_local $6
      (i32.add
       (get_local $6)
       (i32.const 1)
      )
     )
     (br_if $while-in6
      (i32.ne
       (get_local $6)
       (get_local $9)
      )
     )
    )
    ;;@ life.cpp:573:0
    (set_local $1
     (i32.and
      (get_local $2)
      (i32.const 2147483647)
     )
    )
    (if
     (i32.eqz
      (get_local $0)
     )
     ;;@ life.cpp:574:0
     (return
      (get_local $1)
     )
    )
   )
  )
  (i32.const 0)
 )
 (func $__GLOBAL__sub_I_life_cpp (; 30 ;) (; has Stack IR ;)
  ;;@ life.cpp:50:0
  (i64.store align=2
   (i32.const 2229760)
   (i64.const 0)
  )
  (i64.store align=2
   (i32.const 2229768)
   (i64.const 0)
  )
  (drop
   (call $_memset
    (i32.const 2098176)
    (i32.const 0)
    (i32.const 131072)
   )
  )
 )
 (func $_malloc (; 31 ;) (; has Stack IR ;) (param $0 i32) (result i32)
  (local $1 i32)
  (local $2 i32)
  (local $3 i32)
  (local $4 i32)
  (local $5 i32)
  (local $6 i32)
  (local $7 i32)
  (local $8 i32)
  (local $9 i32)
  (local $10 i32)
  (local $11 i32)
  (local $12 i32)
  (local $13 i32)
  (local $14 i32)
  (local $15 i32)
  (local $16 i32)
  (local $17 i32)
  (local $18 i32)
  (local $19 i32)
  (local $20 i32)
  (local $21 i32)
  (local $22 i32)
  (block $folding-inner1
   (block $folding-inner0
    (set_local $1
     (get_global $STACKTOP)
    )
    (set_global $STACKTOP
     (i32.add
      (get_global $STACKTOP)
      (i32.const 16)
     )
    )
    (set_local $14
     (get_local $1)
    )
    (set_local $3
     (block $do-once (result i32)
      (if (result i32)
       (i32.lt_u
        (get_local $0)
        (i32.const 245)
       )
       (block (result i32)
        (set_local $1
         (i32.and
          (i32.add
           (get_local $0)
           (i32.const 11)
          )
          (i32.const -8)
         )
        )
        (if
         (i32.and
          (tee_local $3
           (i32.shr_u
            (tee_local $7
             (i32.load
              (i32.const 2229256)
             )
            )
            (tee_local $0
             (i32.shr_u
              (if (result i32)
               (i32.lt_u
                (get_local $0)
                (i32.const 11)
               )
               (tee_local $1
                (i32.const 16)
               )
               (get_local $1)
              )
              (i32.const 3)
             )
            )
           )
          )
          (i32.const 3)
         )
         (block
          (if
           (i32.eq
            (tee_local $3
             (i32.load
              (tee_local $6
               (i32.add
                (tee_local $0
                 (i32.load
                  (tee_local $4
                   (i32.add
                    (tee_local $2
                     (i32.add
                      (i32.shl
                       (tee_local $1
                        (i32.add
                         (i32.xor
                          (i32.and
                           (get_local $3)
                           (i32.const 1)
                          )
                          (i32.const 1)
                         )
                         (get_local $0)
                        )
                       )
                       (i32.const 3)
                      )
                      (i32.const 2229296)
                     )
                    )
                    (i32.const 8)
                   )
                  )
                 )
                )
                (i32.const 8)
               )
              )
             )
            )
            (get_local $2)
           )
           (i32.store
            (i32.const 2229256)
            (i32.and
             (get_local $7)
             (i32.xor
              (i32.shl
               (i32.const 1)
               (get_local $1)
              )
              (i32.const -1)
             )
            )
           )
           (block
            (if
             (i32.gt_u
              (i32.load
               (i32.const 2229272)
              )
              (get_local $3)
             )
             (call $_abort)
            )
            (if
             (i32.eq
              (i32.load
               (tee_local $5
                (i32.add
                 (get_local $3)
                 (i32.const 12)
                )
               )
              )
              (get_local $0)
             )
             (block
              (i32.store
               (get_local $5)
               (get_local $2)
              )
              (i32.store
               (get_local $4)
               (get_local $3)
              )
             )
             (call $_abort)
            )
           )
          )
          (i32.store offset=4
           (get_local $0)
           (i32.or
            (tee_local $3
             (i32.shl
              (get_local $1)
              (i32.const 3)
             )
            )
            (i32.const 3)
           )
          )
          (i32.store
           (tee_local $0
            (i32.add
             (i32.add
              (get_local $0)
              (get_local $3)
             )
             (i32.const 4)
            )
           )
           (i32.or
            (i32.load
             (get_local $0)
            )
            (i32.const 1)
           )
          )
          (set_global $STACKTOP
           (get_local $14)
          )
          (return
           (get_local $6)
          )
         )
        )
        (if (result i32)
         (i32.gt_u
          (get_local $1)
          (tee_local $15
           (i32.load
            (i32.const 2229264)
           )
          )
         )
         (block (result i32)
          (if
           (get_local $3)
           (block
            (set_local $0
             (i32.and
              (i32.shr_u
               (tee_local $3
                (i32.add
                 (i32.and
                  (tee_local $0
                   (i32.and
                    (i32.shl
                     (get_local $3)
                     (get_local $0)
                    )
                    (i32.or
                     (tee_local $0
                      (i32.shl
                       (i32.const 2)
                       (get_local $0)
                      )
                     )
                     (i32.sub
                      (i32.const 0)
                      (get_local $0)
                     )
                    )
                   )
                  )
                  (i32.sub
                   (i32.const 0)
                   (get_local $0)
                  )
                 )
                 (i32.const -1)
                )
               )
               (i32.const 12)
              )
              (i32.const 16)
             )
            )
            (if
             (i32.eq
              (tee_local $3
               (i32.load
                (tee_local $10
                 (i32.add
                  (tee_local $0
                   (i32.load
                    (tee_local $9
                     (i32.add
                      (tee_local $5
                       (i32.add
                        (i32.shl
                         (tee_local $4
                          (i32.add
                           (i32.or
                            (i32.or
                             (i32.or
                              (i32.or
                               (tee_local $4
                                (i32.and
                                 (i32.shr_u
                                  (tee_local $3
                                   (i32.shr_u
                                    (get_local $3)
                                    (get_local $0)
                                   )
                                  )
                                  (i32.const 5)
                                 )
                                 (i32.const 8)
                                )
                               )
                               (get_local $0)
                              )
                              (tee_local $3
                               (i32.and
                                (i32.shr_u
                                 (tee_local $0
                                  (i32.shr_u
                                   (get_local $3)
                                   (get_local $4)
                                  )
                                 )
                                 (i32.const 2)
                                )
                                (i32.const 4)
                               )
                              )
                             )
                             (tee_local $3
                              (i32.and
                               (i32.shr_u
                                (tee_local $0
                                 (i32.shr_u
                                  (get_local $0)
                                  (get_local $3)
                                 )
                                )
                                (i32.const 1)
                               )
                               (i32.const 2)
                              )
                             )
                            )
                            (tee_local $3
                             (i32.and
                              (i32.shr_u
                               (tee_local $0
                                (i32.shr_u
                                 (get_local $0)
                                 (get_local $3)
                                )
                               )
                               (i32.const 1)
                              )
                              (i32.const 1)
                             )
                            )
                           )
                           (i32.shr_u
                            (get_local $0)
                            (get_local $3)
                           )
                          )
                         )
                         (i32.const 3)
                        )
                        (i32.const 2229296)
                       )
                      )
                      (i32.const 8)
                     )
                    )
                   )
                  )
                  (i32.const 8)
                 )
                )
               )
              )
              (get_local $5)
             )
             (i32.store
              (i32.const 2229256)
              (tee_local $2
               (i32.and
                (get_local $7)
                (i32.xor
                 (i32.shl
                  (i32.const 1)
                  (get_local $4)
                 )
                 (i32.const -1)
                )
               )
              )
             )
             (block
              (if
               (i32.gt_u
                (i32.load
                 (i32.const 2229272)
                )
                (get_local $3)
               )
               (call $_abort)
              )
              (if
               (i32.eq
                (i32.load
                 (tee_local $11
                  (i32.add
                   (get_local $3)
                   (i32.const 12)
                  )
                 )
                )
                (get_local $0)
               )
               (block
                (i32.store
                 (get_local $11)
                 (get_local $5)
                )
                (i32.store
                 (get_local $9)
                 (get_local $3)
                )
                (set_local $2
                 (get_local $7)
                )
               )
               (call $_abort)
              )
             )
            )
            (i32.store offset=4
             (get_local $0)
             (i32.or
              (get_local $1)
              (i32.const 3)
             )
            )
            (i32.store offset=4
             (tee_local $7
              (i32.add
               (get_local $0)
               (get_local $1)
              )
             )
             (i32.or
              (tee_local $5
               (i32.sub
                (tee_local $3
                 (i32.shl
                  (get_local $4)
                  (i32.const 3)
                 )
                )
                (get_local $1)
               )
              )
              (i32.const 1)
             )
            )
            (i32.store
             (i32.add
              (get_local $0)
              (get_local $3)
             )
             (get_local $5)
            )
            (if
             (get_local $15)
             (block
              (set_local $4
               (i32.load
                (i32.const 2229276)
               )
              )
              (set_local $0
               (i32.add
                (i32.shl
                 (tee_local $3
                  (i32.shr_u
                   (get_local $15)
                   (i32.const 3)
                  )
                 )
                 (i32.const 3)
                )
                (i32.const 2229296)
               )
              )
              (if
               (i32.and
                (get_local $2)
                (tee_local $3
                 (i32.shl
                  (i32.const 1)
                  (get_local $3)
                 )
                )
               )
               (if
                (i32.gt_u
                 (i32.load
                  (i32.const 2229272)
                 )
                 (tee_local $1
                  (i32.load
                   (tee_local $3
                    (i32.add
                     (get_local $0)
                     (i32.const 8)
                    )
                   )
                  )
                 )
                )
                (call $_abort)
                (block
                 (set_local $6
                  (get_local $1)
                 )
                 (set_local $13
                  (get_local $3)
                 )
                )
               )
               (block
                (i32.store
                 (i32.const 2229256)
                 (i32.or
                  (get_local $2)
                  (get_local $3)
                 )
                )
                (set_local $6
                 (get_local $0)
                )
                (set_local $13
                 (i32.add
                  (get_local $0)
                  (i32.const 8)
                 )
                )
               )
              )
              (i32.store
               (get_local $13)
               (get_local $4)
              )
              (i32.store offset=12
               (get_local $6)
               (get_local $4)
              )
              (i32.store offset=8
               (get_local $4)
               (get_local $6)
              )
              (i32.store offset=12
               (get_local $4)
               (get_local $0)
              )
             )
            )
            (i32.store
             (i32.const 2229264)
             (get_local $5)
            )
            (i32.store
             (i32.const 2229276)
             (get_local $7)
            )
            (set_global $STACKTOP
             (get_local $14)
            )
            (return
             (get_local $10)
            )
           )
          )
          (if (result i32)
           (tee_local $13
            (i32.load
             (i32.const 2229260)
            )
           )
           (block
            (set_local $0
             (i32.and
              (i32.shr_u
               (tee_local $3
                (i32.add
                 (i32.and
                  (get_local $13)
                  (i32.sub
                   (i32.const 0)
                   (get_local $13)
                  )
                 )
                 (i32.const -1)
                )
               )
               (i32.const 12)
              )
              (i32.const 16)
             )
            )
            (set_local $0
             (tee_local $2
              (i32.load
               (i32.add
                (i32.shl
                 (i32.add
                  (i32.or
                   (i32.or
                    (i32.or
                     (i32.or
                      (tee_local $2
                       (i32.and
                        (i32.shr_u
                         (tee_local $3
                          (i32.shr_u
                           (get_local $3)
                           (get_local $0)
                          )
                         )
                         (i32.const 5)
                        )
                        (i32.const 8)
                       )
                      )
                      (get_local $0)
                     )
                     (tee_local $3
                      (i32.and
                       (i32.shr_u
                        (tee_local $0
                         (i32.shr_u
                          (get_local $3)
                          (get_local $2)
                         )
                        )
                        (i32.const 2)
                       )
                       (i32.const 4)
                      )
                     )
                    )
                    (tee_local $3
                     (i32.and
                      (i32.shr_u
                       (tee_local $0
                        (i32.shr_u
                         (get_local $0)
                         (get_local $3)
                        )
                       )
                       (i32.const 1)
                      )
                      (i32.const 2)
                     )
                    )
                   )
                   (tee_local $3
                    (i32.and
                     (i32.shr_u
                      (tee_local $0
                       (i32.shr_u
                        (get_local $0)
                        (get_local $3)
                       )
                      )
                      (i32.const 1)
                     )
                     (i32.const 1)
                    )
                   )
                  )
                  (i32.shr_u
                   (get_local $0)
                   (get_local $3)
                  )
                 )
                 (i32.const 2)
                )
                (i32.const 2229560)
               )
              )
             )
            )
            (set_local $6
             (i32.sub
              (i32.and
               (i32.load offset=4
                (get_local $2)
               )
               (i32.const -8)
              )
              (get_local $1)
             )
            )
            (loop $while-in
             (block $while-out
              (if
               (tee_local $3
                (i32.load offset=16
                 (get_local $0)
                )
               )
               (set_local $0
                (get_local $3)
               )
               (br_if $while-out
                (i32.eqz
                 (tee_local $0
                  (i32.load offset=20
                   (get_local $0)
                  )
                 )
                )
               )
              )
              (if
               (i32.eqz
                (tee_local $9
                 (i32.lt_u
                  (tee_local $3
                   (i32.sub
                    (i32.and
                     (i32.load offset=4
                      (get_local $0)
                     )
                     (i32.const -8)
                    )
                    (get_local $1)
                   )
                  )
                  (get_local $6)
                 )
                )
               )
               (set_local $3
                (get_local $6)
               )
              )
              (if
               (get_local $9)
               (set_local $2
                (get_local $0)
               )
              )
              (set_local $6
               (get_local $3)
              )
              (br $while-in)
             )
            )
            (if
             (i32.gt_u
              (tee_local $12
               (i32.load
                (i32.const 2229272)
               )
              )
              (get_local $2)
             )
             (call $_abort)
            )
            (if
             (i32.le_u
              (tee_local $8
               (i32.add
                (get_local $2)
                (get_local $1)
               )
              )
              (get_local $2)
             )
             (call $_abort)
            )
            (set_local $11
             (i32.load offset=24
              (get_local $2)
             )
            )
            (block $do-once4
             (if
              (i32.eq
               (tee_local $0
                (i32.load offset=12
                 (get_local $2)
                )
               )
               (get_local $2)
              )
              (block
               (if
                (i32.eqz
                 (tee_local $0
                  (i32.load
                   (tee_local $3
                    (i32.add
                     (get_local $2)
                     (i32.const 20)
                    )
                   )
                  )
                 )
                )
                (br_if $do-once4
                 (i32.eqz
                  (tee_local $0
                   (i32.load
                    (tee_local $3
                     (i32.add
                      (get_local $2)
                      (i32.const 16)
                     )
                    )
                   )
                  )
                 )
                )
               )
               (loop $while-in7
                (block $while-out6
                 (if
                  (i32.eqz
                   (tee_local $10
                    (i32.load
                     (tee_local $9
                      (i32.add
                       (get_local $0)
                       (i32.const 20)
                      )
                     )
                    )
                   )
                  )
                  (br_if $while-out6
                   (i32.eqz
                    (tee_local $10
                     (i32.load
                      (tee_local $9
                       (i32.add
                        (get_local $0)
                        (i32.const 16)
                       )
                      )
                     )
                    )
                   )
                  )
                 )
                 (set_local $3
                  (get_local $9)
                 )
                 (set_local $0
                  (get_local $10)
                 )
                 (br $while-in7)
                )
               )
               (if
                (i32.gt_u
                 (get_local $12)
                 (get_local $3)
                )
                (call $_abort)
                (block
                 (i32.store
                  (get_local $3)
                  (i32.const 0)
                 )
                 (set_local $4
                  (get_local $0)
                 )
                )
               )
              )
              (block
               (if
                (i32.gt_u
                 (get_local $12)
                 (tee_local $3
                  (i32.load offset=8
                   (get_local $2)
                  )
                 )
                )
                (call $_abort)
               )
               (if
                (i32.ne
                 (i32.load
                  (tee_local $9
                   (i32.add
                    (get_local $3)
                    (i32.const 12)
                   )
                  )
                 )
                 (get_local $2)
                )
                (call $_abort)
               )
               (if
                (i32.eq
                 (i32.load
                  (tee_local $10
                   (i32.add
                    (get_local $0)
                    (i32.const 8)
                   )
                  )
                 )
                 (get_local $2)
                )
                (block
                 (i32.store
                  (get_local $9)
                  (get_local $0)
                 )
                 (i32.store
                  (get_local $10)
                  (get_local $3)
                 )
                 (set_local $4
                  (get_local $0)
                 )
                )
                (call $_abort)
               )
              )
             )
            )
            (block $label$break$L78
             (if
              (get_local $11)
              (block
               (if
                (i32.eq
                 (get_local $2)
                 (i32.load
                  (tee_local $3
                   (i32.add
                    (i32.shl
                     (tee_local $0
                      (i32.load offset=28
                       (get_local $2)
                      )
                     )
                     (i32.const 2)
                    )
                    (i32.const 2229560)
                   )
                  )
                 )
                )
                (block
                 (i32.store
                  (get_local $3)
                  (get_local $4)
                 )
                 (if
                  (i32.eqz
                   (get_local $4)
                  )
                  (block
                   (i32.store
                    (i32.const 2229260)
                    (i32.and
                     (get_local $13)
                     (i32.xor
                      (i32.shl
                       (i32.const 1)
                       (get_local $0)
                      )
                      (i32.const -1)
                     )
                    )
                   )
                   (br $label$break$L78)
                  )
                 )
                )
                (if
                 (i32.gt_u
                  (i32.load
                   (i32.const 2229272)
                  )
                  (get_local $11)
                 )
                 (call $_abort)
                 (block
                  (set_local $0
                   (i32.add
                    (get_local $11)
                    (i32.const 20)
                   )
                  )
                  (i32.store
                   (if (result i32)
                    (i32.eq
                     (i32.load
                      (tee_local $3
                       (i32.add
                        (get_local $11)
                        (i32.const 16)
                       )
                      )
                     )
                     (get_local $2)
                    )
                    (get_local $3)
                    (get_local $0)
                   )
                   (get_local $4)
                  )
                  (br_if $label$break$L78
                   (i32.eqz
                    (get_local $4)
                   )
                  )
                 )
                )
               )
               (if
                (i32.gt_u
                 (tee_local $3
                  (i32.load
                   (i32.const 2229272)
                  )
                 )
                 (get_local $4)
                )
                (call $_abort)
               )
               (i32.store offset=24
                (get_local $4)
                (get_local $11)
               )
               (if
                (tee_local $0
                 (i32.load offset=16
                  (get_local $2)
                 )
                )
                (if
                 (i32.gt_u
                  (get_local $3)
                  (get_local $0)
                 )
                 (call $_abort)
                 (block
                  (i32.store offset=16
                   (get_local $4)
                   (get_local $0)
                  )
                  (i32.store offset=24
                   (get_local $0)
                   (get_local $4)
                  )
                 )
                )
               )
               (if
                (tee_local $0
                 (i32.load offset=20
                  (get_local $2)
                 )
                )
                (if
                 (i32.gt_u
                  (i32.load
                   (i32.const 2229272)
                  )
                  (get_local $0)
                 )
                 (call $_abort)
                 (block
                  (i32.store offset=20
                   (get_local $4)
                   (get_local $0)
                  )
                  (i32.store offset=24
                   (get_local $0)
                   (get_local $4)
                  )
                 )
                )
               )
              )
             )
            )
            (if
             (i32.lt_u
              (get_local $6)
              (i32.const 16)
             )
             (block
              (i32.store offset=4
               (get_local $2)
               (i32.or
                (tee_local $0
                 (i32.add
                  (get_local $6)
                  (get_local $1)
                 )
                )
                (i32.const 3)
               )
              )
              (i32.store
               (tee_local $0
                (i32.add
                 (i32.add
                  (get_local $2)
                  (get_local $0)
                 )
                 (i32.const 4)
                )
               )
               (i32.or
                (i32.load
                 (get_local $0)
                )
                (i32.const 1)
               )
              )
             )
             (block
              (i32.store offset=4
               (get_local $2)
               (i32.or
                (get_local $1)
                (i32.const 3)
               )
              )
              (i32.store offset=4
               (get_local $8)
               (i32.or
                (get_local $6)
                (i32.const 1)
               )
              )
              (i32.store
               (i32.add
                (get_local $8)
                (get_local $6)
               )
               (get_local $6)
              )
              (if
               (get_local $15)
               (block
                (set_local $4
                 (i32.load
                  (i32.const 2229276)
                 )
                )
                (set_local $0
                 (i32.add
                  (i32.shl
                   (tee_local $3
                    (i32.shr_u
                     (get_local $15)
                     (i32.const 3)
                    )
                   )
                   (i32.const 3)
                  )
                  (i32.const 2229296)
                 )
                )
                (if
                 (i32.and
                  (tee_local $3
                   (i32.shl
                    (i32.const 1)
                    (get_local $3)
                   )
                  )
                  (get_local $7)
                 )
                 (if
                  (i32.gt_u
                   (i32.load
                    (i32.const 2229272)
                   )
                   (tee_local $1
                    (i32.load
                     (tee_local $3
                      (i32.add
                       (get_local $0)
                       (i32.const 8)
                      )
                     )
                    )
                   )
                  )
                  (call $_abort)
                  (block
                   (set_local $5
                    (get_local $1)
                   )
                   (set_local $16
                    (get_local $3)
                   )
                  )
                 )
                 (block
                  (i32.store
                   (i32.const 2229256)
                   (i32.or
                    (get_local $3)
                    (get_local $7)
                   )
                  )
                  (set_local $5
                   (get_local $0)
                  )
                  (set_local $16
                   (i32.add
                    (get_local $0)
                    (i32.const 8)
                   )
                  )
                 )
                )
                (i32.store
                 (get_local $16)
                 (get_local $4)
                )
                (i32.store offset=12
                 (get_local $5)
                 (get_local $4)
                )
                (i32.store offset=8
                 (get_local $4)
                 (get_local $5)
                )
                (i32.store offset=12
                 (get_local $4)
                 (get_local $0)
                )
               )
              )
              (i32.store
               (i32.const 2229264)
               (get_local $6)
              )
              (i32.store
               (i32.const 2229276)
               (get_local $8)
              )
             )
            )
            (set_global $STACKTOP
             (get_local $14)
            )
            (return
             (i32.add
              (get_local $2)
              (i32.const 8)
             )
            )
           )
           (get_local $1)
          )
         )
         (get_local $1)
        )
       )
       (if (result i32)
        (i32.gt_u
         (get_local $0)
         (i32.const -65)
        )
        (i32.const -1)
        (block (result i32)
         (set_local $4
          (i32.and
           (tee_local $0
            (i32.add
             (get_local $0)
             (i32.const 11)
            )
           )
           (i32.const -8)
          )
         )
         (if (result i32)
          (tee_local $6
           (i32.load
            (i32.const 2229260)
           )
          )
          (block (result i32)
           (set_local $16
            (if (result i32)
             (tee_local $0
              (i32.shr_u
               (get_local $0)
               (i32.const 8)
              )
             )
             (if (result i32)
              (i32.gt_u
               (get_local $4)
               (i32.const 16777215)
              )
              (i32.const 31)
              (i32.or
               (i32.and
                (i32.shr_u
                 (get_local $4)
                 (i32.add
                  (tee_local $0
                   (i32.add
                    (i32.sub
                     (i32.const 14)
                     (i32.or
                      (i32.or
                       (tee_local $2
                        (i32.and
                         (i32.shr_u
                          (i32.add
                           (tee_local $1
                            (i32.shl
                             (get_local $0)
                             (tee_local $0
                              (i32.and
                               (i32.shr_u
                                (i32.add
                                 (get_local $0)
                                 (i32.const 1048320)
                                )
                                (i32.const 16)
                               )
                               (i32.const 8)
                              )
                             )
                            )
                           )
                           (i32.const 520192)
                          )
                          (i32.const 16)
                         )
                         (i32.const 4)
                        )
                       )
                       (get_local $0)
                      )
                      (tee_local $1
                       (i32.and
                        (i32.shr_u
                         (i32.add
                          (tee_local $0
                           (i32.shl
                            (get_local $1)
                            (get_local $2)
                           )
                          )
                          (i32.const 245760)
                         )
                         (i32.const 16)
                        )
                        (i32.const 2)
                       )
                      )
                     )
                    )
                    (i32.shr_u
                     (i32.shl
                      (get_local $0)
                      (get_local $1)
                     )
                     (i32.const 15)
                    )
                   )
                  )
                  (i32.const 7)
                 )
                )
                (i32.const 1)
               )
               (i32.shl
                (get_local $0)
                (i32.const 1)
               )
              )
             )
             (i32.const 0)
            )
           )
           (set_local $2
            (i32.sub
             (i32.const 0)
             (get_local $4)
            )
           )
           (block $__rjto$1
            (block $__rjti$1
             (if
              (tee_local $0
               (i32.load
                (i32.add
                 (i32.shl
                  (get_local $16)
                  (i32.const 2)
                 )
                 (i32.const 2229560)
                )
               )
              )
              (block
               (set_local $5
                (i32.sub
                 (i32.const 25)
                 (i32.shr_u
                  (get_local $16)
                  (i32.const 1)
                 )
                )
               )
               (set_local $1
                (i32.const 0)
               )
               (set_local $7
                (i32.shl
                 (get_local $4)
                 (if (result i32)
                  (i32.eq
                   (get_local $16)
                   (i32.const 31)
                  )
                  (i32.const 0)
                  (get_local $5)
                 )
                )
               )
               (set_local $5
                (i32.const 0)
               )
               (loop $while-in15
                (if
                 (i32.lt_u
                  (tee_local $13
                   (i32.sub
                    (i32.and
                     (i32.load offset=4
                      (get_local $0)
                     )
                     (i32.const -8)
                    )
                    (get_local $4)
                   )
                  )
                  (get_local $2)
                 )
                 (set_local $1
                  (if (result i32)
                   (get_local $13)
                   (block (result i32)
                    (set_local $2
                     (get_local $13)
                    )
                    (get_local $0)
                   )
                   (block
                    (set_local $2
                     (i32.const 0)
                    )
                    (set_local $1
                     (get_local $0)
                    )
                    (br $__rjti$1)
                   )
                  )
                 )
                )
                (if
                 (i32.eqz
                  (i32.or
                   (i32.eqz
                    (tee_local $13
                     (i32.load offset=20
                      (get_local $0)
                     )
                    )
                   )
                   (i32.eq
                    (get_local $13)
                    (tee_local $0
                     (i32.load
                      (i32.add
                       (i32.add
                        (get_local $0)
                        (i32.const 16)
                       )
                       (i32.shl
                        (i32.shr_u
                         (get_local $7)
                         (i32.const 31)
                        )
                        (i32.const 2)
                       )
                      )
                     )
                    )
                   )
                  )
                 )
                 (set_local $5
                  (get_local $13)
                 )
                )
                (set_local $7
                 (i32.shl
                  (get_local $7)
                  (i32.const 1)
                 )
                )
                (br_if $while-in15
                 (get_local $0)
                )
               )
               (set_local $0
                (get_local $1)
               )
              )
              (set_local $0
               (i32.const 0)
              )
             )
             (br_if $__rjti$1
              (tee_local $1
               (if (result i32)
                (i32.or
                 (get_local $5)
                 (get_local $0)
                )
                (get_local $5)
                (block (result i32)
                 (drop
                  (br_if $do-once
                   (get_local $4)
                   (i32.eqz
                    (tee_local $0
                     (i32.and
                      (i32.or
                       (tee_local $0
                        (i32.shl
                         (i32.const 2)
                         (get_local $16)
                        )
                       )
                       (i32.sub
                        (i32.const 0)
                        (get_local $0)
                       )
                      )
                      (get_local $6)
                     )
                    )
                   )
                  )
                 )
                 (set_local $1
                  (i32.and
                   (i32.shr_u
                    (tee_local $5
                     (i32.add
                      (i32.and
                       (get_local $0)
                       (i32.sub
                        (i32.const 0)
                        (get_local $0)
                       )
                      )
                      (i32.const -1)
                     )
                    )
                    (i32.const 12)
                   )
                   (i32.const 16)
                  )
                 )
                 (set_local $0
                  (i32.const 0)
                 )
                 (i32.load
                  (i32.add
                   (i32.shl
                    (i32.add
                     (i32.or
                      (i32.or
                       (i32.or
                        (i32.or
                         (tee_local $7
                          (i32.and
                           (i32.shr_u
                            (tee_local $5
                             (i32.shr_u
                              (get_local $5)
                              (get_local $1)
                             )
                            )
                            (i32.const 5)
                           )
                           (i32.const 8)
                          )
                         )
                         (get_local $1)
                        )
                        (tee_local $5
                         (i32.and
                          (i32.shr_u
                           (tee_local $1
                            (i32.shr_u
                             (get_local $5)
                             (get_local $7)
                            )
                           )
                           (i32.const 2)
                          )
                          (i32.const 4)
                         )
                        )
                       )
                       (tee_local $5
                        (i32.and
                         (i32.shr_u
                          (tee_local $1
                           (i32.shr_u
                            (get_local $1)
                            (get_local $5)
                           )
                          )
                          (i32.const 1)
                         )
                         (i32.const 2)
                        )
                       )
                      )
                      (tee_local $5
                       (i32.and
                        (i32.shr_u
                         (tee_local $1
                          (i32.shr_u
                           (get_local $1)
                           (get_local $5)
                          )
                         )
                         (i32.const 1)
                        )
                        (i32.const 1)
                       )
                      )
                     )
                     (i32.shr_u
                      (get_local $1)
                      (get_local $5)
                     )
                    )
                    (i32.const 2)
                   )
                   (i32.const 2229560)
                  )
                 )
                )
               )
              )
             )
             (set_local $5
              (get_local $0)
             )
             (br $__rjto$1)
            )
            (set_local $7
             (get_local $0)
            )
            (set_local $0
             (get_local $1)
            )
            (loop $while-in17
             (set_local $1
              (i32.load offset=4
               (get_local $0)
              )
             )
             (if
              (i32.eqz
               (tee_local $5
                (i32.load offset=16
                 (get_local $0)
                )
               )
              )
              (set_local $5
               (i32.load offset=20
                (get_local $0)
               )
              )
             )
             (if
              (i32.eqz
               (tee_local $13
                (i32.lt_u
                 (tee_local $1
                  (i32.sub
                   (i32.and
                    (get_local $1)
                    (i32.const -8)
                   )
                   (get_local $4)
                  )
                 )
                 (get_local $2)
                )
               )
              )
              (set_local $1
               (get_local $2)
              )
             )
             (if
              (i32.eqz
               (get_local $13)
              )
              (set_local $0
               (get_local $7)
              )
             )
             (set_local $2
              (if (result i32)
               (get_local $5)
               (block
                (set_local $7
                 (get_local $0)
                )
                (set_local $2
                 (get_local $1)
                )
                (set_local $0
                 (get_local $5)
                )
                (br $while-in17)
               )
               (block (result i32)
                (set_local $5
                 (get_local $0)
                )
                (get_local $1)
               )
              )
             )
            )
           )
           (if (result i32)
            (get_local $5)
            (if (result i32)
             (i32.lt_u
              (get_local $2)
              (i32.sub
               (i32.load
                (i32.const 2229264)
               )
               (get_local $4)
              )
             )
             (block
              (if
               (i32.gt_u
                (tee_local $17
                 (i32.load
                  (i32.const 2229272)
                 )
                )
                (get_local $5)
               )
               (call $_abort)
              )
              (if
               (i32.le_u
                (tee_local $8
                 (i32.add
                  (get_local $5)
                  (get_local $4)
                 )
                )
                (get_local $5)
               )
               (call $_abort)
              )
              (set_local $12
               (i32.load offset=24
                (get_local $5)
               )
              )
              (block $do-once18
               (if
                (i32.eq
                 (tee_local $0
                  (i32.load offset=12
                   (get_local $5)
                  )
                 )
                 (get_local $5)
                )
                (block
                 (if
                  (i32.eqz
                   (tee_local $0
                    (i32.load
                     (tee_local $1
                      (i32.add
                       (get_local $5)
                       (i32.const 20)
                      )
                     )
                    )
                   )
                  )
                  (br_if $do-once18
                   (i32.eqz
                    (tee_local $0
                     (i32.load
                      (tee_local $1
                       (i32.add
                        (get_local $5)
                        (i32.const 16)
                       )
                      )
                     )
                    )
                   )
                  )
                 )
                 (loop $while-in21
                  (block $while-out20
                   (if
                    (i32.eqz
                     (tee_local $10
                      (i32.load
                       (tee_local $7
                        (i32.add
                         (get_local $0)
                         (i32.const 20)
                        )
                       )
                      )
                     )
                    )
                    (br_if $while-out20
                     (i32.eqz
                      (tee_local $10
                       (i32.load
                        (tee_local $7
                         (i32.add
                          (get_local $0)
                          (i32.const 16)
                         )
                        )
                       )
                      )
                     )
                    )
                   )
                   (set_local $1
                    (get_local $7)
                   )
                   (set_local $0
                    (get_local $10)
                   )
                   (br $while-in21)
                  )
                 )
                 (if
                  (i32.gt_u
                   (get_local $17)
                   (get_local $1)
                  )
                  (call $_abort)
                  (block
                   (i32.store
                    (get_local $1)
                    (i32.const 0)
                   )
                   (set_local $9
                    (get_local $0)
                   )
                  )
                 )
                )
                (block
                 (if
                  (i32.gt_u
                   (get_local $17)
                   (tee_local $1
                    (i32.load offset=8
                     (get_local $5)
                    )
                   )
                  )
                  (call $_abort)
                 )
                 (if
                  (i32.ne
                   (i32.load
                    (tee_local $7
                     (i32.add
                      (get_local $1)
                      (i32.const 12)
                     )
                    )
                   )
                   (get_local $5)
                  )
                  (call $_abort)
                 )
                 (if
                  (i32.eq
                   (i32.load
                    (tee_local $10
                     (i32.add
                      (get_local $0)
                      (i32.const 8)
                     )
                    )
                   )
                   (get_local $5)
                  )
                  (block
                   (i32.store
                    (get_local $7)
                    (get_local $0)
                   )
                   (i32.store
                    (get_local $10)
                    (get_local $1)
                   )
                   (set_local $9
                    (get_local $0)
                   )
                  )
                  (call $_abort)
                 )
                )
               )
              )
              (block $label$break$L176
               (if
                (get_local $12)
                (block
                 (if
                  (i32.eq
                   (get_local $5)
                   (i32.load
                    (tee_local $1
                     (i32.add
                      (i32.shl
                       (tee_local $0
                        (i32.load offset=28
                         (get_local $5)
                        )
                       )
                       (i32.const 2)
                      )
                      (i32.const 2229560)
                     )
                    )
                   )
                  )
                  (block
                   (i32.store
                    (get_local $1)
                    (get_local $9)
                   )
                   (if
                    (i32.eqz
                     (get_local $9)
                    )
                    (block
                     (i32.store
                      (i32.const 2229260)
                      (tee_local $3
                       (i32.and
                        (get_local $6)
                        (i32.xor
                         (i32.shl
                          (i32.const 1)
                          (get_local $0)
                         )
                         (i32.const -1)
                        )
                       )
                      )
                     )
                     (br $label$break$L176)
                    )
                   )
                  )
                  (if
                   (i32.gt_u
                    (i32.load
                     (i32.const 2229272)
                    )
                    (get_local $12)
                   )
                   (call $_abort)
                   (block
                    (set_local $0
                     (i32.add
                      (get_local $12)
                      (i32.const 20)
                     )
                    )
                    (i32.store
                     (if (result i32)
                      (i32.eq
                       (i32.load
                        (tee_local $1
                         (i32.add
                          (get_local $12)
                          (i32.const 16)
                         )
                        )
                       )
                       (get_local $5)
                      )
                      (get_local $1)
                      (get_local $0)
                     )
                     (get_local $9)
                    )
                    (if
                     (i32.eqz
                      (get_local $9)
                     )
                     (block
                      (set_local $3
                       (get_local $6)
                      )
                      (br $label$break$L176)
                     )
                    )
                   )
                  )
                 )
                 (if
                  (i32.gt_u
                   (tee_local $1
                    (i32.load
                     (i32.const 2229272)
                    )
                   )
                   (get_local $9)
                  )
                  (call $_abort)
                 )
                 (i32.store offset=24
                  (get_local $9)
                  (get_local $12)
                 )
                 (if
                  (tee_local $0
                   (i32.load offset=16
                    (get_local $5)
                   )
                  )
                  (if
                   (i32.gt_u
                    (get_local $1)
                    (get_local $0)
                   )
                   (call $_abort)
                   (block
                    (i32.store offset=16
                     (get_local $9)
                     (get_local $0)
                    )
                    (i32.store offset=24
                     (get_local $0)
                     (get_local $9)
                    )
                   )
                  )
                 )
                 (if
                  (tee_local $0
                   (i32.load offset=20
                    (get_local $5)
                   )
                  )
                  (if
                   (i32.gt_u
                    (i32.load
                     (i32.const 2229272)
                    )
                    (get_local $0)
                   )
                   (call $_abort)
                   (block
                    (i32.store offset=20
                     (get_local $9)
                     (get_local $0)
                    )
                    (i32.store offset=24
                     (get_local $0)
                     (get_local $9)
                    )
                    (set_local $3
                     (get_local $6)
                    )
                   )
                  )
                  (set_local $3
                   (get_local $6)
                  )
                 )
                )
                (set_local $3
                 (get_local $6)
                )
               )
              )
              (block $label$break$L200
               (if
                (i32.lt_u
                 (get_local $2)
                 (i32.const 16)
                )
                (block
                 (i32.store offset=4
                  (get_local $5)
                  (i32.or
                   (tee_local $0
                    (i32.add
                     (get_local $2)
                     (get_local $4)
                    )
                   )
                   (i32.const 3)
                  )
                 )
                 (i32.store
                  (tee_local $0
                   (i32.add
                    (i32.add
                     (get_local $5)
                     (get_local $0)
                    )
                    (i32.const 4)
                   )
                  )
                  (i32.or
                   (i32.load
                    (get_local $0)
                   )
                   (i32.const 1)
                  )
                 )
                )
                (block
                 (i32.store offset=4
                  (get_local $5)
                  (i32.or
                   (get_local $4)
                   (i32.const 3)
                  )
                 )
                 (i32.store offset=4
                  (get_local $8)
                  (i32.or
                   (get_local $2)
                   (i32.const 1)
                  )
                 )
                 (i32.store
                  (i32.add
                   (get_local $8)
                   (get_local $2)
                  )
                  (get_local $2)
                 )
                 (set_local $1
                  (i32.shr_u
                   (get_local $2)
                   (i32.const 3)
                  )
                 )
                 (if
                  (i32.lt_u
                   (get_local $2)
                   (i32.const 256)
                  )
                  (block
                   (set_local $0
                    (i32.add
                     (i32.shl
                      (get_local $1)
                      (i32.const 3)
                     )
                     (i32.const 2229296)
                    )
                   )
                   (if
                    (i32.and
                     (tee_local $3
                      (i32.load
                       (i32.const 2229256)
                      )
                     )
                     (tee_local $1
                      (i32.shl
                       (i32.const 1)
                       (get_local $1)
                      )
                     )
                    )
                    (if
                     (i32.gt_u
                      (i32.load
                       (i32.const 2229272)
                      )
                      (tee_local $1
                       (i32.load
                        (tee_local $3
                         (i32.add
                          (get_local $0)
                          (i32.const 8)
                         )
                        )
                       )
                      )
                     )
                     (call $_abort)
                     (block
                      (set_local $15
                       (get_local $1)
                      )
                      (set_local $19
                       (get_local $3)
                      )
                     )
                    )
                    (block
                     (i32.store
                      (i32.const 2229256)
                      (i32.or
                       (get_local $3)
                       (get_local $1)
                      )
                     )
                     (set_local $15
                      (get_local $0)
                     )
                     (set_local $19
                      (i32.add
                       (get_local $0)
                       (i32.const 8)
                      )
                     )
                    )
                   )
                   (i32.store
                    (get_local $19)
                    (get_local $8)
                   )
                   (i32.store offset=12
                    (get_local $15)
                    (get_local $8)
                   )
                   (i32.store offset=8
                    (get_local $8)
                    (get_local $15)
                   )
                   (i32.store offset=12
                    (get_local $8)
                    (get_local $0)
                   )
                   (br $label$break$L200)
                  )
                 )
                 (set_local $0
                  (i32.add
                   (i32.shl
                    (tee_local $1
                     (if (result i32)
                      (tee_local $0
                       (i32.shr_u
                        (get_local $2)
                        (i32.const 8)
                       )
                      )
                      (if (result i32)
                       (i32.gt_u
                        (get_local $2)
                        (i32.const 16777215)
                       )
                       (i32.const 31)
                       (i32.or
                        (i32.and
                         (i32.shr_u
                          (get_local $2)
                          (i32.add
                           (tee_local $0
                            (i32.add
                             (i32.sub
                              (i32.const 14)
                              (i32.or
                               (i32.or
                                (tee_local $4
                                 (i32.and
                                  (i32.shr_u
                                   (i32.add
                                    (tee_local $1
                                     (i32.shl
                                      (get_local $0)
                                      (tee_local $0
                                       (i32.and
                                        (i32.shr_u
                                         (i32.add
                                          (get_local $0)
                                          (i32.const 1048320)
                                         )
                                         (i32.const 16)
                                        )
                                        (i32.const 8)
                                       )
                                      )
                                     )
                                    )
                                    (i32.const 520192)
                                   )
                                   (i32.const 16)
                                  )
                                  (i32.const 4)
                                 )
                                )
                                (get_local $0)
                               )
                               (tee_local $1
                                (i32.and
                                 (i32.shr_u
                                  (i32.add
                                   (tee_local $0
                                    (i32.shl
                                     (get_local $1)
                                     (get_local $4)
                                    )
                                   )
                                   (i32.const 245760)
                                  )
                                  (i32.const 16)
                                 )
                                 (i32.const 2)
                                )
                               )
                              )
                             )
                             (i32.shr_u
                              (i32.shl
                               (get_local $0)
                               (get_local $1)
                              )
                              (i32.const 15)
                             )
                            )
                           )
                           (i32.const 7)
                          )
                         )
                         (i32.const 1)
                        )
                        (i32.shl
                         (get_local $0)
                         (i32.const 1)
                        )
                       )
                      )
                      (i32.const 0)
                     )
                    )
                    (i32.const 2)
                   )
                   (i32.const 2229560)
                  )
                 )
                 (i32.store offset=28
                  (get_local $8)
                  (get_local $1)
                 )
                 (i32.store offset=4
                  (tee_local $4
                   (i32.add
                    (get_local $8)
                    (i32.const 16)
                   )
                  )
                  (i32.const 0)
                 )
                 (i32.store
                  (get_local $4)
                  (i32.const 0)
                 )
                 (if
                  (i32.eqz
                   (i32.and
                    (get_local $3)
                    (tee_local $4
                     (i32.shl
                      (i32.const 1)
                      (get_local $1)
                     )
                    )
                   )
                  )
                  (block
                   (i32.store
                    (i32.const 2229260)
                    (i32.or
                     (get_local $3)
                     (get_local $4)
                    )
                   )
                   (i32.store
                    (get_local $0)
                    (get_local $8)
                   )
                   (i32.store offset=24
                    (get_local $8)
                    (get_local $0)
                   )
                   (i32.store offset=12
                    (get_local $8)
                    (get_local $8)
                   )
                   (i32.store offset=8
                    (get_local $8)
                    (get_local $8)
                   )
                   (br $label$break$L200)
                  )
                 )
                 (block $label$break$L218
                  (if
                   (i32.eq
                    (i32.and
                     (i32.load offset=4
                      (tee_local $0
                       (i32.load
                        (get_local $0)
                       )
                      )
                     )
                     (i32.const -8)
                    )
                    (get_local $2)
                   )
                   (set_local $11
                    (get_local $0)
                   )
                   (block
                    (set_local $3
                     (i32.sub
                      (i32.const 25)
                      (i32.shr_u
                       (get_local $1)
                       (i32.const 1)
                      )
                     )
                    )
                    (set_local $1
                     (i32.shl
                      (get_local $2)
                      (if (result i32)
                       (i32.eq
                        (get_local $1)
                        (i32.const 31)
                       )
                       (i32.const 0)
                       (get_local $3)
                      )
                     )
                    )
                    (loop $while-in30
                     (if
                      (tee_local $3
                       (i32.load
                        (tee_local $4
                         (i32.add
                          (i32.add
                           (get_local $0)
                           (i32.const 16)
                          )
                          (i32.shl
                           (i32.shr_u
                            (get_local $1)
                            (i32.const 31)
                           )
                           (i32.const 2)
                          )
                         )
                        )
                       )
                      )
                      (block
                       (set_local $1
                        (i32.shl
                         (get_local $1)
                         (i32.const 1)
                        )
                       )
                       (if
                        (i32.eq
                         (i32.and
                          (i32.load offset=4
                           (get_local $3)
                          )
                          (i32.const -8)
                         )
                         (get_local $2)
                        )
                        (block
                         (set_local $11
                          (get_local $3)
                         )
                         (br $label$break$L218)
                        )
                        (block
                         (set_local $0
                          (get_local $3)
                         )
                         (br $while-in30)
                        )
                       )
                      )
                     )
                    )
                    (if
                     (i32.gt_u
                      (i32.load
                       (i32.const 2229272)
                      )
                      (get_local $4)
                     )
                     (call $_abort)
                     (block
                      (i32.store
                       (get_local $4)
                       (get_local $8)
                      )
                      (i32.store offset=24
                       (get_local $8)
                       (get_local $0)
                      )
                      (i32.store offset=12
                       (get_local $8)
                       (get_local $8)
                      )
                      (i32.store offset=8
                       (get_local $8)
                       (get_local $8)
                      )
                      (br $label$break$L200)
                     )
                    )
                   )
                  )
                 )
                 (if
                  (i32.and
                   (i32.le_u
                    (tee_local $3
                     (i32.load
                      (i32.const 2229272)
                     )
                    )
                    (tee_local $0
                     (i32.load
                      (tee_local $1
                       (i32.add
                        (get_local $11)
                        (i32.const 8)
                       )
                      )
                     )
                    )
                   )
                   (i32.le_u
                    (get_local $3)
                    (get_local $11)
                   )
                  )
                  (block
                   (i32.store offset=12
                    (get_local $0)
                    (get_local $8)
                   )
                   (i32.store
                    (get_local $1)
                    (get_local $8)
                   )
                   (i32.store offset=8
                    (get_local $8)
                    (get_local $0)
                   )
                   (i32.store offset=12
                    (get_local $8)
                    (get_local $11)
                   )
                   (i32.store offset=24
                    (get_local $8)
                    (i32.const 0)
                   )
                  )
                  (call $_abort)
                 )
                )
               )
              )
              (set_global $STACKTOP
               (get_local $14)
              )
              (return
               (i32.add
                (get_local $5)
                (i32.const 8)
               )
              )
             )
             (get_local $4)
            )
            (get_local $4)
           )
          )
          (get_local $4)
         )
        )
       )
      )
     )
    )
    (if
     (i32.ge_u
      (tee_local $1
       (i32.load
        (i32.const 2229264)
       )
      )
      (get_local $3)
     )
     (block
      (set_local $0
       (i32.load
        (i32.const 2229276)
       )
      )
      (if
       (i32.gt_u
        (tee_local $2
         (i32.sub
          (get_local $1)
          (get_local $3)
         )
        )
        (i32.const 15)
       )
       (block
        (i32.store
         (i32.const 2229276)
         (tee_local $4
          (i32.add
           (get_local $0)
           (get_local $3)
          )
         )
        )
        (i32.store
         (i32.const 2229264)
         (get_local $2)
        )
        (i32.store offset=4
         (get_local $4)
         (i32.or
          (get_local $2)
          (i32.const 1)
         )
        )
        (i32.store
         (i32.add
          (get_local $0)
          (get_local $1)
         )
         (get_local $2)
        )
        (i32.store offset=4
         (get_local $0)
         (i32.or
          (get_local $3)
          (i32.const 3)
         )
        )
       )
       (block
        (i32.store
         (i32.const 2229264)
         (i32.const 0)
        )
        (i32.store
         (i32.const 2229276)
         (i32.const 0)
        )
        (i32.store offset=4
         (get_local $0)
         (i32.or
          (get_local $1)
          (i32.const 3)
         )
        )
        (i32.store
         (tee_local $3
          (i32.add
           (i32.add
            (get_local $0)
            (get_local $1)
           )
           (i32.const 4)
          )
         )
         (i32.or
          (i32.load
           (get_local $3)
          )
          (i32.const 1)
         )
        )
       )
      )
      (br $folding-inner1)
     )
    )
    (if
     (i32.gt_u
      (tee_local $1
       (i32.load
        (i32.const 2229268)
       )
      )
      (get_local $3)
     )
     (block
      (i32.store
       (i32.const 2229268)
       (tee_local $1
        (i32.sub
         (get_local $1)
         (get_local $3)
        )
       )
      )
      (br $folding-inner0)
     )
    )
    (if
     (i32.le_u
      (tee_local $4
       (i32.and
        (tee_local $5
         (i32.add
          (tee_local $0
           (if (result i32)
            (i32.load
             (i32.const 2229728)
            )
            (i32.load
             (i32.const 2229736)
            )
            (block (result i32)
             (i32.store
              (i32.const 2229736)
              (i32.const 4096)
             )
             (i32.store
              (i32.const 2229732)
              (i32.const 4096)
             )
             (i32.store
              (i32.const 2229740)
              (i32.const -1)
             )
             (i32.store
              (i32.const 2229744)
              (i32.const -1)
             )
             (i32.store
              (i32.const 2229748)
              (i32.const 0)
             )
             (i32.store
              (i32.const 2229700)
              (i32.const 0)
             )
             (i32.store
              (i32.const 2229728)
              (i32.xor
               (i32.and
                (get_local $14)
                (i32.const -16)
               )
               (i32.const 1431655768)
              )
             )
             (i32.const 4096)
            )
           )
          )
          (tee_local $6
           (i32.add
            (get_local $3)
            (i32.const 47)
           )
          )
         )
        )
        (tee_local $7
         (i32.sub
          (i32.const 0)
          (get_local $0)
         )
        )
       )
      )
      (get_local $3)
     )
     (block
      (set_global $STACKTOP
       (get_local $14)
      )
      (return
       (i32.const 0)
      )
     )
    )
    (if
     (tee_local $0
      (i32.load
       (i32.const 2229696)
      )
     )
     (if
      (i32.or
       (i32.le_u
        (tee_local $9
         (i32.add
          (tee_local $2
           (i32.load
            (i32.const 2229688)
           )
          )
          (get_local $4)
         )
        )
        (get_local $2)
       )
       (i32.gt_u
        (get_local $9)
        (get_local $0)
       )
      )
      (block
       (set_global $STACKTOP
        (get_local $14)
       )
       (return
        (i32.const 0)
       )
      )
     )
    )
    (set_local $9
     (i32.add
      (get_local $3)
      (i32.const 48)
     )
    )
    (block $__rjto$7
     (block $__rjti$7
      (if
       (i32.and
        (i32.load
         (i32.const 2229700)
        )
        (i32.const 4)
       )
       (set_local $1
        (i32.const 0)
       )
       (block
        (block $do-once37
         (block $__rjti$3
          (block $__rjti$2
           (br_if $__rjti$2
            (i32.eqz
             (tee_local $0
              (i32.load
               (i32.const 2229280)
              )
             )
            )
           )
           (set_local $2
            (i32.const 2229704)
           )
           (loop $while-in34
            (block $while-out33
             (if
              (i32.le_u
               (tee_local $11
                (i32.load
                 (get_local $2)
                )
               )
               (get_local $0)
              )
              (br_if $while-out33
               (i32.gt_u
                (i32.add
                 (get_local $11)
                 (i32.load offset=4
                  (get_local $2)
                 )
                )
                (get_local $0)
               )
              )
             )
             (br_if $while-in34
              (tee_local $2
               (i32.load offset=8
                (get_local $2)
               )
              )
             )
             (br $__rjti$2)
            )
           )
           (if
            (i32.lt_u
             (tee_local $1
              (i32.and
               (i32.sub
                (get_local $5)
                (get_local $1)
               )
               (get_local $7)
              )
             )
             (i32.const 2147483647)
            )
            (if
             (i32.eq
              (tee_local $0
               (call $_sbrk
                (get_local $1)
               )
              )
              (i32.add
               (i32.load
                (get_local $2)
               )
               (i32.load offset=4
                (get_local $2)
               )
              )
             )
             (br_if $__rjti$7
              (i32.ne
               (get_local $0)
               (i32.const -1)
              )
             )
             (br $__rjti$3)
            )
            (set_local $1
             (i32.const 0)
            )
           )
           (br $do-once37)
          )
          (set_local $1
           (if (result i32)
            (i32.eq
             (tee_local $0
              (call $_sbrk
               (i32.const 0)
              )
             )
             (i32.const -1)
            )
            (i32.const 0)
            (block (result i32)
             (set_local $1
              (i32.sub
               (i32.and
                (i32.add
                 (tee_local $2
                  (i32.add
                   (tee_local $1
                    (i32.load
                     (i32.const 2229732)
                    )
                   )
                   (i32.const -1)
                  )
                 )
                 (get_local $0)
                )
                (i32.sub
                 (i32.const 0)
                 (get_local $1)
                )
               )
               (get_local $0)
              )
             )
             (set_local $2
              (i32.add
               (tee_local $1
                (i32.add
                 (if (result i32)
                  (i32.and
                   (get_local $2)
                   (get_local $0)
                  )
                  (get_local $1)
                  (i32.const 0)
                 )
                 (get_local $4)
                )
               )
               (tee_local $5
                (i32.load
                 (i32.const 2229688)
                )
               )
              )
             )
             (if (result i32)
              (i32.and
               (i32.gt_u
                (get_local $1)
                (get_local $3)
               )
               (i32.lt_u
                (get_local $1)
                (i32.const 2147483647)
               )
              )
              (block
               (if
                (tee_local $7
                 (i32.load
                  (i32.const 2229696)
                 )
                )
                (if
                 (i32.or
                  (i32.le_u
                   (get_local $2)
                   (get_local $5)
                  )
                  (i32.gt_u
                   (get_local $2)
                   (get_local $7)
                  )
                 )
                 (block
                  (set_local $1
                   (i32.const 0)
                  )
                  (br $do-once37)
                 )
                )
               )
               (br_if $__rjti$7
                (i32.eq
                 (tee_local $2
                  (call $_sbrk
                   (get_local $1)
                  )
                 )
                 (get_local $0)
                )
               )
               (set_local $0
                (get_local $2)
               )
               (br $__rjti$3)
              )
              (i32.const 0)
             )
            )
           )
          )
          (br $do-once37)
         )
         (if
          (i32.eqz
           (i32.and
            (i32.gt_u
             (get_local $9)
             (get_local $1)
            )
            (i32.and
             (i32.lt_u
              (get_local $1)
              (i32.const 2147483647)
             )
             (i32.ne
              (get_local $0)
              (i32.const -1)
             )
            )
           )
          )
          (if
           (i32.eq
            (get_local $0)
            (i32.const -1)
           )
           (block
            (set_local $1
             (i32.const 0)
            )
            (br $do-once37)
           )
           (br $__rjti$7)
          )
         )
         (br_if $__rjti$7
          (i32.ge_u
           (tee_local $2
            (i32.and
             (i32.add
              (i32.sub
               (get_local $6)
               (get_local $1)
              )
              (tee_local $2
               (i32.load
                (i32.const 2229736)
               )
              )
             )
             (i32.sub
              (i32.const 0)
              (get_local $2)
             )
            )
           )
           (i32.const 2147483647)
          )
         )
         (set_local $6
          (i32.sub
           (i32.const 0)
           (get_local $1)
          )
         )
         (set_local $1
          (if (result i32)
           (i32.eq
            (call $_sbrk
             (get_local $2)
            )
            (i32.const -1)
           )
           (block (result i32)
            (drop
             (call $_sbrk
              (get_local $6)
             )
            )
            (i32.const 0)
           )
           (block
            (set_local $1
             (i32.add
              (get_local $2)
              (get_local $1)
             )
            )
            (br $__rjti$7)
           )
          )
         )
        )
        (i32.store
         (i32.const 2229700)
         (i32.or
          (i32.load
           (i32.const 2229700)
          )
          (i32.const 4)
         )
        )
       )
      )
      (if
       (i32.lt_u
        (get_local $4)
        (i32.const 2147483647)
       )
       (block
        (set_local $4
         (i32.and
          (i32.lt_u
           (tee_local $0
            (call $_sbrk
             (get_local $4)
            )
           )
           (tee_local $2
            (call $_sbrk
             (i32.const 0)
            )
           )
          )
          (i32.and
           (i32.ne
            (get_local $0)
            (i32.const -1)
           )
           (i32.ne
            (get_local $2)
            (i32.const -1)
           )
          )
         )
        )
        (if
         (tee_local $6
          (i32.gt_u
           (tee_local $2
            (i32.sub
             (get_local $2)
             (get_local $0)
            )
           )
           (i32.add
            (get_local $3)
            (i32.const 40)
           )
          )
         )
         (set_local $1
          (get_local $2)
         )
        )
        (br_if $__rjti$7
         (i32.eqz
          (i32.or
           (i32.or
            (i32.eq
             (get_local $0)
             (i32.const -1)
            )
            (i32.xor
             (get_local $6)
             (i32.const 1)
            )
           )
           (i32.xor
            (get_local $4)
            (i32.const 1)
           )
          )
         )
        )
       )
      )
      (br $__rjto$7)
     )
     (i32.store
      (i32.const 2229688)
      (tee_local $2
       (i32.add
        (i32.load
         (i32.const 2229688)
        )
        (get_local $1)
       )
      )
     )
     (if
      (i32.gt_u
       (get_local $2)
       (i32.load
        (i32.const 2229692)
       )
      )
      (i32.store
       (i32.const 2229692)
       (get_local $2)
      )
     )
     (block $label$break$L294
      (if
       (tee_local $6
        (i32.load
         (i32.const 2229280)
        )
       )
       (block
        (set_local $2
         (i32.const 2229704)
        )
        (block $__rjto$4
         (block $__rjti$4
          (loop $while-in41
           (block $while-out40
            (br_if $__rjti$4
             (i32.eq
              (get_local $0)
              (i32.add
               (tee_local $4
                (i32.load
                 (get_local $2)
                )
               )
               (tee_local $5
                (i32.load offset=4
                 (get_local $2)
                )
               )
              )
             )
            )
            (br_if $while-in41
             (tee_local $2
              (i32.load offset=8
               (get_local $2)
              )
             )
            )
           )
          )
          (br $__rjto$4)
         )
         (set_local $7
          (i32.add
           (get_local $2)
           (i32.const 4)
          )
         )
         (if
          (i32.eqz
           (i32.and
            (i32.load offset=12
             (get_local $2)
            )
            (i32.const 8)
           )
          )
          (if
           (i32.and
            (i32.gt_u
             (get_local $0)
             (get_local $6)
            )
            (i32.le_u
             (get_local $4)
             (get_local $6)
            )
           )
           (block
            (i32.store
             (get_local $7)
             (i32.add
              (get_local $5)
              (get_local $1)
             )
            )
            (set_local $1
             (i32.add
              (i32.load
               (i32.const 2229268)
              )
              (get_local $1)
             )
            )
            (set_local $0
             (i32.and
              (i32.sub
               (i32.const 0)
               (tee_local $2
                (i32.add
                 (get_local $6)
                 (i32.const 8)
                )
               )
              )
              (i32.const 7)
             )
            )
            (i32.store
             (i32.const 2229280)
             (tee_local $2
              (i32.add
               (get_local $6)
               (if (result i32)
                (i32.and
                 (get_local $2)
                 (i32.const 7)
                )
                (get_local $0)
                (tee_local $0
                 (i32.const 0)
                )
               )
              )
             )
            )
            (i32.store
             (i32.const 2229268)
             (tee_local $0
              (i32.sub
               (get_local $1)
               (get_local $0)
              )
             )
            )
            (i32.store offset=4
             (get_local $2)
             (i32.or
              (get_local $0)
              (i32.const 1)
             )
            )
            (i32.store offset=4
             (i32.add
              (get_local $6)
              (get_local $1)
             )
             (i32.const 40)
            )
            (i32.store
             (i32.const 2229284)
             (i32.load
              (i32.const 2229744)
             )
            )
            (br $label$break$L294)
           )
          )
         )
        )
        (if
         (i32.lt_u
          (get_local $0)
          (tee_local $2
           (i32.load
            (i32.const 2229272)
           )
          )
         )
         (block
          (i32.store
           (i32.const 2229272)
           (get_local $0)
          )
          (set_local $2
           (get_local $0)
          )
         )
        )
        (set_local $5
         (i32.add
          (get_local $0)
          (get_local $1)
         )
        )
        (set_local $4
         (i32.const 2229704)
        )
        (block $__rjto$5
         (block $__rjti$5
          (loop $while-in43
           (block $while-out42
            (br_if $__rjti$5
             (i32.eq
              (i32.load
               (get_local $4)
              )
              (get_local $5)
             )
            )
            (br_if $while-in43
             (tee_local $4
              (i32.load offset=8
               (get_local $4)
              )
             )
            )
           )
          )
          (br $__rjto$5)
         )
         (if
          (i32.eqz
           (i32.and
            (i32.load offset=12
             (get_local $4)
            )
            (i32.const 8)
           )
          )
          (block
           (i32.store
            (get_local $4)
            (get_local $0)
           )
           (i32.store
            (tee_local $4
             (i32.add
              (get_local $4)
              (i32.const 4)
             )
            )
            (i32.add
             (i32.load
              (get_local $4)
             )
             (get_local $1)
            )
           )
           (set_local $4
            (i32.and
             (i32.sub
              (i32.const 0)
              (tee_local $1
               (i32.add
                (get_local $0)
                (i32.const 8)
               )
              )
             )
             (i32.const 7)
            )
           )
           (set_local $12
            (i32.and
             (i32.sub
              (i32.const 0)
              (tee_local $9
               (i32.add
                (get_local $5)
                (i32.const 8)
               )
              )
             )
             (i32.const 7)
            )
           )
           (set_local $7
            (i32.add
             (tee_local $11
              (i32.add
               (get_local $0)
               (if (result i32)
                (i32.and
                 (get_local $1)
                 (i32.const 7)
                )
                (get_local $4)
                (i32.const 0)
               )
              )
             )
             (get_local $3)
            )
           )
           (set_local $4
            (i32.sub
             (i32.sub
              (tee_local $1
               (i32.add
                (get_local $5)
                (if (result i32)
                 (i32.and
                  (get_local $9)
                  (i32.const 7)
                 )
                 (get_local $12)
                 (i32.const 0)
                )
               )
              )
              (get_local $11)
             )
             (get_local $3)
            )
           )
           (i32.store offset=4
            (get_local $11)
            (i32.or
             (get_local $3)
             (i32.const 3)
            )
           )
           (block $label$break$L317
            (if
             (i32.eq
              (get_local $6)
              (get_local $1)
             )
             (block
              (i32.store
               (i32.const 2229268)
               (tee_local $0
                (i32.add
                 (i32.load
                  (i32.const 2229268)
                 )
                 (get_local $4)
                )
               )
              )
              (i32.store
               (i32.const 2229280)
               (get_local $7)
              )
              (i32.store offset=4
               (get_local $7)
               (i32.or
                (get_local $0)
                (i32.const 1)
               )
              )
             )
             (block
              (if
               (i32.eq
                (i32.load
                 (i32.const 2229276)
                )
                (get_local $1)
               )
               (block
                (i32.store
                 (i32.const 2229264)
                 (tee_local $0
                  (i32.add
                   (i32.load
                    (i32.const 2229264)
                   )
                   (get_local $4)
                  )
                 )
                )
                (i32.store
                 (i32.const 2229276)
                 (get_local $7)
                )
                (i32.store offset=4
                 (get_local $7)
                 (i32.or
                  (get_local $0)
                  (i32.const 1)
                 )
                )
                (i32.store
                 (i32.add
                  (get_local $7)
                  (get_local $0)
                 )
                 (get_local $0)
                )
                (br $label$break$L317)
               )
              )
              (set_local $2
               (if (result i32)
                (i32.eq
                 (i32.and
                  (tee_local $0
                   (i32.load offset=4
                    (get_local $1)
                   )
                  )
                  (i32.const 3)
                 )
                 (i32.const 1)
                )
                (block (result i32)
                 (set_local $12
                  (i32.and
                   (get_local $0)
                   (i32.const -8)
                  )
                 )
                 (set_local $5
                  (i32.shr_u
                   (get_local $0)
                   (i32.const 3)
                  )
                 )
                 (block $label$break$L325
                  (if
                   (i32.lt_u
                    (get_local $0)
                    (i32.const 256)
                   )
                   (block
                    (set_local $3
                     (i32.load offset=12
                      (get_local $1)
                     )
                    )
                    (block $do-once46
                     (if
                      (i32.ne
                       (tee_local $6
                        (i32.load offset=8
                         (get_local $1)
                        )
                       )
                       (tee_local $0
                        (i32.add
                         (i32.shl
                          (get_local $5)
                          (i32.const 3)
                         )
                         (i32.const 2229296)
                        )
                       )
                      )
                      (block
                       (if
                        (i32.gt_u
                         (get_local $2)
                         (get_local $6)
                        )
                        (call $_abort)
                       )
                       (br_if $do-once46
                        (i32.eq
                         (i32.load offset=12
                          (get_local $6)
                         )
                         (get_local $1)
                        )
                       )
                       (call $_abort)
                      )
                     )
                    )
                    (if
                     (i32.eq
                      (get_local $3)
                      (get_local $6)
                     )
                     (block
                      (i32.store
                       (i32.const 2229256)
                       (i32.and
                        (i32.load
                         (i32.const 2229256)
                        )
                        (i32.xor
                         (i32.shl
                          (i32.const 1)
                          (get_local $5)
                         )
                         (i32.const -1)
                        )
                       )
                      )
                      (br $label$break$L325)
                     )
                    )
                    (block $do-once48
                     (if
                      (i32.eq
                       (get_local $3)
                       (get_local $0)
                      )
                      (set_local $20
                       (i32.add
                        (get_local $3)
                        (i32.const 8)
                       )
                      )
                      (block
                       (if
                        (i32.gt_u
                         (get_local $2)
                         (get_local $3)
                        )
                        (call $_abort)
                       )
                       (if
                        (i32.eq
                         (i32.load
                          (tee_local $0
                           (i32.add
                            (get_local $3)
                            (i32.const 8)
                           )
                          )
                         )
                         (get_local $1)
                        )
                        (block
                         (set_local $20
                          (get_local $0)
                         )
                         (br $do-once48)
                        )
                       )
                       (call $_abort)
                      )
                     )
                    )
                    (i32.store offset=12
                     (get_local $6)
                     (get_local $3)
                    )
                    (i32.store
                     (get_local $20)
                     (get_local $6)
                    )
                   )
                   (block
                    (set_local $9
                     (i32.load offset=24
                      (get_local $1)
                     )
                    )
                    (block $do-once50
                     (if
                      (i32.eq
                       (tee_local $0
                        (i32.load offset=12
                         (get_local $1)
                        )
                       )
                       (get_local $1)
                      )
                      (block
                       (if
                        (tee_local $0
                         (i32.load
                          (tee_local $6
                           (i32.add
                            (tee_local $3
                             (i32.add
                              (get_local $1)
                              (i32.const 16)
                             )
                            )
                            (i32.const 4)
                           )
                          )
                         )
                        )
                        (set_local $3
                         (get_local $6)
                        )
                        (br_if $do-once50
                         (i32.eqz
                          (tee_local $0
                           (i32.load
                            (get_local $3)
                           )
                          )
                         )
                        )
                       )
                       (loop $while-in53
                        (block $while-out52
                         (if
                          (i32.eqz
                           (tee_local $5
                            (i32.load
                             (tee_local $6
                              (i32.add
                               (get_local $0)
                               (i32.const 20)
                              )
                             )
                            )
                           )
                          )
                          (br_if $while-out52
                           (i32.eqz
                            (tee_local $5
                             (i32.load
                              (tee_local $6
                               (i32.add
                                (get_local $0)
                                (i32.const 16)
                               )
                              )
                             )
                            )
                           )
                          )
                         )
                         (set_local $3
                          (get_local $6)
                         )
                         (set_local $0
                          (get_local $5)
                         )
                         (br $while-in53)
                        )
                       )
                       (if
                        (i32.gt_u
                         (get_local $2)
                         (get_local $3)
                        )
                        (call $_abort)
                        (block
                         (i32.store
                          (get_local $3)
                          (i32.const 0)
                         )
                         (set_local $10
                          (get_local $0)
                         )
                        )
                       )
                      )
                      (block
                       (if
                        (i32.gt_u
                         (get_local $2)
                         (tee_local $3
                          (i32.load offset=8
                           (get_local $1)
                          )
                         )
                        )
                        (call $_abort)
                       )
                       (if
                        (i32.ne
                         (i32.load
                          (tee_local $2
                           (i32.add
                            (get_local $3)
                            (i32.const 12)
                           )
                          )
                         )
                         (get_local $1)
                        )
                        (call $_abort)
                       )
                       (if
                        (i32.eq
                         (i32.load
                          (tee_local $6
                           (i32.add
                            (get_local $0)
                            (i32.const 8)
                           )
                          )
                         )
                         (get_local $1)
                        )
                        (block
                         (i32.store
                          (get_local $2)
                          (get_local $0)
                         )
                         (i32.store
                          (get_local $6)
                          (get_local $3)
                         )
                         (set_local $10
                          (get_local $0)
                         )
                        )
                        (call $_abort)
                       )
                      )
                     )
                    )
                    (br_if $label$break$L325
                     (i32.eqz
                      (get_local $9)
                     )
                    )
                    (block $do-once54
                     (if
                      (i32.eq
                       (i32.load
                        (tee_local $3
                         (i32.add
                          (i32.shl
                           (tee_local $0
                            (i32.load offset=28
                             (get_local $1)
                            )
                           )
                           (i32.const 2)
                          )
                          (i32.const 2229560)
                         )
                        )
                       )
                       (get_local $1)
                      )
                      (block
                       (i32.store
                        (get_local $3)
                        (get_local $10)
                       )
                       (br_if $do-once54
                        (get_local $10)
                       )
                       (i32.store
                        (i32.const 2229260)
                        (i32.and
                         (i32.load
                          (i32.const 2229260)
                         )
                         (i32.xor
                          (i32.shl
                           (i32.const 1)
                           (get_local $0)
                          )
                          (i32.const -1)
                         )
                        )
                       )
                       (br $label$break$L325)
                      )
                      (if
                       (i32.gt_u
                        (i32.load
                         (i32.const 2229272)
                        )
                        (get_local $9)
                       )
                       (call $_abort)
                       (block
                        (set_local $0
                         (i32.add
                          (get_local $9)
                          (i32.const 20)
                         )
                        )
                        (i32.store
                         (if (result i32)
                          (i32.eq
                           (i32.load
                            (tee_local $3
                             (i32.add
                              (get_local $9)
                              (i32.const 16)
                             )
                            )
                           )
                           (get_local $1)
                          )
                          (get_local $3)
                          (get_local $0)
                         )
                         (get_local $10)
                        )
                        (br_if $label$break$L325
                         (i32.eqz
                          (get_local $10)
                         )
                        )
                       )
                      )
                     )
                    )
                    (if
                     (i32.gt_u
                      (tee_local $3
                       (i32.load
                        (i32.const 2229272)
                       )
                      )
                      (get_local $10)
                     )
                     (call $_abort)
                    )
                    (i32.store offset=24
                     (get_local $10)
                     (get_local $9)
                    )
                    (if
                     (tee_local $0
                      (i32.load
                       (tee_local $2
                        (i32.add
                         (get_local $1)
                         (i32.const 16)
                        )
                       )
                      )
                     )
                     (if
                      (i32.gt_u
                       (get_local $3)
                       (get_local $0)
                      )
                      (call $_abort)
                      (block
                       (i32.store offset=16
                        (get_local $10)
                        (get_local $0)
                       )
                       (i32.store offset=24
                        (get_local $0)
                        (get_local $10)
                       )
                      )
                     )
                    )
                    (br_if $label$break$L325
                     (i32.eqz
                      (tee_local $0
                       (i32.load offset=4
                        (get_local $2)
                       )
                      )
                     )
                    )
                    (if
                     (i32.gt_u
                      (i32.load
                       (i32.const 2229272)
                      )
                      (get_local $0)
                     )
                     (call $_abort)
                     (block
                      (i32.store offset=20
                       (get_local $10)
                       (get_local $0)
                      )
                      (i32.store offset=24
                       (get_local $0)
                       (get_local $10)
                      )
                     )
                    )
                   )
                  )
                 )
                 (set_local $1
                  (i32.add
                   (get_local $1)
                   (get_local $12)
                  )
                 )
                 (i32.add
                  (get_local $12)
                  (get_local $4)
                 )
                )
                (get_local $4)
               )
              )
              (i32.store
               (tee_local $0
                (i32.add
                 (get_local $1)
                 (i32.const 4)
                )
               )
               (i32.and
                (i32.load
                 (get_local $0)
                )
                (i32.const -2)
               )
              )
              (i32.store offset=4
               (get_local $7)
               (i32.or
                (get_local $2)
                (i32.const 1)
               )
              )
              (i32.store
               (i32.add
                (get_local $7)
                (get_local $2)
               )
               (get_local $2)
              )
              (set_local $3
               (i32.shr_u
                (get_local $2)
                (i32.const 3)
               )
              )
              (if
               (i32.lt_u
                (get_local $2)
                (i32.const 256)
               )
               (block
                (set_local $0
                 (i32.add
                  (i32.shl
                   (get_local $3)
                   (i32.const 3)
                  )
                  (i32.const 2229296)
                 )
                )
                (block $do-once58
                 (if
                  (i32.and
                   (tee_local $1
                    (i32.load
                     (i32.const 2229256)
                    )
                   )
                   (tee_local $3
                    (i32.shl
                     (i32.const 1)
                     (get_local $3)
                    )
                   )
                  )
                  (block
                   (if
                    (i32.le_u
                     (i32.load
                      (i32.const 2229272)
                     )
                     (tee_local $1
                      (i32.load
                       (tee_local $3
                        (i32.add
                         (get_local $0)
                         (i32.const 8)
                        )
                       )
                      )
                     )
                    )
                    (block
                     (set_local $17
                      (get_local $1)
                     )
                     (set_local $21
                      (get_local $3)
                     )
                     (br $do-once58)
                    )
                   )
                   (call $_abort)
                  )
                  (block
                   (i32.store
                    (i32.const 2229256)
                    (i32.or
                     (get_local $1)
                     (get_local $3)
                    )
                   )
                   (set_local $17
                    (get_local $0)
                   )
                   (set_local $21
                    (i32.add
                     (get_local $0)
                     (i32.const 8)
                    )
                   )
                  )
                 )
                )
                (i32.store
                 (get_local $21)
                 (get_local $7)
                )
                (i32.store offset=12
                 (get_local $17)
                 (get_local $7)
                )
                (i32.store offset=8
                 (get_local $7)
                 (get_local $17)
                )
                (i32.store offset=12
                 (get_local $7)
                 (get_local $0)
                )
                (br $label$break$L317)
               )
              )
              (set_local $0
               (i32.add
                (i32.shl
                 (tee_local $3
                  (block $do-once60 (result i32)
                   (if (result i32)
                    (tee_local $0
                     (i32.shr_u
                      (get_local $2)
                      (i32.const 8)
                     )
                    )
                    (block (result i32)
                     (drop
                      (br_if $do-once60
                       (i32.const 31)
                       (i32.gt_u
                        (get_local $2)
                        (i32.const 16777215)
                       )
                      )
                     )
                     (i32.or
                      (i32.and
                       (i32.shr_u
                        (get_local $2)
                        (i32.add
                         (tee_local $0
                          (i32.add
                           (i32.sub
                            (i32.const 14)
                            (i32.or
                             (i32.or
                              (tee_local $1
                               (i32.and
                                (i32.shr_u
                                 (i32.add
                                  (tee_local $3
                                   (i32.shl
                                    (get_local $0)
                                    (tee_local $0
                                     (i32.and
                                      (i32.shr_u
                                       (i32.add
                                        (get_local $0)
                                        (i32.const 1048320)
                                       )
                                       (i32.const 16)
                                      )
                                      (i32.const 8)
                                     )
                                    )
                                   )
                                  )
                                  (i32.const 520192)
                                 )
                                 (i32.const 16)
                                )
                                (i32.const 4)
                               )
                              )
                              (get_local $0)
                             )
                             (tee_local $3
                              (i32.and
                               (i32.shr_u
                                (i32.add
                                 (tee_local $0
                                  (i32.shl
                                   (get_local $3)
                                   (get_local $1)
                                  )
                                 )
                                 (i32.const 245760)
                                )
                                (i32.const 16)
                               )
                               (i32.const 2)
                              )
                             )
                            )
                           )
                           (i32.shr_u
                            (i32.shl
                             (get_local $0)
                             (get_local $3)
                            )
                            (i32.const 15)
                           )
                          )
                         )
                         (i32.const 7)
                        )
                       )
                       (i32.const 1)
                      )
                      (i32.shl
                       (get_local $0)
                       (i32.const 1)
                      )
                     )
                    )
                    (i32.const 0)
                   )
                  )
                 )
                 (i32.const 2)
                )
                (i32.const 2229560)
               )
              )
              (i32.store offset=28
               (get_local $7)
               (get_local $3)
              )
              (i32.store offset=4
               (tee_local $1
                (i32.add
                 (get_local $7)
                 (i32.const 16)
                )
               )
               (i32.const 0)
              )
              (i32.store
               (get_local $1)
               (i32.const 0)
              )
              (if
               (i32.eqz
                (i32.and
                 (tee_local $1
                  (i32.load
                   (i32.const 2229260)
                  )
                 )
                 (tee_local $4
                  (i32.shl
                   (i32.const 1)
                   (get_local $3)
                  )
                 )
                )
               )
               (block
                (i32.store
                 (i32.const 2229260)
                 (i32.or
                  (get_local $1)
                  (get_local $4)
                 )
                )
                (i32.store
                 (get_local $0)
                 (get_local $7)
                )
                (i32.store offset=24
                 (get_local $7)
                 (get_local $0)
                )
                (i32.store offset=12
                 (get_local $7)
                 (get_local $7)
                )
                (i32.store offset=8
                 (get_local $7)
                 (get_local $7)
                )
                (br $label$break$L317)
               )
              )
              (block $label$break$L410
               (if
                (i32.eq
                 (i32.and
                  (i32.load offset=4
                   (tee_local $0
                    (i32.load
                     (get_local $0)
                    )
                   )
                  )
                  (i32.const -8)
                 )
                 (get_local $2)
                )
                (set_local $8
                 (get_local $0)
                )
                (block
                 (set_local $1
                  (i32.sub
                   (i32.const 25)
                   (i32.shr_u
                    (get_local $3)
                    (i32.const 1)
                   )
                  )
                 )
                 (set_local $1
                  (i32.shl
                   (get_local $2)
                   (if (result i32)
                    (i32.eq
                     (get_local $3)
                     (i32.const 31)
                    )
                    (i32.const 0)
                    (get_local $1)
                   )
                  )
                 )
                 (loop $while-in64
                  (if
                   (tee_local $3
                    (i32.load
                     (tee_local $4
                      (i32.add
                       (i32.add
                        (get_local $0)
                        (i32.const 16)
                       )
                       (i32.shl
                        (i32.shr_u
                         (get_local $1)
                         (i32.const 31)
                        )
                        (i32.const 2)
                       )
                      )
                     )
                    )
                   )
                   (block
                    (set_local $1
                     (i32.shl
                      (get_local $1)
                      (i32.const 1)
                     )
                    )
                    (if
                     (i32.eq
                      (i32.and
                       (i32.load offset=4
                        (get_local $3)
                       )
                       (i32.const -8)
                      )
                      (get_local $2)
                     )
                     (block
                      (set_local $8
                       (get_local $3)
                      )
                      (br $label$break$L410)
                     )
                     (block
                      (set_local $0
                       (get_local $3)
                      )
                      (br $while-in64)
                     )
                    )
                   )
                  )
                 )
                 (if
                  (i32.gt_u
                   (i32.load
                    (i32.const 2229272)
                   )
                   (get_local $4)
                  )
                  (call $_abort)
                  (block
                   (i32.store
                    (get_local $4)
                    (get_local $7)
                   )
                   (i32.store offset=24
                    (get_local $7)
                    (get_local $0)
                   )
                   (i32.store offset=12
                    (get_local $7)
                    (get_local $7)
                   )
                   (i32.store offset=8
                    (get_local $7)
                    (get_local $7)
                   )
                   (br $label$break$L317)
                  )
                 )
                )
               )
              )
              (if
               (i32.and
                (i32.le_u
                 (tee_local $3
                  (i32.load
                   (i32.const 2229272)
                  )
                 )
                 (tee_local $0
                  (i32.load
                   (tee_local $1
                    (i32.add
                     (get_local $8)
                     (i32.const 8)
                    )
                   )
                  )
                 )
                )
                (i32.le_u
                 (get_local $3)
                 (get_local $8)
                )
               )
               (block
                (i32.store offset=12
                 (get_local $0)
                 (get_local $7)
                )
                (i32.store
                 (get_local $1)
                 (get_local $7)
                )
                (i32.store offset=8
                 (get_local $7)
                 (get_local $0)
                )
                (i32.store offset=12
                 (get_local $7)
                 (get_local $8)
                )
                (i32.store offset=24
                 (get_local $7)
                 (i32.const 0)
                )
               )
               (call $_abort)
              )
             )
            )
           )
           (set_global $STACKTOP
            (get_local $14)
           )
           (return
            (i32.add
             (get_local $11)
             (i32.const 8)
            )
           )
          )
         )
        )
        (set_local $2
         (i32.const 2229704)
        )
        (loop $while-in66
         (block $while-out65
          (if
           (i32.le_u
            (tee_local $4
             (i32.load
              (get_local $2)
             )
            )
            (get_local $6)
           )
           (br_if $while-out65
            (i32.gt_u
             (tee_local $9
              (i32.add
               (get_local $4)
               (i32.load offset=4
                (get_local $2)
               )
              )
             )
             (get_local $6)
            )
           )
          )
          (set_local $2
           (i32.load offset=8
            (get_local $2)
           )
          )
          (br $while-in66)
         )
        )
        (set_local $5
         (i32.and
          (i32.sub
           (i32.const 0)
           (tee_local $4
            (i32.add
             (tee_local $2
              (i32.add
               (get_local $9)
               (i32.const -47)
              )
             )
             (i32.const 8)
            )
           )
          )
          (i32.const 7)
         )
        )
        (set_local $7
         (i32.add
          (if (result i32)
           (i32.lt_u
            (tee_local $2
             (i32.add
              (get_local $2)
              (if (result i32)
               (i32.and
                (get_local $4)
                (i32.const 7)
               )
               (get_local $5)
               (i32.const 0)
              )
             )
            )
            (tee_local $11
             (i32.add
              (get_local $6)
              (i32.const 16)
             )
            )
           )
           (tee_local $2
            (get_local $6)
           )
           (get_local $2)
          )
          (i32.const 8)
         )
        )
        (set_local $4
         (i32.add
          (get_local $2)
          (i32.const 24)
         )
        )
        (set_local $10
         (i32.add
          (get_local $1)
          (i32.const -40)
         )
        )
        (set_local $5
         (i32.and
          (i32.sub
           (i32.const 0)
           (tee_local $8
            (i32.add
             (get_local $0)
             (i32.const 8)
            )
           )
          )
          (i32.const 7)
         )
        )
        (i32.store
         (i32.const 2229280)
         (tee_local $8
          (i32.add
           (get_local $0)
           (if (result i32)
            (i32.and
             (get_local $8)
             (i32.const 7)
            )
            (get_local $5)
            (tee_local $5
             (i32.const 0)
            )
           )
          )
         )
        )
        (i32.store
         (i32.const 2229268)
         (tee_local $5
          (i32.sub
           (get_local $10)
           (get_local $5)
          )
         )
        )
        (i32.store offset=4
         (get_local $8)
         (i32.or
          (get_local $5)
          (i32.const 1)
         )
        )
        (i32.store offset=4
         (i32.add
          (get_local $0)
          (get_local $10)
         )
         (i32.const 40)
        )
        (i32.store
         (i32.const 2229284)
         (i32.load
          (i32.const 2229744)
         )
        )
        (i32.store
         (tee_local $5
          (i32.add
           (get_local $2)
           (i32.const 4)
          )
         )
         (i32.const 27)
        )
        (i64.store align=4
         (get_local $7)
         (i64.load align=4
          (i32.const 2229704)
         )
        )
        (i64.store offset=8 align=4
         (get_local $7)
         (i64.load align=4
          (i32.const 2229712)
         )
        )
        (i32.store
         (i32.const 2229704)
         (get_local $0)
        )
        (i32.store
         (i32.const 2229708)
         (get_local $1)
        )
        (i32.store
         (i32.const 2229716)
         (i32.const 0)
        )
        (i32.store
         (i32.const 2229712)
         (get_local $7)
        )
        (set_local $0
         (get_local $4)
        )
        (loop $while-in68
         (i32.store
          (tee_local $1
           (i32.add
            (get_local $0)
            (i32.const 4)
           )
          )
          (i32.const 7)
         )
         (if
          (i32.lt_u
           (i32.add
            (get_local $0)
            (i32.const 8)
           )
           (get_local $9)
          )
          (block
           (set_local $0
            (get_local $1)
           )
           (br $while-in68)
          )
         )
        )
        (if
         (i32.ne
          (get_local $2)
          (get_local $6)
         )
         (block
          (i32.store
           (get_local $5)
           (i32.and
            (i32.load
             (get_local $5)
            )
            (i32.const -2)
           )
          )
          (i32.store offset=4
           (get_local $6)
           (i32.or
            (tee_local $4
             (i32.sub
              (get_local $2)
              (get_local $6)
             )
            )
            (i32.const 1)
           )
          )
          (i32.store
           (get_local $2)
           (get_local $4)
          )
          (set_local $1
           (i32.shr_u
            (get_local $4)
            (i32.const 3)
           )
          )
          (if
           (i32.lt_u
            (get_local $4)
            (i32.const 256)
           )
           (block
            (set_local $0
             (i32.add
              (i32.shl
               (get_local $1)
               (i32.const 3)
              )
              (i32.const 2229296)
             )
            )
            (if
             (i32.and
              (tee_local $2
               (i32.load
                (i32.const 2229256)
               )
              )
              (tee_local $1
               (i32.shl
                (i32.const 1)
                (get_local $1)
               )
              )
             )
             (if
              (i32.gt_u
               (i32.load
                (i32.const 2229272)
               )
               (tee_local $2
                (i32.load
                 (tee_local $1
                  (i32.add
                   (get_local $0)
                   (i32.const 8)
                  )
                 )
                )
               )
              )
              (call $_abort)
              (block
               (set_local $18
                (get_local $2)
               )
               (set_local $22
                (get_local $1)
               )
              )
             )
             (block
              (i32.store
               (i32.const 2229256)
               (i32.or
                (get_local $2)
                (get_local $1)
               )
              )
              (set_local $18
               (get_local $0)
              )
              (set_local $22
               (i32.add
                (get_local $0)
                (i32.const 8)
               )
              )
             )
            )
            (i32.store
             (get_local $22)
             (get_local $6)
            )
            (i32.store offset=12
             (get_local $18)
             (get_local $6)
            )
            (i32.store offset=8
             (get_local $6)
             (get_local $18)
            )
            (i32.store offset=12
             (get_local $6)
             (get_local $0)
            )
            (br $label$break$L294)
           )
          )
          (set_local $0
           (i32.add
            (i32.shl
             (tee_local $1
              (if (result i32)
               (tee_local $0
                (i32.shr_u
                 (get_local $4)
                 (i32.const 8)
                )
               )
               (if (result i32)
                (i32.gt_u
                 (get_local $4)
                 (i32.const 16777215)
                )
                (i32.const 31)
                (i32.or
                 (i32.and
                  (i32.shr_u
                   (get_local $4)
                   (i32.add
                    (tee_local $0
                     (i32.add
                      (i32.sub
                       (i32.const 14)
                       (i32.or
                        (i32.or
                         (tee_local $2
                          (i32.and
                           (i32.shr_u
                            (i32.add
                             (tee_local $1
                              (i32.shl
                               (get_local $0)
                               (tee_local $0
                                (i32.and
                                 (i32.shr_u
                                  (i32.add
                                   (get_local $0)
                                   (i32.const 1048320)
                                  )
                                  (i32.const 16)
                                 )
                                 (i32.const 8)
                                )
                               )
                              )
                             )
                             (i32.const 520192)
                            )
                            (i32.const 16)
                           )
                           (i32.const 4)
                          )
                         )
                         (get_local $0)
                        )
                        (tee_local $1
                         (i32.and
                          (i32.shr_u
                           (i32.add
                            (tee_local $0
                             (i32.shl
                              (get_local $1)
                              (get_local $2)
                             )
                            )
                            (i32.const 245760)
                           )
                           (i32.const 16)
                          )
                          (i32.const 2)
                         )
                        )
                       )
                      )
                      (i32.shr_u
                       (i32.shl
                        (get_local $0)
                        (get_local $1)
                       )
                       (i32.const 15)
                      )
                     )
                    )
                    (i32.const 7)
                   )
                  )
                  (i32.const 1)
                 )
                 (i32.shl
                  (get_local $0)
                  (i32.const 1)
                 )
                )
               )
               (i32.const 0)
              )
             )
             (i32.const 2)
            )
            (i32.const 2229560)
           )
          )
          (i32.store offset=28
           (get_local $6)
           (get_local $1)
          )
          (i32.store offset=20
           (get_local $6)
           (i32.const 0)
          )
          (i32.store
           (get_local $11)
           (i32.const 0)
          )
          (if
           (i32.eqz
            (i32.and
             (tee_local $2
              (i32.load
               (i32.const 2229260)
              )
             )
             (tee_local $5
              (i32.shl
               (i32.const 1)
               (get_local $1)
              )
             )
            )
           )
           (block
            (i32.store
             (i32.const 2229260)
             (i32.or
              (get_local $2)
              (get_local $5)
             )
            )
            (i32.store
             (get_local $0)
             (get_local $6)
            )
            (i32.store offset=24
             (get_local $6)
             (get_local $0)
            )
            (i32.store offset=12
             (get_local $6)
             (get_local $6)
            )
            (i32.store offset=8
             (get_local $6)
             (get_local $6)
            )
            (br $label$break$L294)
           )
          )
          (block $label$break$L451
           (if
            (i32.eq
             (i32.and
              (i32.load offset=4
               (tee_local $0
                (i32.load
                 (get_local $0)
                )
               )
              )
              (i32.const -8)
             )
             (get_local $4)
            )
            (set_local $12
             (get_local $0)
            )
            (block
             (set_local $2
              (i32.sub
               (i32.const 25)
               (i32.shr_u
                (get_local $1)
                (i32.const 1)
               )
              )
             )
             (set_local $2
              (i32.shl
               (get_local $4)
               (if (result i32)
                (i32.eq
                 (get_local $1)
                 (i32.const 31)
                )
                (i32.const 0)
                (get_local $2)
               )
              )
             )
             (loop $while-in71
              (if
               (tee_local $1
                (i32.load
                 (tee_local $5
                  (i32.add
                   (i32.add
                    (get_local $0)
                    (i32.const 16)
                   )
                   (i32.shl
                    (i32.shr_u
                     (get_local $2)
                     (i32.const 31)
                    )
                    (i32.const 2)
                   )
                  )
                 )
                )
               )
               (block
                (set_local $2
                 (i32.shl
                  (get_local $2)
                  (i32.const 1)
                 )
                )
                (if
                 (i32.eq
                  (i32.and
                   (i32.load offset=4
                    (get_local $1)
                   )
                   (i32.const -8)
                  )
                  (get_local $4)
                 )
                 (block
                  (set_local $12
                   (get_local $1)
                  )
                  (br $label$break$L451)
                 )
                 (block
                  (set_local $0
                   (get_local $1)
                  )
                  (br $while-in71)
                 )
                )
               )
              )
             )
             (if
              (i32.gt_u
               (i32.load
                (i32.const 2229272)
               )
               (get_local $5)
              )
              (call $_abort)
              (block
               (i32.store
                (get_local $5)
                (get_local $6)
               )
               (i32.store offset=24
                (get_local $6)
                (get_local $0)
               )
               (i32.store offset=12
                (get_local $6)
                (get_local $6)
               )
               (i32.store offset=8
                (get_local $6)
                (get_local $6)
               )
               (br $label$break$L294)
              )
             )
            )
           )
          )
          (if
           (i32.and
            (i32.le_u
             (tee_local $1
              (i32.load
               (i32.const 2229272)
              )
             )
             (tee_local $0
              (i32.load
               (tee_local $2
                (i32.add
                 (get_local $12)
                 (i32.const 8)
                )
               )
              )
             )
            )
            (i32.le_u
             (get_local $1)
             (get_local $12)
            )
           )
           (block
            (i32.store offset=12
             (get_local $0)
             (get_local $6)
            )
            (i32.store
             (get_local $2)
             (get_local $6)
            )
            (i32.store offset=8
             (get_local $6)
             (get_local $0)
            )
            (i32.store offset=12
             (get_local $6)
             (get_local $12)
            )
            (i32.store offset=24
             (get_local $6)
             (i32.const 0)
            )
           )
           (call $_abort)
          )
         )
        )
       )
       (block
        (if
         (i32.or
          (i32.eqz
           (tee_local $2
            (i32.load
             (i32.const 2229272)
            )
           )
          )
          (i32.lt_u
           (get_local $0)
           (get_local $2)
          )
         )
         (i32.store
          (i32.const 2229272)
          (get_local $0)
         )
        )
        (i32.store
         (i32.const 2229704)
         (get_local $0)
        )
        (i32.store
         (i32.const 2229708)
         (get_local $1)
        )
        (i32.store
         (i32.const 2229716)
         (i32.const 0)
        )
        (i32.store
         (i32.const 2229292)
         (i32.load
          (i32.const 2229728)
         )
        )
        (i32.store
         (i32.const 2229288)
         (i32.const -1)
        )
        (i32.store
         (i32.const 2229308)
         (i32.const 2229296)
        )
        (i32.store
         (i32.const 2229304)
         (i32.const 2229296)
        )
        (i32.store
         (i32.const 2229316)
         (i32.const 2229304)
        )
        (i32.store
         (i32.const 2229312)
         (i32.const 2229304)
        )
        (i32.store
         (i32.const 2229324)
         (i32.const 2229312)
        )
        (i32.store
         (i32.const 2229320)
         (i32.const 2229312)
        )
        (i32.store
         (i32.const 2229332)
         (i32.const 2229320)
        )
        (i32.store
         (i32.const 2229328)
         (i32.const 2229320)
        )
        (i32.store
         (i32.const 2229340)
         (i32.const 2229328)
        )
        (i32.store
         (i32.const 2229336)
         (i32.const 2229328)
        )
        (i32.store
         (i32.const 2229348)
         (i32.const 2229336)
        )
        (i32.store
         (i32.const 2229344)
         (i32.const 2229336)
        )
        (i32.store
         (i32.const 2229356)
         (i32.const 2229344)
        )
        (i32.store
         (i32.const 2229352)
         (i32.const 2229344)
        )
        (i32.store
         (i32.const 2229364)
         (i32.const 2229352)
        )
        (i32.store
         (i32.const 2229360)
         (i32.const 2229352)
        )
        (i32.store
         (i32.const 2229372)
         (i32.const 2229360)
        )
        (i32.store
         (i32.const 2229368)
         (i32.const 2229360)
        )
        (i32.store
         (i32.const 2229380)
         (i32.const 2229368)
        )
        (i32.store
         (i32.const 2229376)
         (i32.const 2229368)
        )
        (i32.store
         (i32.const 2229388)
         (i32.const 2229376)
        )
        (i32.store
         (i32.const 2229384)
         (i32.const 2229376)
        )
        (i32.store
         (i32.const 2229396)
         (i32.const 2229384)
        )
        (i32.store
         (i32.const 2229392)
         (i32.const 2229384)
        )
        (i32.store
         (i32.const 2229404)
         (i32.const 2229392)
        )
        (i32.store
         (i32.const 2229400)
         (i32.const 2229392)
        )
        (i32.store
         (i32.const 2229412)
         (i32.const 2229400)
        )
        (i32.store
         (i32.const 2229408)
         (i32.const 2229400)
        )
        (i32.store
         (i32.const 2229420)
         (i32.const 2229408)
        )
        (i32.store
         (i32.const 2229416)
         (i32.const 2229408)
        )
        (i32.store
         (i32.const 2229428)
         (i32.const 2229416)
        )
        (i32.store
         (i32.const 2229424)
         (i32.const 2229416)
        )
        (i32.store
         (i32.const 2229436)
         (i32.const 2229424)
        )
        (i32.store
         (i32.const 2229432)
         (i32.const 2229424)
        )
        (i32.store
         (i32.const 2229444)
         (i32.const 2229432)
        )
        (i32.store
         (i32.const 2229440)
         (i32.const 2229432)
        )
        (i32.store
         (i32.const 2229452)
         (i32.const 2229440)
        )
        (i32.store
         (i32.const 2229448)
         (i32.const 2229440)
        )
        (i32.store
         (i32.const 2229460)
         (i32.const 2229448)
        )
        (i32.store
         (i32.const 2229456)
         (i32.const 2229448)
        )
        (i32.store
         (i32.const 2229468)
         (i32.const 2229456)
        )
        (i32.store
         (i32.const 2229464)
         (i32.const 2229456)
        )
        (i32.store
         (i32.const 2229476)
         (i32.const 2229464)
        )
        (i32.store
         (i32.const 2229472)
         (i32.const 2229464)
        )
        (i32.store
         (i32.const 2229484)
         (i32.const 2229472)
        )
        (i32.store
         (i32.const 2229480)
         (i32.const 2229472)
        )
        (i32.store
         (i32.const 2229492)
         (i32.const 2229480)
        )
        (i32.store
         (i32.const 2229488)
         (i32.const 2229480)
        )
        (i32.store
         (i32.const 2229500)
         (i32.const 2229488)
        )
        (i32.store
         (i32.const 2229496)
         (i32.const 2229488)
        )
        (i32.store
         (i32.const 2229508)
         (i32.const 2229496)
        )
        (i32.store
         (i32.const 2229504)
         (i32.const 2229496)
        )
        (i32.store
         (i32.const 2229516)
         (i32.const 2229504)
        )
        (i32.store
         (i32.const 2229512)
         (i32.const 2229504)
        )
        (i32.store
         (i32.const 2229524)
         (i32.const 2229512)
        )
        (i32.store
         (i32.const 2229520)
         (i32.const 2229512)
        )
        (i32.store
         (i32.const 2229532)
         (i32.const 2229520)
        )
        (i32.store
         (i32.const 2229528)
         (i32.const 2229520)
        )
        (i32.store
         (i32.const 2229540)
         (i32.const 2229528)
        )
        (i32.store
         (i32.const 2229536)
         (i32.const 2229528)
        )
        (i32.store
         (i32.const 2229548)
         (i32.const 2229536)
        )
        (i32.store
         (i32.const 2229544)
         (i32.const 2229536)
        )
        (i32.store
         (i32.const 2229556)
         (i32.const 2229544)
        )
        (i32.store
         (i32.const 2229552)
         (i32.const 2229544)
        )
        (set_local $2
         (i32.add
          (get_local $1)
          (i32.const -40)
         )
        )
        (set_local $1
         (i32.and
          (i32.sub
           (i32.const 0)
           (tee_local $4
            (i32.add
             (get_local $0)
             (i32.const 8)
            )
           )
          )
          (i32.const 7)
         )
        )
        (i32.store
         (i32.const 2229280)
         (tee_local $4
          (i32.add
           (get_local $0)
           (if (result i32)
            (i32.and
             (get_local $4)
             (i32.const 7)
            )
            (get_local $1)
            (tee_local $1
             (i32.const 0)
            )
           )
          )
         )
        )
        (i32.store
         (i32.const 2229268)
         (tee_local $1
          (i32.sub
           (get_local $2)
           (get_local $1)
          )
         )
        )
        (i32.store offset=4
         (get_local $4)
         (i32.or
          (get_local $1)
          (i32.const 1)
         )
        )
        (i32.store offset=4
         (i32.add
          (get_local $0)
          (get_local $2)
         )
         (i32.const 40)
        )
        (i32.store
         (i32.const 2229284)
         (i32.load
          (i32.const 2229744)
         )
        )
       )
      )
     )
     (if
      (i32.gt_u
       (tee_local $0
        (i32.load
         (i32.const 2229268)
        )
       )
       (get_local $3)
      )
      (block
       (i32.store
        (i32.const 2229268)
        (tee_local $1
         (i32.sub
          (get_local $0)
          (get_local $3)
         )
        )
       )
       (br $folding-inner0)
      )
     )
    )
    (i32.store
     (call $___errno_location)
     (i32.const 12)
    )
    (set_global $STACKTOP
     (get_local $14)
    )
    (return
     (i32.const 0)
    )
   )
   (i32.store
    (i32.const 2229280)
    (tee_local $2
     (i32.add
      (tee_local $0
       (i32.load
        (i32.const 2229280)
       )
      )
      (get_local $3)
     )
    )
   )
   (i32.store offset=4
    (get_local $2)
    (i32.or
     (get_local $1)
     (i32.const 1)
    )
   )
   (i32.store offset=4
    (get_local $0)
    (i32.or
     (get_local $3)
     (i32.const 3)
    )
   )
  )
  (set_global $STACKTOP
   (get_local $14)
  )
  (i32.add
   (get_local $0)
   (i32.const 8)
  )
 )
 (func $_free (; 32 ;) (; has Stack IR ;) (param $0 i32)
  (local $1 i32)
  (local $2 i32)
  (local $3 i32)
  (local $4 i32)
  (local $5 i32)
  (local $6 i32)
  (local $7 i32)
  (local $8 i32)
  (local $9 i32)
  (local $10 i32)
  (local $11 i32)
  (local $12 i32)
  (local $13 i32)
  (local $14 i32)
  (local $15 i32)
  (local $16 i32)
  (local $17 i32)
  (if
   (i32.eqz
    (get_local $0)
   )
   (return)
  )
  (if
   (i32.lt_u
    (tee_local $4
     (i32.add
      (get_local $0)
      (i32.const -8)
     )
    )
    (tee_local $12
     (i32.load
      (i32.const 2229272)
     )
    )
   )
   (call $_abort)
  )
  (if
   (i32.eq
    (tee_local $11
     (i32.and
      (tee_local $0
       (i32.load
        (i32.add
         (get_local $0)
         (i32.const -4)
        )
       )
      )
      (i32.const 3)
     )
    )
    (i32.const 1)
   )
   (call $_abort)
  )
  (set_local $7
   (i32.add
    (get_local $4)
    (tee_local $2
     (i32.and
      (get_local $0)
      (i32.const -8)
     )
    )
   )
  )
  (block $label$break$L10
   (if
    (i32.and
     (get_local $0)
     (i32.const 1)
    )
    (block
     (set_local $1
      (get_local $2)
     )
     (set_local $5
      (tee_local $3
       (get_local $4)
      )
     )
    )
    (block
     (set_local $9
      (i32.load
       (get_local $4)
      )
     )
     (if
      (i32.eqz
       (get_local $11)
      )
      (return)
     )
     (if
      (i32.lt_u
       (tee_local $0
        (i32.sub
         (get_local $4)
         (get_local $9)
        )
       )
       (get_local $12)
      )
      (call $_abort)
     )
     (set_local $4
      (i32.add
       (get_local $9)
       (get_local $2)
      )
     )
     (if
      (i32.eq
       (i32.load
        (i32.const 2229276)
       )
       (get_local $0)
      )
      (block
       (if
        (i32.ne
         (i32.and
          (tee_local $1
           (i32.load
            (tee_local $3
             (i32.add
              (get_local $7)
              (i32.const 4)
             )
            )
           )
          )
          (i32.const 3)
         )
         (i32.const 3)
        )
        (block
         (set_local $1
          (get_local $4)
         )
         (set_local $5
          (tee_local $3
           (get_local $0)
          )
         )
         (br $label$break$L10)
        )
       )
       (i32.store
        (i32.const 2229264)
        (get_local $4)
       )
       (i32.store
        (get_local $3)
        (i32.and
         (get_local $1)
         (i32.const -2)
        )
       )
       (i32.store offset=4
        (get_local $0)
        (i32.or
         (get_local $4)
         (i32.const 1)
        )
       )
       (i32.store
        (i32.add
         (get_local $0)
         (get_local $4)
        )
        (get_local $4)
       )
       (return)
      )
     )
     (set_local $2
      (i32.shr_u
       (get_local $9)
       (i32.const 3)
      )
     )
     (if
      (i32.lt_u
       (get_local $9)
       (i32.const 256)
      )
      (block
       (set_local $1
        (i32.load offset=12
         (get_local $0)
        )
       )
       (if
        (i32.ne
         (tee_local $5
          (i32.load offset=8
           (get_local $0)
          )
         )
         (tee_local $3
          (i32.add
           (i32.shl
            (get_local $2)
            (i32.const 3)
           )
           (i32.const 2229296)
          )
         )
        )
        (block
         (if
          (i32.gt_u
           (get_local $12)
           (get_local $5)
          )
          (call $_abort)
         )
         (if
          (i32.ne
           (i32.load offset=12
            (get_local $5)
           )
           (get_local $0)
          )
          (call $_abort)
         )
        )
       )
       (if
        (i32.eq
         (get_local $1)
         (get_local $5)
        )
        (block
         (i32.store
          (i32.const 2229256)
          (i32.and
           (i32.load
            (i32.const 2229256)
           )
           (i32.xor
            (i32.shl
             (i32.const 1)
             (get_local $2)
            )
            (i32.const -1)
           )
          )
         )
         (set_local $1
          (get_local $4)
         )
         (set_local $5
          (tee_local $3
           (get_local $0)
          )
         )
         (br $label$break$L10)
        )
       )
       (if
        (i32.eq
         (get_local $1)
         (get_local $3)
        )
        (set_local $6
         (i32.add
          (get_local $1)
          (i32.const 8)
         )
        )
        (block
         (if
          (i32.gt_u
           (get_local $12)
           (get_local $1)
          )
          (call $_abort)
         )
         (if
          (i32.eq
           (i32.load
            (tee_local $3
             (i32.add
              (get_local $1)
              (i32.const 8)
             )
            )
           )
           (get_local $0)
          )
          (set_local $6
           (get_local $3)
          )
          (call $_abort)
         )
        )
       )
       (i32.store offset=12
        (get_local $5)
        (get_local $1)
       )
       (i32.store
        (get_local $6)
        (get_local $5)
       )
       (set_local $1
        (get_local $4)
       )
       (set_local $5
        (tee_local $3
         (get_local $0)
        )
       )
       (br $label$break$L10)
      )
     )
     (set_local $13
      (i32.load offset=24
       (get_local $0)
      )
     )
     (block $do-once
      (if
       (i32.eq
        (tee_local $2
         (i32.load offset=12
          (get_local $0)
         )
        )
        (get_local $0)
       )
       (block
        (if
         (tee_local $2
          (i32.load
           (tee_local $9
            (i32.add
             (tee_local $6
              (i32.add
               (get_local $0)
               (i32.const 16)
              )
             )
             (i32.const 4)
            )
           )
          )
         )
         (set_local $6
          (get_local $9)
         )
         (br_if $do-once
          (i32.eqz
           (tee_local $2
            (i32.load
             (get_local $6)
            )
           )
          )
         )
        )
        (loop $while-in
         (block $while-out
          (if
           (i32.eqz
            (tee_local $11
             (i32.load
              (tee_local $9
               (i32.add
                (get_local $2)
                (i32.const 20)
               )
              )
             )
            )
           )
           (br_if $while-out
            (i32.eqz
             (tee_local $11
              (i32.load
               (tee_local $9
                (i32.add
                 (get_local $2)
                 (i32.const 16)
                )
               )
              )
             )
            )
           )
          )
          (set_local $6
           (get_local $9)
          )
          (set_local $2
           (get_local $11)
          )
          (br $while-in)
         )
        )
        (if
         (i32.gt_u
          (get_local $12)
          (get_local $6)
         )
         (call $_abort)
         (block
          (i32.store
           (get_local $6)
           (i32.const 0)
          )
          (set_local $8
           (get_local $2)
          )
         )
        )
       )
       (block
        (if
         (i32.gt_u
          (get_local $12)
          (tee_local $6
           (i32.load offset=8
            (get_local $0)
           )
          )
         )
         (call $_abort)
        )
        (if
         (i32.ne
          (i32.load
           (tee_local $9
            (i32.add
             (get_local $6)
             (i32.const 12)
            )
           )
          )
          (get_local $0)
         )
         (call $_abort)
        )
        (if
         (i32.eq
          (i32.load
           (tee_local $11
            (i32.add
             (get_local $2)
             (i32.const 8)
            )
           )
          )
          (get_local $0)
         )
         (block
          (i32.store
           (get_local $9)
           (get_local $2)
          )
          (i32.store
           (get_local $11)
           (get_local $6)
          )
          (set_local $8
           (get_local $2)
          )
         )
         (call $_abort)
        )
       )
      )
     )
     (if
      (get_local $13)
      (block
       (if
        (i32.eq
         (i32.load
          (tee_local $6
           (i32.add
            (i32.shl
             (tee_local $2
              (i32.load offset=28
               (get_local $0)
              )
             )
             (i32.const 2)
            )
            (i32.const 2229560)
           )
          )
         )
         (get_local $0)
        )
        (block
         (i32.store
          (get_local $6)
          (get_local $8)
         )
         (if
          (i32.eqz
           (get_local $8)
          )
          (block
           (i32.store
            (i32.const 2229260)
            (i32.and
             (i32.load
              (i32.const 2229260)
             )
             (i32.xor
              (i32.shl
               (i32.const 1)
               (get_local $2)
              )
              (i32.const -1)
             )
            )
           )
           (set_local $1
            (get_local $4)
           )
           (set_local $5
            (tee_local $3
             (get_local $0)
            )
           )
           (br $label$break$L10)
          )
         )
        )
        (if
         (i32.gt_u
          (i32.load
           (i32.const 2229272)
          )
          (get_local $13)
         )
         (call $_abort)
         (block
          (set_local $2
           (i32.add
            (get_local $13)
            (i32.const 20)
           )
          )
          (i32.store
           (if (result i32)
            (i32.eq
             (i32.load
              (tee_local $6
               (i32.add
                (get_local $13)
                (i32.const 16)
               )
              )
             )
             (get_local $0)
            )
            (get_local $6)
            (get_local $2)
           )
           (get_local $8)
          )
          (if
           (i32.eqz
            (get_local $8)
           )
           (block
            (set_local $1
             (get_local $4)
            )
            (set_local $5
             (tee_local $3
              (get_local $0)
             )
            )
            (br $label$break$L10)
           )
          )
         )
        )
       )
       (if
        (i32.gt_u
         (tee_local $6
          (i32.load
           (i32.const 2229272)
          )
         )
         (get_local $8)
        )
        (call $_abort)
       )
       (i32.store offset=24
        (get_local $8)
        (get_local $13)
       )
       (if
        (tee_local $2
         (i32.load
          (tee_local $9
           (i32.add
            (get_local $0)
            (i32.const 16)
           )
          )
         )
        )
        (if
         (i32.gt_u
          (get_local $6)
          (get_local $2)
         )
         (call $_abort)
         (block
          (i32.store offset=16
           (get_local $8)
           (get_local $2)
          )
          (i32.store offset=24
           (get_local $2)
           (get_local $8)
          )
         )
        )
       )
       (if
        (tee_local $2
         (i32.load offset=4
          (get_local $9)
         )
        )
        (if
         (i32.gt_u
          (i32.load
           (i32.const 2229272)
          )
          (get_local $2)
         )
         (call $_abort)
         (block
          (i32.store offset=20
           (get_local $8)
           (get_local $2)
          )
          (i32.store offset=24
           (get_local $2)
           (get_local $8)
          )
          (set_local $1
           (get_local $4)
          )
          (set_local $5
           (tee_local $3
            (get_local $0)
           )
          )
         )
        )
        (block
         (set_local $1
          (get_local $4)
         )
         (set_local $5
          (tee_local $3
           (get_local $0)
          )
         )
        )
       )
      )
      (block
       (set_local $1
        (get_local $4)
       )
       (set_local $5
        (tee_local $3
         (get_local $0)
        )
       )
      )
     )
    )
   )
  )
  (if
   (i32.ge_u
    (get_local $5)
    (get_local $7)
   )
   (call $_abort)
  )
  (if
   (i32.eqz
    (i32.and
     (tee_local $0
      (i32.load
       (tee_local $4
        (i32.add
         (get_local $7)
         (i32.const 4)
        )
       )
      )
     )
     (i32.const 1)
    )
   )
   (call $_abort)
  )
  (set_local $1
   (i32.shr_u
    (tee_local $5
     (if (result i32)
      (i32.and
       (get_local $0)
       (i32.const 2)
      )
      (block (result i32)
       (i32.store
        (get_local $4)
        (i32.and
         (get_local $0)
         (i32.const -2)
        )
       )
       (i32.store offset=4
        (get_local $3)
        (i32.or
         (get_local $1)
         (i32.const 1)
        )
       )
       (i32.store
        (i32.add
         (get_local $5)
         (get_local $1)
        )
        (get_local $1)
       )
       (get_local $1)
      )
      (block (result i32)
       (if
        (i32.eq
         (i32.load
          (i32.const 2229280)
         )
         (get_local $7)
        )
        (block
         (i32.store
          (i32.const 2229268)
          (tee_local $0
           (i32.add
            (i32.load
             (i32.const 2229268)
            )
            (get_local $1)
           )
          )
         )
         (i32.store
          (i32.const 2229280)
          (get_local $3)
         )
         (i32.store offset=4
          (get_local $3)
          (i32.or
           (get_local $0)
           (i32.const 1)
          )
         )
         (if
          (i32.ne
           (get_local $3)
           (i32.load
            (i32.const 2229276)
           )
          )
          (return)
         )
         (i32.store
          (i32.const 2229276)
          (i32.const 0)
         )
         (i32.store
          (i32.const 2229264)
          (i32.const 0)
         )
         (return)
        )
       )
       (if
        (i32.eq
         (i32.load
          (i32.const 2229276)
         )
         (get_local $7)
        )
        (block
         (i32.store
          (i32.const 2229264)
          (tee_local $0
           (i32.add
            (i32.load
             (i32.const 2229264)
            )
            (get_local $1)
           )
          )
         )
         (i32.store
          (i32.const 2229276)
          (get_local $5)
         )
         (i32.store offset=4
          (get_local $3)
          (i32.or
           (get_local $0)
           (i32.const 1)
          )
         )
         (i32.store
          (i32.add
           (get_local $5)
           (get_local $0)
          )
          (get_local $0)
         )
         (return)
        )
       )
       (set_local $4
        (i32.add
         (i32.and
          (get_local $0)
          (i32.const -8)
         )
         (get_local $1)
        )
       )
       (set_local $6
        (i32.shr_u
         (get_local $0)
         (i32.const 3)
        )
       )
       (block $label$break$L111
        (if
         (i32.lt_u
          (get_local $0)
          (i32.const 256)
         )
         (block
          (set_local $1
           (i32.load offset=12
            (get_local $7)
           )
          )
          (if
           (i32.ne
            (tee_local $2
             (i32.load offset=8
              (get_local $7)
             )
            )
            (tee_local $0
             (i32.add
              (i32.shl
               (get_local $6)
               (i32.const 3)
              )
              (i32.const 2229296)
             )
            )
           )
           (block
            (if
             (i32.gt_u
              (i32.load
               (i32.const 2229272)
              )
              (get_local $2)
             )
             (call $_abort)
            )
            (if
             (i32.ne
              (i32.load offset=12
               (get_local $2)
              )
              (get_local $7)
             )
             (call $_abort)
            )
           )
          )
          (if
           (i32.eq
            (get_local $1)
            (get_local $2)
           )
           (block
            (i32.store
             (i32.const 2229256)
             (i32.and
              (i32.load
               (i32.const 2229256)
              )
              (i32.xor
               (i32.shl
                (i32.const 1)
                (get_local $6)
               )
               (i32.const -1)
              )
             )
            )
            (br $label$break$L111)
           )
          )
          (if
           (i32.eq
            (get_local $1)
            (get_local $0)
           )
           (set_local $16
            (i32.add
             (get_local $1)
             (i32.const 8)
            )
           )
           (block
            (if
             (i32.gt_u
              (i32.load
               (i32.const 2229272)
              )
              (get_local $1)
             )
             (call $_abort)
            )
            (if
             (i32.eq
              (i32.load
               (tee_local $0
                (i32.add
                 (get_local $1)
                 (i32.const 8)
                )
               )
              )
              (get_local $7)
             )
             (set_local $16
              (get_local $0)
             )
             (call $_abort)
            )
           )
          )
          (i32.store offset=12
           (get_local $2)
           (get_local $1)
          )
          (i32.store
           (get_local $16)
           (get_local $2)
          )
         )
         (block
          (set_local $8
           (i32.load offset=24
            (get_local $7)
           )
          )
          (block $do-once6
           (if
            (i32.eq
             (tee_local $0
              (i32.load offset=12
               (get_local $7)
              )
             )
             (get_local $7)
            )
            (block
             (if
              (tee_local $0
               (i32.load
                (tee_local $2
                 (i32.add
                  (tee_local $1
                   (i32.add
                    (get_local $7)
                    (i32.const 16)
                   )
                  )
                  (i32.const 4)
                 )
                )
               )
              )
              (set_local $1
               (get_local $2)
              )
              (br_if $do-once6
               (i32.eqz
                (tee_local $0
                 (i32.load
                  (get_local $1)
                 )
                )
               )
              )
             )
             (loop $while-in9
              (block $while-out8
               (if
                (i32.eqz
                 (tee_local $6
                  (i32.load
                   (tee_local $2
                    (i32.add
                     (get_local $0)
                     (i32.const 20)
                    )
                   )
                  )
                 )
                )
                (br_if $while-out8
                 (i32.eqz
                  (tee_local $6
                   (i32.load
                    (tee_local $2
                     (i32.add
                      (get_local $0)
                      (i32.const 16)
                     )
                    )
                   )
                  )
                 )
                )
               )
               (set_local $1
                (get_local $2)
               )
               (set_local $0
                (get_local $6)
               )
               (br $while-in9)
              )
             )
             (if
              (i32.gt_u
               (i32.load
                (i32.const 2229272)
               )
               (get_local $1)
              )
              (call $_abort)
              (block
               (i32.store
                (get_local $1)
                (i32.const 0)
               )
               (set_local $10
                (get_local $0)
               )
              )
             )
            )
            (block
             (if
              (i32.gt_u
               (i32.load
                (i32.const 2229272)
               )
               (tee_local $1
                (i32.load offset=8
                 (get_local $7)
                )
               )
              )
              (call $_abort)
             )
             (if
              (i32.ne
               (i32.load
                (tee_local $2
                 (i32.add
                  (get_local $1)
                  (i32.const 12)
                 )
                )
               )
               (get_local $7)
              )
              (call $_abort)
             )
             (if
              (i32.eq
               (i32.load
                (tee_local $6
                 (i32.add
                  (get_local $0)
                  (i32.const 8)
                 )
                )
               )
               (get_local $7)
              )
              (block
               (i32.store
                (get_local $2)
                (get_local $0)
               )
               (i32.store
                (get_local $6)
                (get_local $1)
               )
               (set_local $10
                (get_local $0)
               )
              )
              (call $_abort)
             )
            )
           )
          )
          (if
           (get_local $8)
           (block
            (if
             (i32.eq
              (i32.load
               (tee_local $1
                (i32.add
                 (i32.shl
                  (tee_local $0
                   (i32.load offset=28
                    (get_local $7)
                   )
                  )
                  (i32.const 2)
                 )
                 (i32.const 2229560)
                )
               )
              )
              (get_local $7)
             )
             (block
              (i32.store
               (get_local $1)
               (get_local $10)
              )
              (if
               (i32.eqz
                (get_local $10)
               )
               (block
                (i32.store
                 (i32.const 2229260)
                 (i32.and
                  (i32.load
                   (i32.const 2229260)
                  )
                  (i32.xor
                   (i32.shl
                    (i32.const 1)
                    (get_local $0)
                   )
                   (i32.const -1)
                  )
                 )
                )
                (br $label$break$L111)
               )
              )
             )
             (if
              (i32.gt_u
               (i32.load
                (i32.const 2229272)
               )
               (get_local $8)
              )
              (call $_abort)
              (block
               (set_local $0
                (i32.add
                 (get_local $8)
                 (i32.const 20)
                )
               )
               (i32.store
                (if (result i32)
                 (i32.eq
                  (i32.load
                   (tee_local $1
                    (i32.add
                     (get_local $8)
                     (i32.const 16)
                    )
                   )
                  )
                  (get_local $7)
                 )
                 (get_local $1)
                 (get_local $0)
                )
                (get_local $10)
               )
               (br_if $label$break$L111
                (i32.eqz
                 (get_local $10)
                )
               )
              )
             )
            )
            (if
             (i32.gt_u
              (tee_local $1
               (i32.load
                (i32.const 2229272)
               )
              )
              (get_local $10)
             )
             (call $_abort)
            )
            (i32.store offset=24
             (get_local $10)
             (get_local $8)
            )
            (if
             (tee_local $0
              (i32.load
               (tee_local $2
                (i32.add
                 (get_local $7)
                 (i32.const 16)
                )
               )
              )
             )
             (if
              (i32.gt_u
               (get_local $1)
               (get_local $0)
              )
              (call $_abort)
              (block
               (i32.store offset=16
                (get_local $10)
                (get_local $0)
               )
               (i32.store offset=24
                (get_local $0)
                (get_local $10)
               )
              )
             )
            )
            (if
             (tee_local $0
              (i32.load offset=4
               (get_local $2)
              )
             )
             (if
              (i32.gt_u
               (i32.load
                (i32.const 2229272)
               )
               (get_local $0)
              )
              (call $_abort)
              (block
               (i32.store offset=20
                (get_local $10)
                (get_local $0)
               )
               (i32.store offset=24
                (get_local $0)
                (get_local $10)
               )
              )
             )
            )
           )
          )
         )
        )
       )
       (i32.store offset=4
        (get_local $3)
        (i32.or
         (get_local $4)
         (i32.const 1)
        )
       )
       (i32.store
        (i32.add
         (get_local $5)
         (get_local $4)
        )
        (get_local $4)
       )
       (if (result i32)
        (i32.eq
         (get_local $3)
         (i32.load
          (i32.const 2229276)
         )
        )
        (block
         (i32.store
          (i32.const 2229264)
          (get_local $4)
         )
         (return)
        )
        (get_local $4)
       )
      )
     )
    )
    (i32.const 3)
   )
  )
  (if
   (i32.lt_u
    (get_local $5)
    (i32.const 256)
   )
   (block
    (set_local $0
     (i32.add
      (i32.shl
       (get_local $1)
       (i32.const 3)
      )
      (i32.const 2229296)
     )
    )
    (if
     (i32.and
      (tee_local $5
       (i32.load
        (i32.const 2229256)
       )
      )
      (tee_local $1
       (i32.shl
        (i32.const 1)
        (get_local $1)
       )
      )
     )
     (if
      (i32.gt_u
       (i32.load
        (i32.const 2229272)
       )
       (tee_local $5
        (i32.load
         (tee_local $1
          (i32.add
           (get_local $0)
           (i32.const 8)
          )
         )
        )
       )
      )
      (call $_abort)
      (block
       (set_local $15
        (get_local $5)
       )
       (set_local $17
        (get_local $1)
       )
      )
     )
     (block
      (i32.store
       (i32.const 2229256)
       (i32.or
        (get_local $5)
        (get_local $1)
       )
      )
      (set_local $15
       (get_local $0)
      )
      (set_local $17
       (i32.add
        (get_local $0)
        (i32.const 8)
       )
      )
     )
    )
    (i32.store
     (get_local $17)
     (get_local $3)
    )
    (i32.store offset=12
     (get_local $15)
     (get_local $3)
    )
    (i32.store offset=8
     (get_local $3)
     (get_local $15)
    )
    (i32.store offset=12
     (get_local $3)
     (get_local $0)
    )
    (return)
   )
  )
  (set_local $0
   (i32.add
    (i32.shl
     (tee_local $1
      (if (result i32)
       (tee_local $0
        (i32.shr_u
         (get_local $5)
         (i32.const 8)
        )
       )
       (if (result i32)
        (i32.gt_u
         (get_local $5)
         (i32.const 16777215)
        )
        (i32.const 31)
        (i32.or
         (i32.and
          (i32.shr_u
           (get_local $5)
           (i32.add
            (tee_local $0
             (i32.add
              (i32.sub
               (i32.const 14)
               (i32.or
                (i32.or
                 (tee_local $4
                  (i32.and
                   (i32.shr_u
                    (i32.add
                     (tee_local $1
                      (i32.shl
                       (get_local $0)
                       (tee_local $0
                        (i32.and
                         (i32.shr_u
                          (i32.add
                           (get_local $0)
                           (i32.const 1048320)
                          )
                          (i32.const 16)
                         )
                         (i32.const 8)
                        )
                       )
                      )
                     )
                     (i32.const 520192)
                    )
                    (i32.const 16)
                   )
                   (i32.const 4)
                  )
                 )
                 (get_local $0)
                )
                (tee_local $1
                 (i32.and
                  (i32.shr_u
                   (i32.add
                    (tee_local $0
                     (i32.shl
                      (get_local $1)
                      (get_local $4)
                     )
                    )
                    (i32.const 245760)
                   )
                   (i32.const 16)
                  )
                  (i32.const 2)
                 )
                )
               )
              )
              (i32.shr_u
               (i32.shl
                (get_local $0)
                (get_local $1)
               )
               (i32.const 15)
              )
             )
            )
            (i32.const 7)
           )
          )
          (i32.const 1)
         )
         (i32.shl
          (get_local $0)
          (i32.const 1)
         )
        )
       )
       (i32.const 0)
      )
     )
     (i32.const 2)
    )
    (i32.const 2229560)
   )
  )
  (i32.store offset=28
   (get_local $3)
   (get_local $1)
  )
  (i32.store offset=20
   (get_local $3)
   (i32.const 0)
  )
  (i32.store offset=16
   (get_local $3)
   (i32.const 0)
  )
  (block $label$break$L197
   (if
    (i32.and
     (tee_local $4
      (i32.load
       (i32.const 2229260)
      )
     )
     (tee_local $2
      (i32.shl
       (i32.const 1)
       (get_local $1)
      )
     )
    )
    (block
     (block $label$break$L200
      (if
       (i32.eq
        (i32.and
         (i32.load offset=4
          (tee_local $0
           (i32.load
            (get_local $0)
           )
          )
         )
         (i32.const -8)
        )
        (get_local $5)
       )
       (set_local $14
        (get_local $0)
       )
       (block
        (set_local $4
         (i32.sub
          (i32.const 25)
          (i32.shr_u
           (get_local $1)
           (i32.const 1)
          )
         )
        )
        (set_local $4
         (i32.shl
          (get_local $5)
          (if (result i32)
           (i32.eq
            (get_local $1)
            (i32.const 31)
           )
           (i32.const 0)
           (get_local $4)
          )
         )
        )
        (loop $while-in17
         (if
          (tee_local $1
           (i32.load
            (tee_local $2
             (i32.add
              (i32.add
               (get_local $0)
               (i32.const 16)
              )
              (i32.shl
               (i32.shr_u
                (get_local $4)
                (i32.const 31)
               )
               (i32.const 2)
              )
             )
            )
           )
          )
          (block
           (set_local $4
            (i32.shl
             (get_local $4)
             (i32.const 1)
            )
           )
           (if
            (i32.eq
             (i32.and
              (i32.load offset=4
               (get_local $1)
              )
              (i32.const -8)
             )
             (get_local $5)
            )
            (block
             (set_local $14
              (get_local $1)
             )
             (br $label$break$L200)
            )
            (block
             (set_local $0
              (get_local $1)
             )
             (br $while-in17)
            )
           )
          )
         )
        )
        (if
         (i32.gt_u
          (i32.load
           (i32.const 2229272)
          )
          (get_local $2)
         )
         (call $_abort)
         (block
          (i32.store
           (get_local $2)
           (get_local $3)
          )
          (i32.store offset=24
           (get_local $3)
           (get_local $0)
          )
          (i32.store offset=12
           (get_local $3)
           (get_local $3)
          )
          (i32.store offset=8
           (get_local $3)
           (get_local $3)
          )
          (br $label$break$L197)
         )
        )
       )
      )
     )
     (if
      (i32.and
       (i32.le_u
        (tee_local $1
         (i32.load
          (i32.const 2229272)
         )
        )
        (tee_local $0
         (i32.load
          (tee_local $5
           (i32.add
            (get_local $14)
            (i32.const 8)
           )
          )
         )
        )
       )
       (i32.le_u
        (get_local $1)
        (get_local $14)
       )
      )
      (block
       (i32.store offset=12
        (get_local $0)
        (get_local $3)
       )
       (i32.store
        (get_local $5)
        (get_local $3)
       )
       (i32.store offset=8
        (get_local $3)
        (get_local $0)
       )
       (i32.store offset=12
        (get_local $3)
        (get_local $14)
       )
       (i32.store offset=24
        (get_local $3)
        (i32.const 0)
       )
      )
      (call $_abort)
     )
    )
    (block
     (i32.store
      (i32.const 2229260)
      (i32.or
       (get_local $4)
       (get_local $2)
      )
     )
     (i32.store
      (get_local $0)
      (get_local $3)
     )
     (i32.store offset=24
      (get_local $3)
      (get_local $0)
     )
     (i32.store offset=12
      (get_local $3)
      (get_local $3)
     )
     (i32.store offset=8
      (get_local $3)
      (get_local $3)
     )
    )
   )
  )
  (i32.store
   (i32.const 2229288)
   (tee_local $0
    (i32.add
     (i32.load
      (i32.const 2229288)
     )
     (i32.const -1)
    )
   )
  )
  (if
   (get_local $0)
   (return)
  )
  (set_local $0
   (i32.const 2229712)
  )
  (loop $while-in19
   (set_local $0
    (i32.add
     (tee_local $3
      (i32.load
       (get_local $0)
      )
     )
     (i32.const 8)
    )
   )
   (br_if $while-in19
    (get_local $3)
   )
  )
  (i32.store
   (i32.const 2229288)
   (i32.const -1)
  )
 )
 (func $___errno_location (; 33 ;) (; has Stack IR ;) (result i32)
  (i32.const 2229752)
 )
 (func $_srand (; 34 ;) (; has Stack IR ;) (param $0 i32)
  (i64.store
   (i32.const 2229248)
   (i64.extend_u/i32
    (i32.add
     (get_local $0)
     (i32.const -1)
    )
   )
  )
 )
 (func $_rand (; 35 ;) (; has Stack IR ;) (result i32)
  (local $0 i64)
  (i64.store
   (i32.const 2229248)
   (tee_local $0
    (i64.add
     (i64.mul
      (i64.load
       (i32.const 2229248)
      )
      (i64.const 6364136223846793005)
     )
     (i64.const 1)
    )
   )
  )
  (i32.wrap/i64
   (i64.shr_u
    (get_local $0)
    (i64.const 33)
   )
  )
 )
 (func $runPostSets (; 36 ;) (; has Stack IR ;)
  (nop)
 )
 (func $_memset (; 37 ;) (; has Stack IR ;) (param $0 i32) (param $1 i32) (param $2 i32) (result i32)
  (local $3 i32)
  (local $4 i32)
  (local $5 i32)
  (local $6 i32)
  (set_local $4
   (i32.add
    (get_local $0)
    (get_local $2)
   )
  )
  (set_local $1
   (i32.and
    (get_local $1)
    (i32.const 255)
   )
  )
  (if
   (i32.ge_s
    (get_local $2)
    (i32.const 67)
   )
   (block
    (loop $while-in
     (if
      (i32.and
       (get_local $0)
       (i32.const 3)
      )
      (block
       (i32.store8
        (get_local $0)
        (get_local $1)
       )
       (set_local $0
        (i32.add
         (get_local $0)
         (i32.const 1)
        )
       )
       (br $while-in)
      )
     )
    )
    (set_local $6
     (i32.add
      (tee_local $5
       (i32.and
        (get_local $4)
        (i32.const -4)
       )
      )
      (i32.const -64)
     )
    )
    (set_local $3
     (i32.or
      (i32.or
       (i32.or
        (get_local $1)
        (i32.shl
         (get_local $1)
         (i32.const 8)
        )
       )
       (i32.shl
        (get_local $1)
        (i32.const 16)
       )
      )
      (i32.shl
       (get_local $1)
       (i32.const 24)
      )
     )
    )
    (loop $while-in1
     (if
      (i32.le_s
       (get_local $0)
       (get_local $6)
      )
      (block
       (i32.store
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=4
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=8
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=12
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=16
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=20
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=24
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=28
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=32
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=36
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=40
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=44
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=48
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=52
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=56
        (get_local $0)
        (get_local $3)
       )
       (i32.store offset=60
        (get_local $0)
        (get_local $3)
       )
       (set_local $0
        (i32.sub
         (get_local $0)
         (i32.const -64)
        )
       )
       (br $while-in1)
      )
     )
    )
    (loop $while-in3
     (if
      (i32.lt_s
       (get_local $0)
       (get_local $5)
      )
      (block
       (i32.store
        (get_local $0)
        (get_local $3)
       )
       (set_local $0
        (i32.add
         (get_local $0)
         (i32.const 4)
        )
       )
       (br $while-in3)
      )
     )
    )
   )
  )
  (loop $while-in5
   (if
    (i32.lt_s
     (get_local $0)
     (get_local $4)
    )
    (block
     (i32.store8
      (get_local $0)
      (get_local $1)
     )
     (set_local $0
      (i32.add
       (get_local $0)
       (i32.const 1)
      )
     )
     (br $while-in5)
    )
   )
  )
  (i32.sub
   (get_local $4)
   (get_local $2)
  )
 )
 (func $_sbrk (; 38 ;) (; has Stack IR ;) (param $0 i32) (result i32)
  (local $1 i32)
  (local $2 i32)
  (if
   (i32.or
    (i32.and
     (i32.gt_s
      (get_local $0)
      (i32.const 0)
     )
     (i32.lt_s
      (tee_local $0
       (i32.add
        (tee_local $1
         (i32.load
          (get_global $DYNAMICTOP_PTR)
         )
        )
        (get_local $0)
       )
      )
      (get_local $1)
     )
    )
    (i32.lt_s
     (get_local $0)
     (i32.const 0)
    )
   )
   (block
    (drop
     (call $abortOnCannotGrowMemory)
    )
    (call $___setErrNo
     (i32.const 12)
    )
    (return
     (i32.const -1)
    )
   )
  )
  (i32.store
   (get_global $DYNAMICTOP_PTR)
   (get_local $0)
  )
  (set_local $2
   (call $getTotalMemory)
  )
  (if
   (i32.gt_s
    (get_local $0)
    (get_local $2)
   )
   (if
    (i32.eqz
     (call $enlargeMemory)
    )
    (block
     (i32.store
      (get_global $DYNAMICTOP_PTR)
      (get_local $1)
     )
     (call $___setErrNo
      (i32.const 12)
     )
     (return
      (i32.const -1)
     )
    )
   )
  )
  (get_local $1)
 )
)
