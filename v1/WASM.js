/* tslint:disable:no-bitwise no-magic-numbers whitespace */
// tslint:disable-next-line:no-reference
///<reference path="./index.d.ts" />
if (typeof WebAssembly !== 'object') {
    throw new TypeError('No native WebAssembly support');
}
const UTF8Decoder = new TextDecoder('utf8');
const UTF16Decoder = new TextDecoder('utf-16le');
function bind(self, member) {
    return self[member].bind(self);
}
/* tslint:disable:no-console */
const err = bind(console, 'warn');
const out = bind(console, 'log');
/* tslint:enable:no-console */
let WasmModuleInstance = /** @class */ (() => {
    class WasmModuleInstance {
        /* tslint:enable:variable-name */
        constructor(module, descriptor) {
            this.module = module;
            this.STATIC_BASE = 1024 /* StaticBase */;
            this.STATIC_TOP = 0;
            this.STATIC_SEALED = false;
            this.STACK_BASE = 0;
            this.STACK_TOP = 0;
            this.STACK_MAX = 0;
            this.DYNAMIC_BASE = 0;
            this.DYNAMIC_TOP_PTR = 0;
            /** Miscellaneous members necessary for syscalls and exceptions */
            this.pos = 0;
            this.outputBuffers = [null, [], []];
            this.exceptionInfo = new Map();
            // @ts-ignore
            this.caughtExceptions = [];
            /* tslint:disable:variable-name */
            this.__ZSt18uncaught_exceptionv_uncaught_exception = 0;
            const { ___errno_location, initialMemory, maxTableSize, initialTableSize = maxTableSize, onAbort, staticBump, totalMemory = initialMemory, totalStack = (totalMemory * 0.3125) } = {
                ...WasmModuleInstance.BASE_DESCRIPTOR,
                ...descriptor,
            };
            if (totalMemory < totalStack) {
                // tslint:disable-next-line:max-line-length
                err(`Total memory should be larger than total stack, was ${totalMemory} (< ${totalStack}`);
            }
            this.initialMemory = initialMemory;
            this.totalMemory = totalMemory;
            this.initialTableSize = initialTableSize;
            this.maxTableSize = maxTableSize;
            this.___errno_location = ___errno_location;
            this.onAbort = onAbort;
            this.STATIC_TOP = this.STATIC_BASE + staticBump;
            this.DYNAMIC_TOP_PTR = this.staticAlloc(4);
            this.STACK_TOP = WasmModuleInstance.ALIGN_MEM(this.STATIC_TOP);
            this.STACK_BASE = this.STACKTOP;
            this.STACK_MAX = this.STACK_BASE + totalStack;
            this.DYNAMIC_BASE = WasmModuleInstance.ALIGN_MEM(this.STACK_MAX);
        }
        static ALIGN_UP(x) {
            if (x % 65536 /* PageSize */ > 0) {
                x += 65536 /* PageSize */ - (x % 65536 /* PageSize */);
            }
            return x;
        }
        static ALIGN_MEM(size, factor = 16 /* StackAlign */) {
            return Math.ceil(size / factor) * factor;
        }
        get buffer() {
            return this.memory.buffer;
        }
        get STACKTOP() {
            return this.STACK_TOP;
        }
        set STACKTOP(value) {
            this.STACK_TOP = value;
        }
        get DYNAMICTOP_PTR() {
            return this.DYNAMIC_TOP_PTR;
        }
        set DYNAMICTOP_PTR(value) {
            this.DYNAMIC_TOP_PTR = value;
        }
        async init() {
            if (!this.instance) {
                const instance = WebAssembly.instantiate(this.module, this.createImports());
                this.instance = this.addHooks((await instance).exports);
            }
            // NOTE: Middle type conversion is for compatibility with
            // WasmInstantiatedModule
            return this;
        }
        addHooks(instanceExports) {
            Object.entries(instanceExports).forEach(([key, value]) => {
                this[key] = value;
            });
            return instanceExports;
        }
        createImports() {
            const self = this;
            this.memory = new WebAssembly.Memory({
                initial: this.initialMemory / 65536 /* PageSize */,
                maximum: this.totalMemory / 65536 /* PageSize */,
            });
            this.setHeapReferences();
            this.HEAP32[this.DYNAMIC_TOP_PTR >> 2] = this.DYNAMIC_BASE;
            this.STATIC_SEALED = true;
            const table = new WebAssembly.Table({
                element: 'anyfunc',
                initial: this.initialTableSize,
                maximum: this.maxTableSize,
            });
            const imports = {
                env: {
                    DYNAMICTOP_PTR: self.DYNAMICTOP_PTR,
                    STACKTOP: self.STACKTOP,
                    STACK_MAX: self.STACK_MAX,
                    memory: self.memory,
                    memoryBase: this.STATIC_BASE,
                    table,
                    tableBase: 0,
                    _abort: bind(self, 'abort'),
                    _emscripten_memcpy_big: bind(self, '_memcpy_big'),
                    _time: bind(self, '_time'),
                    abort: bind(self, 'abort'),
                    abortOnCannotGrowMemory: bind(self, 'abortOnCannotGrowMemory'),
                    enlargeMemory: bind(self, 'enlargeMemory'),
                    getTotalMemory() { return self.totalMemory; },
                    invoke_vii: bind(self, 'invoke_vii'),
                    ___cxa_allocate_exception: bind(self, '___cxa_allocate_exception'),
                    ___cxa_find_matching_catch_2: bind(self, '___cxa_find_matching_catch_2'),
                    ___cxa_free_exception: bind(self, '___cxa_free_exception'),
                    ___cxa_throw: bind(self, '___cxa_throw'),
                    ___resumeException: bind(self, '___resumeException'),
                    __ZSt18uncaught_exceptionv: bind(self, '__ZSt18uncaught_exceptionv'),
                    /* tslint:disable:object-literal-sort-keys */
                    ___setErrNo: bind(self, '___setErrNo'),
                    ___syscall6: bind(self, '___syscall6'),
                    ___syscall54: bind(self, '___syscall54'),
                    ___syscall140: bind(self, '___syscall140'),
                    ___syscall146: bind(self, '___syscall146'),
                },
            };
            return imports;
        }
        _memcpy_big(dest, src, size) {
            const heap = this.HEAPU8;
            heap.set(heap.subarray(src, src + size), dest);
            return dest;
        }
        _time(ptr) {
            const time = (Date.now() / 1000) | 0;
            if (ptr) {
                this.HEAP32[ptr >> 2] = time;
            }
            return time;
        }
        abort(what) {
            if (this.onAbort) {
                this.onAbort(what);
            }
            if (what !== undefined) {
                out(what);
                err(what);
                what = JSON.stringify(what);
            }
            else {
                what = '';
            }
            throw new Error(`Aborted: ${what}`);
        }
        abortOnCannotGrowMemory() {
            this.abort('Cannot enlarge memory arrays.');
        }
        enlargeMemory() {
            const heap = this.HEAP32;
            if (heap[this.DYNAMICTOP_PTR >> 2] > 2147418112 /* MaxMemoryLimit */) {
                return false;
            }
            let totalMemory = Math.max(this.totalMemory, 16777216 /* MinTotalMemory */);
            while (totalMemory < heap[this.DYNAMICTOP_PTR >> 2]) {
                if (totalMemory <= 536870912 /* QuarterMaxTotalMemory */) {
                    totalMemory = WasmModuleInstance.ALIGN_UP(totalMemory * 2);
                }
                else {
                    totalMemory = Math.min(WasmModuleInstance.ALIGN_UP((totalMemory * 3 + 2147483648 /* MaxTotalMemory */) / 4), 2147418112 /* MaxMemoryLimit */);
                }
            }
            const replacement = this.reallocateBuffer(totalMemory);
            if (!replacement || replacement.byteLength !== totalMemory) {
                return false;
            }
            this.setHeapReferences();
            return true;
        }
        reallocateBuffer(size) {
            const newSize = WasmModuleInstance.ALIGN_UP(size);
            const old = this.buffer;
            const oldSize = old.byteLength;
            try {
                const result = this.memory.grow((newSize - oldSize) / 65536 /* PageSize */);
                if (result !== (-1 | 0)) {
                    return this.buffer;
                }
            }
            catch {
                /* Do nothing */
            }
            return null;
        }
        setHeapReferences() {
            const { buffer } = this;
            this.HEAP8 = new Int8Array(buffer);
            this.HEAP16 = new Int16Array(buffer);
            this.HEAP32 = new Int32Array(buffer);
            this.HEAPU8 = new Uint8Array(buffer);
            this.HEAPU16 = new Uint16Array(buffer);
            this.HEAPU32 = new Uint32Array(buffer);
            this.HEAPF32 = new Float32Array(buffer);
            this.HEAPF64 = new Float64Array(buffer);
        }
        staticAlloc(size) {
            this.STATIC_TOP = (this.STATIC_TOP + size + 15) & -16;
            return this.STATIC_TOP;
        }
        dynamicAlloc(size) {
            const ret = this.HEAP32[this.DYNAMIC_TOP_PTR >> 2];
            const end = (ret + size + 15) & -16;
            this.HEAP32[this.DYNAMIC_TOP_PTR >> 2] = end;
            if (end >= this.totalMemory && !this.enlargeMemory()) {
                this.HEAP32[this.DYNAMIC_TOP_PTR >> 2] = ret;
                return 0;
            }
            return ret;
        }
        // @ts-ignore
        getMemory(size) {
            if (!this.STATIC_SEALED) {
                return this.staticAlloc(size);
            }
            else if (!this.instance) {
                return this.dynamicAlloc(size);
            }
            return this.instance._malloc(size);
        }
        invoke_vii(index, a1, a2) {
            const sp = this.stackSave();
            try {
                this.dynCall_vii(index, a1, a2);
            }
            catch (e) {
                this.stackRestore(sp);
                // @ts-ignore: Extended `typeof` matches
                if (typeof e !== 'number' && typeof e !== 'longjmp')
                    throw e;
                this.setThrew(1, 0);
            }
        }
        /** Exceptions */
        // @ts-ignore
        deAdjustException(adjusted) {
            if (!adjusted || this.exceptionInfo.get(adjusted)) {
                return adjusted;
            }
            for (const [ptr, exception] of this.exceptionInfo) {
                if (exception.adjusted === adjusted) {
                    return ptr;
                }
            }
            return adjusted;
        }
        // @ts-ignore
        addExceptionRef(ptr) {
            if (!ptr) {
                return;
            }
            this.exceptionInfo.get(ptr).refcount++;
        }
        // @ts-ignore
        decExceptionRef(ptr) {
            if (!ptr) {
                return;
            }
            const info = this.exceptionInfo.get(ptr);
            if (info.refcount > 0) {
                info.refcount--;
                if (info.refcount === 0 && !info.rethrown) {
                    if (info.destructor) {
                        this.dynCall_vi(info.destructor, ptr);
                    }
                    this.exceptionInfo.delete(ptr);
                    this.___cxa_free_exception(ptr);
                }
            }
        }
        // @ts-ignore
        clearExceptionRef(ptr) {
            if (!ptr) {
                return;
            }
            this.exceptionInfo.get(ptr).refcount = 0;
        }
        ___cxa_allocate_exception(size) {
            return this._malloc(size);
        }
        ___cxa_find_matching_catch_2(...a) {
            return this.___cxa_find_matching_catch(...a);
        }
        ___cxa_free_exception(ptr) {
            try {
                /*return */ this._free(ptr);
            }
            catch {
                /* Do nothing */
            }
        }
        ___resumeException(ptr) {
            if (!this.lastException) {
                this.lastException = ptr;
            }
            throw ptr;
        }
        ___cxa_find_matching_catch(...args) {
            let thrown = this.lastException;
            if (!thrown) {
                this.setTempRet0(0);
                return 0 | 0;
            }
            const info = this.exceptionInfo.get(thrown);
            const { type } = info;
            if (!type) {
                this.setTempRet0(0);
                return thrown | 0;
            }
            // @ts-ignore: This may be used in another context that I haven't added yet
            const ptr = this.___cxa_is_pointer_type(type);
            if (!this.___cxa_find_matching_catch_buffer) {
                this.___cxa_find_matching_catch_buffer = this._malloc(4);
            }
            this.HEAP32[this.___cxa_find_matching_catch_buffer >> 2] = thrown;
            thrown = this.___cxa_find_matching_catch_buffer;
            for (const arg of args) {
                if (arg && this.___cxa_can_catch(arg, type, thrown)) {
                    thrown = this.HEAP32[thrown >> 2];
                    info.adjusted = thrown;
                    this.setTempRet0(arg);
                    return thrown | 0;
                }
            }
            thrown = this.HEAP32[thrown >> 2];
            this.setTempRet0(type);
            return thrown | 0;
        }
        ___cxa_throw(ptr, type, destructor) {
            this.exceptionInfo.set(ptr, {
                adjusted: ptr,
                caught: false,
                destructor,
                ptr,
                refcount: 0,
                rethrown: false,
                type,
            });
            this.lastException = ptr;
            this.__ZSt18uncaught_exceptionv_uncaught_exception++;
            throw ptr;
        }
        __ZSt18uncaught_exceptionv() {
            return !!this.__ZSt18uncaught_exceptionv_uncaught_exception;
        }
        /** Miscellaneous helpers */
        UTF8ArrayToString(ptr, idx) {
            let end = idx;
            while (ptr[end])
                ++end;
            if (end - idx > 16 && ptr instanceof Uint8Array) {
                return UTF8Decoder.decode(ptr.subarray(idx, end));
            }
            let str = '';
            while (1) {
                let u0 = ptr[idx++];
                if (!u0) {
                    return str;
                }
                if (!(u0 & 128)) {
                    str += String.fromCharCode(u0);
                    continue;
                }
                const u1 = ptr[idx++] & 63;
                if ((u0 & 224) === 192) {
                    str += String.fromCharCode((u0 & 31) << 6 | u1);
                    continue;
                }
                const u2 = ptr[idx++] & 63;
                if ((u0 & 240) === 224) {
                    u0 = (u0 & 7) << 12 | u1 << 6 | u2;
                }
                else {
                    const u3 = ptr[idx++] & 63;
                    if ((u0 & 248) === 240) {
                        u0 = (u0 & 7) << 18 | u1 << 12 | u2 << 6 | u3;
                    }
                    else {
                        const u4 = ptr[idx++] & 63;
                        if ((u0 & 252) === 248) {
                            u0 = (u0 & 3) << 24 | u1 << 18 | u2 << 12 | u3 << 6 | u4;
                        }
                        else {
                            const u5 = ptr[idx++] & 63;
                            u0 = (u1 & 1) << 30 | u1 << 24 | u2 << 18 | u3 << 12 | u4 << 6 | u5;
                        }
                    }
                }
                if (u0 < 65536) {
                    str += String.fromCharCode(u0);
                }
                else {
                    const ch = u0 - 65536;
                    // tslint:disable-next-line:binary-expression-operand-order
                    str += String.fromCharCode(55296 | ch >> 10, 56320 | ch & 1023);
                }
            }
        }
        // public UTF8ToString(ptr: pointer) {
        // 	return this.UTF8ArrayToString(this.HEAPU8, ptr);
        // }
        printChar(stream, curr) {
            const buffer = this.outputBuffers[stream];
            if (curr === 0 || curr === 10) {
                (stream === 1 ? out : err)(this.UTF8ArrayToString(buffer, 0));
                buffer.length = 0;
            }
            else {
                buffer.push(curr);
            }
        }
        get(_varargs) {
            this.pos += 4;
            return this.HEAP32[(this.pos - 4) >> 2];
        }
        // public get64() {
        // 	const low = this.get();
        // 	const high = this.get();
        // 	if (
        // 		(low >= 0 && high === 0) ||
        // 		high === -1
        // 	) {
        // 		return low;
        // 	}
        // 	this.abort();
        // }
        // public getStr() {
        // 	return this.stringifyPointer(this.get());
        // }
        // public getZero() {
        // 	return this.get() === 0;
        // }
        // public stringifyPointer(ptr: pointer, length?: number) {
        // 	if (length === 0 || !ptr) {
        // 		return '';
        // 	}
        // 	let hasUtf = 0;
        // 	let t: number;
        // 	let i = 0;
        // 	while (1) {
        // 		t = this.HEAPU8[ptr + i >> 0];
        // 		hasUtf |= t;
        // 		if (
        // 			(t === 0 || !length) ||
        // 			(length && ++i === length)
        // 		) {
        // 			break;
        // 		}
        // 	}
        // 	if (!length) length = i;
        // 	let ret = '';
        // 	if (hasUtf < 128) {
        // 		const MAX_CHUNK = 1024;
        // 		while (length > 0) {
        // 			const curr = String.fromCharCode.apply(
        // 				String,
        // 				this.HEAPU8.subarray(ptr, ptr + Math.min(length, MAX_CHUNK))
        // 			);
        // 			ret += curr;
        // 			ptr += MAX_CHUNK;
        // 			length -= MAX_CHUNK;
        // 		}
        // 		return ret;
        // 	}
        // 	return this.UTF8ToString(ptr);
        // }
        getStreamFromFD() {
            throw new Error('Not implemented');
        }
        ___setErrNo(value) {
            if (this.___errno_location) {
                this.HEAP32[this.___errno_location() >> 2] = value;
            }
            return value;
        }
        /** Syscalls */
        ___syscall6(_which, varargs) {
            this.pos = varargs;
            try {
                const stream = this.getStreamFromFD();
                FS.close(stream);
                return 0;
            }
            catch (e) {
                if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) {
                    this.abort(e);
                }
                return -e.errno;
            }
        }
        ___syscall54(_which, varargs) {
            this.pos = varargs;
            try {
                return 0;
            }
            catch (e) {
                if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) {
                    this.abort(e);
                }
                return -e.errno;
            }
        }
        ___syscall140(_which, varargs) {
            this.pos = varargs;
            try {
                const stream = this.getStreamFromFD();
                // @ts-ignore
                const offsetHi = this.get();
                const offsetLo = this.get();
                const result = this.get();
                const whence = this.get();
                const offset = offsetLo;
                FS.llseek(stream, offset, whence);
                this.HEAP32[result >> 2] = stream.position;
                if (stream.getdents && offset === 0 && whence === 0) {
                    stream.getdents = null;
                }
                return 0;
            }
            catch (e) {
                if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) {
                    this.abort(e);
                }
                return -e.errno;
            }
        }
        ___syscall146(_which, varargs) {
            this.pos = varargs;
            try {
                const stream = this.get();
                const iov = this.get();
                const iovcnt = this.get();
                let ret = 0;
                for (let i = 0; i < iovcnt; i++) {
                    const ptr = this.HEAP32[(iov + (i * 8)) >> 2];
                    const len = this.HEAP32[(iov + (i * 8) + 4) >> 2];
                    for (let j = 0; j < len; j++) {
                        this.printChar(stream, this.HEAPU8[ptr + j]);
                    }
                    ret += len;
                }
                return ret;
            }
            catch (e) {
                if (typeof FS === 'undefined' || !(e instanceof FS.ErrnoError)) {
                    this.abort(e);
                }
                return -e.errno;
            }
        }
    }
    WasmModuleInstance.BASE_DESCRIPTOR = {
        initialMemory: 16777216 /* MinTotalMemory */,
        maxTableSize: 0,
    };
    return WasmModuleInstance;
})();
// @ts-ignore: Bad duplicate identifier detection
class WasmModule {
    constructor(file) {
        this.file = file;
    }
    static module(file) {
        return new WasmModule(file);
    }
    static instance(file, descriptor) {
        return new WasmModule(file).instance(descriptor);
    }
    static compile(file) {
        const resp = fetch(file, { credentials: 'same-origin' });
        return WebAssembly.compileStreaming(resp);
    }
    async instance(descriptor) {
        return new WasmModuleInstance(await this.compile(), descriptor).init();
    }
    async compile() {
        if (!this.compiledModule) {
            this.compiledModule = await WasmModule.compile(this.file);
        }
        return this.compiledModule;
    }
}
//# sourceMappingURL=WASM.js.map